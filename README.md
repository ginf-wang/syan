# SyAn (english version)

_(german version below)_

SyAn (short for SyndromAnsatz - syndrome approach in german) is a graph editor
targeted at student teachers.

Its development discontinued on the **10th March 2019**.


## How to build

The software is built using [Apache Maven](https://maven.apache.org/). To get a
working .jar, the goal `clean package` has to be run, resulting in a
**jar**-File in the **target** folder.


## Used libraries

* [OpenJDK 11](https://openjdk.java.net/) licensed under [GPLv2 with Classpath
Exception](https://openjdk.java.net/legal/gplv2+ce.html)
* [OpenJFX 12](https://openjfx.io/) licensed under [GPLv2 with Classpath
Exception](https://openjdk.java.net/legal/gplv2+ce.html)
* [H2 Database](https://h2database.com/) licensed under [MPL
2.0](http://www.mozilla.org/MPL/2.0)
* [Hibernate ORM](http://hibernate.org/orm/) licensed under
[LGPLv2.1](http://hibernate.org/license/)
* [C3P0](https://sourceforge.net/projects/c3p0/) licensed under
[LPGLv2](https://sourceforge.net/directory/license:lgpl/)
* [Apache PDFBox](https://pdfbox.apache.org/) licensed under [Apache License
2.0](https://www.apache.org/licenses/LICENSE-2.0)
* [Apache Commons CSV](https://commons.apache.org/csv/) licensed under [Apache
License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
* [Font Awesome Free Icons](https://fontawesome.com/) von Fonticons Inc.
licensed under [CC BY
4.0](https://creativecommons.org/licenses/by/4.0/legalcode)


## License

This project is licensed under GNU General Public License v3 or later. See
[LICENSE](https://gitlab.com/ginf-wang/syan/blob/master/LICENSE) for more
information.




# SyAn (Deutsche Version)

SyAn (SyndromAnsatz) ist ein Grapheneditor, welcher die
[Systemkompetenz](https://monsieur-becker.de/2011/systemkompetenz-geografie/)
von Lehramtsstudierenden fördern soll.

Die aktive Entwicklung wurde am **10. März 2019** eingestellt.


## Bauen der Programmdatei

Das Ausführen der Maven Goals `clean package` baut eine ausführbare **jar**
Datei im `target` des **SyAn** Ordners. Diese Datei trägt den Suffix
`-with-dependencies.jar`, was das unkomplizierte Starten der Anwendung erlaubt.


## Lizenz

Dieses Projekt ist lizenziert unter der GNU General Public License v3 oder
später.  Siehe [LICENSE](https://gitlab.com/ginf-wang/syan/blob/master/LICENSE)
für mehr Informationen.


## Weitere Informationen

Um weitere Informationen, beispielsweise bezüglich der Bedienung der Software,
   zu erhalten, können Quellen wie das Benutzerhandbuch konsultiert werden.
