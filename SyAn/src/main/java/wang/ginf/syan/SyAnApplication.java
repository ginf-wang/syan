package wang.ginf.syan;

/**
 * Die Hauptklasse der SyAn Anwendung.
 * <p>
 * Sie ist notwendig, damit alle Module von u.a. JavaFx korrekt geladen werden koennen.
 *
 * @author Ruben Smidt
 */
public class SyAnApplication {
    public static void main(final String[] args) {
        SyAnApplicationFX.main(args);
    }
}
