package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.model.syndrome.Sphere;

/**
 * Das Kommando, das eine Sphaerenbewegung ausfuehrt.
 *
 * @author Hannes Masuch
 */
public class MoveSphereCommand extends Command {
    private final Sphere sphere;
    private final double oldX;
    private final double oldY;
    private final double newX;
    private final double newY;

    /**
     * Erstellt eine neue Instanz dieses Commands mit der gegebenen Sphaere und ihrer neuen Position.
     *
     * @param sphere das zu verschiebende Symptom.
     * @param oldX   die alte x-Koordinate.
     * @param oldY   die alte y-Koordinate.
     * @param newX   die neue x-Koordinate.
     * @param newY   die neue y-Koordinate.
     */
    public MoveSphereCommand(final Sphere sphere,
                             final double oldX,
                             final double oldY,
                             final double newX,
                             final double newY) {
        this.sphere = sphere;
        this.oldX = oldX;
        this.oldY = oldY;
        this.newX = newX;
        this.newY = newY;

        params = sphere.getName();
    }

    @Override
    protected boolean executeImpl() {
        sphere.setLayoutX(newX);
        sphere.setLayoutY(newY);

        return true;
    }

    @Override
    protected void undoImpl() {
        sphere.setLayoutX(oldX);
        sphere.setLayoutY(oldY);
    }
}
