package wang.ginf.syan.model.syndrome;

import javafx.beans.property.ReadOnlySetProperty;
import javafx.beans.property.SetProperty;
import javafx.beans.property.SimpleSetProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.scene.paint.Color;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.SerialEntityReader;
import wang.ginf.syan.model.SerialEntityWriter;
import wang.ginf.syan.model.SyanCollections;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Repraesentiert einen Syndromansatz mit seinen Sphaeren.
 *
 * @author Arne Kiesewetter
 * @author Jan-Luca Kiok
 */
public class Syndrome implements Serializable {
    private Sphere toolboxSphere = new Sphere("Toolbox", Color.WHITE, null);
    private transient SetProperty<Sphere> spheres = new SimpleSetProperty<>(SyanCollections.observableLinkedSet());
    private transient ObservableSet<Relation> relations = FXCollections.observableSet();

    /**
     * Erstellt einen neuen Syndromansatz mit den gegebenen Sphaeren.
     *
     * @param spheres Die Sphaeren des Syndromansatzes.
     */
    public Syndrome(@NotNull final Set<Sphere> spheres) {
        this.spheres.addAll(spheres);
    }

    private void init() {
        spheres = new SimpleSetProperty<>(SyanCollections.observableLinkedSet());
        relations = FXCollections.observableSet();
    }

    public ObservableSet<Relation> getRelations() {
        return relations;
    }

    public void setRelations(final List<Relation> relations) {
        this.relations.addAll(relations);
    }

    public boolean addRelation(final Relation relation) {
        return relations.add(relation);
    }

    public boolean removeRelation(final Relation relation) {
        return relations.remove(relation);
    }

    /**
     * Ueberprueft ob das gegebene {@Symptom} in diesem Syndrom enthalten ist.
     *
     * @param symptom das Symptom nach dem gesucht wird.
     * @return ob das Symptom in diesem Syndrom enthalten ist.
     */
    public boolean containsSymptom(@NotNull final Symptom symptom) {
        if (toolboxSphere.containsSymptom(symptom)) {
            return true;
        }

        return getSpheres().stream().anyMatch(sphere -> sphere.containsSymptom(symptom));
    }

    /**
     * Ueberprueft ob ein {@see Symptom} mit dem gegebenen Namen in diesem Syndrom enthalten ist.
     *
     * @param name der Name des Symptoms nach dem gesucht wird.
     * @return ob ein Symptom mit diesem Namen in diesem Symptom enthalten ist.
     */
    public boolean containsSymptom(@NotNull final String name) {
        if (toolboxSphere.containsSymptom(name)) {
            return true;
        }

        return getSpheres().stream().anyMatch(sphere -> sphere.containsSymptom(name));
    }

    /**
     * Ueberprueft ob die gegebene {@Relation} in diesem Syndrom enthalten ist.
     *
     * @param relation die Relation nach der gesucht wird.
     * @return ob die Relation in diesem Syndrom enthalten ist.
     */
    public boolean containsRelation(@NotNull Relation relation) {
        return getSpheres().stream()
                           .flatMap(sphere -> sphere.getSymptoms().stream())
                           .anyMatch(symptom -> symptom.getRelations().contains(relation));
    }

    public ReadOnlySetProperty<Sphere> spheresProperty() {
        return spheres;
    }

    public ObservableSet<Sphere> getSpheres() { return spheres.get(); }

    public Sphere getToolboxSphere() { return toolboxSphere; }

    public void setToolboxSphere(final Sphere toolboxSphere) { this.toolboxSphere = toolboxSphere; }

    private synchronized void writeObject(ObjectOutputStream s) throws IOException {
        SerialEntityWriter.writeSyndrome(s, getToolboxSphere(), spheres.get(), relations);
    }

    private synchronized void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        init();
        SerialEntityReader.readSyndrome(s, this);
    }
}
