package wang.ginf.syan.model.history.commands;

import javafx.scene.paint.Color;
import wang.ginf.syan.model.syndrome.Sphere;

/**
 * Das Kommando, das eine Sphaerenumfaerbung ausfuehrt.
 *
 * @author Hannes Masuch
 */
public class RecolorSphereCommand extends Command {
    private final Sphere sphere;
    private Color color;

    /**
     * Erstellt eine neue Instanz dieses Commands mit der gegebenen Zielsphere und der gegebenen neuen Farbe.
     *
     * @param sphere die Sphere welche veraendert werden soll.
     * @param color  die neue Farbe fuer die Sphere.
     */
    public RecolorSphereCommand(final Sphere sphere, final Color color) {
        this.sphere = sphere;
        this.color = color;

        params = "(" + sphere.getName() + ") " + sphere.getColor().toString() + " -> " + color.toString();
    }

    @Override
    protected boolean executeImpl() {
        final var oldColor = sphere.getColor();
        sphere.setColor(color);

        color = oldColor;
        return true;
    }

    @Override
    protected void undoImpl() {
        executeImpl();
    }
}
