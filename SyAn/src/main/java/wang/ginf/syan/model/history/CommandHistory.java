package wang.ginf.syan.model.history;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.GlobalState;
import wang.ginf.syan.model.history.commands.*;
import wang.ginf.syan.persistence.CommandsDatabase;

import java.util.List;
import java.util.Objects;

/**
 * Realisiert einen Bearbeitungsverlauf fuer alle Aenderungen am Syndrom.
 *
 * @author Arne Kiesewetter
 * @author Hannes Masuch
 * @author Ruben Smidt
 * @author Jan-Luca Kiok
 */
public class CommandHistory {
    private static CommandHistory instance;

    private final ObservableList<Command> history =
            FXCollections.observableArrayList(item -> new Observable[]{item.nonCanonicalProperty()});

    private final CommandsDatabase commandsDb;

    public CommandHistory() {
        commandsDb = new CommandsDatabase();
    }

    /**
     * Erstellt einen neuen Bearbeitungsverlauf mit der gegebenen Datenbankanbindung.
     *
     * @param commandsDb die Schnittstelle zur Datenbank.
     */
    public CommandHistory(@NotNull final CommandsDatabase commandsDb) {
        this.commandsDb = commandsDb;

        history.addAll(getCommandsDb().getAll());
    }

    public static CommandHistory getInstance() {
        if (Objects.isNull(instance)) {
            instance = new CommandHistory();
        }

        return instance;
    }

    public static CommandHistory getInstance(@NotNull CommandsDatabase commandsDatabase) {
        if (Objects.isNull(instance)) {
            instance = new CommandHistory(commandsDatabase);
        }

        return instance;
    }

    /**
     * Fuehrt den gegebenen Command aus und fuegt ihn dem Verlauf hinzu.
     *
     * @param command der auszufuehrende Command. Muss {@see CommandStatus.NEW} haben.
     * @return ob der Command erfolgreich ausgefuehrt wurde.
     */
    public synchronized boolean execute(@NotNull Command command) {
        if (!command.execute()) {
            return false;
        }

        history.add(command);

        if (commandsDb != null) {
            commandsDb.add(command);
        }

        updateNonCanonical();

        return true;
    }

    /**
     * Setzt nonCanonical aller Kommandos, die nicht den Status EXECUTET haben und keine Undo-/Redocommands sind
     * auf true, falls das zuletzt ausgeführte Kommando kein Undo-/Redocommand ist.
     */
    private void updateNonCanonical() {
        final var lastCmd = history.get(history.size() - 1);
        if (lastCmd instanceof UndoCommand || lastCmd instanceof RedoCommand) {
            return;
        }

        for (var i = history.size() - 2; i >= 0; --i) {
            final var cmd = history.get(i);

            if (cmd.getStatus() == CommandStatus.EXECUTED &&
                !(cmd instanceof UndoCommand || cmd instanceof RedoCommand)) {
                return;
            }

            cmd.setNonCanonical(true);
        }
    }

    /**
     * Macht den letzten, nicht bereits rueckgaengig gemachten, Command rueckgaengig und fuegt dieses dem Verlauf hinzu.
     */
    public void undo() {
        // Potential performance boost by keeping an index of the last undone command
        for (var i = getHistory().size() - 1; i >= 0; i--) {
            final var cmd = history.get(i);

            if (cmd instanceof LegacyCommand) {
                break;
            }

            if (cmd.getStatus() == CommandStatus.EXECUTED && !(cmd instanceof UndoCommand)) {
                execute(new UndoCommand(cmd));
                commandsDb.setCommandStatus(cmd);
                return;
            }
        }
    }

    /**
     * Wiederholt den letzten, nicht bereits wiederholten, Command und fuegt dieses dem Verlauf hinzu.
     */
    public void redo() {
        for (var i = history.size() - 1; i >= 0; --i) {
            final var cmd = history.get(i);

            if (cmd instanceof UndoCommand &&
                cmd.getStatus() == CommandStatus.EXECUTED &&
                !cmd.isNonCanonical()) {
                execute(new RedoCommand((UndoCommand) cmd));
                commandsDb.setCommandStatus(((UndoCommand) cmd).getCommand());
                return;
            }
        }
    }

    /**
     * Stellt den Zustand wieder her, der nach Ausfuehrung des Kommandos mit dem gegebenen Index bestand.
     *
     * @param index Der Index des Kommandos, dessen Zustand wiederhergestellt werden soll.
     */
    public void moveTo(final Integer index) {
        if (!GlobalState.isUndoRedoAllowed()) {
            return;
        }

        final var targetCmd = history.get(index);

        if (targetCmd.isNonCanonical() ||
            targetCmd instanceof UndoCommand ||
            targetCmd instanceof RedoCommand ||
            targetCmd instanceof LegacyCommand) {
            return;
        }

        final var wasTargetUndone = targetCmd.getStatus() == CommandStatus.UNDONE;

        var i = history.size() - 1;
        while ((wasTargetUndone && targetCmd.getStatus() == CommandStatus.UNDONE) ||
               (!wasTargetUndone && targetCmd.getStatus() == CommandStatus.EXECUTED)) {
            final var cmd = history.get(i);

            if (cmd instanceof LegacyCommand) {
                break;
            }

            execute(new UndoCommand(cmd));

            --i;
        }
    }

    public CommandsDatabase getCommandsDb() {
        return commandsDb;
    }

    public ObservableList<Command> getHistory() {
        return history;
    }

    /**
     * Ueberschreibt die aktuelle CommandHistory mit einem neuen Satz an Kommandos.
     *
     * @param commands die neuen Kommandos.
     */
    public void resetWith(final List<? extends Command> commands) {
        getHistory().clear();
        getHistory().addAll(commands);
    }
}
