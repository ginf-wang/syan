package wang.ginf.syan.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;
import wang.ginf.syan.businesslogic.analysis.GraphicalAnalysis;

import java.io.Serializable;

public abstract class EntityBase implements Serializable {
    private transient ObjectProperty<Color> color = new SimpleObjectProperty<>(Color.BLACK);
    private transient BooleanProperty changeable = new SimpleBooleanProperty(true);
    private transient GraphicalAnalysis graphicalAnalysis = new GraphicalAnalysis();


    /**
     * Initialisiert die Felder der Klasse nach vor einer Deserialisiernung
     */
    public void init(final Color color) {
        this.color = new SimpleObjectProperty<>(Color.BLACK);
        changeable = new SimpleBooleanProperty(true);
        graphicalAnalysis = new GraphicalAnalysis();
        this.color.setValue(color);
    }

    public abstract String getTypeKey();

    public Color getColor() {
        return color.get();
    }

    public void setColor(final Color color) {
        this.color.setValue(color);
    }

    public ObjectProperty<Color> colorProperty() {
        return color;
    }

    public boolean isChangeable() {
        return changeable.get();
    }

    public void setChangeable(final boolean changeable) {
        this.changeable.set(changeable);
    }

    public BooleanProperty changeableProperty() {
        return changeable;
    }

    public GraphicalAnalysis getGraphicalAnalysis() {
        return graphicalAnalysis;
    }
}
