package wang.ginf.syan.model;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Node;

import java.util.*;

/**
 * Settings Singleton-Klasse mit der man die Einstellungen manipulieren kann.
 *
 * @author Ibrahim Apachi
 * @author Ruben Smidt
 */
public class Settings {
    public static final int MIN_FONT_SIZE = 8;
    public static final int MAX_FONT_SIZE = 42;
    public static final List<Locale> SUPPORTED_LOCALES = new ArrayList<>(Arrays.asList(Locale.GERMAN, Locale.ENGLISH));
    private static final int DEFAULT_FONT_SIZE = 13;

    private static final IntegerProperty fontSize = new SimpleIntegerProperty(DEFAULT_FONT_SIZE);

    private static Settings instance = new Settings();
    private final Set<Node> listeningNodes = new HashSet<>();

    public static Settings getInstance() {
        return instance;
    }

    /**
     * Registriert beliebig viele Knoten, damit diese bei Aenderungen an den Settings
     * automatisch aktualisiert oder benachrichtigt werden.
     *
     * @param nodes die zu registrierenden Knoten.
     */
    public static void registerNodes(final Collection<? extends Node> nodes) {
        final Settings settings = getInstance();

        settings.listeningNodes.addAll(nodes);

        // Set the font-size on register to make sure that a previous set font size will be take into account.
        settings.setFontSizeFor(getFontSize(), nodes);
    }

    /**
     * Registriert beliebig viele Knoten, damit diese bei Aenderungen an den Settings
     * automatisch aktualisiert oder benachrichtigt werden.
     *
     * @param nodes die zu registrierenden Knoten.
     */
    public static void registerNodes(final Node... nodes) {
        registerNodes(Arrays.asList(nodes));
    }

    /**
     * Gibt eine Schriftgroesse in Abhaengigkeit der bereits gesetzten Schriftgroesse zurueck.
     *
     * @param initialFontSize die initiale Schriftgroesse.
     * @return ein Binding mit der angepassten Schriftgroesse.
     */
    public static DoubleBinding scaleFontSizeBinding(final double initialFontSize) {
        final var currentFontSize = getFontSize();

        return Bindings.createDoubleBinding(
                () -> Math.max(currentFontSize, initialFontSize - currentFontSize + getFontSize()),
                fontSize
        );
    }

    /**
     * Gibt ein Binding zurueck, dass den aktuellen Faktor der Schriftgroessenadnerung beinhaltet.
     *
     * @return das Binding.
     */
    public static DoubleBinding getScaleFactorBinding() {
        final var currentFontSize = getFontSize();

        return Bindings.createDoubleBinding(
                () -> getFontSize() / ((double) currentFontSize),
                fontSize
        );
    }

    public static boolean isDefaultLocaleSupported() {
        return SUPPORTED_LOCALES.contains(Locale.getDefault());
    }

    public static int getFontSize() {
        return fontSize.get();
    }

    public void setFontSize(final int fontSize) {
        setFontSizeFor(fontSize, listeningNodes);
    }

    /**
     * Setzt die Schriftgroesse fuer eine Liste von Knoten.
     *
     * @param newValue Die neue Schriftgroesse.
     * @param nodes    Knoten die veraendert werden sollen.
     */
    private void setFontSizeFor(final int newValue, final Collection<? extends Node> nodes) {
        if (isFontSizeValid(newValue)) {
            Settings.fontSize.set(newValue);
            final String newFontStyle = String.format("-fx-font-size: %d", newValue);
            nodes.forEach(node -> node.setStyle(newFontStyle));
        }
    }

    private boolean isFontSizeValid(final int valueToCheck) {
        return valueToCheck >= MIN_FONT_SIZE && valueToCheck <= MAX_FONT_SIZE;
    }
}
