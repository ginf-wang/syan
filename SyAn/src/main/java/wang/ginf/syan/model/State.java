package wang.ginf.syan.model;

/**
 * Repraesentiert einen beliebigen Zustand, den die Anwendung annehmen kann,
 * aber nicht muss.
 * <p>
 * {@link GlobalState} verwaltet den Anwendungszustand.
 *
 * @author Ruben Smidt
 */
public class State {
    private final boolean strengtheningRelationAllowed;
    private final boolean unknownRelationAllowed;
    private final boolean weakeningRelationAllowed;
    private final boolean symptomCreationAllowed;
    private final boolean template;

    State(final boolean strengtheningRelationAllowed,
          final boolean unknownRelationAllowed,
          final boolean weakeningRelationAllowed,
          final boolean symptomCreationAllowed,
          final boolean template) {
        this.strengtheningRelationAllowed = strengtheningRelationAllowed;
        this.unknownRelationAllowed = unknownRelationAllowed;
        this.weakeningRelationAllowed = weakeningRelationAllowed;
        this.symptomCreationAllowed = symptomCreationAllowed;
        this.template = template;
    }

    boolean isStrengtheningRelationAllowed() {
        return strengtheningRelationAllowed;
    }

    boolean isUnknownRelationAllowed() {
        return unknownRelationAllowed;
    }

    boolean isWeakeningRelationAllowed() {
        return weakeningRelationAllowed;
    }

    boolean isSymptomCreationAllowed() {
        return symptomCreationAllowed;
    }

    public boolean isTemplate() {
        return template;
    }
}
