package wang.ginf.syan.model;

import java.util.Set;

/**
 * Ein TemplateCreationResult dient als Datentransferobjekt,
 * um das Ergebnis einer Vorlagenerstellung zu buendeln.
 *
 * @author Ruben Smidt
 */
public class TemplateCreationResult {
    private final Set<String> uniqueSphereNames;
    private final String name;
    private final boolean creationOfUnknownRelationAllowed;
    private final boolean creationOfStrengtheningRelationAllowed;
    private final boolean creationOfWeakeningRelationAllowed;
    private final boolean creationOfSymptomsAllowed;

    public TemplateCreationResult(
            final Set<String> uniqueSphereNames,
            final String name,
            final boolean creationOfUnknownRelationAllowed,
            final boolean creationOfStrengtheningRelationAllowed,
            final boolean creationOfWeakeningRelationAllowed,
            final boolean creationOfSymptomsAllowed) {
        this.uniqueSphereNames = uniqueSphereNames;
        this.name = name;
        this.creationOfUnknownRelationAllowed = creationOfUnknownRelationAllowed;
        this.creationOfStrengtheningRelationAllowed = creationOfStrengtheningRelationAllowed;
        this.creationOfWeakeningRelationAllowed = creationOfWeakeningRelationAllowed;
        this.creationOfSymptomsAllowed = creationOfSymptomsAllowed;
    }

    public boolean isCreationOfSymptomsAllowed() {
        return creationOfSymptomsAllowed;
    }

    public boolean isCreationOfUnknownRelationAllowed() {
        return creationOfUnknownRelationAllowed;
    }

    public boolean isCreationOfStrengtheningRelationAllowed() {
        return creationOfStrengtheningRelationAllowed;
    }

    public boolean isCreationOfWeakeningRelationAllowed() {
        return creationOfWeakeningRelationAllowed;
    }

    public Set<String> getUniqueSphereNames() {
        return uniqueSphereNames;
    }

    public String getName() {
        return name;
    }
}
