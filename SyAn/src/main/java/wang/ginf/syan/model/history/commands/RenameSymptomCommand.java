package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;

/**
 * Das Kommando, das eine Symptombennung ausfuehrt.
 *
 * @author Hannes Masuch
 */
public class RenameSymptomCommand extends Command {
    private Syndrome syndrome;
    private Symptom symptom;
    private String newName;

    /**
     * Erstellt eine neue Instanz dieses Commands mit dem gegebenen Zielsymptom, dem Syndrom und dem neuen Namen.
     *
     * @param syndrome das Syndrom, in welchem die Aenderung vorgenommen wird.
     * @param symptom  das Symptom welches veraendert werden soll.
     * @param name     der neue Name das Symptoms.
     */
    public RenameSymptomCommand(final Syndrome syndrome, final Symptom symptom, final String name) {
        this.syndrome = syndrome;
        this.symptom = symptom;
        this.newName = name;

        params = symptom.getName() + " -> " + name;
    }

    @Override
    protected boolean executeImpl() {
        // Ist es derselbe Name, aber anders geschrieben, dann wuerde trotzdem containsSymptom true zurueckgeben.
        // Daher wird erstmal ueberprueft, ob der neue Name der gleiche ist, wie er bisher im Symptom vorliegt.
        if (!newName.equalsIgnoreCase(symptom.getName()) && syndrome.containsSymptom(newName)) {
            return false;
        }

        final var oldName = symptom.getName();

        symptom.setName(newName);
        newName = oldName;

        return true;
    }

    @Override
    protected void undoImpl() {
        executeImpl();
    }
}
