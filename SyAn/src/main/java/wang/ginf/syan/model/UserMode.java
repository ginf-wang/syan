package wang.ginf.syan.model;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Enum, dass die User Modi darstellt und den aktuellen Modus als String zurueckgeben kann.
 *
 * @author Arne Kiesewetter
 * @author Ibrahim Apachi
 */
public enum UserMode {
    OBSERVER("observerMode"),
    ANALYST("analystMode"),
    CREATOR("creatorMode");

    private final String key;

    UserMode(final String key) {
        this.key = key;
    }

    public String getName() {
        return getName(Locale.getDefault());
    }

    public String getName(final Locale locale) {
        return ResourceBundle.getBundle("lang/UserModes", locale).getString(key);
    }

    @Override
    public String toString() {
        return getName();
    }
}