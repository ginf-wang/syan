package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.model.syndrome.Symptom;

/**
 * Das Kommando, das eine Symptombewegung ausfuehrt.
 *
 * @author Hannes Masuch
 */
public class MoveSymptomCommand extends Command {
    private final Symptom symptom;
    private final double oldX;
    private final double oldY;
    private final double newX;
    private final double newY;

    /**
     * Erstellt eine neue Instanz dieses Commands mit dem gegebenen Symptom und seiner neuen Position.
     *
     * @param symptom das zu verschiebende Symptom.
     * @param oldX    die alte x-Koordinate.
     * @param oldY    die alte y-Koordinate.
     * @param newX    die neue x-Koordinate.
     * @param newY    die neue y-Koordinate.
     */
    public MoveSymptomCommand(final Symptom symptom,
                              final double oldX,
                              final double oldY,
                              final double newX,
                              final double newY) {
        this.symptom = symptom;
        this.oldX = oldX;
        this.oldY = oldY;
        this.newX = newX;
        this.newY = newY;

        params = symptom.getName();
    }

    @Override
    protected boolean executeImpl() {
        symptom.setLayoutX(newX);
        symptom.setLayoutY(newY);

        return true;
    }

    @Override
    protected void undoImpl() {
        symptom.setLayoutX(oldX);
        symptom.setLayoutY(oldY);
    }
}
