package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.RelationStrength;

/**
 * Das Kommando, das eine Relationsstaerkeaenderung ausfuehrt.
 *
 * @author Hannes Masuch
 */
public class ChangeRelationStrengthCommand extends Command {
    private final Relation relation;
    private RelationStrength newStrength;

    /**
     * Erstellt eine neue Instanz dieses Commands mit der gegebenen Zielrelation und der Relationsstaerke.
     *
     * @param relation die Relation die veraendert werden soll.
     * @param strength die Relationsstaerke.
     */
    public ChangeRelationStrengthCommand(final Relation relation, final RelationStrength strength) {
        this.relation = relation;
        this.newStrength = strength;

        params = String.format(
                "(%s ==> %s) %s -> %s",
                relation.getStartSymptom().getName(),
                relation.getEndSymptom().getName(),
                relation.getRelationStrength(),
                strength
        );
    }

    @Override
    protected boolean executeImpl() {
        if (relation.getRelationStrength() != newStrength) {
            final var oldStrength = relation.getRelationStrength();

            relation.setRelationStrength(newStrength);
            newStrength = oldStrength;

            return true;
        }

        return false;
    }

    @Override
    protected void undoImpl() {
        executeImpl();
    }
}
