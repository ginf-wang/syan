package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.NotNull;

/**
 * Ein Command, welcher einen anderen rueckgaengig macht.
 *
 * @author Arne Kiesewetter
 */
public class UndoCommand extends Command {

    private final Command command;

    /**
     * Erstellt eine neue Instanz der UndoCommand Klasse fuer den gegebebenen Command.
     *
     * @param command der Command der rueckgaengig gemacht werden soll.
     *                Muss {@see CommandStatus.EXECUTED} haben, wenn dieser Command ausgefuehrt wird.
     */
    public UndoCommand(@NotNull final Command command) {
        this.command = command;
        params = "(" + command.getId() + ") " + command.toString();
    }

    @Override
    protected boolean executeImpl() {
        command.undo();

        return true;
    }

    @Override
    protected void undoImpl() {
        command.redo();
    }

    public Command getCommand() {
        return command;
    }
}
