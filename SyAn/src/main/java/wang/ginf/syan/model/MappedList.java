package wang.ginf.syan.model;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.TransformationList;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Eine Klasse, die aehnlich wie eine {@link javafx.collections.transformation.FilteredList},
 * eine Inputliste transformiert in eine Outputliste. Dabei werden die Elemente des Inputs umgewandelt.
 * <p>
 * Basierend auf https://bugs.openjdk.java.net/browse/JDK-8091967
 * und https://gist.github.com/TomasMikula/8883719 (JavaFx EasyBind)
 *
 * @param <E> der Typ der Inputliste.
 * @param <F> der Typ der umgewandelten Liste.
 * @author Ruben Smidt
 */
public class MappedList<E, F> extends TransformationList<E, F> {
    private final Function<F, E> mapper;

    /**
     * Creates a new Transformation list wrapped around the source list.
     *
     * @param source the wrapped list
     */
    private MappedList(final ObservableList<? extends F> source, final Function<F, E> mapper) {
        super(source);

        this.mapper = mapper;
    }

    public static <E, F> MappedList<E, F> of(final ObservableList<? extends F> source, final Function<F, E> mapper) {
        return new MappedList<>(source, mapper);
    }

    @Override
    protected void sourceChanged(final ListChangeListener.Change<? extends F> c) {
        fireChange(new ListChangeListener.Change<>(this) {
            @Override
            public boolean wasAdded() {
                return c.wasAdded();
            }

            @Override
            public boolean wasRemoved() {
                return c.wasRemoved();
            }

            @Override
            public boolean wasReplaced() {
                return c.wasReplaced();
            }

            @Override
            public boolean wasUpdated() {
                return c.wasUpdated();
            }

            @Override
            public boolean next() {
                return c.next();
            }

            @Override
            public void reset() {
                c.reset();
            }

            @Override
            public int getFrom() {
                return c.getFrom();
            }

            @Override
            public int getTo() {
                return c.getTo();
            }

            @Override
            public List<E> getRemoved() {
                return c.getRemoved().stream()
                        .map(mapper)
                        .collect(Collectors.toList());
            }

            @Override
            public int getPermutation(final int i) {
                return c.getPermutation(i);
            }

            @Override
            public boolean wasPermutated() {
                return c.wasPermutated();
            }

            @Override
            protected int[] getPermutation() {
                throw new AssertionError("Should not happen.");
            }
        });
    }

    @Override
    public int getSourceIndex(final int index) {
        return index;
    }

    @Override
    public int getViewIndex(final int index) {
        return index;
    }

    @Override
    public E get(final int i) {
        return mapper.apply(getSource().get(i));
    }

    @Override
    public int size() {
        return getSource().size();
    }
}
