package wang.ginf.syan.model;

import javafx.collections.ObservableSet;
import javafx.scene.paint.Color;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.*;

import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Utility, welche das Serialisieren von Graphentitaeten vereinheitlicht
 * <p>
 * Code in Teilen angelehnt an:
 * https://gist.github.com/james-d/a7202039b00170256293
 * https://stackoverflow.com/questions/18791566/notserializableexception-on-simplelistproperty
 * https://stackoverflow.com/questions/34794995/how-to-serialize-observablelist
 *
 * @author Jan-Luca Kiok
 */
public class SerialEntityWriter {

    /**
     * Serialisiert ein Syndrom, konvertiert dafuer die nichtserialisierbaren Felder
     *
     * @param s         Der zu fuellende ObjectOutputStream
     * @param spheres   Die in einem Syndrom enthaltenen Sphaeren
     * @param relations Die in einem Syndrom enthaltenen Relationen
     * @throws IOException
     */
    public static void writeSyndrome(
            @NotNull final ObjectOutputStream s,
            @NotNull final Sphere toolboxSphere,
            @NotNull final ObservableSet<Sphere> spheres,
            @NotNull final ObservableSet<Relation> relations) throws IOException {
        s.writeObject(toolboxSphere);

        s.writeInt(spheres.size());
        for (final var sphere : spheres) {
            s.writeObject(sphere);
        }

        s.writeInt(relations.size());
        for (final var relation : relations) {
            s.writeObject(relation);
        }
    }

    /**
     * Serialisiert abstrakte Basisentitaeten
     *
     * @param s          Der zu fuellende ObjectOutputStream
     * @param color      Die Farbe der Basisentitaet
     * @param changeable Erlaubnis, waehrend der Ausfuehrung Aenderungen an der Entitaet vorzunehmen
     * @throws IOException
     */
    private static void writeEntityBase(@NotNull final ObjectOutputStream s,
                                        @NotNull final Color color,
                                        @NotNull final boolean changeable) throws IOException {
        s.writeDouble(color.getRed());
        s.writeDouble(color.getGreen());
        s.writeDouble(color.getBlue());
        s.writeDouble(color.getOpacity());
        s.writeBoolean(changeable);
    }

    /**
     * Serialisiert abstrakte Entitaeten
     *
     * @param s Der zu fuellende ObjectOutputStream
     * @param e Die Entitaet
     * @throws IOException
     */
    private static void writeAbEntity(@NotNull final ObjectOutputStream s, @NotNull final Entity e) throws IOException {
        writeEntityBase(s, e.getColor(), e.isChangeable());
        s.writeDouble(e.getLayoutX());
        s.writeDouble(e.getLayoutY());
        s.writeDouble(e.getWidth());
        s.writeDouble(e.getHeight());
    }

    /**
     * Serialisiert Sphaeren
     *
     * @param s      Der zu fuellende ObjectOutputStream
     * @param sphere Die Sphaere
     * @throws IOException
     */
    public static void writeEntity(@NotNull final ObjectOutputStream s, final @NotNull Sphere sphere)
            throws IOException {
        writeAbEntity(s, sphere);
        s.writeUTF(sphere.getName());

        s.writeInt(sphere.getSymptoms().size());
        for (final Symptom symptom : sphere.getSymptoms()) {
            s.writeObject(symptom);
        }
    }

    /**
     * Serialisiert Symptome
     *
     * @param s       Der zu fuellende ObjectOutputStream
     * @param symptom Das Symptom
     * @throws IOException
     */
    public static void writeEntity(@NotNull final ObjectOutputStream s, @NotNull final Symptom symptom)
            throws IOException {
        writeAbEntity(s, symptom);
        s.writeUTF(symptom.getName());

        s.writeInt(symptom.getRelations().size());
        for (final var relation : symptom.getRelations()) {
            s.writeObject(relation);
        }
    }

    /**
     * Serialisiert Relationen
     *
     * @param s        Der zu fuellende ObjectOutputStream
     * @param relation Die Relation
     * @throws IOException
     */
    public static void writeEntity(@NotNull final ObjectOutputStream s, @NotNull final Relation relation)
            throws IOException {
        writeEntityBase(s, relation.getColor(), relation.isChangeable());

        s.writeObject(relation.getRelationStrength());
        s.writeObject(relation.getRelationType());
        s.writeObject(relation.getStartSymptom());
        s.writeObject(relation.getEndSymptom());

        s.writeInt(relation.getWaypoints().size());
        for (final var waypoint : relation.getWaypoints()) {
            s.writeObject(waypoint);
        }

        s.writeInt(relation.getStartSymptomAttachedSide());
        s.writeInt(relation.getEndSymptomAttachedSide());
    }

    /**
     * Serialisiert Wegpunkte
     *
     * @param s        Der zu fuellende ObjectOutputStream
     * @param waypoint Der Wegpunkt
     * @throws IOException
     */
    public static void writeWaypoint(@NotNull final ObjectOutputStream s, @NotNull final Waypoint waypoint)
            throws IOException {
        s.writeDouble(waypoint.getLayoutX());
        s.writeDouble(waypoint.getLayoutY());
    }
}

