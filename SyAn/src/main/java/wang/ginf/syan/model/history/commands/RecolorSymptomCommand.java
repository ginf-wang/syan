package wang.ginf.syan.model.history.commands;

import javafx.scene.paint.Color;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Symptom;

/**
 * Ein Command, um die Farbe eines Symptoms zu aendern.
 *
 * @author Arne Kiesewetter
 */
public class RecolorSymptomCommand extends Command {
    private final Symptom symptom;
    private Color color;

    /**
     * Erstellt eine neue Instanz dieses Commands mit dem gegebenen Zielsymptom und der gegebenen neuen Farbe.
     *
     * @param symptom das Symptom welches veraendert werden soll.
     * @param color   die neue Farbe fuer das Symptom.
     */
    public RecolorSymptomCommand(@NotNull final Symptom symptom, @NotNull final Color color) {
        this.symptom = symptom;
        this.color = color;

        params = "(" + symptom.getName() + ") " + symptom.getColor().toString() + " -> " + color.toString();
    }

    @Override
    protected boolean executeImpl() {
        final var oldColor = symptom.getColor();

        symptom.setColor(color);
        color = oldColor;

        return true;
    }

    @Override
    protected void undoImpl() {
        executeImpl();
    }
}
