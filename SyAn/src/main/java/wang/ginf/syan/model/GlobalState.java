package wang.ginf.syan.model;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableSet;

/**
 * Der GlobalState stellt den globalen Zustand der Anwendung dar.
 * Er haelt Werte, wie den aktullen Modus.
 *
 * @author Ruben Smidt
 */
public class GlobalState {
    private static final ObservableSet<UserMode> userMode = SyanCollections.observableLinkedSet();
    private static final BooleanProperty strengtheningRelationAllowed = new SimpleBooleanProperty(true);
    private static final BooleanProperty unknownRelationAllowed = new SimpleBooleanProperty(true);
    private static final BooleanProperty weakeningRelationAllowed = new SimpleBooleanProperty(true);
    private static final BooleanProperty symptomCreationAllowed = new SimpleBooleanProperty(true);
    private static final BooleanProperty undoRedoAllowed = new SimpleBooleanProperty(true);
    private static final BooleanProperty template = new SimpleBooleanProperty(false);
    private static final BooleanProperty unsavedChanges = new SimpleBooleanProperty(false);
    private static final BooleanBinding atLeastCreator = Bindings.createBooleanBinding(
            () -> userMode.contains(UserMode.CREATOR),
            userMode
    );
    private static final BooleanBinding atLeastAnalyst = Bindings.createBooleanBinding(
            () -> userMode.contains(UserMode.ANALYST),
            userMode
    );
    private static final BooleanBinding atLeastObserver = Bindings.createBooleanBinding(
            () -> userMode.contains(UserMode.OBSERVER),
            userMode
    );
    private static final BooleanBinding analystButNotCreator =
            GlobalState.atLeastAnalystProperty().and(GlobalState.atLeastCreatorProperty().not());
    private GlobalState() {}

    public static boolean isUndoRedoAllowed() {
        return undoRedoAllowed.get();
    }

    public static void setUndoRedoAllowed(final boolean undoRedoAllowed) {
        GlobalState.undoRedoAllowed.set(undoRedoAllowed);
    }

    public static BooleanProperty undoRedoAllowedProperty() {
        return undoRedoAllowed;
    }

    public static boolean isTemplate() {
        return template.get();
    }

    public static void setTemplate(final boolean template) {
        GlobalState.template.set(template);
    }

    public static BooleanProperty templateProperty() {
        return template;
    }

    public static Boolean isAnalystButNotCreator() {
        return analystButNotCreator.get();
    }

    public static BooleanBinding analystButNotCreatorProperty() {
        return analystButNotCreator;
    }

    public static boolean shouldControlsBeDisabled(final boolean isControlChangeable) {
        return (isAtLeastAnalyst() && !isAtLeastCreator()) || (!GlobalState.isAtLeastCreator() && !isControlChangeable);
    }

    public static boolean isSymptomCreationAllowed() {
        return symptomCreationAllowed.get();
    }

    public static void setSymptomCreationAllowed(final boolean symptomCreationAllowed) {
        GlobalState.symptomCreationAllowed.set(symptomCreationAllowed);
    }

    public static BooleanProperty symptomCreationAllowedProperty() {
        return symptomCreationAllowed;
    }

    public static boolean isStrengtheningRelationAllowed() {
        return strengtheningRelationAllowed.get();
    }

    public static void setStrengtheningRelationAllowed(final boolean strengtheningRelationAllowed) {
        GlobalState.strengtheningRelationAllowed.set(strengtheningRelationAllowed);
    }

    public static BooleanProperty strengtheningRelationAllowedProperty() {
        return strengtheningRelationAllowed;
    }

    public static boolean isUnknownRelationAllowed() {
        return unknownRelationAllowed.get();
    }

    public static void setUnknownRelationAllowed(final boolean unknownRelationAllowed) {
        GlobalState.unknownRelationAllowed.set(unknownRelationAllowed);
    }

    public static BooleanProperty unknownRelationAllowedProperty() {
        return unknownRelationAllowed;
    }

    public static boolean isWeakeningRelationAllowed() {
        return weakeningRelationAllowed.get();
    }

    public static void setWeakeningRelationAllowed(final boolean weakeningRelationAllowed) {
        GlobalState.weakeningRelationAllowed.set(weakeningRelationAllowed);
    }

    public static BooleanProperty weakeningRelationAllowedProperty() {
        return weakeningRelationAllowed;
    }

    public static boolean isAtLeastCreator() {
        return atLeastCreator.get();
    }

    public static BooleanBinding atLeastCreatorProperty() {
        return atLeastCreator;
    }

    public static boolean isAtLeastAnalyst() {
        return atLeastAnalyst.get();
    }

    public static BooleanBinding atLeastAnalystProperty() {
        return atLeastAnalyst;
    }

    public static boolean isAtLeastObserver() {
        return atLeastObserver.get();
    }

    public static BooleanBinding atLeastObserverProperty() {
        return atLeastObserver;
    }

    public static ObservableSet<UserMode> getUserMode() {
        return userMode;
    }

    public static boolean isUnsavedChanges() {
        return unsavedChanges.get();
    }

    public static void setUnsavedChanges(final boolean unsavedChanges) {
        GlobalState.unsavedChanges.set(unsavedChanges);
    }

    public static BooleanProperty unsavedChangesProperty() {
        return unsavedChanges;
    }

    /**
     * Versetzt die Anwendung in einen neuen Mode.
     * Ist der Modus bereits aktiv, wird {@code false} zurueckgeben.
     *
     * @param userMode der zusaetzliche Mode.
     * @return ob die Aktion erfolgreich war.
     */
    public static boolean enableMode(final UserMode userMode) {
        return GlobalState.userMode.add(userMode);
    }

    /**
     * Versetzt die Anwendung in einen neuen Mode.
     * Ist der Modus nicht aktiv, wird {@code false} zurueckgeben.
     *
     * @param userMode der zu entfernende Mode.
     * @return ob die Aktion erfolgreich war.
     */
    public static boolean disableMode(final UserMode userMode) {
        return GlobalState.userMode.remove(userMode);
    }

    /**
     * Versetzt die Anwendung in einen neuen Mode.
     *
     * @param userModes die zu entfernenden Modi.
     */
    public static void disableModes(final UserMode... userModes) {
        for (final var mode : userModes) {
            disableMode(mode);
        }
    }

    /**
     * Setzt den Zustand der Anwendung zurueck anhand eines neuen Zustands.
     *
     * @param newState der neue Zustand.
     */
    public static void resetWith(final State newState) {
        disableModes(UserMode.ANALYST, UserMode.CREATOR);
        setTemplate(newState.isTemplate());
        setSymptomCreationAllowed(newState.isSymptomCreationAllowed());
        setUnknownRelationAllowed(newState.isUnknownRelationAllowed());
        setWeakeningRelationAllowed(newState.isWeakeningRelationAllowed());
        setStrengtheningRelationAllowed(newState.isStrengtheningRelationAllowed());
    }
}
