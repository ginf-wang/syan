package wang.ginf.syan.model.syndrome;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Beinhaltet die verschiedenen Moeglichkeiten fuer Art einer Wirkungsbeziehung.
 *
 * @author Arne Kiesewetter
 */
public enum RelationType {
    UNKNOWN,
    LESSENING,
    INCREASING;

    @Override
    public String toString() {
        return toString(Locale.getDefault());
    }

    public String toString(final Locale locale) {
        return ResourceBundle.getBundle("lang/RelationType", locale).getString(name());
    }
}
