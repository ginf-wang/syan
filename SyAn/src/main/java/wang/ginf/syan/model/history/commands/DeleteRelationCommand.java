package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;

import java.util.Objects;

/**
 * Das Kommando, das eine Relationsloeschung ausfuehrt.
 *
 * @author Hannes Masuch
 */
public class DeleteRelationCommand extends Command {
    private final Relation relation;
    private final Syndrome syndrome;

    private Symptom symptom;
    private Symptom start;
    private Symptom end;

    /**
     * Erstellt eine neue Instanz dieses Commands mit der zu loeschenden Relation.
     *
     * @param relation die zu loeschende Relation.
     * @param syndrome das Syndrom.
     * @param symptom  das Symptom welches geloescht wird und alle anliegenden Relationen loescht.
     */
    public DeleteRelationCommand(final Relation relation, final Syndrome syndrome, final Symptom symptom) {
        this.relation = relation;
        this.syndrome = syndrome;
        this.symptom = symptom;

        this.start = relation.getStartSymptom();
        this.end = relation.getEndSymptom();

        params = relation.getStartSymptom().getName() + " ==> " + relation.getEndSymptom().getName();
    }

    @Override
    protected boolean executeImpl() {
        if (!syndrome.getRelations().contains(relation)) {
            return false;
        }

        if (Objects.isNull(symptom)) {
            start.getRelations().remove(relation);
            end.getRelations().remove(relation);
            syndrome.removeRelation(relation);
        }
        if (symptom == start) {
            end.getRelations().remove(relation);
        }
        if (symptom == end) {
            start.getRelations().remove(relation);
        }

        return true;
    }

    @Override
    protected void undoImpl() {
        start.getRelations().add(relation);
        end.getRelations().add(relation);
        syndrome.addRelation(relation);
        symptom = null;
    }
}
