package wang.ginf.syan.model.history.commands;

import javafx.beans.property.*;
import wang.ginf.syan.model.GlobalState;

import java.sql.Timestamp;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Abstrakte Basisklasse fuer alle anderen Kommandos.
 *
 * @author Arne Kiesewetter
 * @author Jan-Luca Kiok
 * @author Hannes Masuch
 */
public abstract class Command {
    private final ObjectProperty<CommandStatus> status = new SimpleObjectProperty<>(CommandStatus.NEW);
    private final BooleanProperty nonCanonical = new SimpleBooleanProperty(false);
    protected String params;
    private String concreteClass = toString();
    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    private int id;

    /**
     * Fuehrt dieses Kommando zum ersten mal aus,
     * das heisst der Status muss {@see CommandStatus.NEW} sein.
     *
     * @return ob das Kommando erfolgreich ausgefuehrt wurde.
     */
    public boolean execute() {
        if (getStatus() != CommandStatus.NEW) {
            throw new IllegalStateException("Command already executed and not undone!");
        }

        if (executeImpl()) {
            setStatus(CommandStatus.EXECUTED);
            GlobalState.setUnsavedChanges(true);

            return true;
        }

        return false;
    }

    /**
     * Macht die Auswirkungen der Ausfuehrung dieses Commands rueckgaengig,
     * das heisst der Status muss {@see CommandStatus.EXECUTED} sein.
     */
    public void undo() {
        if (getStatus() != CommandStatus.EXECUTED) {
            throw new IllegalStateException("Command not executed or not undone!");
        }

        undoImpl();
        GlobalState.setUnsavedChanges(true);
        setStatus(CommandStatus.UNDONE);
    }

    /**
     * Fuehrt diesen Command aus, nachdem er zuvor rueckgaengig gemacht wurde,
     * das heisst der Status muss {@see CommandStatus.UNDONE} sein.
     */
    public void redo() {
        if (getStatus() != CommandStatus.UNDONE) {
            throw new IllegalStateException("Command not undone!");
        }

        executeImpl();
        setStatus(CommandStatus.EXECUTED);
        GlobalState.setUnsavedChanges(true);
    }

    @Override
    public String toString() {
        return toString(Locale.getDefault());
    }

    public String toString(final Locale locale) {
        return ResourceBundle.getBundle("lang/Commands", locale).getString(getClass().getSimpleName());
    }

    /**
     * Die eigentliche Implemenetierung der Ausfuehrung.
     * Muss in konkreten Commands ueberschrieben werden.
     *
     * @return ob die Ausfuehrung erfolgreich war.
     */
    protected abstract boolean executeImpl();

    /**
     * Die eigentliche Implemenetierung des rueckgaengig machens.
     * Muss in konkreten Commands ueberschrieben werden.
     */
    protected abstract void undoImpl();

    public ReadOnlyObjectProperty<CommandStatus> statusProperty() { return status; }

    public BooleanProperty nonCanonicalProperty() { return nonCanonical; }

    public boolean isNonCanonical() { return nonCanonical.get(); }

    /**
     * Sollte von der {@see CommandHistory} gesetzt werden, wenn das Kommando aus dem kanonischen Pfad entfernt (oder
     * wieder eingefügt) wird.
     *
     * @param value Der neue Wert.
     */
    public void setNonCanonical(final boolean value) { nonCanonical.set(value); }

    public CommandStatus getStatus() {
        return status.get();
    }

    public void setStatus(final CommandStatus value) {
        status.set(value);
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getParams() { return params; }

    public void setParams(final String params) {
        this.params = params;
    }

    public String getConcreteClass() {
        return concreteClass;
    }

    public void setConcreteClass(final String concreteClass) {
        this.concreteClass = concreteClass;
    }
}
