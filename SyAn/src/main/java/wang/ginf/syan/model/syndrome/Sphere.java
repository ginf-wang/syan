package wang.ginf.syan.model.syndrome;

import javafx.beans.property.ReadOnlySetProperty;
import javafx.beans.property.SetProperty;
import javafx.beans.property.SimpleSetProperty;
import javafx.collections.ObservableSet;
import javafx.scene.paint.Color;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.Nullable;
import wang.ginf.syan.model.SerialEntityReader;
import wang.ginf.syan.model.SerialEntityWriter;
import wang.ginf.syan.model.SyanCollections;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Objects;

/**
 * Repraesentiert eine Sphaere mit ihren Symptomen im Syndromansatz.
 *
 * @author Arne Kiesewetter
 * @author Ruben Smidt
 */
public class Sphere extends Entity {
    private static final Color DEFAULT_COLOR = Color.WHITE;
    private transient SetProperty<Symptom> symptoms = new SimpleSetProperty<>(SyanCollections.observableLinkedSet());
    private String name;

    /**
     * Erstellt eine neue Sphaere mit dem gegebenen Namen, der gegebenen {@see Color} und den gegebenen Symptomen.
     *
     * @param name     Der Name der Sphaere.
     * @param color    Die Hintergrundfarbe der Sphaere.
     * @param symptoms Die bestehenden Symptome in der Sphaere.
     */
    public Sphere(@NotNull final String name,
                  @NotNull final Color color,
                  @Nullable final Collection<Symptom> symptoms) {
        super(color, 0, 0, 1, 1);

        this.name = name;

        if (Objects.nonNull(symptoms)) {
            this.symptoms.addAll(symptoms);
        }
    }

    /**
     * Erstellt eine neue Sphaere mit dem gegebenem Namen und der gegebenen {@see Color} ohne Symptomen.
     *
     * @param name  Der Name der Sphaere.
     * @param color Die Hintergrundfarbe der Sphaere.
     */
    public Sphere(@NotNull final String name, @NotNull final Color color) {
        this(name, color, null);
    }

    /**
     * Erstellt eine neue Sphaere mit dem gegebenem Namen.
     * Farbe und Symptome werden mit Standardwerten initialisiert.
     *
     * @param name Der Name der Sphaere.
     */
    public Sphere(@NotNull final String name) {
        this(name, DEFAULT_COLOR, null);
    }

    /**
     * Weist der Symptom-Property eine initiale Auspraegung zu
     */
    public void init() {
        symptoms = new SimpleSetProperty<>(this, "symptoms", SyanCollections.observableLinkedSet());
    }

    /**
     * Ueberprueft ob das gegebene {@see Symptom} in dieser Sphaere enthalten ist.
     *
     * @param symptom das Symptom nach dem gesucht wird.
     * @return ob das Symptom in dieser Sphaere enthalten ist.
     */
    public boolean containsSymptom(@NotNull final Symptom symptom) {
        return getSymptoms().contains(symptom);
    }

    /**
     * Ueberprueft ob ein {@see Symptom} mit dem gegebenen Namen in dieser Sphaere enthalten ist.
     *
     * @param name der Name des Symptoms nach dem gesucht wird.
     * @return ob ein Symptom mit diesem Namen in dieser Sphaere enthalten ist.
     */
    public boolean containsSymptom(@NotNull final String name) {
        return getSymptoms().stream().anyMatch(symptom -> symptom.getName().equalsIgnoreCase(name));
    }

    public String getName() { return name; }

    public void setName(final String name) { this.name = name; }

    public ReadOnlySetProperty<Symptom> symptomProperty() { return symptoms; }

    public ObservableSet<Symptom> getSymptoms() { return symptoms.get(); }

    @Override
    public String getTypeKey() {
        return "sphereType";
    }

    private synchronized void writeObject(ObjectOutputStream s) throws IOException {
        SerialEntityWriter.writeEntity(s, this);
    }

    private synchronized void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        init();
        SerialEntityReader.readEntity(s, this);
    }
}
