package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.NotNull;

/**
 * Ein Command, welcher einen {@see UndoCommand} rueckgaengig macht.
 *
 * @author Arne Kiesewetter
 * @author Hannes Masuch
 */
public class RedoCommand extends Command {
    private final UndoCommand command;

    /**
     * Erstellt eine neue Instanz der RedoCommand Klasse fuer den gegebebenen UndoCommand.
     *
     * @param command der UndoCommand der rueckgaengig gemacht werden soll.
     *                Muss {@see CommandStatus.EXECUTED} haben, wenn dieser Command ausgefuehrt wird.
     */
    public RedoCommand(@NotNull final UndoCommand command) {
        this.command = command;
        params = "(" + command.getId() + ") " + command.toString();
    }

    @Override
    protected boolean executeImpl() {
        command.undo();

        return true;
    }

    @Override
    protected void undoImpl() {
        command.redo();
    }
}
