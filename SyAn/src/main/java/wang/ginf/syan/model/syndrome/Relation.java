package wang.ginf.syan.model.syndrome;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.scene.paint.Color;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.Nullable;
import wang.ginf.syan.model.EntityBase;
import wang.ginf.syan.model.SerialEntityReader;
import wang.ginf.syan.model.SerialEntityWriter;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * Repraesentiert eine Wirkungsbeziehung zwischen zwei Symptomen.
 *
 * @author Arne Kiesewetter
 * @author Ruben Smidt
 */
public class Relation extends EntityBase {
    public static final int ZONE_COUNT = 8;
    private static final Color DEFAULT_COLOR = Color.BLACK;

    private transient ObjectProperty<RelationStrength> relationStrength = new SimpleObjectProperty<>();
    private transient ObjectProperty<RelationType> relationType = new SimpleObjectProperty<>();
    private transient IntegerProperty startSymptomAttachedSide = new SimpleIntegerProperty();
    private transient IntegerProperty endSymptomAttachedSide = new SimpleIntegerProperty();
    private transient ListProperty<Waypoint> waypoints = new SimpleListProperty<>(FXCollections.observableArrayList());

    private Symptom startSymptom;
    private Symptom endSymptom;

    /**
     * Erstellt eine neue Wirkungsbeziehung der gegebenen Staerke und der gegebenen Art zwischen den gegebenen Symptomen
     * und fuegt diese Relation den Symptomen hinzu.
     *
     * @param relationStrength die Staerke der Wirkungsbeziehung.
     * @param relationType     die Art der Wirkungsbezeihung.
     * @param startSymptom     das Symptom, von dem die Wirkung ausgeht.
     * @param endSymptom       das Symptom, das beeinflusst wird.
     * @param waypoints        die Wegpunkte der Relation.
     * @param color            die Farbe der Relation.
     */
    public Relation(@NotNull final RelationStrength relationStrength,
                    @NotNull final RelationType relationType,
                    @NotNull final Symptom startSymptom,
                    @NotNull final Symptom endSymptom,
                    @Nullable final List<Waypoint> waypoints,
                    @NotNull final Color color) {
        setColor(color);

        setRelationStrength(relationStrength);
        setRelationType(relationType);

        this.startSymptom = startSymptom;
        this.endSymptom = endSymptom;

        if (waypoints != null) {
            this.waypoints.addAll(waypoints);
        }
    }

    /**
     * Erstellt eine neue Wirkungsbeziehung der gegebenen Staerke und der gegebenen Art zwischen den gegebenen Symptomen
     * und fuegt diese Relation den Symptomen hinzu. Hierbei wird eine Standardfarbe benutzt.
     *
     * @param relationStrength die Staerke der Wirkungsbeziehung.
     * @param relationType     die Art der Wirkungsbezeihung.
     * @param startSymptom     das Symptom, von dem die Wirkung ausgeht.
     * @param endSymptom       das Symptom, das beeinflusst wird.
     * @param waypoints        die Wegpunkte der Relation.
     */
    public Relation(@NotNull final RelationStrength relationStrength,
                    @NotNull final RelationType relationType,
                    @NotNull final Symptom startSymptom,
                    @NotNull final Symptom endSymptom,
                    @Nullable final List<Waypoint> waypoints
    ) {
        this(relationStrength, relationType, startSymptom, endSymptom, waypoints, DEFAULT_COLOR);
    }

    public void addToSymptoms() {
        startSymptom.getRelations().add(this);
        endSymptom.getRelations().add(this);
    }

    public void removeFromSymptoms() {
        startSymptom.getRelations().remove(this);
        endSymptom.getRelations().remove(this);
    }

    /**
     * Initialisiert die Felder der Klasse nach vor einer Deserialisiernung
     */
    public void init() {
        relationStrength = new SimpleObjectProperty<>();
        relationType = new SimpleObjectProperty<>();
        waypoints = new SimpleListProperty<>(FXCollections.observableArrayList());
        startSymptomAttachedSide = new SimpleIntegerProperty();
        endSymptomAttachedSide = new SimpleIntegerProperty();
    }

    public boolean containsSymptomsOrdered(final Symptom startSymptom, final Symptom endSymptom) {
        return this.startSymptom == startSymptom && this.endSymptom == endSymptom;
    }

    public boolean containsSymptomsUnordered(final Symptom symptom1, final Symptom symptom2) {
        return (startSymptom == symptom1 || startSymptom == symptom2) &&
               (endSymptom == symptom1 || endSymptom == symptom2);
    }

    public int getStartSymptomAttachedSide() {
        return startSymptomAttachedSide.get();
    }

    public void setStartSymptomAttachedSide(final int startSymptomAttachedSide) {
        this.startSymptomAttachedSide.set(startSymptomAttachedSide);
    }

    public IntegerProperty startSymptomAttachedSideProperty() {
        return startSymptomAttachedSide;
    }

    public int getEndSymptomAttachedSide() {
        return endSymptomAttachedSide.get();
    }

    public void setEndSymptomAttachedSide(final int endSymptomAttachedSide) {
        this.endSymptomAttachedSide.set(endSymptomAttachedSide);
    }

    @Override
    public String getTypeKey() {
        return "relationType";
    }

    public IntegerProperty endSymptomAttachedSideProperty() {
        return endSymptomAttachedSide;
    }

    public ObjectProperty<RelationStrength> relationStrengthProperty() {
        return relationStrength;
    }

    public RelationStrength getRelationStrength() {
        return relationStrength.get();
    }

    public void setRelationStrength(@NotNull final RelationStrength relationStrength) {
        this.relationStrength.set(relationStrength);
    }

    public ObjectProperty<RelationType> relationTypeProperty() {
        return relationType;
    }

    public RelationType getRelationType() {
        return relationType.get();
    }

    public void setRelationType(@NotNull final RelationType relationType) {
        this.relationType.set(relationType);
    }

    public ReadOnlyListProperty<Waypoint> waypointsProperty() { return waypoints; }

    public List<Waypoint> getWaypoints() { return waypoints.get(); }

    public Symptom getStartSymptom() {
        return startSymptom;
    }

    public void setStartSymptom(final Symptom startSymptom) { this.startSymptom = startSymptom; }

    public Symptom getEndSymptom() {
        return endSymptom;
    }

    public void setEndSymptom(final Symptom endSymptom) { this.endSymptom = endSymptom; }

    private synchronized void writeObject(ObjectOutputStream s) throws IOException {
        SerialEntityWriter.writeEntity(s, this);
    }

    private synchronized void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        init();
        SerialEntityReader.readEntity(s, this);
    }
}
