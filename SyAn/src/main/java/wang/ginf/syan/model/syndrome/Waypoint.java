package wang.ginf.syan.model.syndrome;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.SerialEntityReader;
import wang.ginf.syan.model.SerialEntityWriter;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Wegpunkt fuer das Rendering einer {@see Relation}.
 */
public class Waypoint implements Serializable {
    private transient DoubleProperty layoutX = new SimpleDoubleProperty();
    private transient DoubleProperty layoutY = new SimpleDoubleProperty();

    /**
     * Erstellt eine neue Waypoint Instanz mit den gegebenen Koordinaten.
     *
     * @param x die X-Koordinate des Wegpunkts.
     * @param y die Y-Koordinate des Wegpunkts.
     */
    public Waypoint(@NotNull final double x, @NotNull final double y) {
        setLayoutX(x);
        setLayoutY(y);
    }

    public void init() {
        layoutX = new SimpleDoubleProperty();
        layoutY = new SimpleDoubleProperty();
    }

    /**
     * Bewegt die Entitaet an die neuen Koordinaten.
     *
     * @param x die neue X-Koordinate.
     * @param y die neue Y-Koordinate.
     */
    public void move(@NotNull double x, @NotNull double y) {
        setLayoutX(x);
        setLayoutY(y);
    }

    public DoubleProperty layoutXProperty() { return layoutX; }

    public double getLayoutX() { return layoutX.get(); }

    public void setLayoutX(@NotNull double x) { layoutX.set(x); }

    public DoubleProperty layoutYProperty() { return layoutY; }

    public double getLayoutY() { return layoutY.get(); }

    public void setLayoutY(@NotNull double y) { layoutY.set(y); }

    private synchronized void writeObject(ObjectOutputStream s) throws IOException {
        SerialEntityWriter.writeWaypoint(s, this);
    }

    private synchronized void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        init();
        SerialEntityReader.readWaypoint(s, this);
    }
}
