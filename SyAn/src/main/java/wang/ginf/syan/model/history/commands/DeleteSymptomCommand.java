package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;
import wang.ginf.syan.userinterface.controls.AttributeEditor;

import java.util.ArrayList;

/**
 * Das Kommando, das eine Symptomloeschung ausfuehrt.
 *
 * @author Hannes Masuch
 */
public class DeleteSymptomCommand extends Command {
    private final Symptom symptom;
    private final Sphere sphere;
    private final Syndrome syndrome;
    private final ArrayList<Relation> relations = new ArrayList<>();

    /**
     * Erstellt eine neue Instanz dieses Commands mit dem gegebenen Zielsymptom und der Sphere in der das Symptom
     * enthalten ist.
     *
     * @param symptom das Symptom welches geloescht werden soll.
     * @param sphere  die Sphere in der sich das Symptom befindet.
     */
    public DeleteSymptomCommand(final Symptom symptom, final Sphere sphere, final Syndrome syndrome) {
        this.symptom = symptom;
        this.sphere = sphere;
        this.syndrome = syndrome;

        relations.addAll(symptom.getRelations());
        params = symptom.getName();
    }

    @Override
    protected boolean executeImpl() {
        if (getStatus() == CommandStatus.NEW) {
            symptom.getRelations().clear();
            relations.forEach(syndrome::removeRelation);
        }

        AttributeEditor.resetIf(entity -> entity.equals(symptom));
        return sphere.getSymptoms().remove(symptom);
    }

    @Override
    protected void undoImpl() {
        symptom.getRelations().addAll(relations);
        sphere.getSymptoms().add(symptom);
    }
}
