package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.model.syndrome.Sphere;

/**
 * Das Kommando, das eine Sphaerengroessenveraenderung ausfuehrt.
 *
 * @author Hannes Masuch
 * @author Ruben Smidt
 */
public class ResizeSphereCommand extends Command {
    private final Sphere sphere;
    private final double oldWidth;
    private final double oldHeight;
    private final double newWidth;
    private final double newHeight;

    /**
     * Erstellt eine neue Instanz dieses Commands mit der gegebenen Sphere und ihrer neuen Groesse.
     *
     * @param sphere    die zu veraendernde Sphere.
     * @param oldWidth  die alte Breite der Sphere.
     * @param oldHeight die alte Hoehe der Sphere.
     * @param newWidth  die neue Breite der Sphere.
     * @param newHeight die neue Hoehe der Sphere.
     */
    public ResizeSphereCommand(
            final Sphere sphere,
            final double oldWidth,
            final double oldHeight,
            final double newWidth,
            final double newHeight) {
        this.sphere = sphere;
        this.oldWidth = oldWidth;
        this.oldHeight = oldHeight;
        this.newWidth = newWidth;
        this.newHeight = newHeight;

        params = sphere.getName();
    }

    @Override
    protected boolean executeImpl() {
        sphere.resize(newWidth, newHeight);

        return true;
    }

    @Override
    protected void undoImpl() {
        sphere.resize(oldWidth, oldHeight);
    }
}
