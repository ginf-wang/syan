package wang.ginf.syan.model;

import java.util.Set;

/**
 * Der TemplateCreationResultBuilder nutzt das Builder-Pattern, um ein TemplateCreationResult zu bauen.
 *
 * @author Ruben Smidt
 */
public class TemplateCreationResultBuilder {
    private Set<String> uniqueSphereNames;
    private String name;
    private boolean creationOfUnknownRelationAllowed;
    private boolean creationOfStrengtheningRelationAllowed;
    private boolean creationOfWeakeningRelationAllowed;
    private boolean creationOfSymptomsAllowed;

    public TemplateCreationResultBuilder setUniqueSphereNames(final Set<String> uniqueSphereNames) {
        this.uniqueSphereNames = uniqueSphereNames;
        return this;
    }

    public TemplateCreationResultBuilder setName(final String name) {
        this.name = name;
        return this;
    }

    public TemplateCreationResultBuilder setCreationOfUnknownRelationAllowed(final boolean creationOfUnknownRelationAllowed) {
        this.creationOfUnknownRelationAllowed = creationOfUnknownRelationAllowed;
        return this;
    }

    public TemplateCreationResultBuilder setCreationOfStrengtheningRelationAllowed(final boolean creationOfStrengtheningRelationAllowed) {
        this.creationOfStrengtheningRelationAllowed = creationOfStrengtheningRelationAllowed;
        return this;
    }

    public TemplateCreationResultBuilder setCreationOfWeakeningRelationAllowed(final boolean creationOfWeakeningRelationAllowed) {
        this.creationOfWeakeningRelationAllowed = creationOfWeakeningRelationAllowed;
        return this;
    }

    public TemplateCreationResultBuilder setCreationOfSymptomsAllowed(final boolean creationOfSymptomsAllowed) {
        this.creationOfSymptomsAllowed = creationOfSymptomsAllowed;
        return this;
    }

    public TemplateCreationResult createTemplateCreationResult() {
        return new TemplateCreationResult(uniqueSphereNames,
                                          name,
                                          creationOfUnknownRelationAllowed,
                                          creationOfStrengtheningRelationAllowed,
                                          creationOfWeakeningRelationAllowed, creationOfSymptomsAllowed);
    }
}
