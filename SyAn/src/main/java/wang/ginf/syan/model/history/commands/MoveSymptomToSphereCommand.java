package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;

/**
 * Das Kommando, das eine Symptom zu Sphaerenbewgung ausfuehrt.
 *
 * @author Ruben Smidt
 */
public class MoveSymptomToSphereCommand extends Command {
    private final Symptom symptom;
    private final Sphere start;
    private final Sphere end;

    /**
     * Erstellt ein neues Kommando.
     *
     * @param symptom das zu bewegende Symptom.
     * @param start   die Startsphaere.
     * @param end     die Zielsphaere.
     */
    public MoveSymptomToSphereCommand(final Symptom symptom, final Sphere start, final Sphere end) {
        this.symptom = symptom;
        this.start = start;
        this.end = end;

        params = "(" + start.getName() + " -> " + end.getName() + ") " + symptom.getName();
    }

    @Override
    protected boolean executeImpl() {
        start.getSymptoms().remove(symptom);
        end.getSymptoms().add(symptom);

        return true;
    }

    @Override
    protected void undoImpl() {
        end.getSymptoms().remove(symptom);
        start.getSymptoms().add(symptom);
    }
}
