package wang.ginf.syan.model;

/**
 * Der StateBuilder nutzt das Builder-Pattern, um einen neuen Anwendungszustand zu bauen.
 *
 * @author Ruben Smidt
 */
public class StateBuilder {
    private boolean strengtheningRelationAllowed;
    private boolean unknownRelationAllowed;
    private boolean weakeningRelationAllowed;
    private boolean symptomCreationAllowed;
    private boolean template;

    public StateBuilder setStrengtheningRelationAllowed(final boolean strengtheningRelationAllowed) {
        this.strengtheningRelationAllowed = strengtheningRelationAllowed;
        return this;
    }

    public StateBuilder setUnknownRelationAllowed(final boolean unknownRelationAllowed) {
        this.unknownRelationAllowed = unknownRelationAllowed;
        return this;
    }

    public StateBuilder setWeakeningRelationAllowed(final boolean weakeningRelationAllowed) {
        this.weakeningRelationAllowed = weakeningRelationAllowed;
        return this;
    }

    public StateBuilder setSymptomCreationAllowed(final boolean symptomCreationAllowed) {
        this.symptomCreationAllowed = symptomCreationAllowed;
        return this;
    }

    public StateBuilder setTemplate(final boolean template) {
        this.template = template;
        return this;
    }

    public State createState() {
        return new State(strengtheningRelationAllowed,
                         unknownRelationAllowed,
                         weakeningRelationAllowed,
                         symptomCreationAllowed,
                         template);
    }
}
