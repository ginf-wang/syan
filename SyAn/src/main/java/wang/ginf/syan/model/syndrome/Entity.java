package wang.ginf.syan.model.syndrome;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Color;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.EntityBase;

/**
 * Abstrakte Basisklasse fuer {@Sphere}s und {@Symptom}s.
 *
 * @author Arne Kiesewetter
 */
public abstract class Entity extends EntityBase {
    private transient DoubleProperty layoutX = new SimpleDoubleProperty();
    private transient DoubleProperty layoutY = new SimpleDoubleProperty();
    private transient DoubleProperty height = new SimpleDoubleProperty();
    private transient DoubleProperty width = new SimpleDoubleProperty();

    /**
     * Erstellt eine Entitaet mit den gegebenen Parametern.
     *
     * @param color  die Farbe der Entitaet.
     * @param x      die X-Koordinate der Entitaet.
     * @param y      die Y-Koordinate der Entitaet.
     * @param width  die Breite der Entitaet.
     * @param height die Hoehe der Entitaet.
     */
    protected Entity(@NotNull Color color,
                     @NotNull double x,
                     @NotNull double y,
                     @NotNull double width,
                     @NotNull double height) {
        setColor(color);
        setLayoutX(x);
        setLayoutY(y);
        setWidth(width);
        setHeight(height);
    }

    /**
     * Initialisiert die Felder der Klasse nach vor einer Deserialisiernung
     */
    public void init() {
        layoutX = new SimpleDoubleProperty();
        layoutY = new SimpleDoubleProperty();
        width = new SimpleDoubleProperty();
        height = new SimpleDoubleProperty();
    }

    public void init(@NotNull double x,
                     @NotNull double y,
                     @NotNull double width,
                     @NotNull double height) {
        layoutX = new SimpleDoubleProperty(x);
        layoutY = new SimpleDoubleProperty(y);
        this.width = new SimpleDoubleProperty(width);
        this.height = new SimpleDoubleProperty(height);
    }

    /**
     * Bewegt die Entitaet an die neuen Koordinaten.
     *
     * @param x die neue X-Koordinate.
     * @param y die neue Y-Koordinate.
     */
    public void move(@NotNull double x, @NotNull double y) {
        setLayoutX(x);
        setLayoutY(y);
    }

    /**
     * Passt die Groeße der Entitaet den neuen Werten an.
     *
     * @param w die neue Breite.
     * @param h die neue Hoehe.
     */
    public void resize(@NotNull double w, @NotNull double h) {
        setWidth(w);
        setHeight(h);
    }

    public DoubleProperty layoutXProperty() { return layoutX; }

    public double getLayoutX() { return layoutX.get(); }

    public void setLayoutX(@NotNull double x) { layoutX.set(x); }

    public DoubleProperty layoutYProperty() { return layoutY; }

    public double getLayoutY() { return layoutY.get(); }

    public void setLayoutY(@NotNull double y) { layoutY.set(y); }

    public DoubleProperty widthProperty() { return width; }

    public double getWidth() { return width.get(); }

    public void setWidth(@NotNull double w) { width.set(w); }

    public DoubleProperty heightProperty() { return height; }

    public double getHeight() { return height.get(); }

    public void setHeight(@NotNull double h) { height.set(h); }
}
