package wang.ginf.syan.model.syndrome;

import wang.ginf.syan.NotNull;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Beinhaltet die verschiedenen Moeglichkeiten für die Staerke einer Wirkungsbeziehung.
 *
 * @author Arne Kiesewetter
 */
public enum RelationStrength {
    UNKNOWN("unknown", 0.5),
    WEAK("weak", 1),
    WEAK_MEDIUM("weakMedium", 1.5),
    MEDIUM("medium", 2),
    STRONG_MEDIUM("strongMedium", 2.5),
    STRONG("strong", 3);

    private final double width;
    private final String key;

    RelationStrength(@NotNull final String key, @NotNull final double width) {
        this.key = key;
        this.width = width;
    }

    public double getWidth() { return width; }

    @Override
    public String toString() {
        final var localizedName = ResourceBundle.getBundle("lang/RelationStrength", Locale.getDefault())
                                                .getString(key);

        return String.format("(%s): %s", width, localizedName);
    }
}
