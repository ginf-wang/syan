package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.Syndrome;

/**
 * Das Kommando, das eine Relationserstellung ausfuehrt.
 *
 * @author Hannes Masuch
 */
public class AddRelationCommand extends Command {
    private final Relation relation;
    private final Syndrome syndrome;

    /**
     * Erstellt eine neue Instanz dieses Commands mit der gegebenen Zielrelation und den gegebenen Start-/Endsymptomen.
     *
     * @param relation die Relation, die hinzugefügt wird.
     * @param syndrome das Syndrom der Relation.
     */
    public AddRelationCommand(final Relation relation, final Syndrome syndrome) {
        final var start = relation.getStartSymptom();
        final var end = relation.getEndSymptom();

        this.relation = relation;
        this.syndrome = syndrome;

        params = start.getName() + " ==> " + end.getName() + ", " + relation.getRelationType().toString();
    }

    @Override
    protected boolean executeImpl() {
        if (syndrome.containsRelation(relation)) {
            return false;
        }

        relation.addToSymptoms();
        syndrome.addRelation(relation);

        return true;
    }

    @Override
    protected void undoImpl() {
        relation.removeFromSymptoms();
        syndrome.removeRelation(relation);
    }
}
