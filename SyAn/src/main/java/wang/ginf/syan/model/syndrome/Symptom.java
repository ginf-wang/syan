package wang.ginf.syan.model.syndrome;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.scene.paint.Color;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.Nullable;
import wang.ginf.syan.model.SerialEntityReader;
import wang.ginf.syan.model.SerialEntityWriter;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Repraesentiert ein Symptom, welches Teil einer {@see Sphere} ist und {@see Relation}s hat.
 *
 * @author Arne Kiesewetter
 */
public class Symptom extends Entity {
    private transient StringProperty name = new SimpleStringProperty();
    private transient SetProperty<Relation> relations = new SimpleSetProperty<>(FXCollections.observableSet());

    /**
     * Erstellt ein neues Symptom mit dem gegebenen Namen,
     * der gegebenen Hintergrundfarbe und optional den bestehenden Relationen.
     *
     * @param color     die Hintergrundfarbe des Symptoms.
     * @param relations die optionalen bestehenden Relationen des Symptoms.
     */
    public Symptom(@NotNull final String name,
                   @NotNull final Color color,
                   @Nullable final Collection<Relation> relations) {
        super(color, 0, 0, 1, 1);

        setName(name);

        if (Objects.nonNull(relations)) {
            this.relations.addAll(relations);
        }
    }

    /**
     * Erstellt ein neues Symptom mit dem gegebenen Namen,
     * der gegebenen Hintergrundfarbe, ohne Relationen.
     *
     * @param name  der Name des Symptoms.
     * @param color die Hintergrundfarbe des Symptoms.
     */
    public Symptom(@NotNull final String name, @NotNull final Color color) {
        this(name, color, Collections.emptySet());
    }

    /**
     * Weist dem Symptom initiale Properties zu
     */
    public void init() {
        name = new SimpleStringProperty();
        relations = new SimpleSetProperty<>(FXCollections.observableSet());
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getName() {
        return name.get();
    }

    public void setName(@NotNull String name) {
        this.name.set(name);
    }

    public ReadOnlySetProperty<Relation> relationProperty() {
        return relations;
    }

    public ObservableSet<Relation> getRelations() {
        return relations.get();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (Objects.isNull(o) || getClass() != o.getClass()) {
            return false;
        }
        final var symptom = (Symptom) o;
        return getName().equalsIgnoreCase(symptom.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName().toLowerCase());
    }

    @Override
    public String getTypeKey() {
        return "symptomType";
    }

    private boolean isValidRelationStart(final int startSide) {
        return relations.stream()
                        .filter(relation -> relation.getEndSymptom().equals(this))
                        .noneMatch(relation -> relation.getEndSymptomAttachedSide() == startSide);
    }

    private boolean isValidRelationEnd(final int endSide, final RelationType relationType) {
        final var endIsNotUsedAsStart =
                relations.stream()
                         .filter(relation -> relation.getStartSymptom().equals(this))
                         .noneMatch(relation -> relation.getStartSymptomAttachedSide() == endSide);

        final var endIsNotUsedByAnotherType =
                relations.stream()
                         .filter(relation -> relation.getEndSymptom().equals(this))
                         .noneMatch(relation -> relation.getEndSymptomAttachedSide() == endSide &&
                                                relationType != relation.getRelationType());

        return endIsNotUsedAsStart && endIsNotUsedByAnotherType;
    }

    public int getFreeStartIfOccupied(final int wantedStart) {
        if (isValidRelationStart(wantedStart)) {
            return wantedStart;
        }

        final var getAnotherStartZone =
                relations.stream()
                         .filter(relation -> relation.getStartSymptom().equals(this))
                         .collect(Collectors.groupingBy(Relation::getStartSymptomAttachedSide, Collectors.counting()));

        // Use already existing zone
        if (!getAnotherStartZone.isEmpty()) {
            final var lowestPair =
                    Collections.min(getAnotherStartZone.entrySet(), Comparator.comparing(Map.Entry::getValue));

            return lowestPair.getKey();
        }

        // Use free zone
        return getFreeZone();
    }

    public int getFreeEndIfOccupied(final int wantedEnd, final RelationType relationType) {
        if (isValidRelationEnd(wantedEnd, relationType)) {
            return wantedEnd;
        }

        final var getAnotherEndZone =
                relations.stream()
                         .filter(relation -> relation.getEndSymptom().equals(this) &&
                                             relation.getRelationType() == relationType)
                         .collect(Collectors.groupingBy(Relation::getEndSymptomAttachedSide, Collectors.counting()));

        // Use already existing zone
        if (!getAnotherEndZone.isEmpty()) {
            final var lowestPair =
                    Collections.min(getAnotherEndZone.entrySet(), Comparator.comparing(Map.Entry::getValue));

            return lowestPair.getKey();
        }

        // Use free zone
        return getFreeZone();
    }

    private int getFreeZone() {
        final var zonesUsedAsStart = relations.stream()
                                              .filter(relation -> relation.getStartSymptom().equals(this))
                                              .map(Relation::getStartSymptomAttachedSide);

        final var zonesUsedAsEnd = relations.stream()
                                            .filter(relation -> relation.getEndSymptom().equals(this))
                                            .map(Relation::getEndSymptomAttachedSide);

        final var usedZones = Stream.concat(zonesUsedAsStart, zonesUsedAsEnd)
                                    .collect(Collectors.toSet());

        final var freeZones = IntStream.range(0, Relation.ZONE_COUNT)
                                       .boxed()
                                       .collect(Collectors.toSet());

        freeZones.removeAll(usedZones);

        return freeZones.stream()
                        .findFirst()
                        .orElse(-1);
    }

    public boolean isOnlyEndingRelationAtZone(final Relation relationToTest) {
        return relations.stream()
                        .filter(relation -> !relation.equals(relationToTest))
                        .noneMatch(relation -> relation.getEndSymptom().equals(this) &&
                                               relation.getEndSymptomAttachedSide() ==
                                               relationToTest.getEndSymptomAttachedSide());
    }

    private synchronized void writeObject(ObjectOutputStream s) throws IOException {
        SerialEntityWriter.writeEntity(s, this);
    }

    private synchronized void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        init();
        SerialEntityReader.readEntity(s, this);
    }
}
