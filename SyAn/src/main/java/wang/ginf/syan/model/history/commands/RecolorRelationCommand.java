package wang.ginf.syan.model.history.commands;

import javafx.scene.paint.Color;
import wang.ginf.syan.model.syndrome.Relation;

/**
 * Das Kommando, das eine Relationsumfaerbung ausfuehrt.
 *
 * @author Hannes Masuch
 */
public class RecolorRelationCommand extends Command {
    private final Relation relation;
    private Color color;

    /**
     * Erstellt eine neue Instanz dieses Commands mit der gegebenen Zielrelation und der gegebenen neuen Farbe.
     *
     * @param relation die Relation die veraendert werden soll.
     * @param color    die neue Farbe fuer die Relation.
     */
    public RecolorRelationCommand(final Relation relation, final Color color) {
        this.relation = relation;
        this.color = color;

        params = String.format(
                "(%s ==> %s) %s -> %s",
                relation.getStartSymptom().getName(),
                relation.getEndSymptom().getName(),
                relation.getColor().toString(),
                color.toString()
        );
    }

    @Override
    protected boolean executeImpl() {
        final var oldColor = relation.getColor();

        relation.setColor(color);
        color = oldColor;

        return true;
    }

    @Override
    protected void undoImpl() {
        executeImpl();
    }
}
