package wang.ginf.syan.model;

import com.sun.javafx.collections.ObservableSetWrapper;
import javafx.collections.ObservableSet;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;

/**
 * Eine Utility-Klasse, die individuelle Datenstrukturen zur Verfuegung stellt.
 *
 * @author Ruben Smidt
 */
public class SyanCollections {
    private SyanCollections() {}

    /**
     * Erstellt ein {@see javafx.collections.ObservableSet}, dem aber ein
     * LinkedHashSet zugrunde liegt.
     *
     * @param elements die initialen Elemente des Sets.
     * @param <E>      der Typ der Elemente des Sets.
     * @return das ObservableSet.
     */
    public static <E> ObservableSet<E> observableLinkedSet(E... elements) {
        Objects.requireNonNull(elements);

        final var set = new LinkedHashSet<>(Arrays.asList(elements));
        return new ObservableSetWrapper<>(set);
    }
}
