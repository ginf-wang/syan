package wang.ginf.syan.model;

import javafx.scene.paint.Color;
import wang.ginf.syan.model.syndrome.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * Utility, welche das Deserialisieren von Graphentitaeten vereinheitlicht
 * <p>
 * Code in Teilen angelehnt an:
 * https://gist.github.com/james-d/a7202039b00170256293
 * https://stackoverflow.com/questions/18791566/notserializableexception-on-simplelistproperty
 * https://stackoverflow.com/questions/34794995/how-to-serialize-observablelist
 *
 * @author Jan-Luca Kiok
 */
public class SerialEntityReader {

    /**
     * Deserialisiert ein Syndrom, liest dabei zu serialisierbaren Klassen konvertierte Werte aus und setzt sie
     * passend in korrekte Attribute
     *
     * @param s        Der InputStream, welcher die Daten bereitstellt
     * @param syndrome Das auszulesende Syndrom
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void readSyndrome(final ObjectInputStream s, final Syndrome syndrome)
            throws IOException, ClassNotFoundException {
        syndrome.setToolboxSphere((Sphere) s.readObject());

        final var sphereCount = s.readInt();
        final var spheres = new ArrayList<Sphere>(sphereCount);

        for (var i = 0; i < sphereCount; i++) {
            spheres.add((Sphere) s.readObject());
        }

        syndrome.spheresProperty().addAll(spheres);

        final var relationCount = s.readInt();
        final var relations = new ArrayList<Relation>(relationCount);

        for (var i = 0; i < relationCount; i++) {
            relations.add((Relation) s.readObject());
        }
        syndrome.setRelations(relations);
    }

    /**
     * Deserialisiert Werte der abstrakten Basisentitaet
     *
     * @param s          Der InputStream, welcher die Daten bereitstellt
     * @param entityBase Die Basisentitaet
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private static void readEntityBase(final ObjectInputStream s, final EntityBase entityBase)
            throws IOException, ClassNotFoundException {
        entityBase.init(new Color(s.readDouble(), s.readDouble(), s.readDouble(), s.readDouble()));
        entityBase.setChangeable(s.readBoolean());
    }

    /**
     * Deserialisiert Werte der abstrakten Entitaet
     *
     * @param s      Der InputStream, welcher die Daten bereitstellt
     * @param entity Die abstrakte Entitaet
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private static void readAbEntity(final ObjectInputStream s, final Entity entity)
            throws IOException, ClassNotFoundException {
        readEntityBase(s, entity);
        entity.init(s.readDouble(), s.readDouble(), s.readDouble(), s.readDouble());
    }

    /**
     * Deserialisiert eine Sphaere
     *
     * @param s      Der InputStream, welcher die Daten bereitstellt
     * @param sphere Die auszulesende Sphaere
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void readEntity(ObjectInputStream s, Sphere sphere) throws IOException, ClassNotFoundException {
        readAbEntity(s, sphere);
        sphere.setName(s.readUTF());

        final var symptomCount = s.readInt();
        final var symptoms = new ArrayList<Symptom>(symptomCount);

        for (var i = 0; i < symptomCount; i++) {
            symptoms.add((Symptom) s.readObject());
        }

        sphere.getSymptoms().addAll(symptoms);
    }

    /**
     * Deserialisiert ein Symptom
     *
     * @param s       Der InputStream, welcher die Daten bereitstellt
     * @param symptom Das auszulesende Symptom
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void readEntity(final ObjectInputStream s, final Symptom symptom)
            throws IOException, ClassNotFoundException {
        readAbEntity(s, symptom);
        symptom.setName(s.readUTF());

        final var relationCount = s.readInt();
        final var relations = new ArrayList<Relation>(relationCount);

        for (var i = 0; i < relationCount; i++) {
            relations.add((Relation) s.readObject());
        }

        symptom.getRelations().addAll(relations);
    }

    /**
     * Deserialisiert eine Relation
     *
     * @param s        Der InputStream, welcher die Daten bereitstellt
     * @param relation Die auszulesende Relation
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void readEntity(final ObjectInputStream s, final Relation relation)
            throws IOException, ClassNotFoundException {
        readEntityBase(s, relation);

        relation.setRelationStrength((RelationStrength) s.readObject());
        relation.setRelationType((RelationType) s.readObject());
        relation.setStartSymptom((Symptom) s.readObject());
        relation.setEndSymptom((Symptom) s.readObject());

        final var waypointCount = s.readInt();
        final var waypoints = new ArrayList<Waypoint>(waypointCount);

        for (var i = 0; i < waypointCount; i++) {
            waypoints.add((Waypoint) s.readObject());
        }

        relation.waypointsProperty().addAll(waypoints);
        relation.setStartSymptomAttachedSide(s.readInt());
        relation.setEndSymptomAttachedSide(s.readInt());
    }

    /**
     * Deserialisiert einen Wegpunkt
     *
     * @param s        Der InputStream, welcher die Daten bereitstellt
     * @param waypoint Der auszulesende Wegpunkt
     * @throws IOException
     */
    public static void readWaypoint(final ObjectInputStream s, final Waypoint waypoint) throws IOException {
        waypoint.setLayoutX(s.readDouble());
        waypoint.setLayoutY(s.readDouble());
    }
}
