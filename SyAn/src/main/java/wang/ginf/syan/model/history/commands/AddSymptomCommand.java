package wang.ginf.syan.model.history.commands;

import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;
import wang.ginf.syan.userinterface.controls.AttributeEditor;

/**
 * Das Kommando, das eine Symptomerstellung ausfuehrt.
 *
 * @author Hannes Masuch
 */
public class AddSymptomCommand extends Command {
    private final Symptom symptom;
    private final Sphere sphere;
    private final Syndrome syndrome;

    /**
     * Erstellt eine neue Instanz dieses Commands mit dem gegebenen Symptom,Syndrom und Sphere.
     *
     * @param symptom  das Symptom welches hinzugefügt wird.
     * @param sphere   die Sphere in der das Symptom hinzugefügt wird.
     * @param syndrome das Syndrom des Symptoms.
     */
    public AddSymptomCommand(Symptom symptom, Sphere sphere, Syndrome syndrome) {
        this.symptom = symptom;
        this.sphere = sphere;
        this.syndrome = syndrome;

        params = "(" + sphere.getName() + ") " + symptom.getName();
    }

    @Override
    protected boolean executeImpl() {
        if (syndrome.containsSymptom(symptom.getName())) {
            return false;
        }

        return sphere.getSymptoms().add(symptom);
    }

    @Override
    protected void undoImpl() {
        AttributeEditor.resetIf(entity -> entity.equals(symptom));
        sphere.getSymptoms().remove(symptom);
    }
}
