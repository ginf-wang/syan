package wang.ginf.syan.model.history.commands;

import java.sql.Timestamp;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Kommandotransferklasse zwischen verschiendenen Sitzungen, vereinheitlicht alle konkreten Kommandos zur einfachen
 * Speicherung in der Datenbank
 *
 * @author Jan-Luca Kiok
 */
public class LegacyCommand extends Command {

    /**
     * Initalisiert ein neues Kommando mit den zur Persistierung notwendigen Werten
     *
     * @param id            Der Identifier der ausloesenden Klasse als Nummer
     * @param concreteClass Die Klasse des ausloesenden Kommandos
     * @param status        Der Status des ausloesenden Kommandos
     * @param timestamp     Der Zeitpunkt des ausloesenden Kommandos
     * @param params        Die an der Ausfuehrung des ausloesenden Kommandos beteiligten Entitaeten als
     *                      verstaendlich lesbarer String
     */
    public LegacyCommand(final int id,
                         final String concreteClass,
                         final CommandStatus status,
                         final Timestamp timestamp,
                         final String params) {
        setId(id);
        setConcreteClass(concreteClass);
        setStatus(status);
        setTimestamp(timestamp);
        setParams(params);
        if (getStatus() == CommandStatus.UNDONE ||
            concreteClass.equals(ResourceBundle.getBundle("lang/Commands", Locale.getDefault()
            ).getString(UndoCommand.class.getSimpleName())) ||
            concreteClass.equals(ResourceBundle.getBundle("lang/Commands", Locale.getDefault())
                                               .getString(RedoCommand.class.getSimpleName()))) {
            setNonCanonical(true);
        }
    }

    @Override
    protected boolean executeImpl() { return false; }

    @Override
    protected void undoImpl() {}

    @Override
    public String toString() {
        return getConcreteClass();
    }
}
