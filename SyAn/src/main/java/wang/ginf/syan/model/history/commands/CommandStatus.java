package wang.ginf.syan.model.history.commands;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import static java.util.Map.entry;

/**
 * Beinhaltet die moeglichen Zustaende eines Kommandos.
 *
 * @author Ruben Smidt
 * @author Jan-Luca Kiok
 */
public enum CommandStatus {
    NEW,
    EXECUTED,
    UNDONE;

    private static Map<String, CommandStatus> values = Map.ofEntries(
            entry("Neu", NEW),
            entry("New", NEW),
            entry("Ausgef\u00FChrt", EXECUTED),
            entry("Executed", EXECUTED),
            entry("R\u00FCckg\u00E4ngig gemacht", UNDONE),
            entry("Undone", UNDONE)
    );

    public static CommandStatus getTypeOf(String name) {
        return values.get(name);
    }

    @Override
    public String toString() {
        return toString(Locale.getDefault());
    }

    public String toString(final Locale locale) {
        return ResourceBundle.getBundle("lang/CommandStatus", locale).getString(name());
    }
}
