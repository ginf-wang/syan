package wang.ginf.syan;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.hibernate.SessionFactory;
import wang.ginf.syan.persistence.CommandsDatabase;
import wang.ginf.syan.persistence.LanguageService;
import wang.ginf.syan.userinterface.windows.SyndromeEditorWindow;

import java.util.Locale;

/**
 * Die JavaFX integrierte Hauptklasse.
 * <p>
 * Sie startet die Anwendung und die damit verbundenen Abhaengigkeiten
 * wie beispielsweise Hibernate.
 *
 * @author Arne Kiesewetter
 * @author Jan-Luca Kiok
 * @author Ruben Smidt
 */
public class SyAnApplicationFX extends Application {
    private final LanguageService languageService = LanguageService.getInstance();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        final SessionFactory sessionFactory = CommandsDatabase.getSessionFactory();

        // Wir versuchen eine moegliche abgespeicherte Sprache zu lesen.
        final var chosenLocale = languageService.getLocale()
                                                .orElse(Locale.getDefault());

        Thread.currentThread().setUncaughtExceptionHandler((thread, throwable) -> {
            if (!(throwable instanceof ArrayIndexOutOfBoundsException)) {
                throwable.printStackTrace();
            }
        });

        Locale.setDefault(chosenLocale);

        final var syndromeEditorWindow = new SyndromeEditorWindow(sessionFactory);

        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/icons/syan.png")));
        primaryStage.titleProperty().bind(syndromeEditorWindow.titleProperty());
        primaryStage.setScene(new Scene(syndromeEditorWindow));
        primaryStage.setMaximized(true);
        primaryStage.show();

        primaryStage.setOnCloseRequest(event -> {
            if (!syndromeEditorWindow.close()) {
                event.consume();
            }
        });
    }

    @Override
    public void stop() {
        CommandsDatabase.shutdown();
    }
}
