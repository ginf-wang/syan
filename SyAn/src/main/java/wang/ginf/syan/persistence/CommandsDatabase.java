package wang.ginf.syan.persistence;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.type.IntegerType;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.history.CommandHistory;
import wang.ginf.syan.model.history.commands.Command;
import wang.ginf.syan.model.history.commands.CommandStatus;
import wang.ginf.syan.model.history.commands.LegacyCommand;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Realisiert die Verbindung zwischen der Datenbank und der {@see CommandHistory}.
 *
 * @author Jan-Luca Kiok
 * @author Ruben Smidt
 */
public class CommandsDatabase {
    private static final Logger logger = Logger.getLogger(CommandsDatabase.class.getName());
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static CommandsDatabase instance;

    public static CommandsDatabase getInstance() {
        if (Objects.isNull(instance)) {
            instance = new CommandsDatabase();
        }

        return instance;
    }

    public static CommandHistory createHistoryWithDatabase() {
        return CommandHistory.getInstance(instance);
    }

    /**
     * Versucht, eine Hibernatesessionfactory zu instanziieren
     *
     * @throws ExceptionInInitializerError falls die SessionFactory-Creation nicht erfolgreich
     *                                     abgeschlossen werden konnte und loggt diese
     */
    private static SessionFactory buildSessionFactory() {
        try {
            final var resourceURL = CommandsDatabase.class.getResource("/hibernate/hibernate.cfg.xml");
            return new Configuration().configure(resourceURL).buildSessionFactory();
        } catch (final HibernateException e) {
            logger.info("SessionFactory creation failed." + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Schliesst die offene SessionFactory
     */
    public static void shutdown() {
        getSessionFactory().close();
    }

    /**
     * Gibt die von Hibernate vergebene unique ID eines Kommandos zurueck
     *
     * @param cmd Das Kommando
     * @return Die ID
     */
    public static int getCommandId(@NotNull final Command cmd) {
        final var session = sessionFactory.openSession();
        final var transaction = session.beginTransaction();

        final int id = (int) session.createSQLQuery("SELECT ID FROM COMMANDS WHERE TIMESTAMP=(?)")
                                    .addScalar("ID", new IntegerType())
                                    .setParameter(1, cmd.getTimestamp()).uniqueResult();

        transaction.commit();
        session.close();
        return id;
    }

    /**
     * Fuegt das gegebene Kommando in der Datenbank ein.
     *
     * @param command das einzufuegende Kommando.
     */
    public void add(@NotNull final Command command) {
        final var session = sessionFactory.openSession();
        final var transaction = session.beginTransaction();

        session.createSQLQuery("INSERT INTO COMMANDS (COMMAND, STATUS, TIMESTAMP, ENTITIES) VALUES (?, ?, ?, ?)")
               .setParameter(1, command.getConcreteClass())
               .setParameter(2, command.getStatus().toString())
               .setParameter(3, command.getTimestamp())
               .setParameter(4, command.getParams())
               .executeUpdate();

        final var result = (BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()")
                                               .uniqueResult();

        command.setId(result.intValue());

        transaction.commit();
        session.close();
    }

    /**
     * Laed alle Kommandos aus der Datenbank.
     *
     * @return eine Liste aller Kommandos.
     */
    public List<LegacyCommand> getAll() {
        final var session = sessionFactory.openSession();
        final var transaction = session.beginTransaction();

        @SuppressWarnings("unchecked") final var stream = (Stream<Object[]>) session.createSQLQuery(
                "SELECT c.ID, c.COMMAND, c.STATUS, c.TIMESTAMP, c.ENTITIES FROM COMMANDS as c")
                                                                                    .stream();

        final var commands = stream.map(o -> {
            final var id = (int) o[0];
            final var concreteClass = (String) o[1];
            final var status = (String) o[2];
            final var timestamp = (Timestamp) o[3];
            final var params = (String) o[4];

            return new LegacyCommand(id, concreteClass, CommandStatus.getTypeOf(status), timestamp, params);
        }).collect(Collectors.toList());

        stream.close();
        transaction.commit();
        session.close();

        return commands;
    }

    /**
     * Ueberschreibt die Datenbank mit einem neuen Satz Kommandos.
     *
     * @param commands die neuen Kommandos.
     */
    public void resetWith(@NotNull final List<? extends Command> commands) {
        final var session = sessionFactory.openSession();
        final var transaction = session.beginTransaction();

        session.createSQLQuery("TRUNCATE TABLE COMMANDS")
               .executeUpdate();

        transaction.commit();
        session.close();

        commands.forEach(this::add);
    }

    /**
     * Updated den Status eines Kommandos
     *
     * @param cmd Das zu updatende Kommando
     */
    public void setCommandStatus(@NotNull final Command cmd) {
        final var session = sessionFactory.openSession();
        final var transaction = session.beginTransaction();

        session.createSQLQuery("UPDATE COMMANDS SET STATUS=(?) WHERE ID=(?)")
               .setParameter(1, cmd.getStatus().toString())
               .setParameter(2, cmd.getId())
               .executeUpdate();

        transaction.commit();
        session.close();
    }
}
