package wang.ginf.syan.persistence;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.history.commands.Command;
import wang.ginf.syan.model.history.commands.CommandStatus;
import wang.ginf.syan.model.history.commands.LegacyCommand;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * Ein Exporter bietet die Moeglichkeit, Daten in verschiedene Dateiformate zu exportieren.
 *
 * @author Jan-Luca Kiok
 * @author Ruben Smidt
 * @author Ibrahim Apachi
 * @author Arne Kiesewetter
 */
public final class ExportService {
    private static final ResourceBundle bundle =
            ResourceBundle.getBundle("lang/ExportService", Locale.getDefault());

    private static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL
            .withHeader(bundle.getString("ID"),
                        bundle.getString("COMMAND"),
                        bundle.getString("STATUS"),
                        bundle.getString("TIMESTAMP"),
                        bundle.getString("ENTITIES"))
            .withDelimiter(',');

    private ExportService() {}

    /**
     * Gibt die Dateiendungen fuer die unterstuetzten Bildformate zurueck.
     *
     * @return die Dateiendungen der unterstuetzten Bildformate.
     */
    public static String[] getImageFormatSuffixes() {
        return ImageIO.getWriterFileSuffixes();
    }

    /**
     * Gibt die informellen Namen der unterstuetzten Bildformate zurueck.
     *
     * @return die informellen Namen der untersteutzten Bildformate.
     */
    public static String[] getImageFormatNames() {
        return Arrays.stream(getImageFormatSuffixes())
                     .map(suffix -> ImageIO.getImageWritersBySuffix(suffix)
                                           .next()
                                           .getOriginatingProvider()
                                           .getFormatNames()[0])
                     .toArray(String[]::new);
    }

    /**
     * Speichert den uebergebenen Snapshot als Bild am gegebenen {@see Path}.
     *
     * @param image der zu speichernde Snapshot.
     * @param file  der Speicherort inkl. Dateinamen und Endung.
     * @return ob das Exportieren erfolgreich war.
     */
    public static boolean exportAsImage(@NotNull final Image image, @NotNull final File file) {
        try {
            final var path = file.getAbsolutePath();
            final var index = path.lastIndexOf('.');
            final var formatName = ImageIO.getImageWritersBySuffix(path.substring(index + 1))
                                          .next()
                                          .getOriginatingProvider()
                                          .getFormatNames()[0];
            return ImageIO.write(SwingFXUtils.fromFXImage(image, null), formatName, file);
        } catch (final IOException ex) {
            return false;
        }
    }

    /**
     * Speichert den uebergebenen Snapshot als PDF.
     *
     * @param image der zu speichernde Snapshot.
     * @param file  der Speicherort inkl. Dateinamen und Endung.
     * @return ob das Exportieren erfolgreich war.
     */
    public static boolean exportAsPdf(@NotNull final Image image, @NotNull final File file) {
        final var width = (float) image.getWidth();
        final var height = (float) image.getHeight();
        final var imageRect = new PDRectangle(width, height);

        try (final var document = new PDDocument()) {
            // JavaFX Image to byte[]
            // https://stackoverflow.com/questions/24038524/how-to-get-byte-from-javafx-imageview
            final var page = new PDPage(imageRect);
            final var imageType = "png";
            final var bufferedImage = SwingFXUtils.fromFXImage(image, null);
            final var byteArrayOutputStream = new ByteArrayOutputStream();

            ImageIO.write(bufferedImage, imageType, byteArrayOutputStream);

            final var imageBytesArray = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();

            final var pdImageXObject = PDImageXObject.createFromByteArray(document, imageBytesArray, imageType);

            try (final var pdPageContentStream = new PDPageContentStream(document, page)) {
                pdPageContentStream.drawImage(pdImageXObject, 0, 0, width, height);
            }

            document.addPage(page);
            document.save(file.getAbsolutePath());
        } catch (final IOException e) {
            return false;
        }
        return true;
    }

    /**
     * Skaliert das gegebene Bild basierend auf der uebergebenen Breite,
     * wobei das bereits vorhandene Seitenverhaeltnis beibehalten wird.
     *
     * @param beforeBufferedImage das urspruengliche Bild
     * @param afterWidth          die Zielbreite des Bilds
     * @return gibt das neu skalierte Bild wieder.
     */
    private static BufferedImage resizeBufferedImage(final BufferedImage beforeBufferedImage, final double afterWidth) {
        final var beforeWidth = beforeBufferedImage.getWidth();
        final var beforeHeight = beforeBufferedImage.getHeight();
        final var scaleBy = afterWidth / beforeWidth;

        final var afterBufferedImage = new BufferedImage(beforeWidth, beforeHeight, BufferedImage.TYPE_INT_ARGB);
        final var affineTransform = new AffineTransform();
        affineTransform.scale(scaleBy, scaleBy);

        final var affineTransformOp = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BILINEAR);

        return affineTransformOp.filter(beforeBufferedImage, afterBufferedImage);
    }

    /**
     * Gibt das gegebene Image als PDPage fuer eine PDF aus.
     *
     * @param image    Das zu verarbeitende Bild
     * @param document Das Dokument, zu welchem die Seite gehoeren soll
     * @return die fertige PDF-Seite
     * <p>
     * JavaFX Image to byte[] entnommen von:
     * https://stackoverflow.com/questions/24038524/how-to-get-byte-from-javafx-imageview
     */
    private static PDPage imageToPage(final Image image, final PDDocument document) throws IOException {
        final var page = new PDPage();
        final var maxSize = 595;
        final var beforeWidth = image.getWidth();

        final var imageType = "png";
        var bufferedImage = SwingFXUtils.fromFXImage(image, null);

        if (beforeWidth > maxSize) {
            bufferedImage = resizeBufferedImage(bufferedImage, maxSize);
        }

        final var byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, imageType, byteArrayOutputStream);

        final var imageBytesArray = byteArrayOutputStream.toByteArray();

        final var pdImageXObject = PDImageXObject.createFromByteArray(document, imageBytesArray, imageType);

        try (final var pdPageContentStream = new PDPageContentStream(document, page)) {
            pdPageContentStream.drawImage(pdImageXObject, 0, 0);
        }

        return page;
    }

    /**
     * Speichert das übergebene Bild temporär und sendet eine Druckanfrage dafür an das System.
     *
     * @param image Das zu druckende Bild, der Canvas
     * @return ob die Druckanfrage erfolgreich war
     */
    public static boolean print(final Image image) {
        File file;

        try {
            file = File.createTempFile("printjob", ".png");
            if (!exportAsImage(image, file)) {
                return false;
            }
        } catch (Exception ioe) {
            return false;
        }

        try {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.PRINT)) {
                Desktop.getDesktop().print(file);
                return true;
            }
        } catch (final Exception e) { }

        try (final var document = new PDDocument()) {
            final var page = imageToPage(image, document);
            document.addPage(page);
            file = File.createTempFile("printjob", ".pdf");
            document.save(file);
        } catch (Exception ioe) {
            return false;
        }

        try {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.PRINT)) {
                Desktop.getDesktop().print(file);
                return true;
            }
        } catch (final Exception e) { }

        return false;
    }

    /**
     * Erzeugt eine CSV-Ausgabe von Kommandoattributen auf einer gegebenen Datei
     *
     * @param commands Die zu schreibenden Kommandos
     * @param out      Die zu beschreibene Datei
     * @throws IOException
     */
    public static void writeCommandsToCSV(final List<? extends Command> commands, final File out) throws IOException {
        try (final var writer = new CSVPrinter(new OutputStreamWriter(new FileOutputStream(out, false)), CSV_FORMAT)) {
            for (final var command : commands) {
                writer.printRecord(command.getId(),
                                   command.getConcreteClass(),
                                   command.getStatus().toString(),
                                   command.getTimestamp(),
                                   command.getParams());
            }
        }
    }

    /**
     * Liest eine Liste von Kommandos aus einer gegebenen CSV-Datei aus
     *
     * @param in Die auszulesende Datei
     * @return Ausgelesene Kommandos
     * @throws IOException
     */
    public static List<LegacyCommand> readCommandsFromCSV(final File in) throws IOException {
        try (final var parser = CSVParser.parse(in, Charset.defaultCharset(), CSV_FORMAT)) {
            return parser.getRecords().stream()
                         .filter(record -> record.getRecordNumber() != 1)
                         .map(record -> {
                             final var id = Integer.parseInt(record.get(0));
                             final var concreteClass = record.get(1);
                             final var status = CommandStatus.getTypeOf(record.get(2));
                             final var timestamp = Timestamp.valueOf(record.get(3));
                             final var params = record.get(4);

                             return new LegacyCommand(id, concreteClass, status, timestamp, params);
                         }).collect(Collectors.toList());
        }
    }
}
