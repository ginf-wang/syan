package wang.ginf.syan.persistence;

import org.hibernate.SessionFactory;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.GlobalState;
import wang.ginf.syan.model.State;
import wang.ginf.syan.model.StateBuilder;
import wang.ginf.syan.model.history.commands.Command;
import wang.ginf.syan.model.syndrome.Syndrome;
import wang.ginf.syan.userinterface.windows.SyndromeEditorWindow;

import java.io.*;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Der SyanFileService managed das Laden und Speichern von Inhalten aus/in einer .syan Datei.
 * Er stellt Funktionalitaeten bereit, die das Importieren von Einheiten des Models aus serialisierten
 * Dateien sowie das Exportieren von Einheiten des Models zu serialisierten Dateien erlauben.
 *
 * @author Jan-Luca Kiok
 */
public class SyanFileService {
    private final CommandsDatabase commandsDatabase = CommandsDatabase.getInstance();
    private final FileSystem zipFileSystem;
    private final SessionFactory sessionFactory;
    private final String title;

    /**
     * Erstellt eine Instanz des Syan File Service fuer den gegebenen {@see Path}.
     * Die Datei wird geoeffnet, wenn sie bereits existiert, oder neu erstellt.
     *
     * @param file der Pfad zur Syan Datei.
     * @throws IOException wenn die Initialisierung des FileSystems fehlschlaegt.
     */
    public SyanFileService(@NotNull final File file, final SessionFactory sessionFactory, final String title)
            throws IOException {
        this.sessionFactory = sessionFactory;
        this.title = title;

        // Angepasst von: https://docs.oracle.com/javase/7/docs/technotes/guides/io/fsp/zipfilesystemprovider.html
        final var uri = URI.create("jar:" + file.toURI().toString());
        final var env = Map.of("create", "true");

        FileSystem fs;
        try {
            fs = FileSystems.newFileSystem(uri, env);
        } catch (FileSystemAlreadyExistsException ex) {
            fs = FileSystems.getFileSystem(uri);
        }

        if (Objects.isNull(fs)) {
            throw new IOException("Error initializing zip filesystem!");
        }

        zipFileSystem = fs;
    }

    /**
     * Speichert gegebene Historie und Syndrom in der Syan Datei, fuer die dieser File Service erstellt wurde.
     *
     * @param syndrome das zu speichernde Syndrom.
     * @throws IOException wenn das Speichern fehlschlaegt.
     */
    public void save(@NotNull final Syndrome syndrome) throws IOException {
        if (!zipFileSystem.isOpen()) {
            throw new IOException("ZipFileSystem is not open.");
        }

        final var serializedSyndrome = File.createTempFile("syndrome", "ser");
        final var dbbackup = File.createTempFile("dbbackup", "h2");

        final var os = new FileOutputStream(serializedSyndrome);

        try (final var o = new ObjectOutputStream(os); final var d = new DataOutputStream(os)) {
            o.writeObject(syndrome);
            d.writeUTF(title);
            d.writeBoolean(GlobalState.isTemplate());
            d.writeBoolean(GlobalState.isStrengtheningRelationAllowed());
            d.writeBoolean(GlobalState.isUnknownRelationAllowed());
            d.writeBoolean(GlobalState.isWeakeningRelationAllowed());
            d.writeBoolean(GlobalState.isSymptomCreationAllowed());
        }

        ExportService.writeCommandsToCSV(commandsDatabase.getAll(), dbbackup);

        Files.move(
                serializedSyndrome.toPath(),
                zipFileSystem.getPath("syndrome.ser"),
                StandardCopyOption.REPLACE_EXISTING
        );

        Files.move(dbbackup.toPath(), zipFileSystem.getPath("dbbackup"), StandardCopyOption.REPLACE_EXISTING);

        zipFileSystem.close();
    }

    /**
     * Oeffnet gespeicherten Zustand aus dem initialisierten FileSystem bzw. der dem Service uebergebenen Datei
     *
     * @return Syndrom, Kommandos und Status der in der Datei hinterlegten Ausfuehrung
     * @throws IOException            wenn das Oeffnen fehlschlaegt.
     * @throws ClassNotFoundException wenn die serialisierten Daten korrumpiert sind.
     */
    public OpenResult open() throws IOException, ClassNotFoundException {
        if (!zipFileSystem.isOpen()) {
            throw new IOException("ZipFileSystem is not open.");
        }

        final var serializedSyndrome = File.createTempFile("syndrome", "ser");
        final var dbbackup = File.createTempFile("dbbackup", "h2");

        Files.copy(
                zipFileSystem.getPath("syndrome.ser"),
                serializedSyndrome.toPath(),
                StandardCopyOption.REPLACE_EXISTING
        );

        Files.copy(zipFileSystem.getPath("dbbackup"), dbbackup.toPath(), StandardCopyOption.REPLACE_EXISTING);

        final var commands = ExportService.readCommandsFromCSV(dbbackup);
        final var is = new FileInputStream(serializedSyndrome);

        final OpenResult result;

        try (final var ois = new ObjectInputStream(is); final var dis = new DataInputStream(is)) {
            final var syndrome = (Syndrome) ois.readObject();

            SyndromeEditorWindow.title.set(dis.readUTF());

            final var newState = new StateBuilder()
                    .setTemplate(dis.readBoolean())
                    .setStrengtheningRelationAllowed(dis.readBoolean())
                    .setUnknownRelationAllowed(dis.readBoolean())
                    .setWeakeningRelationAllowed(dis.readBoolean())
                    .setSymptomCreationAllowed(dis.readBoolean())
                    .createState();

            result = new OpenResult(syndrome, commands, newState);
        }

        serializedSyndrome.delete();
        dbbackup.delete();

        zipFileSystem.close();

        return result;
    }

    /**
     * Ein Transferobjekt, das das Ergebnis einer Dateioeffnung traegt.
     *
     * @author Ruben Smidt
     */
    public static class OpenResult {
        private final List<Command> commands = new ArrayList<>();
        private final Syndrome syndrome;
        private final State state;

        private OpenResult(final Syndrome syndrome,
                           final List<? extends Command> commands,
                           final State state) {
            this.syndrome = syndrome;
            this.state = state;
            this.commands.addAll(commands);
        }

        public State getState() {
            return state;
        }

        public Syndrome getSyndrome() {
            return syndrome;
        }

        public List<Command> getCommands() {
            return commands;
        }
    }
}
