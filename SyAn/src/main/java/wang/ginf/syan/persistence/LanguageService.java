package wang.ginf.syan.persistence;

import wang.ginf.syan.businesslogic.SyanFileLogic;
import wang.ginf.syan.model.Settings;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Der LanguageService speichert und liest die Sprachpraeferenz eines Anwenders.
 *
 * @author Ruben Smidt
 */
public class LanguageService {
    private static final Logger logger = Logger.getLogger(LanguageService.class.getName());
    private static LanguageService instance = new LanguageService();

    private LanguageService() {}

    public static LanguageService getInstance() { return instance; }

    /**
     * Speichert die Sprachpraeferenz. Dabei wird versucht, diese in dem Konfigurationsverzeichnis
     * ($HOME/.config/SyAn) zu speichern.
     *
     * @param locale die Sprachpraeferenz.
     * @return ob das Speichern erfolgreich war.
     */
    public boolean saveLocale(final Locale locale) {
        final var userDirectory = SyanFileLogic.getHomeDir();

        if (Objects.isNull(userDirectory)) {
            logger.severe("Failed to get user directory.");
            return false;
        }

        final var configDirectory = new File(userDirectory, ".config");

        if (isDirNotOk(configDirectory)) {
            logger.severe("Config directory is not ok.");
            return false;
        }

        final var syanConfigDirectory = new File(configDirectory, "SyAn");

        if (isDirNotOk(syanConfigDirectory)) {
            logger.severe("Syan config directory is not ok.");
            return false;
        }

        final var settingsFile = new File(syanConfigDirectory, "locale");

        if (!settingsFile.exists()) {
            logger.info("File does not exists. Creating and writing it now");
            writeToFile(settingsFile, locale.toLanguageTag());
        }

        if (settingsFile.exists()) {
            logger.info("File exists");
            if (!settingsFile.canWrite() || !settingsFile.isFile()) {
                logger.info("File not a file or not writable");
                return false;
            }

            logger.info("Writing file now.");

            writeToFile(settingsFile, locale.toLanguageTag());
        }

        return true;
    }

    /**
     * Holt die Sprachpraeferenz des Nutzers. Dabei wird versucht, diese aus dem Konfigurationsverzeichnis
     * ($HOME/.config/SyAn) zu lesen.
     *
     * @return ein {@link Optional}, das die Praeferenz enthaelt.
     */
    public Optional<Locale> getLocale() {
        final var settingsFilePath = Path.of(System.getProperty("user.home"), ".config", "SyAn", "locale");
        final var settingsFile = new File(settingsFilePath.toUri());

        logger.info("Trying to locate settings file.");

        if (!settingsFile.exists()) {
            logger.info("Settings file does not exist.");

            if (settingsFile.canWrite()) {
                logger.info("Settings file is writable");

                if (Settings.isDefaultLocaleSupported()) {
                    writeToFile(settingsFile, Locale.getDefault().toLanguageTag());
                    return Optional.of(Locale.getDefault());
                }
                writeToFile(settingsFile, Locale.GERMAN.toLanguageTag());

                return Optional.of(Locale.GERMAN);
            }

            logger.severe("Settings file is not writable");

            return Optional.empty();
        }

        logger.info("Settings file exists. Trying to read it.");

        if (!settingsFile.canRead()) {
            logger.severe("Can not read settings file.");

            return Optional.empty();
        }

        Locale readLocale;

        try {
            final var languageTag = new String(Files.readAllBytes(settingsFilePath)).trim();

            logger.info("Read language tag: " + languageTag);

            readLocale = Locale.forLanguageTag(languageTag);
        } catch (final IOException e) {
            logger.severe(e.getMessage());

            return Optional.empty();
        }

        logger.info("Read locale: " + readLocale.getDisplayLanguage());

        return Optional.of(readLocale);
    }

    private void writeToFile(final File file, final String payload) {
        try (final PrintWriter out = new PrintWriter(new FileOutputStream(file.getAbsolutePath(), false))) {
            out.print(payload);
        } catch (final FileNotFoundException e) {
            // das darf eigentlich nicht passieren (tm), da wir bereits alles geprueft haben.
            // nur race moeglich.
            throw new IllegalStateException("Previously ok file not ok anymore.");
        }
    }

    private boolean isDirNotOk(final File dir) {
        if (!dir.exists() && !dir.mkdir()) {
            return false;
        }

        return !dir.canWrite() || !dir.isDirectory();
    }
}
