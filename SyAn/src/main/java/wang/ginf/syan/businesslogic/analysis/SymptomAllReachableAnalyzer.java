package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Symptom;

import java.util.List;

/**
 * Analyzer, der alle vorwaerts und rueckwaerts vom ausgewaehlten Symptom erreichbaren Symptome hervorhebt.
 *
 * @author Jonas Luehrs
 */
public class SymptomAllReachableAnalyzer extends GraphicalAnalyzer<Symptom> {
    private final SymptomForwardReachableAnalyzer symptomForwardReachableAnalyzer =
            new SymptomForwardReachableAnalyzer();
    private final SymptomReverseReachableAnalyzer symptomReverseReachableAnalyzer =
            new SymptomReverseReachableAnalyzer();

    @Override
    public void showAnalysis(@NotNull final Symptom target) {
        symptomForwardReachableAnalyzer.showAnalysis(target);
        symptomReverseReachableAnalyzer.showAnalysis(target);
    }

    public void showAnalysisOnlyBetweenAnalysis(@NotNull final Symptom root, @NotNull final List<Symptom> targets) {
        symptomForwardReachableAnalyzer.showOnlyBetweenAnalysis(root, targets);
        symptomReverseReachableAnalyzer.showOnlyBetweenAnalysis(root, targets);
    }

    @Override
    public void hideAnalysis(@NotNull final Symptom target) {
        symptomReverseReachableAnalyzer.hideAnalysis(target);
        symptomForwardReachableAnalyzer.hideAnalysis(target);
    }
}
