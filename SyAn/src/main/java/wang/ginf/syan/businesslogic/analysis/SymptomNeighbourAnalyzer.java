package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.Symptom;

/**
 * Analyzer, der die Anzahl der Nachbarn eines Symptoms berechnet.
 *
 * @author Jonas Lührs
 */
public class SymptomNeighbourAnalyzer extends ValueAnalyzer<Symptom, Integer> {

    @Override
    public Integer analyze(@NotNull final Symptom target) {
        return getNumberOfPredecessors(target) + getNumberOfSuccessors(target);
    }

    public int getNumberOfPredecessors(@NotNull final Symptom target) {
        int numOfPredecessors = 0;
        for (Relation relation : target.getRelations()) {
            if (relation.getEndSymptom() == target) {
                numOfPredecessors++;
            }
        }
        return numOfPredecessors;
    }

    public int getNumberOfSuccessors(@NotNull final Symptom target) {
        int numOfSuccessors = 0;
        for (Relation relation : target.getRelations()) {
            if (relation.getStartSymptom() == target) {
                numOfSuccessors++;
            }
        }
        return numOfSuccessors;
    }
}
