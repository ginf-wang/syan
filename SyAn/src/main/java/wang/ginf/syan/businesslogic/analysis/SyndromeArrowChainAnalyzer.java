package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;

/**
 * Analyzer, der die Anzahl der Pfeilketten eines Syndroms berechnet.
 *
 * @author Jonas Lührs
 */
public class SyndromeArrowChainAnalyzer extends ValueAnalyzer<Syndrome, Integer> {

    private final SymptomArrowChainAnalyzer symptomArrowChainAnalyzer = new SymptomArrowChainAnalyzer();

    /**
     * Methode, die die Anzahl der Pfeilketten im Syndrom zurueckgibt.
     * <p>
     * Info: Der Analyzer zählt beim Durchgehen der Symptome die Pfeilketten jedes Start- oder Endknotens.
     * Da eine Pfeilkette sowohl ein Start- als auch ein Endknoten besitzt, erhalten wir als Ergebnis die
     * doppelte Anzahl an Pfeilketten. Aus diesem Grund wird am Ende der Methode die Anzahl der Pfeilketten
     * durch zwei geteilt.
     *
     * @param target das Syndrom..
     * @return die Anzahl der Pfeilketten.
     */
    @Override
    public Integer analyze(@NotNull final Syndrome target) {
        int numOfArrowChains = 0;
        for (Sphere sphere : target.getSpheres()) {
            for (Symptom symptom : sphere.getSymptoms()) {
                if (!symptomArrowChainAnalyzer.isInternalNode(symptom)) {
                    numOfArrowChains += symptomArrowChainAnalyzer.getNumberOfArrowChainsStartOrEndNode(symptom);
                }
            }
        }
        return numOfArrowChains / 2;
    }
}
