package wang.ginf.syan.businesslogic;

/**
 * Wird geworfen, wenn ein FileChooser-Dialog abgebrochen wird
 *
 * @author Jan-Luca Kiok
 */
public class AbortedException extends Exception {

    public AbortedException() {}

    public AbortedException(String message) {
        super(message);
    }
}
