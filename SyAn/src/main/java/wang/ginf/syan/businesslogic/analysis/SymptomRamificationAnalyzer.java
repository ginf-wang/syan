package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Symptom;

/**
 * Analyzer, der die Anzahl der Verzweigungen eines Symptoms berechnet.
 * <p>
 * Eine Verzweigung ist ein Knoten, zu dem mindestens zwei Kanten hin- bzw. wegführen (oder beides)
 *
 * @author Jonas Lührs
 */
public class SymptomRamificationAnalyzer extends ValueAnalyzer<Symptom, Integer> {

    private final SymptomRelationAnalyzer symptomRelationAnalyzer = new SymptomRelationAnalyzer();

    @Override
    public Integer analyze(@NotNull final Symptom target) {
        return getConvergentRamification(target) + getDivergentRamification(target);
    }

    public int getConvergentRamification(@NotNull final Symptom target) {
        if (symptomRelationAnalyzer.getNumberOfAllIncomingRelations(target) > 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public int getDivergentRamification(@NotNull final Symptom target) {
        if (symptomRelationAnalyzer.getNumberOfAllOutgoingRelations(target) > 1) {
            return 1;
        } else {
            return 0;
        }
    }
}
