package wang.ginf.syan.businesslogic;

/**
 * Wird geworfen, wenn der einzulesende File nicht unserer Spezifikation entspricht
 *
 * @author Jan-Luca Kiok
 */
public class IllegalFileException extends Exception {

    public IllegalFileException() {}

    public IllegalFileException(String message) {
        super(message);
    }
}
