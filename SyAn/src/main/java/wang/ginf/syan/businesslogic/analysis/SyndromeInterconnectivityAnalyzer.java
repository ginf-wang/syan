package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Syndrome;

/**
 * Analyzer, der den Vernetzungsindex eines Syndroms berechnet.
 * <p>
 * Vernetzungsindex (vx) = 2 x Anzahl wertbarer Relationen / Anzahl wertbarer Symptome [nach Ossimitz, 2000, S. 210]
 *
 * @author Arne Kiesewetter
 * @author Jonas Lührs
 */
public class SyndromeInterconnectivityAnalyzer extends ValueAnalyzer<Syndrome, Double> {

    private final SyndromeRelationAnalyzer syndromeRelationAnalyzer = new SyndromeRelationAnalyzer();
    private final SyndromeSymptomAnalyzer syndromeSymptomAnalyzer = new SyndromeSymptomAnalyzer();

    @Override
    public Double analyze(@NotNull final Syndrome target) {
        if (syndromeSymptomAnalyzer.analyze(target) != 0) {
            return 2 * (double) syndromeRelationAnalyzer.analyze(target) /
                   (double) syndromeSymptomAnalyzer.analyze(target);
        } else {
            return 0.0;
        }
    }
}
