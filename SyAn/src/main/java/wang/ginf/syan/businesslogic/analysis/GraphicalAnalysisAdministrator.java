package wang.ginf.syan.businesslogic.analysis;

import org.hibernate.dialect.function.AbstractAnsiTrimEmulationFunction;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.userinterface.controls.SyndromeEditor;

import java.util.ArrayList;
import java.util.List;

import static wang.ginf.syan.userinterface.controls.SymptomControl.USE_STATUS_BORDER_COLOR;

/**
 * Klasse, die die graphische Analyse aller Entitaeten verwaltet.
 *
 * @author Jonas Lührs
 */
public class GraphicalAnalysisAdministrator {

    private static final GraphicalAnalysisAdministrator administrator = new GraphicalAnalysisAdministrator();
    private SyndromeEditor syndromeEditor;
    private List<GraphicalAnalysis> graphicalAnalysisList = new ArrayList<>();

    public static GraphicalAnalysisAdministrator getInstance() {
        return administrator;
    }

    public void add(@NotNull final GraphicalAnalysis graphicalAnalysis) {
        graphicalAnalysisList.add(graphicalAnalysis);
    }

    public void remove(@NotNull final GraphicalAnalysis graphicalAnalysis) {
        graphicalAnalysisList.remove(graphicalAnalysis);
    }

    /**
     * Fuehrt auf allen zur graphischen Analyse ausgewaehlten Symptomen des Syndroms erneut die jeweilige
     * Analyse aus.
     */
    public void updateGraphicalAnalysis() {
        for (GraphicalAnalysis graphicalAnalysis : graphicalAnalysisList) {
            if (graphicalAnalysis.getSymptom() != null) {
                if (graphicalAnalysis.isAllReachable()) {
                    graphicalAnalysis.setAllReachable(graphicalAnalysis.getSymptom());
                } else if (graphicalAnalysis.isOnlyForwardReachable()) {
                    graphicalAnalysis.setOnlyForwardReachable(graphicalAnalysis.getSymptom());
                } else if (graphicalAnalysis.isOnlyReverseReachable()) {
                    graphicalAnalysis.setOnlyReverseReachable(graphicalAnalysis.getSymptom());
                } else if (graphicalAnalysis.isAllReachableOBSN()) {
                    graphicalAnalysis.setAllReachableOBSN(graphicalAnalysis.getSymptom());
                } else if (graphicalAnalysis.isOnlyForwardReachableOBSN()) {
                    graphicalAnalysis.setOnlyForwardReachableOBSN(graphicalAnalysis.getSymptom());
                } else if (graphicalAnalysis.isOnlyReverseReachableOBSN()) {
                    graphicalAnalysis.setOnlyReverseReachableOBSN(graphicalAnalysis.getSymptom());
                }
            }
        }
    }

    /**
     * Deaktiviert die graphischen Analyse bei allen Symptomen des Syndroms.
     */
    public void disableGraphicalAnalysis() {
        for (GraphicalAnalysis graphicalAnalysis : graphicalAnalysisList) {
            if(graphicalAnalysis.getSymptom()!=null) {
                if (graphicalAnalysis.isAllReachable()) {
                    graphicalAnalysis.allReachableProperty().setValue(false);
                } else if (graphicalAnalysis.isOnlyForwardReachable()) {
                    graphicalAnalysis.onlyForwardReachableProperty().setValue(false);
                } else if (graphicalAnalysis.isOnlyReverseReachable()) {
                    graphicalAnalysis.onlyReverseReachableProperty().setValue(false);
                } else if (graphicalAnalysis.isAllReachableOBSN()) {
                    graphicalAnalysis.allReachableOBSNProperty().setValue(false);
                } else if (graphicalAnalysis.isOnlyForwardReachableOBSN()) {
                    graphicalAnalysis.onlyForwardReachableOBSNProperty().setValue(false);
                } else if (graphicalAnalysis.isOnlyReverseReachableOBSN()) {
                    graphicalAnalysis.onlyReverseReachableOBSNProperty().setValue(false);
                }
            }
        }
        for (Sphere sphere : syndromeEditor.getSyndrome().getSpheres()) {
             for(Symptom symptom: sphere.getSymptoms()) {
                syndromeEditor.getSymptomControl(symptom).setBorderColor(USE_STATUS_BORDER_COLOR);
             }
        }

    }

    /**
     * Gibt eine Liste mit allen zur graphischen Analyse ausgewaehlen Symptome zurueck,
     * abzueglich des uebergebenen Symptoms.
     */
    public List<Symptom> getListOfSelectedNodes(@NotNull final Symptom symptom) {
        ArrayList<Symptom> list = new ArrayList<>();
        for (GraphicalAnalysis graphicalAnalysis : graphicalAnalysisList) {
            if (graphicalAnalysis.isAllReachable() || graphicalAnalysis.isOnlyForwardReachable() ||
                graphicalAnalysis.isOnlyReverseReachable() ||
                graphicalAnalysis.isAllReachableOBSN() ||
                 graphicalAnalysis.isOnlyForwardReachableOBSN() ||
                  graphicalAnalysis.isOnlyReverseReachableOBSN()) {
                list.add(graphicalAnalysis.getSymptom());
            }
        }
        list.remove(symptom);
        return list;
    }

    public void setSyndromeEditor(@NotNull final SyndromeEditor syndromeEditor) {
        this.syndromeEditor=syndromeEditor;
    }

    public SyndromeEditor getSyndromeEditor() {
        return syndromeEditor;
    }
}
