package wang.ginf.syan.businesslogic.analysis;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.userinterface.controls.SyndromeEditor;

/**
 * Analyzer, der alle Symptome gemäß ihrer Wichtigkeit hervorhebt. Die Wichtigkeit ergibt sich
 * aus der Anzahl an Relationen des Symptoms
 *
 * @author Jonas Lührs
 */
public class SyndromeInterconnectivityRateAnalyzer extends GraphicalAnalyzer<SyndromeEditor> {
    public static final SyndromeInterconnectivityRateAnalyzer analyzer = new SyndromeInterconnectivityRateAnalyzer();
    private static final double MAX_VALUE = 7;
    private static final double EXPONENT_FACTOR = Math.log(5d / 7d);
    private final BooleanProperty analyzable = new SimpleBooleanProperty(false);

    public static SyndromeInterconnectivityRateAnalyzer getInstance() {
        return analyzer;
    }

    public final BooleanProperty analyzableProperty() {
        return analyzable;
    }

    @Override
    public void showAnalysis(@NotNull final SyndromeEditor syndromeEditor) {
        if (analyzableProperty().get()) {
            for (Sphere sphere : syndromeEditor.getSyndrome().getSpheres()) {
                for (Symptom symptom : sphere.getSymptoms()) {
                    //2 ist die Standard Breite des Symptomrandes
                    syndromeEditor.getSymptomControl(symptom)
                                  .setBorderWidth(MAX_VALUE -
                                                  MAX_VALUE *
                                                  Math.pow(Math.E, EXPONENT_FACTOR * symptom.getRelations().size()));
                }
            }
        }
    }

    @Override
    public void hideAnalysis(@NotNull final SyndromeEditor syndromeEditor) {
        for (Sphere sphere : syndromeEditor.getSyndrome().getSpheres()) {
            for (Symptom symptom : sphere.getSymptoms()) {
                syndromeEditor.getSymptomControl(symptom).setBorderWidth(2);
            }
        }
    }
}
