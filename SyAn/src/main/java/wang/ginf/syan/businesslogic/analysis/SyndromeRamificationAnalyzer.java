package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;

/**
 * Analyzer, der die Anzahl der Verzweigungen eines Syndroms berechnet.
 *
 * @author Jonas Lührs
 */
public class SyndromeRamificationAnalyzer extends ValueAnalyzer<Syndrome, Integer> {

    private final SymptomRamificationAnalyzer symptomRamificationAnalyzer = new SymptomRamificationAnalyzer();

    @Override
    public Integer analyze(@NotNull final Syndrome target) {
        return getConvergentRamification(target) + getDivergentRamification(target);
    }

    public int getConvergentRamification(@NotNull final Syndrome target) {
        int numOfConvergentRamifications = 0;
        for (Sphere sphere : target.getSpheres()) {
            for (Symptom symptom : sphere.getSymptoms()) {
                numOfConvergentRamifications += symptomRamificationAnalyzer.getConvergentRamification(symptom);
            }
        }
        return numOfConvergentRamifications;
    }

    public int getDivergentRamification(@NotNull final Syndrome target) {
        int numOfDivergentRamifications = 0;
        for (Sphere sphere : target.getSpheres()) {
            for (Symptom symptom : sphere.getSymptoms()) {
                numOfDivergentRamifications += symptomRamificationAnalyzer.getDivergentRamification(symptom);
            }
        }
        return numOfDivergentRamifications;
    }
}
