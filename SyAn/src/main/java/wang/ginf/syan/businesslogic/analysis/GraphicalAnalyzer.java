package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;

/**
 * Abstrakte Basisklasse fuer Analyzer, die keinen Wert zurueck geben,
 * sondern direkt die Darstellung ausgehend vom Eingabewert veraendern.
 *
 * @param <T> der Typ der Eingabewerte.
 * @author Arne Kiesewetter
 */
public abstract class GraphicalAnalyzer<T> {
    /**
     * Veraendert die Darstellung ausgehend vom Eingabewert, um die Analyse anzuzeigen.
     *
     * @param target der Eingabewert.
     */
    public abstract void showAnalysis(@NotNull T target);

    /**
     * Veraendert die Darstellung ausgehend vom Eingabewert, um die Analyse wieder zu verstecken.
     *
     * @param target der Eingabewert.
     */
    public abstract void hideAnalysis(@NotNull T target);
}
