package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.RelationType;
import wang.ginf.syan.model.syndrome.Symptom;

import static wang.ginf.syan.model.syndrome.RelationType.LESSENING;
import static wang.ginf.syan.model.syndrome.RelationType.UNKNOWN;

/**
 * Analyzer, der die Anzahl der Relationen eines Symptoms berechnet.
 *
 * @author Jonas Lührs
 */
public class SymptomRelationAnalyzer extends ValueAnalyzer<Symptom, Integer> {

    private final SymptomNeighbourAnalyzer symptomNeighbourAnalyzer = new SymptomNeighbourAnalyzer();

    @Override
    public Integer analyze(@NotNull final Symptom target) {
        return getNumberOfAllIncomingRelations(target) + getNumberOfAllOutgoingRelations(target);
    }

    public int getNumberOfAllIncomingRelations(@NotNull final Symptom target) {
        return symptomNeighbourAnalyzer.getNumberOfPredecessors(target);
    }

    public int getNumberOfAllOutgoingRelations(@NotNull final Symptom target) {
        return symptomNeighbourAnalyzer.getNumberOfSuccessors(target);
    }

    /**
     * Methode, die die Anzahl der Relationen mit dem uebergebenen Relationstypen und
     * der uebergebenen Richtung des uebergebenen Symptoms zurueckgibt.
     *
     * @param target    das uebergebene Symptom.
     * @param type      der zu analysierende Relationstyp.
     * @param direction die Richtung der Relation.
     * @return die Anzahl der Relationen mit dem uebergebenen Relationstypen.
     */
    public int getNumberOfSpecialTypeRelations(@NotNull final Symptom target,
                                               @NotNull final RelationType type,
                                               @NotNull final String direction) {
        int numOfUnknownRelations = 0;
        int numOfLesseningRelations = 0;
        int numOfIncreasingRelations = 0;

        for (Relation relation : target.getRelations()) {
            Symptom symptom;
            if (direction.equals("Incoming")) {
                symptom = relation.getEndSymptom();
            } else if (direction.equals("Outgoing")) {
                symptom = relation.getStartSymptom();
            } else {
                throw new IllegalArgumentException();
            }
            if (symptom == target) {
                if (relation.getRelationType() == UNKNOWN) {
                    numOfUnknownRelations++;
                } else if (relation.getRelationType() == LESSENING) {
                    numOfLesseningRelations++;
                } else {
                    numOfIncreasingRelations++;
                }
            }
        }
        if (type == UNKNOWN) {
            return numOfUnknownRelations;
        } else if (type == LESSENING) {
            return numOfLesseningRelations;
        } else {
            return numOfIncreasingRelations;
        }
    }
}
