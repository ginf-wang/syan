package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;

/**
 * Abstrakte Basisklasse fuer Analyzer, die einen Wert zurueck geben.
 *
 * @param <TIn>  der Typ der Eingabewerte.
 * @param <TOut> der Typ der Ausgabewerte.
 * @author Arne Kiesewetter
 * @author Jonas Luehrs
 */
public abstract class ValueAnalyzer<TIn, TOut> {

    /**
     * Analysiert den Eingabewert und gibt ein Ergebnis zurueck.
     *
     * @param target der Eingabewert.
     * @return das Analyseergebnis.
     */
    public abstract TOut analyze(@NotNull TIn target);
}


