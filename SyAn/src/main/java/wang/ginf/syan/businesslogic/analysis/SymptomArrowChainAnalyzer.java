package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.Symptom;

/**
 * Analyzer, der die Anzahl der Pfeilketten eines Symptoms berechnet.
 * <p>
 * Eine Pfeilkette ist eine Abfolge von mindestens drei Kanten in dieselbe Richtung,
 * ohne dass eine Verzweigung bei den inneren Knoten der Kette integriert ist. Ein
 * Knoten einer Kette ist ein innerer, wenn er nicht der Start- und nicht der Endknoten
 * der Kette ist.
 *
 * @author Jonas Lührs
 */
public class SymptomArrowChainAnalyzer extends ValueAnalyzer<Symptom, Integer> {

    private final SymptomRamificationAnalyzer symptomRamificationAnalyzer = new SymptomRamificationAnalyzer();

    @Override
    public Integer analyze(@NotNull final Symptom target) {
        if (isInternalNode(target)) {
            return getNumberOfArrowChainsInternalNode(target);
        }
        return getNumberOfArrowChainsStartOrEndNode(target);
    }

    public boolean isInternalNode(@NotNull final Symptom target) {
        return symptomRamificationAnalyzer.analyze(target) == 0 &&
               target.getRelations().size() == 2;
    }

    /**
     * Methode, die die Anzahl der Pfeilketten zurueckgibt, wenn es bei
     * dem uebergebenen Symptom um einen inneren Knoten handelt.
     * <p>
     * Info: Ein innerer Knoten kann nur maximal Teil einer Pfeilkette sein.
     *
     * @param target das uebergebene Symptom.
     * @return die Anzahl der Pfeilketten.
     */
    public int getNumberOfArrowChainsInternalNode(@NotNull final Symptom target) {
        if (isInternalNode(target)) {
            for (Relation relation : target.getRelations()) {
                if (relation.getEndSymptom() == target && isInternalNode(relation.getStartSymptom())) {
                    return 1;
                }
                if (relation.getStartSymptom() == target && isInternalNode(relation.getEndSymptom())) {
                    return 1;
                }
            }
        }
        return 0;
    }

    public int getNumberOfArrowChainsStartOrEndNode(@NotNull final Symptom target) {
        int numOfArrowChainsStartOrEndNode = 0;
        for (Relation relation : target.getRelations()) {
            if (relation.getEndSymptom() == target) {
                if (isInternalNode(relation.getStartSymptom())) {
                    for (Relation relation2 : relation.getStartSymptom().getRelations()) {
                        if (relation2.getEndSymptom() == relation.getStartSymptom() &&
                            isInternalNode(relation2.getStartSymptom())) {
                            numOfArrowChainsStartOrEndNode++;
                        }
                    }
                }
            }
            if (relation.getStartSymptom() == target) {
                if (isInternalNode(relation.getEndSymptom())) {
                    for (Relation relation2 : relation.getEndSymptom().getRelations()) {
                        if (relation2.getStartSymptom() == relation.getEndSymptom() &&
                            isInternalNode(relation2.getEndSymptom())) {
                            numOfArrowChainsStartOrEndNode++;
                        }
                    }
                }
            }
        }
        return numOfArrowChainsStartOrEndNode;
    }
}
