package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Syndrome;

/**
 * Analyzer, der den Umfang eines Syndroms berechnet.
 * <p>
 * Umfang (U) = Anzahl wertbarer Symptome + Anzahl wertbarer Relationen [nach Sommer, 2005, S. 151]
 *
 * @author Arne Kiesewetter
 * @author Jonas Lührs
 */
public class SyndromeCircumferenceAnalyzer extends ValueAnalyzer<Syndrome, Integer> {

    private final SyndromeRelationAnalyzer syndromeRelationAnalyzer = new SyndromeRelationAnalyzer();
    private final SyndromeSymptomAnalyzer syndromeSymptomAnalyzer = new SyndromeSymptomAnalyzer();

    @Override
    public Integer analyze(@NotNull final Syndrome target) {
        return syndromeSymptomAnalyzer.analyze(target) + syndromeRelationAnalyzer.analyze(target);
    }
}
