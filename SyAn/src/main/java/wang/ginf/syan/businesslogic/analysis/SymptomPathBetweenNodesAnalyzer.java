package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.model.syndrome.Symptom;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Set;

/**
 * Analyzer, der alle Pfade zwischen dem Start-Symptom und dem Ziel-Symptom abspeichert.
 *
 * @author Arne Kiesewetter
 */
public final class SymptomPathBetweenNodesAnalyzer {
    private SymptomPathBetweenNodesAnalyzer() { }

    public static Set<Symptom> getForwardReachableSymptomsInbetween(Symptom start, Symptom end) {
        final var forwardFromStart = getForwardReachableSymptomsFromTo(start, end);
        final var reverseFromEnd = getReverseReachableSymptomsFromTo(end, start);

        forwardFromStart.retainAll(reverseFromEnd);
        return forwardFromStart;
    }

    public static Set<Symptom> getReverseReachableSymptomsInbetween(Symptom start, Symptom end) {
        final var reverseFromStart = getReverseReachableSymptomsFromTo(start, end);
        final var forwardFromEnd = getForwardReachableSymptomsFromTo(end, start);

        reverseFromStart.retainAll(forwardFromEnd);
        return reverseFromStart;
    }

    private static Set<Symptom> getForwardReachableSymptomsFromTo(final Symptom start, final Symptom end) {
        final var result = new HashSet<Symptom>();
        final var toSearch = new ArrayDeque<Symptom>();
        toSearch.add(start);

        while (!toSearch.isEmpty()) {
            final var current = toSearch.pop();

            for (var relation : current.getRelations()) {
                if (relation.getEndSymptom().equals(current) ||
                    result.contains(relation.getEndSymptom()) ||
                    end.equals(relation.getEndSymptom())) {
                    continue;
                }

                result.add(relation.getEndSymptom());
                toSearch.add(relation.getEndSymptom());
            }
        }

        return result;
    }

    private static Set<Symptom> getReverseReachableSymptomsFromTo(final Symptom start, final Symptom end) {
        final var result = new HashSet<Symptom>();
        final var toSearch = new ArrayDeque<Symptom>();
        toSearch.add(start);

        while (!toSearch.isEmpty()) {
            final var current = toSearch.pop();

            for (var relation : current.getRelations()) {
                if (relation.getStartSymptom().equals(current) ||
                    result.contains(relation.getStartSymptom()) ||
                    end.equals(relation.getStartSymptom())) {
                    continue;
                }

                result.add(relation.getStartSymptom());
                toSearch.add(relation.getStartSymptom());
            }
        }

        return result;
    }
}
