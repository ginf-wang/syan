package wang.ginf.syan.businesslogic.analysis;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;

import static wang.ginf.syan.model.syndrome.RelationType.*;

/**
 * Klasse, die die Analyzer verwaltet und für das Aktualisieren der Statistik-Werte zuständig ist.
 *
 * @author Jonas Lührs
 */
public class Statistics {
    private static final Statistics statistics = new Statistics();

    private final SymptomNeighbourAnalyzer symptomNeighbourAnalyzer = new SymptomNeighbourAnalyzer();
    private final IntegerProperty numOfNeighbours = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfPredecessors = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfSuccessors = new SimpleIntegerProperty(0);

    private final SymptomArrowChainAnalyzer symptomArrowChainAnalyzer = new SymptomArrowChainAnalyzer();
    private final IntegerProperty numOfArrowChains = new SimpleIntegerProperty(0);

    private final SymptomRamificationAnalyzer symptomRamificationAnalyzer = new SymptomRamificationAnalyzer();
    private final IntegerProperty numOfRamification = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfConvergentRamification = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfDivergentRamification = new SimpleIntegerProperty(0);

    private final SymptomRelationAnalyzer symptomRelationAnalyzer = new SymptomRelationAnalyzer();
    private final IntegerProperty numOfRelations = new SimpleIntegerProperty(0);

    private final IntegerProperty numOfIncomingRelations = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfIncomingUnknownRelations = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfIncomingLesseningRelations = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfIncomingIncreasingRelations = new SimpleIntegerProperty(0);

    private final IntegerProperty numOfOutgoingRelations = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfOutgoingUnknownRelations = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfOutgoingLesseningRelations = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfOutgoingIncreasingRelations = new SimpleIntegerProperty(0);

    private final SyndromeSymptomAnalyzer syndromeSymptomAnalyzer = new SyndromeSymptomAnalyzer();
    private final IntegerProperty numOfSyndromeSymptoms = new SimpleIntegerProperty(0);

    private final SyndromeArrowChainAnalyzer syndromeArrowChainAnalyzer = new SyndromeArrowChainAnalyzer();
    private final IntegerProperty numOfSyndromeArrowChains = new SimpleIntegerProperty(0);

    private final SyndromeRamificationAnalyzer syndromeRamificationAnalyzer = new SyndromeRamificationAnalyzer();
    private final IntegerProperty numOfSyndromeRamifications = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfSyndromeConvergentRamifications = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfSyndromeDivergentRamifications = new SimpleIntegerProperty(0);

    private final SyndromeRelationAnalyzer syndromeRelationAnalyzer = new SyndromeRelationAnalyzer();
    private final IntegerProperty numOfSyndromeRelations = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfSyndromeUnknownRelations = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfSyndromeLesseningRelations = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfSyndromeIncreasingRelations = new SimpleIntegerProperty(0);

    private final SyndromeCircumferenceAnalyzer syndromeCircumferenceAnalyzer = new SyndromeCircumferenceAnalyzer();
    private final IntegerProperty circumference = new SimpleIntegerProperty(0);
    private final SyndromeInterconnectivityAnalyzer syndromeInterconnectivityAnalyzer =
            new SyndromeInterconnectivityAnalyzer();
    private final DoubleProperty interconnectivity = new SimpleDoubleProperty(0);

    public static Statistics getInstance() {
        return statistics;
    }

    public final IntegerProperty numOfNeighboursProperty() {
        return numOfNeighbours;
    }

    public final IntegerProperty numOfPredecessorsProperty() {
        return numOfPredecessors;
    }

    public final IntegerProperty numOfSuccessorsProperty() {
        return numOfSuccessors;
    }

    public final IntegerProperty numOfArrowChainsProperty() {
        return numOfArrowChains;
    }

    public final IntegerProperty numOfRamificationProperty() {
        return numOfRamification;
    }

    public final IntegerProperty numOfConvergentRamificationProperty() {
        return numOfConvergentRamification;
    }

    public final IntegerProperty numOfDivergentRamificationProperty() {
        return numOfDivergentRamification;
    }

    public final IntegerProperty numOfRelationsProperty() {
        return numOfRelations;
    }

    public final IntegerProperty numOfIncomingRelationsProperty() {
        return numOfIncomingRelations;
    }

    public final IntegerProperty numOfIncomingUnknownRelationsProperty() {
        return numOfIncomingUnknownRelations;
    }

    public final IntegerProperty numOfIncomingLesseningRelationsProperty() {
        return numOfIncomingLesseningRelations;
    }

    public final IntegerProperty numOfIncomingIncreasingRelationsProperty() {
        return numOfIncomingIncreasingRelations;
    }

    public final IntegerProperty numOfOutgoingRelationsProperty() {
        return numOfOutgoingRelations;
    }

    public final IntegerProperty numOfOutgoingUnknownRelationsProperty() {
        return numOfOutgoingUnknownRelations;
    }

    public final IntegerProperty numOfOutgoingLesseningRelationsProperty() {
        return numOfOutgoingLesseningRelations;
    }

    public final IntegerProperty numOfOutgoingIncreasingRelationsProperty() {
        return numOfOutgoingIncreasingRelations;
    }

    public final IntegerProperty numOfSyndromeSymptomsProperty() {
        return numOfSyndromeSymptoms;
    }

    public final IntegerProperty numOfSyndromeArrowChainsProperty() {
        return numOfSyndromeArrowChains;
    }

    public final IntegerProperty numOfSyndromeRamificationsProperty() {
        return numOfSyndromeRamifications;
    }

    public final IntegerProperty numOfSyndromeConvergentRamificationsProperty() {
        return numOfSyndromeConvergentRamifications;
    }

    public final IntegerProperty numOfSyndromeDivergentRamificationsProperty() {
        return numOfSyndromeDivergentRamifications;
    }

    public final IntegerProperty numOfSyndromeRelationsProperty() {
        return numOfSyndromeRelations;
    }

    public final IntegerProperty numOfSyndromeUnknownRelationsProperty() {
        return numOfSyndromeUnknownRelations;
    }

    public final IntegerProperty numOfSyndromeLesseningRelationsProperty() {
        return numOfSyndromeLesseningRelations;
    }

    public final IntegerProperty numOfSyndromeIncreasingRelationsProperty() {
        return numOfSyndromeIncreasingRelations;
    }

    public final IntegerProperty circumferenceProperty() {
        return circumference;
    }

    public final DoubleProperty interconnectivityProperty() {
        return interconnectivity;
    }

    public final void resetSymptomStatistics() {
        this.numOfNeighbours.set(0);
        this.numOfPredecessors.set(0);
        this.numOfSuccessors.set(0);
        this.numOfArrowChains.set(0);
        this.numOfRamification.set(0);
        this.numOfConvergentRamification.set(0);
        this.numOfDivergentRamification.set(0);
        this.numOfRelations.set(0);
        this.numOfIncomingRelations.set(0);
        this.numOfIncomingUnknownRelations.set(0);
        this.numOfIncomingLesseningRelations.set(0);
        this.numOfIncomingIncreasingRelations.set(0);
        this.numOfOutgoingRelations.set(0);
        this.numOfOutgoingUnknownRelations.set(0);
        this.numOfOutgoingLesseningRelations.set(0);
        this.numOfOutgoingIncreasingRelations.set(0);
    }

    public final void setSymptomStatistics(Symptom target) {
        this.numOfNeighbours.set(symptomNeighbourAnalyzer.analyze(target));
        this.numOfPredecessors.set(symptomNeighbourAnalyzer.getNumberOfPredecessors(target));
        this.numOfSuccessors.set(symptomNeighbourAnalyzer.getNumberOfSuccessors(target));
        this.numOfArrowChains.set(symptomArrowChainAnalyzer.analyze(target));
        this.numOfRamification.set(symptomRamificationAnalyzer.analyze(target));
        this.numOfConvergentRamification.set(symptomRamificationAnalyzer.getConvergentRamification(target));
        this.numOfDivergentRamification.set(symptomRamificationAnalyzer.getDivergentRamification(target));
        this.numOfRelations.set(symptomRelationAnalyzer.analyze(target));
        this.numOfIncomingRelations.set(symptomRelationAnalyzer.getNumberOfAllIncomingRelations(target));
        this.numOfIncomingUnknownRelations.set(symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(target,
                                                                                                       UNKNOWN,
                                                                                                       "Incoming"));
        this.numOfIncomingLesseningRelations.set(symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(target,
                                                                                                         LESSENING,
                                                                                                         "Incoming"));
        this.numOfIncomingIncreasingRelations.set(symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(target,
                                                                                                          INCREASING,
                                                                                                          "Incoming"));
        this.numOfOutgoingRelations.set(symptomRelationAnalyzer.getNumberOfAllOutgoingRelations(target));
        this.numOfOutgoingUnknownRelations.set(symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(target,
                                                                                                       UNKNOWN,
                                                                                                       "Outgoing"));
        this.numOfOutgoingLesseningRelations.set(symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(target,
                                                                                                         LESSENING,
                                                                                                         "Outgoing"));
        this.numOfOutgoingIncreasingRelations.set(symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(target,
                                                                                                          INCREASING,
                                                                                                          "Outgoing"));
    }

    public final void setSyndromeStatistics(Syndrome target) {
        this.numOfSyndromeSymptoms.set(syndromeSymptomAnalyzer.analyze(target));
        this.numOfSyndromeArrowChains.set(syndromeArrowChainAnalyzer.analyze(target));
        this.numOfSyndromeRamifications.set(syndromeRamificationAnalyzer.analyze(target));
        this.numOfSyndromeConvergentRamifications.set(syndromeRamificationAnalyzer.getConvergentRamification(target));
        this.numOfSyndromeDivergentRamifications.set(syndromeRamificationAnalyzer.getDivergentRamification(target));
        this.numOfSyndromeRelations.set(syndromeRelationAnalyzer.analyze(target));
        this.numOfSyndromeUnknownRelations.set(syndromeRelationAnalyzer.getNumberOfSpecialTypeRelations(target, UNKNOWN));
        this.numOfSyndromeLesseningRelations.set(syndromeRelationAnalyzer.getNumberOfSpecialTypeRelations(target, LESSENING));
        this.numOfSyndromeIncreasingRelations.set(syndromeRelationAnalyzer.getNumberOfSpecialTypeRelations(target, INCREASING));
        this.circumference.set(syndromeCircumferenceAnalyzer.analyze(target));
        this.interconnectivity.set(syndromeInterconnectivityAnalyzer.analyze(target));
    }
}
