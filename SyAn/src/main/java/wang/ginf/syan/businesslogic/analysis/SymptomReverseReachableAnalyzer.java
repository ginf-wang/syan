package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.userinterface.controls.SyndromeEditor;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import static wang.ginf.syan.userinterface.controls.SymptomControl.USE_STATUS_BORDER_COLOR;

/**
 * Analyzer, der alle rueckwaerts vom ausgewaehlten Symptom erreichbaren Symptome hervorhebt, also alle,
 * die das ausgewaehlte Symptom vorwaerts erreichen koennen.
 *
 * @author Arne Kiesewetter
 * @author Jonas Luehrs
 */
public class SymptomReverseReachableAnalyzer extends GraphicalAnalyzer<Symptom> {

    private final GraphicalAnalysisAdministrator administrator = GraphicalAnalysisAdministrator.getInstance();
    private SyndromeEditor syndromeEditor;

    /**
     * Methode, die alle rueckwaerts vom uebergebenen Symptom erreichbaren Symptome graphisch hervorhebt.
     *
     * @param target das auszuwertende Symptom.
     */
    @Override
    public void showAnalysis(@NotNull final Symptom target) {
        syndromeEditor = administrator.getSyndromeEditor();
        color(target, getReverseReachableNodes(target));
    }

    /**
     * Methode, die alle rueckwaertserreichbaren Symptome auf allen Wegen zu den selektierten Elementen
     * graphisch hervorhebt.
     *
     * @param root das auszuwertende Symptom.
     * @param targets die Liste der zur Analyse ausgewaehlten Symptome.
     */
    public void showOnlyBetweenAnalysis(@NotNull final Symptom root, @NotNull final List<Symptom> targets) {
        syndromeEditor=administrator.getSyndromeEditor();
        HashSet<Symptom> onlyBetweenReverseReachable = new HashSet<>();

        for(Symptom target : targets) {
            onlyBetweenReverseReachable.addAll(
                    SymptomPathBetweenNodesAnalyzer.getReverseReachableSymptomsInbetween(root, target));
        }

        color(root, onlyBetweenReverseReachable);
    }

    /**
     * Methode, die alle rueckwaerts vom uebergebenen Symptom erreichbaren Symptome graphisch hervorhebt.
     * <p>
     * Der Rand rueckwaertserreichbarer Symptome wird PALEVIOLETRED gefaerbt. Sollte ein Symptomrand im Zuge
     * einer bereits existierenden graphischen Analyse bereits LIGHTGREEN gefaerbt wurden sein, wird nun
     * der Rand YELLOW gefaerbt. Das analysierte Symptom wird mit CORNFLOWERBLUE hervorgehoben.
     * <p>
     * PALEVIOLETRED = rueckwaertserreichbares Symptom.
     * LIGHTGREEN = vorwaertserreichbares Symptom.
     * YELLOW = vorwaerts- und rueckwaertserreichbares Symptom.
     * CORNFLOWERBLUE = analysiertes Symptom.
     *
     * @param root             das auszuwertende Symptom.
     * @param reverseReachable das HashSet der rueckwaertserreichbaren Symptome.
     */
    private void color(@NotNull final Symptom root, @NotNull final HashSet<Symptom> reverseReachable) {
        for (Symptom symptom : reverseReachable) {
            if (syndromeEditor.getSymptomControl(symptom).getBorderColor() == Color.BLACK) {
                syndromeEditor.getSymptomControl(symptom).setBorderColor(Color.PALEVIOLETRED);
            } else if (syndromeEditor.getSymptomControl(symptom).getBorderColor() == Color.LIGHTGREEN) {
                syndromeEditor.getSymptomControl(symptom).setBorderColor(Color.YELLOW);
            }
        }
        syndromeEditor.getSymptomControl(root).setBorderColor(Color.CORNFLOWERBLUE);
    }

    /**
     * Hilfsmethode, die ein HashSet mit allen rueckwaerts vom übergebenen Symptom erreichbaren
     * Symptome zurueckgibt.
     *
     * @param target das auszuwertende Symptom.
     * @return Hashset mit rueckwaertserreichbaren Symptomen.
     */
    public HashSet<Symptom> getReverseReachableNodes(@NotNull final Symptom target) {
        LinkedList<Symptom> nextToVisit = new LinkedList<>();
        HashSet<Symptom> visited = new HashSet<>();
        nextToVisit.add(target);
        while (!nextToVisit.isEmpty()) {
            Symptom symptom = nextToVisit.remove();
            for (Relation relation : symptom.getRelations()) {
                if (symptom != relation.getEndSymptom()) {
                    continue;
                }
                if (visited.contains(relation.getStartSymptom()) || nextToVisit.contains(relation.getStartSymptom())) {
                    continue;
                }
                nextToVisit.add(relation.getStartSymptom());
            }
            visited.add(symptom);
        }
        visited.remove(target);
        return visited;
    }

    @Override
    public void hideAnalysis(@NotNull final Symptom target) {
        for (Symptom symptom : getReverseReachableNodes(target)) {
            syndromeEditor.getSymptomControl(symptom).setBorderColor(USE_STATUS_BORDER_COLOR);
        }
        syndromeEditor.getSymptomControl(target).setBorderColor(USE_STATUS_BORDER_COLOR);
    }
}
