package wang.ginf.syan.businesslogic.analysis;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Symptom;

/**
 * Klasse, die die graphische Analyse einer einzelnen Entitaet verwaltet.
 *
 * @author Jonas Lührs
 */
public class GraphicalAnalysis {

    private final GraphicalAnalysisAdministrator administrator = GraphicalAnalysisAdministrator.getInstance();

    private final BooleanProperty allReachable = new SimpleBooleanProperty(false);
    private final BooleanProperty onlyForwardReachable = new SimpleBooleanProperty(false);
    private final BooleanProperty onlyReverseReachable = new SimpleBooleanProperty(false);

    //OBSN=Only Between Selected Nodes
    private final BooleanProperty allReachableOBSN = new SimpleBooleanProperty(false);
    private final BooleanProperty onlyForwardReachableOBSN = new SimpleBooleanProperty(false);
    private final BooleanProperty onlyReverseReachableOBSN = new SimpleBooleanProperty(false);

    private final SymptomForwardReachableAnalyzer symptomForwardReachableAnalyzer =
            new SymptomForwardReachableAnalyzer();
    private final SymptomReverseReachableAnalyzer symptomReverseReachableAnalyzer =
            new SymptomReverseReachableAnalyzer();
    private final SymptomAllReachableAnalyzer symptomAllReachableAnalyzer = new SymptomAllReachableAnalyzer();

    private Symptom symptom;

    public GraphicalAnalysis() {
        administrator.add(this);
    }

    public Symptom getSymptom() {
        return symptom;
    }

    public void setSymptom(@NotNull final Symptom symptom) {
        this.symptom = symptom;
    }

    public boolean isAllReachable() {
        return allReachable.get();
    }

    public void setAllReachable(@NotNull final Symptom symptom) {
        if (isAllReachable()) {
            symptomAllReachableAnalyzer.showAnalysis(symptom);
        } else {
            symptomAllReachableAnalyzer.hideAnalysis(symptom);
        }
    }

    public BooleanProperty allReachableProperty() { return allReachable; }

    public boolean isOnlyForwardReachable() {
        return onlyForwardReachable.get();
    }

    public void setOnlyForwardReachable(@NotNull final Symptom symptom) {
        if (isOnlyForwardReachable()) {
            symptomForwardReachableAnalyzer.showAnalysis(symptom);
        } else {
            symptomForwardReachableAnalyzer.hideAnalysis(symptom);
        }
    }

    public BooleanProperty onlyForwardReachableProperty() { return onlyForwardReachable; }

    public boolean isOnlyReverseReachable() {
        return onlyReverseReachable.get();
    }

    public void setOnlyReverseReachable(@NotNull final Symptom symptom) {
        if (isOnlyReverseReachable()) {
            symptomReverseReachableAnalyzer.showAnalysis(symptom);
        } else {
            symptomReverseReachableAnalyzer.hideAnalysis(symptom);
        }
    }

    public BooleanProperty onlyReverseReachableProperty() { return onlyReverseReachable; }


    public boolean isAllReachableOBSN() {
        return allReachableOBSN.get();
    }

    public void setAllReachableOBSN(@NotNull final Symptom symptom) {
        if (isAllReachableOBSN()) {
            symptomAllReachableAnalyzer.showAnalysisOnlyBetweenAnalysis(symptom, administrator.getListOfSelectedNodes(symptom));
        } else {
            symptomAllReachableAnalyzer.hideAnalysis(symptom);
        }
    }

    public BooleanProperty allReachableOBSNProperty() { return allReachableOBSN; }

    public boolean isOnlyForwardReachableOBSN() {
        return onlyForwardReachableOBSN.get();
    }

    public void setOnlyForwardReachableOBSN(@NotNull final Symptom symptom) {
        if (isOnlyForwardReachableOBSN()) {
            symptomForwardReachableAnalyzer.showOnlyBetweenAnalysis(symptom, administrator.getListOfSelectedNodes(symptom));
        } else {
            symptomForwardReachableAnalyzer.hideAnalysis(symptom);
        }
    }

    public BooleanProperty onlyForwardReachableOBSNProperty() { return onlyForwardReachableOBSN; }

    public boolean isOnlyReverseReachableOBSN() {
        return onlyReverseReachableOBSN.get();
    }

    public void setOnlyReverseReachableOBSN(@NotNull final Symptom symptom) {
        if (isOnlyReverseReachableOBSN()) {
            symptomReverseReachableAnalyzer.showOnlyBetweenAnalysis(symptom, administrator.getListOfSelectedNodes(symptom));
        } else {
            symptomReverseReachableAnalyzer.hideAnalysis(symptom);
        }
    }

    public BooleanProperty onlyReverseReachableOBSNProperty() { return onlyReverseReachableOBSN; }
}
