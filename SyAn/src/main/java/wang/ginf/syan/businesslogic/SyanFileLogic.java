package wang.ginf.syan.businesslogic;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.hibernate.SessionFactory;
import wang.ginf.syan.model.syndrome.Syndrome;
import wang.ginf.syan.persistence.ExportService;
import wang.ginf.syan.persistence.SyanFileService;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * Die SyanFileLogic dient als logisches Bindeglied zwischen GUI und Persistence. Sie uebernimmt die Pruefung und
 * Ausgestaltung konkreter Dateifunktionalitaeten und stoesst diese dann durch einen SyanFileService an.
 *
 * @author Jan-Luca Kiok
 */
public class SyanFileLogic {
    private final ResourceBundle resources = ResourceBundle.getBundle("lang/FileLogic");

    /**
     * Auffinden des Home Verzeichnisses nach:
     * https://stackoverflow.com/questions/14256588/opening-a-javafx-filechooser-in-the-user-directory
     */
    public static File getHomeDir() {
        final var userDir = System.getProperty("user.home");
        final var userDirectory = new File(userDir);

        if (userDirectory.exists() && userDirectory.isDirectory() && userDirectory.canWrite()) {
            return userDirectory;
        }

        return null;
    }

    /**
     * Oeffnet einen neuen FileChooser und versucht, die Speicherung bzw Exportierung ueber den SyanFileService
     * einzuleiten
     *
     * @param fileType der DateiTyp, der in dem FileChooser gefiltert werden soll.
     * @param owner der Besitzer dieses FileChoosers.
     * @param title der Titel es FileChooser Fensters.
     * @throws AbortedException sollte der FileChooser Dialog abgebrochen werden.
     */
    public File getFile(final String fileType, final Window owner, final String title) throws AbortedException {
        final var fileChooser = new FileChooser();
        fileChooser.setTitle(resources.getString("Title"));
        fileChooser.setInitialDirectory(getHomeDir());
        fileChooser.setInitialFileName(title);

        switch (fileType) {
            case "syan":
                fileChooser.getExtensionFilters()
                           .addAll(new FileChooser.ExtensionFilter(resources.getString("SyanFilter"), "*.syan"));

                break;
            case "pdf":
                fileChooser.getExtensionFilters()
                           .add(new FileChooser.ExtensionFilter("Portable Document Format", "*.pdf"));
                break;
            case "img":
                final var imageFormatSuffixes = ExportService.getImageFormatSuffixes();
                final var imageFilters = fileChooser.getExtensionFilters();

                for (final var imageFormatSuffix : imageFormatSuffixes) {
                    final var filter =
                            new FileChooser.ExtensionFilter(imageFormatSuffix, "*." + imageFormatSuffix);
                    imageFilters.add(filter);

                    if (imageFormatSuffix.equals("png")) {
                        fileChooser.setSelectedExtensionFilter(filter);
                    }
                }
                break;
            case "csv":
                fileChooser.getExtensionFilters()
                           .add(new FileChooser.ExtensionFilter("CSV", "*.csv"));
                break;
            case "syantemplate":
                fileChooser.getExtensionFilters()
                           .add(new FileChooser.ExtensionFilter(resources.getString("SyanTemplate"), "*.syantemplate"));
                break;
            default:
                fileChooser.getExtensionFilters()
                           .add(new FileChooser.ExtensionFilter(resources.getString("DefaultFilter"), "*.*"));
        }

        var file = fileChooser.showSaveDialog(owner);
        if (Objects.isNull(file)) {
            throw new AbortedException();
        }

        boolean addExtension = true;
        for (final var extension : fileChooser.getSelectedExtensionFilter().getExtensions()) {
            final var ext = extension.substring(1);
            if (file.getName().endsWith(ext)) {
                addExtension = false;
                break;
            }
        }
        if (addExtension) {
            file = new File(
                    file.toString() + fileChooser.getSelectedExtensionFilter().getExtensions().get(0).substring(1)
            );
        }

        return file;
    }

    /**
     * Initialisiert einen FileService, der wiederum auf einer gegebenen Datei das gegebene Syndrom speichert
     *
     * @param file           Die zum speichern zu verwendende Datei
     * @param syndrome       Das zu speichernde Syndrom
     * @param sessionFactory Die derzeitge SessionFactory, von Hibernate bereitgestellt
     * @param title          Der derzeitige Anwendungstitel
     * @return Das Ergebnis des Vorgangs
     * @throws IOException wenn das Speichern fehlschlaegt.
     */
    public boolean save(File file, final Syndrome syndrome, final SessionFactory sessionFactory, final String title)
            throws IOException {
        final var fileService = new SyanFileService(file, sessionFactory, title);

        try {
            fileService.save(syndrome);
        } catch (final IOException e) {
            return false;
        }

        return true;
    }

    /**
     * Initialisiert einen Fehler beim Speichern einer Datei
     *
     * @param msg Der konkrete Fehlertext
     */
    public void saveError(final String msg) {
        final var alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(resources.getString("ErrorTitle"));
        alert.setContentText(resources.getString("ErrorContent") + " " + msg);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait().filter(response -> response == ButtonType.OK).ifPresent(response -> alert.close());
    }

    /**
     * Initialisiert einen FileChooser und versucht, die darin gewaehlte Datei zu oeffnen
     *
     * @param owner          Das Ownerfenster zum FileChooser
     * @param sessionFactory Die derzeitge SessionFactory, von Hibernate bereitgestellt
     * @param title          Der derzeitige Anwendungstitel
     * @return Das Ergebnis des Vorgangs als Transferobjekt OpenResult
     * @throws IOException wenn das Oeffnen fehlschlaegt.
     * @throws AbortedException wenn das Oeffnen abgebrochen wird.
     * @throws IllegalFileException wenn die Datei keine anwendungskompatible Datei ist.
     * @throws ClassNotFoundException wenn die serialisierten Datei korrumpiert sind.
     */
    public SyanFileService.OpenResult open(final Window owner, final SessionFactory sessionFactory, final String title)
            throws IOException, AbortedException,
            IllegalFileException, ClassNotFoundException {

        final var fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(getHomeDir());
        fileChooser.getExtensionFilters()
                   .addAll(new FileChooser.ExtensionFilter(resources.getString("SyanFilter"),
                                                           "*.syan",
                                                           "*.syantemplate"),
                           new FileChooser.ExtensionFilter(resources.getString("DefaultFilter"), "*.*"));

        final var file = fileChooser.showOpenDialog(owner);
        if (Objects.isNull(file)) {
            throw new AbortedException();
        }
        if (!file.toString().endsWith(".syan") && !file.toString().endsWith(".syantemplate")) {
            throw new IllegalFileException();
        }

        final var fileService = new SyanFileService(file, sessionFactory, title);
        return fileService.open();
    }
}
