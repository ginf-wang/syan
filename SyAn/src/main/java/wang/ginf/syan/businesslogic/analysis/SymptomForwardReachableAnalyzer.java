package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.userinterface.controls.SyndromeEditor;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import static wang.ginf.syan.userinterface.controls.SymptomControl.USE_STATUS_BORDER_COLOR;

/**
 * Analyzer, der alle vorwaerts vom ausgewaehlten Symptom erreichbaren Symptome hervorhebt.
 *
 * @author Arne Kiesewetter
 * @author Jonas Lührs
 */
public class SymptomForwardReachableAnalyzer extends GraphicalAnalyzer<Symptom> {

    private final GraphicalAnalysisAdministrator administrator = GraphicalAnalysisAdministrator.getInstance();
    private SyndromeEditor syndromeEditor;

    /**
     * Methode, die alle vorwaerts vom uebergebenen Symptom erreichbaren Symptome graphisch hervorhebt.
     *
     * @param target das auszuwertende Symptom.
     */
    @Override
    public void showAnalysis(@NotNull final Symptom target) {
        syndromeEditor = administrator.getSyndromeEditor();
        color(target, getForwardReachableNodes(target));
    }

    /**
     * Methode, die alle vorwaertserreichbaren Symptome auf allen Wegen zu den selektierten Elementen
     * graphisch hervorhebt.
     *
     * @param root    das auszuwertende Symptom.
     * @param targets die Liste der zur Analyse ausgewaehlten Symptome.
     */
    public void showOnlyBetweenAnalysis(@NotNull final Symptom root, @NotNull final List<Symptom> targets) {
        syndromeEditor = administrator.getSyndromeEditor();
        HashSet<Symptom> onlyBetweenForwardReachable = new HashSet<>();

        for (Symptom target : targets) {
            onlyBetweenForwardReachable.addAll(
                    SymptomPathBetweenNodesAnalyzer.getForwardReachableSymptomsInbetween(root, target));
        }

        color(root, onlyBetweenForwardReachable);
    }

    /**
     * Methode, die alle vorwaerts vom uebergebenen Symptom erreichbaren Symptome graphisch hervorhebt.
     * <p>
     * Der Rand vorwaertserreichbarer Symptome wird LIGHTGREEN gefaerbt. Sollte ein Symptomrand im Zuge
     * einer bereits existierenden graphischen Analyse bereits PALEVIOLETRED gefaerbt wurden sein, wird nun
     * der Rand YELLOW gefaerbt. Das analysierte Symptom wird mit CORNFLOWERBLUE hervorgehoben.
     * <p>
     * PALEVIOLETRED = rueckwaertserreichbares Symptom.
     * LIGHTGREEN = vorwaertserreichbares Symptom.
     * YELLOW = vorwaerts- und rueckwaertserreichbares Symptom.
     * CORNFLOWERBLUE = analysiertes Symptom.
     *
     * @param root             das auszuwertende Symptom.
     * @param forwardReachable das HashSet der vorwaertserreichbaren Symptome.
     */
    private void color(@NotNull final Symptom root, @NotNull final HashSet<Symptom> forwardReachable) {
        for (Symptom symptom : forwardReachable) {
            if (syndromeEditor.getSymptomControl(symptom).getBorderColor() == Color.BLACK) {
                syndromeEditor.getSymptomControl(symptom).setBorderColor(Color.LIGHTGREEN);
            } else if (syndromeEditor.getSymptomControl(symptom).getBorderColor() == Color.PALEVIOLETRED) {
                syndromeEditor.getSymptomControl(symptom).setBorderColor(Color.YELLOW);
            }
        }
        syndromeEditor.getSymptomControl(root).setBorderColor(Color.CORNFLOWERBLUE);
    }

    /**
     * Hilfsmethode, die ein HashSet mit allen vorwaerts vom uebergebenen Symptom erreichbaren
     * Symptome zurueckgibt.
     *
     * @param target das auszuwertende Symptom.
     * @return Hashset mit vorwaertserreichbaren Symptomen.
     */
    public HashSet<Symptom> getForwardReachableNodes(@NotNull final Symptom target) {
        LinkedList<Symptom> nextToVisit = new LinkedList<>();
        HashSet<Symptom> visited = new HashSet<>();
        nextToVisit.add(target);
        while (!nextToVisit.isEmpty()) {
            Symptom symptom = nextToVisit.remove();
            for (Relation relation : symptom.getRelations()) {
                if (symptom != relation.getStartSymptom()) {
                    continue;
                }
                if (visited.contains(relation.getEndSymptom()) || nextToVisit.contains(relation.getEndSymptom())) {
                    continue;
                }
                nextToVisit.add(relation.getEndSymptom());
            }
            visited.add(symptom);
        }
        visited.remove(target);
        return visited;
    }

    @Override
    public void hideAnalysis(@NotNull final Symptom target) {
        for (Symptom symptom : getForwardReachableNodes(target)) {
            syndromeEditor.getSymptomControl(symptom).setBorderColor(USE_STATUS_BORDER_COLOR);
        }
        syndromeEditor.getSymptomControl(target).setBorderColor(USE_STATUS_BORDER_COLOR);
    }
}

