package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.RelationType;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;

import static wang.ginf.syan.model.syndrome.RelationType.*;

/**
 * Analyzer, der die Anzahl der Relationen eines Syndroms berechnet.
 *
 * @author Jonas Lührs
 */
public class SyndromeRelationAnalyzer extends ValueAnalyzer<Syndrome, Integer> {

    private final SymptomRelationAnalyzer symptomRelationAnalyzer = new SymptomRelationAnalyzer();

    /**
     * Methode, die die Anzahl der Relationen im Syndrom zurueckgibt.
     * <p>
     * Info: Der Analyzer zählt beim Durchgehen der Symptome die Relationen pro Symptom. Da eine Relation zwei
     * Symptomen zugeordnet ist, erhalten wir als Ergebnis die doppelte Anzahl von Relationen. Aus diesem Grund
     * wird am Ende der Methode die Anzahl der Relationen durch zwei geteilt.
     *
     * @param target das Syndrom..
     * @return die Anzahl der Relationen.
     */
    @Override
    public Integer analyze(@NotNull final Syndrome target) {
        int numOfRelations = 0;
        for (Sphere sphere : target.getSpheres()) {
            for (Symptom symptom : sphere.getSymptoms()) {
                numOfRelations += symptom.getRelations().size();
            }
        }
        return numOfRelations / 2;
    }

    public int getNumberOfSpecialTypeRelations(@NotNull final Syndrome target, @NotNull final RelationType type) {
        int numOfUnknownRelations = 0;
        int numOfLesseningRelations = 0;
        int numOfIncreasingRelations = 0;
        for (Sphere sphere : target.getSpheres()) {
            for (Symptom symptom : sphere.getSymptoms()) {
                numOfUnknownRelations +=
                        symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(symptom, UNKNOWN, "Outgoing") +
                        symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(symptom, UNKNOWN, "Incoming");
                numOfLesseningRelations +=
                        symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(symptom, LESSENING, "Outgoing") +
                        symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(symptom, LESSENING, "Incoming");
                numOfIncreasingRelations +=
                        symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(symptom, INCREASING, "Outgoing") +
                        symptomRelationAnalyzer.getNumberOfSpecialTypeRelations(symptom, INCREASING, "Incoming");
            }
        }
        if (type == UNKNOWN) {
            return numOfUnknownRelations / 2;
        } else if (type == LESSENING) {
            return numOfLesseningRelations / 2;
        } else {
            return numOfIncreasingRelations / 2;
        }
    }
}
