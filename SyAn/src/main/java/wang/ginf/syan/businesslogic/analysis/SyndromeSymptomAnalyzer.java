package wang.ginf.syan.businesslogic.analysis;

import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Syndrome;

/**
 * Analyzer, der die Anzahl der Symptome eines Syndroms berechnet.
 *
 * @author Jonas Lührs
 */
public class SyndromeSymptomAnalyzer extends ValueAnalyzer<Syndrome, Integer> {

    @Override
    public Integer analyze(@NotNull final Syndrome target) {
        int numOfSymptoms = 0;
        for (Sphere sphere : target.getSpheres()) {
            numOfSymptoms += sphere.getSymptoms().size();
        }
        return numOfSymptoms;
    }
}
