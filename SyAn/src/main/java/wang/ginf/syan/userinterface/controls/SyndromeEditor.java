package wang.ginf.syan.userinterface.controls;

import javafx.beans.DefaultProperty;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableSet;
import javafx.collections.SetChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.businesslogic.analysis.GraphicalAnalysisAdministrator;
import wang.ginf.syan.businesslogic.analysis.Statistics;
import wang.ginf.syan.businesslogic.analysis.SyndromeInterconnectivityRateAnalyzer;
import wang.ginf.syan.model.GlobalState;
import wang.ginf.syan.model.history.CommandHistory;
import wang.ginf.syan.model.history.commands.*;
import wang.ginf.syan.model.syndrome.*;
import wang.ginf.syan.persistence.CommandsDatabase;
import wang.ginf.syan.userinterface.util.*;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Custom Control, welches die Anzeige und Bearbeitung von {@see Syndrome}n ermoeglicht.
 *
 * @author Arne Kiesewetter
 * @author Ruben Smidt
 * @author Hannes Masuch
 */
@DefaultProperty("children")
public class SyndromeEditor extends SnapPane implements Initializable, RelationDissolvenceListener {
    private final ObjectProperty<Syndrome> syndrome = new SimpleObjectProperty<>();
    private final ObjectProperty<Status> status = new SimpleObjectProperty<>(Status.WAITING);
    private final BooleanProperty symptomsSelectable = new SimpleBooleanProperty(true);

    private final ObjectProperty<CommandHistory> commandHistory =
            new SimpleObjectProperty<>(CommandsDatabase.createHistoryWithDatabase());

    private final SyndromeInterconnectivityRateAnalyzer interconnectivityRateAnalyzer =
            SyndromeInterconnectivityRateAnalyzer.getInstance();

    private final BooleanBinding isActiveStatus = status.isNotEqualTo(Status.WAITING);

    private final LinkedHashSet<SpherePane> spherePanes = new LinkedHashSet<>();
    private final Statistics stats = Statistics.getInstance();
    private final FanMenu fanMenu = new FanMenu();

    private ResourceBundle bundle;

    /**
     * Erstellt einen neuen Syndrom Editor.
     *
     * @throws IOException wenn das Laden der FXML Datei fehlschlaegt.
     */
    public SyndromeEditor() throws IOException {
        final var loader = new FXMLLoader();

        ResourceHelper.inject(loader, "lang/SyndromeEditor");
        loader.setController(this);
        loader.setRoot(this);
        loader.setLocation(getClass().getResource("/fxml/SyndromeEditor.fxml"));
        loader.load();

        GlobalState.undoRedoAllowedProperty().bind(status.isNotEqualTo(Status.WAITING_FOR_SYMPTOM));

        // we pass these so the collision detection ignores them.
        addIgnoredNodeTypes(RelationControl.class, Circle.class, Canvas.class, ImageView.class);

        setOnContextMenuRequested(event -> fanMenu.show(this, event.getScreenX(), event.getScreenY()));
        setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown()) {
                // RelationHandler resetten sollte einfach irgendwo hingeklickt worden sein.
                if (RelationHandler.isRelationBeingSet()) {
                    setStatus(Status.WAITING);
                }
                if (!(event.getTarget() instanceof SymptomControl) &&
                    !(event.getTarget() instanceof RelationControl) &&
                    getStatus() == Status.DELETE) {
                    setStatus(Status.WAITING);
                }
                fanMenu.hide();
            }
        });
        setOnMouseClicked(event -> {
            if (event.getTarget() == this && event.getButton() == MouseButton.PRIMARY) {
                AttributeEditor.renewWith(null);
            }
        });
        focusedProperty().addListener((obsValue, oldValue, newValue) -> {
            if (!newValue) {
                fanMenu.hide();
            }
        });

        status.addListener(this::statusChanged);

        setMaxSize(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        syndromeProperty().addListener((observable, oldValue, newValue) -> {
            if (Objects.nonNull(oldValue)) {
                oldValue.getRelations().removeListener(this::relationsChanged);
                oldValue.getSpheres().forEach(sphere -> {
                    sphere.getSymptoms().removeListener(this::sphereSymptomsChanged);
                });
            }

            if (Objects.nonNull(newValue)) {
                stats.setSyndromeStatistics(getSyndrome());
                interconnectivityRateAnalyzer.showAnalysis(this);
                newValue.getRelations().addListener(this::relationsChanged);
                newValue.getSpheres().forEach(sphere -> {
                    sphere.getSymptoms().addListener(this::sphereSymptomsChanged);
                });
            }
        });

        GlobalState.analystButNotCreatorProperty().addListener((observable, oldValue, newValue) -> {
            if (Objects.nonNull(newValue)) {
                setStatus(Status.WAITING);
            }
        });

        setEdgePadding(20);
        setBackground(new Background(new BackgroundFill(Color.rgb(240, 240, 242), null, null)));

        GraphicalAnalysisAdministrator.getInstance()
                                      .setSyndromeEditor(this);
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        bundle = resources;

        setupFanMenu();
        syndromeProperty().addListener((observableValue, oldSyndrome, newSyndrome) -> renderSyndrome(newSyndrome));

        RelationHandler.getInstance().registerListener(this);
    }

    private void sphereSymptomsChanged(SetChangeListener.Change<? extends Symptom> change) {
        stats.setSyndromeStatistics(getSyndrome());
        interconnectivityRateAnalyzer.showAnalysis(this);
    }

    /**
     * Durchsucht den Editor nach einem Symptom Control.
     * <p>
     * Es wird erwartet, dass ein Symptom Control gefunden wird, da niemals ein Symptom im Syndrome existieren sollte,
     * das kein korresondierendes UI Element besitzt.
     *
     * @param symptom das Symptom, dessen UI Control gefunden werden soll.
     * @return das UI Control.
     */
    public SymptomControl getSymptomControl(final Symptom symptom) {
        return spherePanes.stream()
                          .map(spherePane -> spherePane.getControlForSymptom(symptom))
                          .filter(Objects::nonNull)
                          .findFirst()
                          .orElseThrow(() -> new IllegalStateException("Could not find symptom control for symptom."));
    }

    private void symptomControlsInSphereChanged(final ListChangeListener.Change<? extends Node> change) {
        while (change.next()) {
            if (change.wasAdded()) {
                change.getAddedSubList()
                      .forEach(node -> node.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                          symptomControlClicked(((SymptomControl) event.getSource()));
                      }));
            }
        }
    }

    private void symptomControlClicked(final SymptomControl symptomControl) {
        if (getStatus() == Status.DELETE && symptomControl.getSymptom().isChangeable()) {
            final var hostingSphere = getSyndrome().getSpheres()
                                                   .stream()
                                                   .filter(sphere -> sphere.containsSymptom(symptomControl.getSymptom()))
                                                   .findFirst()
                                                   .orElseThrow(() -> new IllegalStateException(
                                                           "Symptom in SymptomControl does not have a parent sphere."));
            final Function<Relation, Command> createDeleteRelationCommand
                    = relation -> new DeleteRelationCommand(relation, getSyndrome(), symptomControl.getSymptom());

            symptomControl.getSymptom()
                          .getRelations()
                          .forEach(relation -> getCommandHistory().execute(createDeleteRelationCommand.apply(relation)));

            final Function<Symptom, Command> createDeleteSymptomCommand
                    = symptom -> new DeleteSymptomCommand(symptom, hostingSphere, getSyndrome());

            getCommandHistory().execute(createDeleteSymptomCommand.apply(symptomControl.getSymptom()));
        }
    }

    private void selectRelation(final MouseEvent mouseEvent) {
        if (mouseEvent.getButton() != MouseButton.PRIMARY) {
            return;
        }

        final var relationControl = (RelationControl) mouseEvent.getTarget();

        if (getStatus() == Status.WAITING) {
            AttributeEditor.renewWith(relationControl.getRelation());
        }

        if (getStatus() == Status.DELETE) {
            deleteControl(relationControl);
        }
    }

    private void deleteControl(final RelationControl relationControl) {
        if (getStatus() == Status.DELETE &&
            relationControl.getRelation().isChangeable() &&
            getSyndrome().containsRelation(relationControl.getRelation())) {
            final var deleteCommand =
                    new DeleteRelationCommand(relationControl.getRelation(), getSyndrome(), null);

            getCommandHistory().execute(deleteCommand);
        }
    }

    private void relationsChanged(final SetChangeListener.Change<? extends Relation> change) {
        stats.setSyndromeStatistics(getSyndrome());
        interconnectivityRateAnalyzer.showAnalysis(this);

        if (change.wasAdded()) {
            renderRelation(change.getElementAdded());
        }

        if (change.wasRemoved()) {
            final var relation = change.getElementRemoved();
            final var relationControl = getRelationControl(relation)
                    .orElseThrow(() -> new IllegalStateException("Failed to get relation control for deleted " +
                                                                 "relation"));

            getChildren().remove(relationControl);

            // Sollte die Zone jetzt leer sein, muss das Relationssymbol geloescht werden.
            if (Objects.nonNull(relationControl.getGlyph())) {
                getChildren().remove(relationControl.getGlyph());
            }
        }
    }

    private Optional<RelationControl> getRelationControl(final Relation relation) {
        return getChildren()
                .stream()
                .filter(node -> node instanceof RelationControl &&
                                ((RelationControl) node).getRelation() == relation)
                .map(RelationControl.class::cast)
                .findFirst();
    }

    private void renderRelation(final Relation relation) {
        final var startSymptomControl = getSymptomControl(relation.getStartSymptom());
        final var endSymptomControl = getSymptomControl(relation.getEndSymptom());

        final var relationControl = new RelationControl(this, relation, startSymptomControl, endSymptomControl);
        relationControl.setOnMouseClicked(this::selectRelation);

        getChildren().add(relationControl);

        final var relationGlyph = buildRelationGlyph(relationControl);

        if (Objects.nonNull(relationGlyph)) {
            getChildren().add(relationGlyph);
            relationControl.setGlyph(relationGlyph);
        }
    }

    /**
     * Baut das Relationssymbol, das fuer das Ende einer Relation benutzt wird (Kreis, Dreieck, Fragezeichen),
     * abhaengig von der Seite, an der die Relation endet.
     *
     * @param newRelationControl das UI Control der Relation.
     * @return das Relationssymbol.
     */
    private Node buildRelationGlyph(final RelationControl newRelationControl) {
        final var newRelation = newRelationControl.getRelation();

        if (newRelationControl.getRelation().getRelationType() == RelationType.LESSENING) {
            final var lesseningCircle = new Circle(7.5);
            lesseningCircle.centerXProperty().bind(newRelationControl.endXProperty());

            if (1 <= newRelation.getEndSymptomAttachedSide() && newRelation.getEndSymptomAttachedSide() <= 3) {
                lesseningCircle.layoutYProperty().bind(newRelationControl.endYProperty().add(7.5));
            } else if (newRelation.getEndSymptomAttachedSide() == 0) {
                lesseningCircle.centerXProperty().bind(newRelationControl.endXProperty().add(7.5));
                lesseningCircle.layoutYProperty().bind(newRelationControl.endYProperty());
            } else if (newRelation.getEndSymptomAttachedSide() == 4) {
                lesseningCircle.centerXProperty().bind(newRelationControl.endXProperty().subtract(7.5));
                lesseningCircle.layoutYProperty().bind(newRelationControl.endYProperty());
            } else {
                lesseningCircle.layoutYProperty().bind(newRelationControl.endYProperty().subtract(7.5));
            }

            return lesseningCircle;
        }

        if (newRelationControl.getRelation().getRelationType() == RelationType.UNKNOWN) {
            final var imageView = new ImageView(
                    new Image("/icons/fa-question-transparent.png", 15, 15, true, true)
            );
            imageView.layoutXProperty().bind(newRelationControl.endXProperty().subtract(7.5));
            imageView.layoutYProperty().bind(newRelationControl.endYProperty());

            if (1 <= newRelation.getEndSymptomAttachedSide() && newRelation.getEndSymptomAttachedSide() <= 3) {
                imageView.layoutYProperty().bind(newRelationControl.endYProperty().add(2));
            } else if (newRelation.getEndSymptomAttachedSide() == 0) {
                imageView.layoutXProperty().bind(newRelationControl.endXProperty().add(1));
                imageView.layoutYProperty().bind(newRelationControl.endYProperty().subtract(7.5));
            } else if (newRelation.getEndSymptomAttachedSide() == 4) {
                imageView.layoutXProperty().bind(newRelationControl.endXProperty().subtract(17));
                imageView.layoutYProperty().bind(newRelationControl.endYProperty().subtract(7.5));
            } else {
                imageView.layoutYProperty().bind(newRelationControl.endYProperty().subtract(13));
            }

            return imageView;
        }

        if (newRelationControl.getRelation().getRelationType() == RelationType.INCREASING) {
            final var triangleCanvas = new Canvas(15, 15);
            final var triangleGc = triangleCanvas.getGraphicsContext2D();
            triangleCanvas.layoutXProperty().bind(newRelationControl.endXProperty().subtract(7.5));
            triangleCanvas.layoutYProperty().bind(newRelationControl.endYProperty());

            if (1 <= newRelation.getEndSymptomAttachedSide() && newRelation.getEndSymptomAttachedSide() <= 3) {
                triangleGc.fillPolygon(new double[]{0.0, 7.5, 15.0}, new double[]{0.0, 15.0, 0.0}, 3);
            } else if (newRelation.getEndSymptomAttachedSide() == 0) {
                triangleGc.fillPolygon(new double[]{0.0, 15, 0}, new double[]{15, 7.5, 0.0}, 3);
                triangleCanvas.layoutXProperty().bind(newRelationControl.endXProperty());
                triangleCanvas.layoutYProperty().bind(newRelationControl.endYProperty().subtract(7.5));
            } else if (newRelation.getEndSymptomAttachedSide() == 4) {
                triangleGc.fillPolygon(new double[]{15, 0, 15}, new double[]{15, 7.5, 0.0}, 3);
                triangleCanvas.layoutXProperty().bind(newRelationControl.endXProperty().subtract(15));
                triangleCanvas.layoutYProperty().bind(newRelationControl.endYProperty().subtract(7.5));
            } else {
                triangleCanvas.layoutYProperty().bind(newRelationControl.endYProperty().subtract(15));
                triangleGc.fillPolygon(new double[]{0.0, 7.5, 15.0}, new double[]{15.0, 0.0, 15.0}, 3);
            }

            return triangleCanvas;
        }

        return null;
    }

    private void statusChanged(final ObservableValue<? extends Status> observableValue,
                               final Status oldStatus,
                               final Status newStatus) {
        switch (newStatus) {
            case WAITING:
                symptomsSelectable.set(true);
                RelationHandler.reset();
                setCursor(Cursor.DEFAULT);
                break;

            case ADD_SYMPTOM:
                symptomsSelectable.set(false);
                setCursor(Cursor.CROSSHAIR);
                break;

            case ADD_STRENGTHENING_RELATION:
                setCursor(Cursor.HAND);
                break;

            case ADD_UNKNOWN_RELATION:
                setCursor(Cursor.HAND);
                break;

            case ADD_WEAKENING_RELATION:
                setCursor(Cursor.HAND);
                break;

            case DELETE:
                setCursor(Cursor.CROSSHAIR);
                break;

            case WAITING_FOR_SYMPTOM:
                symptomsSelectable.set(false);
                setCursor(Cursor.DEFAULT);
                break;
        }
    }

    private void setupFanMenu() {
        fanMenu.disabledProperty()
               .bind(
                       GlobalState.analystButNotCreatorProperty()
                                  .or(GlobalState.undoRedoAllowedProperty().not())
                                  .or(syndromeProperty().isNull())
               );

        final var undoIcon = new ImageView("/icons/fa-undo.png");
        undoIcon.setFitWidth(50);
        undoIcon.setFitHeight(50);
        undoIcon.setSmooth(true);

        final var undoItem = new MenuItem(bundle.getString("undoMenuItem"), undoIcon);
        undoItem.setOnAction(action -> getCommandHistory().undo());

        final var addSymptomIcon = new ImageView("/icons/fa-plus-circle.png");
        addSymptomIcon.setFitWidth(100);
        addSymptomIcon.setFitHeight(100);
        addSymptomIcon.setSmooth(true);

        final var addSymptomItem =
                new MenuItem(bundle.getString("addSymptomMenuItem"), addSymptomIcon);
        addSymptomItem.setOnAction(this::startAddSymptom);
        addSymptomItem.disableProperty().bind(
                GlobalState.symptomCreationAllowedProperty().not()
                           .and(GlobalState.atLeastCreatorProperty().not())
        );

        final var strengtheningRelationIcon = new ImageView("/icons/fa-oi-caret-left.png");
        strengtheningRelationIcon.setFitWidth(200);
        strengtheningRelationIcon.setFitHeight(150);
        strengtheningRelationIcon.setSmooth(true);

        final var strengtheningRelationItem =
                new MenuItem(bundle.getString("addStrengthRelMenuItem"), strengtheningRelationIcon);
        strengtheningRelationItem.setOnAction(this::startAddStrengtheningRelation);
        strengtheningRelationItem.disableProperty().bind(
                GlobalState.strengtheningRelationAllowedProperty().not()
                           .and(GlobalState.atLeastCreatorProperty().not())
        );

        final var unknownRelationIcon = new ImageView("/icons/fa-question.png");
        unknownRelationIcon.setFitWidth(100);
        unknownRelationIcon.setFitHeight(100);
        unknownRelationIcon.setSmooth(true);

        final var unknownRelationItem =
                new MenuItem(bundle.getString("addUnknownRelMenuItem"), unknownRelationIcon);
        unknownRelationItem.setOnAction(this::startAddUnknownRelation);
        unknownRelationItem.disableProperty().bind(
                GlobalState.unknownRelationAllowedProperty().not()
                           .and(GlobalState.atLeastCreatorProperty().not())
        );

        final var weakeningRelationIcon = new ImageView("/icons/fa-circle.png");
        weakeningRelationIcon.setFitWidth(50);
        weakeningRelationIcon.setFitHeight(50);
        weakeningRelationIcon.setSmooth(true);

        final var weakeningRelationItem =
                new MenuItem(bundle.getString("addWeakRelMenuItem"), weakeningRelationIcon);
        weakeningRelationItem.setOnAction(this::startAddWeakeningRelation);
        weakeningRelationItem.disableProperty().bind(
                GlobalState.weakeningRelationAllowedProperty().not()
                           .and(GlobalState.atLeastCreatorProperty().not())
        );

        final var deleteIcon = new ImageView("/icons/fa-trash.png");
        deleteIcon.setFitWidth(50);
        deleteIcon.setFitHeight(50);
        deleteIcon.setSmooth(true);

        final var deleteItem = new MenuItem(bundle.getString("deleteMenuItem"), deleteIcon);
        deleteItem.setOnAction(this::startDeletion);

        final var redoIcon = new ImageView("/icons/fa-repeat.png");
        redoIcon.setFitWidth(50);
        redoIcon.setFitHeight(50);
        redoIcon.setSmooth(true);

        final var redoItem = new MenuItem(bundle.getString("redoMenuItem"), redoIcon);
        redoItem.setOnAction(action -> getCommandHistory().redo());

        fanMenu.getItems()
               .addAll(undoItem,
                       addSymptomItem,
                       strengtheningRelationItem,
                       unknownRelationItem,
                       weakeningRelationItem,
                       deleteItem,
                       redoItem);
    }

    private void startAddSymptom(final ActionEvent actionEvent) {
        if (getStatus() == Status.WAITING_FOR_SYMPTOM) {
            return;
        }

        setStatus(Status.ADD_SYMPTOM);
    }

    private void startAddStrengtheningRelation(final ActionEvent actionEvent) {
        status.set(Status.ADD_STRENGTHENING_RELATION);
        RelationHandler.startRelationAddingOfType(RelationType.INCREASING);
    }

    private void startAddUnknownRelation(final ActionEvent actionEvent) {
        status.set(Status.ADD_UNKNOWN_RELATION);
        RelationHandler.startRelationAddingOfType(RelationType.UNKNOWN);
    }

    private void startAddWeakeningRelation(final ActionEvent actionEvent) {
        status.set(Status.ADD_WEAKENING_RELATION);
        RelationHandler.startRelationAddingOfType(RelationType.LESSENING);
    }

    private void startDeletion(final ActionEvent actionEvent) {
        setStatus(Status.DELETE);
    }

    /**
     * Wird aufgerufen sobald die Erstellung einer neuen Relatoin abgeschlossen ist.
     * Hierbei handelt es sich lediglich um die Erstellung einer Relationsentitaet und nicht um ein UI Control.
     * <p>
     * Das Resultat kann entweder die neue Relation enthalten oder einen Fehler, der bei der Erstellung aufgetreten ist.
     *
     * @param result Das Resultat der Erstellung.
     */
    @Override
    public void onRelationDissolvence(final RelationCreationResult result) {
        if (result.getRelation().isEmpty()) {
            final Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(result.getErrorMessage());
            alert.show();
            setStatus(Status.WAITING);
            return;
        }

        final var relation = result.getRelation().get();

        final var newRelationCommand = new AddRelationCommand(relation, getSyndrome());
        getCommandHistory().execute(newRelationCommand);

        setStatus(Status.WAITING);
    }

    private void renderSyndrome(final Syndrome syndrome) {
        syndrome.getSpheres()
                .addListener((SetChangeListener<? super Sphere>) change -> renderSpheres(syndrome.getSpheres()));

        renderSpheres(syndrome.getSpheres());
        syndrome.getRelations().forEach(this::renderRelation);
    }

    private void renderSpheres(final ObservableSet<Sphere> spheres) {
        spherePanes.clear();
        spheres.stream().map(SpherePane::new).forEach(spherePanes::add);
        final var hook = new MoveableResizer.MoveableResizeHook() {
            @Override
            public boolean onDrag(final Region region, final double newX, final double newY) {
                for (final Region pane : spherePanes) {
                    if (pane != region &&
                        pane.getBoundsInParent()
                            .intersects(newX,
                                        newY,
                                        region.getBoundsInParent().getWidth(),
                                        region.getBoundsInParent().getHeight())) {
                        return false;
                    }
                }

                return true;
            }

            @Override
            public boolean onResize(final Region region, final double newWidth, final double newHeight) {
                for (final Region pane : spherePanes) {
                    if (pane != region &&
                        pane.getBoundsInParent()
                            .intersects(region.getLayoutX(), region.getLayoutY(), newWidth, newHeight)) {
                        return false;
                    }
                }

                return true;
            }
        };

        spherePanes.forEach(spherePane -> {
            // SpherePane wird hier schon eingefuegt, damit das getParent im MR nicht null ist.
            getChildren().add(spherePane);

            spherePane.getControls()
                      .addListener(this::symptomControlsInSphereChanged);

            spherePane.getControls()
                      .forEach(symptomControl -> symptomControl.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                          symptomControlClicked(((SymptomControl) event.getSource()));
                      }));

            // Wenn der Nutzer mit dem Fan Menu interagiert, soll das Bewegen und Groesseveraendern
            // deaktiviert werden.
            final var resizer = MoveableResizer.enhance(spherePane);

            // Disabled wenn nicht 'waiting' und wenn im Beobachter und Sphaere nicht veraenderbar.
            final var disabled = isActiveStatus.or(
                    GlobalState.atLeastCreatorProperty()
                               .not()
                               .and(spherePane.getSphere().changeableProperty().not())
                               .or(GlobalState.analystButNotCreatorProperty())
            );

            resizer.movingDisabledProperty().bind(disabled);
            resizer.resizingDisabledProperty().bind(disabled);
            resizer.onMoved((oldX, oldY, newX, newY) -> {
                getCommandHistory().execute(new MoveSphereCommand(spherePane.getSphere(), oldX, oldY, newX, newY));
            });
            resizer.onResize((oldWidth, oldHeight, newWidth, newHeight) -> {
                getCommandHistory().execute(new ResizeSphereCommand(spherePane.getSphere(),
                                                                    oldWidth,
                                                                    oldHeight,
                                                                    newWidth,
                                                                    newHeight));
            });
            resizer.attachHook(hook);

            spherePane.setOnMouseClicked(this::spherePaneClicked);
        });
    }

    private void spherePaneClicked(final MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.SECONDARY && getStatus() != Status.WAITING_FOR_SYMPTOM) {
            setStatus(Status.WAITING);
            return;
        }

        if (mouseEvent.getButton() != MouseButton.PRIMARY) {
            return;
        }

        if (getStatus() == Status.WAITING) {
            AttributeEditor.renewWith(((SpherePane) mouseEvent.getSource()).getSphere());
        }

        if (getStatus() != Status.ADD_SYMPTOM) {
            return;
        }

        mouseEvent.consume();
        setStatus(Status.WAITING_FOR_SYMPTOM);

        final var spherePane = (SpherePane) mouseEvent.getSource();

        final var symptomControl = new SymptomControl("", name -> true, true);
        symptomControl.setTextChangedEventPredicate(getSymptomNamePredicate(spherePane, symptomControl));
        symptomControl.selectableProperty().bind(symptomsSelectable);
        symptomControl.movingDisabledProperty().bind(isActiveStatusBinding());
        symptomControl.setLayoutX(Math.max(0, mouseEvent.getX()));
        symptomControl.setLayoutY(Math.max(0, mouseEvent.getY()));

        spherePane.addSymptomControl(symptomControl);
        symptomControl.requestFocus();
    }

    /**
     * Baut ein Praedikat, das genutzt wird, um zu ueberpruefen, ob ein Symptomname valide ist.
     * <p>
     * Aktualisiert darueber hinaus den Status des Editors.
     *
     * @param symptomControl das SymptomControl, das das Praedikat nutzen soll.
     * @return das Praedikat.
     */
    public Predicate<String> getSymptomNamePredicate(final SymptomControl symptomControl) {
        return newName -> {
            if (newName.isBlank()) {
                symptomControl.setErrorText(bundle.getString("unnamedSymptomRenameError"));
                setStatus(Status.WAITING_FOR_SYMPTOM);
                return false;
            }

            final var renameCommand = new RenameSymptomCommand(getSyndrome(), symptomControl.getSymptom(), newName);
            if (!getCommandHistory().execute(renameCommand)) {
                symptomControl.setErrorText(bundle.getString("duplicateSymptomRenameError"));
                setStatus(Status.WAITING_FOR_SYMPTOM);
                return false;
            }

            setStatus(Status.WAITING);
            return true;
        };
    }

    /**
     * Baut ein Praedikat, das genutzt wird, um zu ueberpruefen, ob ein Symptomname valide ist.
     * <p>
     * Aktualisiert darueber hinaus den Status des Editors und fuehrt ein AddSymptomCommand aus.
     *
     * @param symptomControl das SymptomControl, das das Praedikat nutzen soll.
     * @return das Praedikat.
     */
    public Predicate<String> getSymptomNamePredicate(final SpherePane spherePane,
                                                     final SymptomControl symptomControl) {
        return name -> {
            if (name.isBlank()) {
                symptomControl.setErrorText(bundle.getString("unnamedSymptomCreationError"));
                setStatus(Status.WAITING_FOR_SYMPTOM);
                return false;
            }

            symptomControl.getSymptom().setName(name);
            final var addCommand =
                    new AddSymptomCommand(symptomControl.getSymptom(), spherePane.getSphere(), getSyndrome());

            if (!getCommandHistory().execute(addCommand)) {
                symptomControl.setErrorText(bundle.getString("duplicateSymptomCreationError"));
                setStatus(Status.WAITING_FOR_SYMPTOM);
                return false;
            }

            symptomControl.setTextChangedEventPredicate(getSymptomNamePredicate(symptomControl));

            setStatus(Status.WAITING);
            return true;
        };
    }

    public ObjectProperty<Syndrome> syndromeProperty() { return syndrome; }

    public Syndrome getSyndrome() { return syndrome.get(); }

    public void setSyndrome(@NotNull Syndrome syndrome) { this.syndrome.set(syndrome); }

    public ObjectProperty<CommandHistory> commandHistoryProperty() { return commandHistory; }

    public CommandHistory getCommandHistory() { return commandHistory.get(); }

    public void setCommandHistory(CommandHistory value) { commandHistory.set(value); }

    public void clear() {
        getChildren().clear();
    }

    /**
     * Ueberschreibt den aktuellen Editor mit einem neuen Syndrom.
     *
     * @param syndrome das neue Syndrom.
     */
    public void resetWith(final Syndrome syndrome) {
        clear();
        setSyndrome(syndrome);
    }

    private Status getStatus() { return status.get(); }

    private void setStatus(final Status value) { status.set(value); }

    public boolean isActiveStatus() { return isActiveStatus.get(); }

    public BooleanBinding isActiveStatusBinding() { return isActiveStatus; }

    private enum Status {
        WAITING,
        ADD_SYMPTOM,
        ADD_STRENGTHENING_RELATION,
        ADD_UNKNOWN_RELATION,
        ADD_WEAKENING_RELATION,
        DELETE,
        WAITING_FOR_SYMPTOM,
    }
}
