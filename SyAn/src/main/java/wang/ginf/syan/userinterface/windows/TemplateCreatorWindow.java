package wang.ginf.syan.userinterface.windows;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wang.ginf.syan.model.TemplateCreationResult;
import wang.ginf.syan.userinterface.forms.TemplateCreatorForm;
import wang.ginf.syan.userinterface.util.ResourceHelper;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Fenster das fuer die Konfiguration einer Vorlage verwendet wird.
 *
 * @author Arne Kiesewetter
 * @author Ruben Smidt
 */
public class TemplateCreatorWindow extends Stage implements Initializable {
    @FXML
    private TemplateCreatorForm templateCreator;

    @FXML
    private Scene templateCreatorScene;

    private TemplateCreationResult templateCreationResult;

    /**
     * Erstellt ein neues Fenster fuer die Erstellung einer Vorlage.
     */
    public TemplateCreatorWindow() throws IOException {
        final var fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/TemplateCreatorWindow.fxml"));

        ResourceHelper.inject(fxmlLoader, "lang/TemplateCreatorWindow");
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        initModality(Modality.APPLICATION_MODAL);
        setAlwaysOnTop(true);
        addFormSubmitListener();
        setTitle(resourceBundle.getString("windowTitle"));
        sizeToScene();
        setResizable(false);
        templateCreatorScene.getStylesheets().add(getClass().getResource("/stylesheets/inputs.css").toExternalForm());
    }

    private void addFormSubmitListener() {
        templateCreator.addContentChangeListener(this::sizeToScene);

        templateCreator.addSubmitListener(result -> {
            templateCreationResult = result;
            close();
        });
    }

    /**
     * Zeigt den Dialog und wartet auf das Ergebnis der Erstellung.
     *
     * @return das Ergebnis der Erstellung. Kann {@code null} sein.
     */
    public Optional<TemplateCreationResult> showAndWaitForResult() {
        showAndWait();

        return Optional.ofNullable(templateCreationResult);
    }
}
