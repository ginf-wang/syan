package wang.ginf.syan.userinterface.controls;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.shape.Line;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.syndrome.Relation;

/**
 * @author Ibrahim Apachi  * @author Ruben Smidt
 */
public class RelationControl extends Line {
    private static final double BASE_STROKE_WIDTH = 2;
    private static final double ICON_REMOVE_AMOUNT = 7.5;

    private final SyndromeEditor syndromeEditor;
    private final Relation relation;

    private Node glyph;

    public RelationControl(@NotNull final SyndromeEditor syndromeEditor,
                           @NotNull final Relation relation,
                           @NotNull final SymptomControl startSymptomControl,
                           @NotNull final SymptomControl endSymptomControl) {
        this.syndromeEditor = syndromeEditor;
        this.relation = relation;
        bind(startSymptomControl, endSymptomControl);
    }

    public Node getGlyph() { return glyph; }

    public void setGlyph(final Node glyph) { this.glyph = glyph; }

    private void bind(final SymptomControl startSymptomControl, final SymptomControl endSymptomControl) {
        final var startAttachedSide = relation.getStartSymptomAttachedSide();
        final var endAttachedSide = relation.getEndSymptomAttachedSide();

        bindLayout(startSymptomControl, startXProperty(), startYProperty(), startAttachedSide, false);
        bindLayout(endSymptomControl, endXProperty(), endYProperty(), endAttachedSide, true);

        final var strokeWidthBinding = Bindings.createObjectBinding(
                () -> relation.getRelationStrength().getWidth() + BASE_STROKE_WIDTH,
                relation.relationStrengthProperty()
        );

        strokeWidthProperty().bind(strokeWidthBinding);
        strokeProperty().bind(relation.colorProperty());
    }

    private void bindLayout(@NotNull final SymptomControl symptomControl,
                            @NotNull final DoubleProperty xProperty,
                            @NotNull final DoubleProperty yProperty,
                            final int attachedSide,
                            final boolean includeIcon) {
        var spherePane = symptomControl.getParent().getParent();

        switch (attachedSide) {
            case 0:
                xProperty.bind(Bindings.createDoubleBinding(() -> getSymptomLeftX(symptomControl, includeIcon),
                                                            symptomControl.layoutXProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                yProperty.bind(Bindings.createDoubleBinding(() -> getSymptomMiddleY(symptomControl),
                                                            symptomControl.layoutYProperty(),
                                                            symptomControl.heightProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                break;
            case 7:
                xProperty.bind(Bindings.createDoubleBinding(() -> getSymptomMiddleLeftX(symptomControl),
                                                            symptomControl.layoutXProperty(),
                                                            symptomControl.widthProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                yProperty.bind(Bindings.createDoubleBinding(() -> getSymptomBottomY(symptomControl, includeIcon),
                                                            symptomControl.layoutYProperty(),
                                                            symptomControl.heightProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                break;
            case 2:
                xProperty.bind(Bindings.createDoubleBinding(() -> getSymptomMiddleX(symptomControl),
                                                            symptomControl.layoutXProperty(),
                                                            symptomControl.widthProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                yProperty.bind(Bindings.createDoubleBinding(() -> getSymptomTopY(symptomControl, includeIcon),
                                                            symptomControl.layoutYProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                break;
            case 5:
                xProperty.bind(Bindings.createDoubleBinding(() -> getSymptomMiddleRightX(symptomControl),
                                                            symptomControl.layoutXProperty(),
                                                            symptomControl.widthProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                yProperty.bind(Bindings.createDoubleBinding(() -> getSymptomBottomY(symptomControl, includeIcon),
                                                            symptomControl.layoutYProperty(),
                                                            symptomControl.heightProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                break;
            case 4:
                xProperty.bind(Bindings.createDoubleBinding(() -> getSymptomRightX(symptomControl, includeIcon),
                                                            symptomControl.layoutXProperty(),
                                                            symptomControl.widthProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                yProperty.bind(Bindings.createDoubleBinding(() -> getSymptomMiddleY(symptomControl),
                                                            symptomControl.layoutYProperty(),
                                                            symptomControl.heightProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                break;
            case 3:
                xProperty.bind(Bindings.createDoubleBinding(() -> getSymptomMiddleRightX(symptomControl),
                                                            symptomControl.layoutXProperty(),
                                                            symptomControl.widthProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                yProperty.bind(Bindings.createDoubleBinding(() -> getSymptomTopY(symptomControl, includeIcon),
                                                            symptomControl.layoutYProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                break;
            case 6:
                xProperty.bind(Bindings.createDoubleBinding(() -> getSymptomMiddleX(symptomControl),
                                                            symptomControl.layoutXProperty(),
                                                            symptomControl.widthProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                yProperty.bind(Bindings.createDoubleBinding(() -> getSymptomBottomY(symptomControl, includeIcon),
                                                            symptomControl.layoutYProperty(),
                                                            symptomControl.heightProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                break;
            case 1:
                xProperty.bind(Bindings.createDoubleBinding(() -> getSymptomMiddleLeftX(symptomControl),
                                                            symptomControl.layoutXProperty(),
                                                            symptomControl.widthProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                yProperty.bind(Bindings.createDoubleBinding(() -> getSymptomTopY(symptomControl, includeIcon),
                                                            symptomControl.layoutYProperty(),
                                                            spherePane.layoutXProperty(),
                                                            spherePane.layoutYProperty()));
                break;
            default:
                throw new IllegalArgumentException(String.format("Unsupported side to attach to: %d",
                                                                 attachedSide));
        }
    }

    private double getSymptomTopY(@NotNull final SymptomControl control, final boolean includeIcon) {
        var editorBounds = getBoundsInEditor(control);

        return editorBounds.getMinY() - (includeIcon ? ICON_REMOVE_AMOUNT : 0);
    }

    private double getSymptomMiddleY(@NotNull final SymptomControl control) {
        var editorBounds = getBoundsInEditor(control);

        return editorBounds.getCenterY();
    }

    private double getSymptomBottomY(@NotNull final SymptomControl control, final boolean includeIcon) {
        var editorBounds = getBoundsInEditor(control);

        return editorBounds.getMaxY() + (includeIcon ? ICON_REMOVE_AMOUNT : 0);
    }

    private double getSymptomLeftX(@NotNull final SymptomControl control, final boolean includeIcon) {
        var editorBounds = getBoundsInEditor(control);

        return editorBounds.getMinX() - (includeIcon ? ICON_REMOVE_AMOUNT : 0);
    }

    private double getSymptomMiddleLeftX(@NotNull final SymptomControl control) {
        var editorBounds = getBoundsInEditor(control);

        return editorBounds.getMinX() + 0.3 * editorBounds.getWidth();
    }

    private double getSymptomMiddleX(@NotNull final SymptomControl control) {
        var editorBounds = getBoundsInEditor(control);

        return editorBounds.getCenterX();
    }

    private double getSymptomMiddleRightX(@NotNull final SymptomControl control) {
        var editorBounds = getBoundsInEditor(control);

        return editorBounds.getMinX() + 0.7 * editorBounds.getWidth();
    }

    private double getSymptomRightX(@NotNull final SymptomControl control, final boolean includeIcon) {
        var editorBounds = getBoundsInEditor(control);

        return editorBounds.getMaxX() + (includeIcon ? ICON_REMOVE_AMOUNT : 0);
    }

    private Bounds getBoundsInEditor(@NotNull final SymptomControl control) {
        var bounds = control.getBoundsInLocal();
        Parent currentElement = control;
        while (currentElement != syndromeEditor) {
            bounds = currentElement.getLocalToParentTransform().transform(bounds);
            currentElement = currentElement.getParent();
        }

        return bounds;
    }

    public Relation getRelation() { return relation; }
}

