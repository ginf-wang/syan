package wang.ginf.syan.userinterface.windows;

import javafx.scene.control.Alert;
import javafx.scene.layout.Region;

/**
 * Klasse zum vereinfachten Handling von Exceptions im GUI
 *
 * @author Jan-Luca Kiok
 */
public class SyanAlert extends Alert {

    /**
     * Erzeugt einen Alert mit passendem Typ, Titel und Content
     *
     * @param alertType  an alert with the given AlertType
     * @param title      Der Fenstertitel
     * @param contentMsg Der Inhalt der eigentlichen Warnung
     */
    public SyanAlert(final AlertType alertType, final String title, final String contentMsg) {
        super(alertType);
        setTitle(title);
        setContentText(contentMsg);
        getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
    }
}
