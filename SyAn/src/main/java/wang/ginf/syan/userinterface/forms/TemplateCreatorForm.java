package wang.ginf.syan.userinterface.forms;

import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import wang.ginf.syan.model.Settings;
import wang.ginf.syan.model.TemplateCreationResult;
import wang.ginf.syan.model.TemplateCreationResultBuilder;
import wang.ginf.syan.userinterface.util.ResourceHelper;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Die TemplateCreatorForm ist das zentrale Formular,
 * das die notwendigen Daten zur Initialisierung einer Vorlage sammelt.
 *
 * @author Ruben Smidt
 */
public class TemplateCreatorForm extends BorderPane implements Initializable {
    private static final int MIN_SPHERE_COUNT = 3;
    private static final int MAX_SPHERE_COUNT = 12;
    private static final int DEFAULT_GRID_CONTROLS_AMOUNT = 2;
    private static final String DYNAMIC_SPHERE_LABEL = "dynamicSphereLabel";

    private ResourceBundle bundle;

    @FXML
    private TextField nameField;

    @FXML
    private GridPane formGridPane;

    @FXML
    private Button submitButton;

    @FXML
    private Spinner<Integer> numberSpinner;

    @FXML
    private CheckBox weakeningRelationCheckbox;

    @FXML
    private CheckBox unknownRelationCheckbox;

    @FXML
    private CheckBox strengtheningRelationCheckbox;

    @FXML
    private CheckBox symptomCreationCheckbox;

    @FXML
    private VBox content;

    private boolean duplicateLabelVisible;

    public TemplateCreatorForm() throws IOException {
        final var fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/TemplateCreatorForm.fxml"));

        ResourceHelper.inject(fxmlLoader, "lang/TemplateCreatorForm");
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        bundle = resourceBundle;

        setUpSpinner();
        setUpInitialSphereFields();
        handleChangeOfSphereCount();

        Settings.registerNodes(this);
    }

    private void setUpSpinner() {
        final var factory = new SpinnerValueFactory.IntegerSpinnerValueFactory(MIN_SPHERE_COUNT, MAX_SPHERE_COUNT);

        numberSpinner.setValueFactory(factory);
    }

    private void setUpInitialSphereFields() {
        IntStream.range(0, MIN_SPHERE_COUNT).forEach(this::addSphereRow);
    }

    private void addSphereRow(final int index) {
        final var labelText = String.format(bundle.getString(DYNAMIC_SPHERE_LABEL), index + 1);
        final var label = new Label(labelText);
        final var field = new TextField();
        field.setId("sphereField" + index);

        formGridPane.addRow(index + DEFAULT_GRID_CONTROLS_AMOUNT, label, field);
    }

    private void handleChangeOfSphereCount() {
        numberSpinner.valueProperty().addListener((observable, oldValue, newValue) -> {
            testForSphereCountChanges(oldValue, newValue);
        });
    }

    private void testForSphereCountChanges(final int oldCount, final int newCount) {
        if (oldCount == newCount) {
            return;
        }

        if (newCount > oldCount) {
            IntStream.range(oldCount, newCount).forEach(this::addSphereRow);
        }

        removeSphereRowsTo(newCount);
    }

    /**
     * Entfernt alle Sphaerennamenfelder bis zu einem bestimmten Grenzwert.
     *
     * @param margin der Grenzwert.
     */
    private void removeSphereRowsTo(final int margin) {
        // https://stackoverflow.com/questions/23002532/javafx-2-how-do-i-delete-a-row-or-column-in-gridpane
        final Predicate<Node> greaterIndexThanNewCount = node -> {
            if (Objects.isNull(GridPane.getRowIndex(node))) {
                return false;
            }

            return GridPane.getRowIndex(node).compareTo(margin + 1) > 0;
        };

        formGridPane.getChildren().removeIf(greaterIndexThanNewCount);
    }

    /**
     * Erlaubt es dem Nutzer dieser Komponente einen individuellen Event-Callback
     * zu nutzen. Dieser wird aufgerufen, sobald das Formular bestaetigt wurde.
     *
     * @param consumer der Callback, dem die Vorlagendaten uebergeben werden.
     */
    public void addSubmitListener(final Consumer<TemplateCreationResult> consumer) {
        this.submitButton.setOnAction(actionEvent -> {
            if (!testValidity()) {
                return;
            }

            final var template = new TemplateCreationResultBuilder()
                    .setName(nameField.getText())
                    .setCreationOfStrengtheningRelationAllowed(strengtheningRelationCheckbox.isSelected())
                    .setCreationOfUnknownRelationAllowed(unknownRelationCheckbox.isSelected())
                    .setCreationOfWeakeningRelationAllowed(weakeningRelationCheckbox.isSelected())
                    .setCreationOfSymptomsAllowed(symptomCreationCheckbox.isSelected())
                    .setUniqueSphereNames(getUniqueSphereNames())
                    .createTemplateCreationResult();

            consumer.accept(template);
        });
    }

    private Set<String> getUniqueSphereNames() {
        return getTextFields().stream()
                              .map(TextInputControl::getText)
                              .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private boolean testValidity() {
        final var textFields = getTextFields();
        final var uniqueNames = new HashSet<>();

        var isValid = new AtomicBoolean(true);
        var hasDuplicates = new AtomicBoolean(false);

        if (nameField.getText().isEmpty()) {
            nameField.setPromptText(bundle.getString("emptyFieldLabel"));
            isValid.set(false);
        }

        textFields.forEach(currentField -> {
            if (currentField.getText().isEmpty()) {
                isValid.set(false);
                currentField.setPromptText(bundle.getString("emptyFieldLabel"));

                return;
            }

            if (!uniqueNames.add(currentField.getText())) {
                hasDuplicates.set(true);
                currentField.getStyleClass().add("error");
            } else {
                currentField.getStyleClass().removeAll(Collections.singletonList("error"));
            }
        });

        if (hasDuplicates.get() && !duplicateLabelVisible) {
            final var label = new Label(bundle.getString("duplicateLabel"));

            label.setId("duplicateErrorLabel");
            content.getChildren().add(content.getChildren().size() - 1, label);
            duplicateLabelVisible = true;
        } else if (!hasDuplicates.get() && duplicateLabelVisible) {
            content.getChildren()
                   .removeIf(node -> Objects.nonNull(node.getId()) && node.getId().equals("duplicateErrorLabel"));
            duplicateLabelVisible = false;
        }

        return isValid.get() && !hasDuplicates.get();
    }

    /**
     * Findet alle Textfelder der Form.
     *
     * @return die Textfelder.
     */
    private List<TextField> getTextFields() {
        final Predicate<Node> isSphereTextField =
                node -> Objects.nonNull(node.getId()) && node.getId().startsWith("sphereField");

        return formGridPane.getChildren()
                           .stream()
                           .filter(isSphereTextField)
                           .map(TextField.class::cast)
                           .collect(Collectors.toList());
    }

    /**
     * Ermoeglicht das Anhaengen von Aktionen, die gefeuert werden,
     * sobald sich der konkrete, relevante Inhalt dieser Form aendert.
     *
     * @param runnable die auszufuehrende Aktion.
     */
    public void addContentChangeListener(final Runnable runnable) {
        formGridPane.getChildren().addListener((ListChangeListener<? super Node>) change -> runnable.run());
        content.getChildren().addListener((ListChangeListener<? super Node>) change -> runnable.run());
    }
}
