package wang.ginf.syan.userinterface.controls;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SnapPane extends Pane {
    private final Set<Class<? extends Node>> ignoredNodeTypes = new HashSet<>();
    private final SimpleDoubleProperty edgePadding = new SimpleDoubleProperty(this, "edgePadding", 5);

    @SafeVarargs
    final void addIgnoredNodeTypes(final Class<? extends Node>... ignoredNodeTypes) {
        this.ignoredNodeTypes.addAll(Arrays.asList(ignoredNodeTypes));
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();

        final var nodesWithOutRelations = getChildren()
                .filtered(node -> ignoredNodeTypes.stream()
                                                  .noneMatch(ignoredNodeType -> ignoredNodeType.isInstance(node)));

        for (var node : nodesWithOutRelations) {
            final var nodeBounds = node.getBoundsInParent();

            if (nodeBounds.getMinX() < getEdgePadding()) {
                node.setLayoutX(getEdgePadding());
            }

            final var xDif = nodeBounds.getMaxX() - getMaxWidth();
            if (xDif > -getEdgePadding()) {
                node.setLayoutX(Math.max(getEdgePadding(), node.getLayoutX() - xDif - getEdgePadding()));
            }

            if (nodeBounds.getMinY() < getEdgePadding()) {
                node.setLayoutY(getEdgePadding());
            }

            final var yDif = nodeBounds.getMaxY() - getMaxHeight();
            if (yDif > -getEdgePadding()) {
                node.setLayoutY(Math.max(getEdgePadding(), node.getLayoutY() - yDif - getEdgePadding()));
            }
        }

        for (var node : nodesWithOutRelations) {
            for (var possibleCollision : nodesWithOutRelations) {
                if (node == possibleCollision ||
                    !node.getBoundsInParent().intersects(possibleCollision.getBoundsInParent())) {
                    continue;
                }

                final var nodeBounds = node.getBoundsInParent();
                final var collisionBounds = possibleCollision.getBoundsInParent();

                final var xCenterDif = nodeBounds.getCenterX() - collisionBounds.getCenterX(); // >0 -> node is right
                final var xCenterTargetDistance = (nodeBounds.getWidth() + collisionBounds.getWidth()) / 2;
                final var xShift = (xCenterTargetDistance - Math.abs(xCenterDif) + 2) / 2;

                final var yCenterDif = nodeBounds.getCenterY() - collisionBounds.getCenterY(); // >0 -> node is low
                final var yCenterTargetDistance = (nodeBounds.getHeight() + collisionBounds.getHeight()) / 2;
                final var yShift = (yCenterTargetDistance - Math.abs(yCenterDif) + 2) / 2;

                if (Math.abs(xCenterDif) < xCenterTargetDistance && xShift <= yShift) {
                    if (xCenterDif <= 0) {
                        node.setLayoutX(node.getLayoutX() - xShift);
                        possibleCollision.setLayoutX(possibleCollision.getLayoutX() + xShift);
                    } else {
                        node.setLayoutX(node.getLayoutX() + xShift);
                        possibleCollision.setLayoutX(possibleCollision.getLayoutX() - xShift);
                    }
                }

                if (Math.abs(yCenterDif) < yCenterTargetDistance && yShift <= xShift) {
                    if (yCenterDif <= 0) {
                        node.setLayoutY(node.getLayoutY() - yShift);
                        possibleCollision.setLayoutY(possibleCollision.getLayoutY() + yShift);
                    } else {
                        node.setLayoutY(node.getLayoutY() + yShift);
                        possibleCollision.setLayoutY(possibleCollision.getLayoutY() - yShift);
                    }
                }
            }
        }

        setPrefWidth(getChildren().stream().mapToDouble(n -> n.getBoundsInParent().getMaxX()).max().orElse(0) +
                     getEdgePadding());
        setPrefHeight(getChildren().stream().mapToDouble(n -> n.getBoundsInParent().getMaxY()).max().orElse(0) +
                      getEdgePadding());
    }

    public DoubleProperty edgePaddingProperty() { return edgePadding; }

    public double getEdgePadding() { return edgePadding.get(); }

    public void setEdgePadding(final double value) { edgePadding.set(value); }
}
