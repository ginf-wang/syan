package wang.ginf.syan.userinterface.controls;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.SetChangeListener;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import wang.ginf.syan.model.GlobalState;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Ein SpherePane stellt eine Sphaere des Syndroms dar.
 *
 * @author Ruben Smidt
 */
public class SpherePane extends StackPane implements Initializable {
    private static final int DEFAULT_RADIUS = 10;
    private final Sphere sphere;
    private final ObservableList<SymptomControl> symptomControls = FXCollections.observableArrayList();

    /**
     * Pane fuer das Darstellen der Symptome einer Sphaere.
     */
    private final Pane symptomPane;

    public SpherePane(final Sphere sphere) {
        super();
        this.sphere = sphere;

        layoutXProperty().bindBidirectional(sphere.layoutXProperty());
        layoutYProperty().bindBidirectional(sphere.layoutYProperty());

        prefWidthProperty().bindBidirectional(sphere.widthProperty());
        prefHeightProperty().bindBidirectional(sphere.heightProperty());

        final var sphereColorBackgroundBinding = Bindings.createObjectBinding(
                () -> new Background(new BackgroundFill(sphere.getColor(),
                                                        new CornerRadii(DEFAULT_RADIUS),
                                                        Insets.EMPTY)),
                sphere.colorProperty()
        );

        setBorder(new Border(new BorderStroke(Color.BLACK,
                                              BorderStrokeStyle.SOLID,
                                              new CornerRadii(DEFAULT_RADIUS),
                                              BorderWidths.DEFAULT)));

        backgroundProperty().bind(sphereColorBackgroundBinding);
        backgroundProperty().addListener(((observable, oldValue, newValue) -> requestParentLayout()));

        setAlignment(Pos.TOP_CENTER);

        final var sphereName = new Label(sphere.getName());
        sphereName.setPickOnBounds(false);
        sphereName.paddingProperty().set(new Insets(20, 0, 20, 0));
        getChildren().add(sphereName);

        // Abhaengig von der Helligkeit der Sphaere wird die Schriftfarbe des Labels geandert.
        final var sphereNameTextFillBinding = Bindings.createObjectBinding(
                () -> sphere.getColor().getBrightness() < 0.7 ? Color.WHITE : Color.BLACK,
                sphere.colorProperty()
        );

        sphereName.textFillProperty().bind(sphereNameTextFillBinding);

        symptomPane = new SnapPane();
        symptomPane.setPickOnBounds(false);

        final var minWidthBinding =
                Bindings.createDoubleBinding(() -> Math.max(Math.max(getPrefWidth(),
                                                                     sphereName.getBoundsInParent().getMaxX()),
                                                            symptomPane.getPrefWidth()),
                                             prefWidthProperty(),
                                             symptomPane.prefWidthProperty());
        final var minHeightBinding =
                Bindings.createDoubleBinding(() -> Math.max(Math.max(getPrefHeight(),
                                                                     sphereName.getBoundsInParent().getMaxY()),
                                                            symptomPane.getPrefHeight()),
                                             prefHeightProperty(),
                                             symptomPane.prefHeightProperty());

        symptomPane.minWidthProperty().bind(minWidthBinding);
        symptomPane.maxWidthProperty().bind(minWidthBinding);
        symptomPane.minHeightProperty().bind(minHeightBinding);
        symptomPane.maxHeightProperty().bind(minHeightBinding);

        minWidthProperty().bind(minWidthBinding);
        maxWidthProperty().bind(minWidthBinding);
        minHeightProperty().bind(minHeightBinding);
        maxHeightProperty().bind(minHeightBinding);

        getChildren().add(symptomPane);
        StackPane.setAlignment(symptomPane, Pos.TOP_CENTER);

        sphere.getSymptoms().addListener(this::symptomsChanged);

        // Wir wollen die initialen Symptome der Sphaere erst dann rendern,
        // wenn das SpherePane im SyndromeEditor liegt.
        final ChangeListener<Boolean> parentNotNullListener = (observable, oldValue, newValue) -> {
            if (newValue) {
                sphere.getSymptoms().forEach(this::addSymptom);
            }
        };

        parentProperty().isNotNull().addListener(parentNotNullListener);

        final var resizeZoneColorBinding = Bindings.createObjectBinding(
                () -> sphereNameTextFillBinding.get().deriveColor(1, 1, 1, 0.5),
                sphereNameTextFillBinding
        );

        final var resizeLine = new Line(0, 20, 20, 0);
        resizeLine.strokeProperty().bind(resizeZoneColorBinding);
        StackPane.setAlignment(resizeLine, Pos.BOTTOM_RIGHT);

        resizeLine.visibleProperty().bind(
                GlobalState.atLeastCreatorProperty()
                           .or(sphere.changeableProperty())
                           .and(GlobalState.analystButNotCreatorProperty().not())
        );

        getChildren().add(resizeLine);
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        throw new UnsupportedOperationException();
    }

    private synchronized void symptomsChanged(final SetChangeListener.Change<? extends Symptom> change) {
        if (change.wasAdded()) {
            addSymptom(change.getElementAdded());
        }

        if (change.wasRemoved()) {
            removeSymptom(change.getElementRemoved());
        }
    }

    private void addSymptom(final Symptom symptom) {
        if (getControlForSymptom(symptom) != null) {
            return;
        }

        final var syndromeEditor = (SyndromeEditor) getParent();
        final var newSymptomControl = new SymptomControl(symptom, n -> true, false);
        newSymptomControl.setTextChangedEventPredicate(
                syndromeEditor.getSymptomNamePredicate(newSymptomControl)
        );

        newSymptomControl.movingDisabledProperty().bind(syndromeEditor.isActiveStatusBinding());
        newSymptomControl.setRelationsAllowed(true);
        symptomControls.add(newSymptomControl);
        symptomPane.getChildren().add(newSymptomControl);
    }

    private void removeSymptom(final Symptom symptom) {
        symptomControls.removeIf(node -> {
            if (node.getSymptom().equals(symptom)) {
                symptom.layoutXProperty().unbind();
                symptom.layoutYProperty().unbind();

                symptomPane.getChildren().remove(node);
                return true;
            }

            return false;
        });
    }

    /**
     * Durchsucht die SymptomControls der Sphaere nach einem bestimmten Symptom.
     * <p>
     * Gibt null zurueck, sollte das SymptomControl nicht gefunden werden.
     *
     * @param symptom das Symptom, dessen UI Control gefunden werden soll.
     * @return das SymptomControl.
     */
    public SymptomControl getControlForSymptom(final Symptom symptom) {
        return symptomControls.stream()
                              .filter(node -> node.getSymptom().equals(symptom))
                              .findFirst()
                              .orElse(null);
    }

    /**
     * Fuegt ein neues SymptomControl der Sphaere hinzu.
     *
     * @param symptomControl
     */
    public void addSymptomControl(final SymptomControl symptomControl) {
        if (!symptomControls.contains(symptomControl)) {
            symptomControls.addAll(symptomControl);
            symptomPane.getChildren().add(symptomControl);
        }
    }

    /**
     * Gibt alle UI Controls eines Symptoms dieser Sphaere zurueck.
     *
     * @return die Liste an UI Controls.
     */
    public ObservableList<SymptomControl> getControls() {
        return symptomControls;
    }

    /**
     * Gibt die zugrunde liegende Sphaere dieser UI Control zurueck.
     *
     * @return die Sphaere.
     */
    public Sphere getSphere() { return sphere; }
}
