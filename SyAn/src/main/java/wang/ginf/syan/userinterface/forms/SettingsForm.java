package wang.ginf.syan.userinterface.forms;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import wang.ginf.syan.model.Settings;
import wang.ginf.syan.persistence.LanguageService;
import wang.ginf.syan.userinterface.util.ResourceHelper;
import wang.ginf.syan.userinterface.windows.SyanAlert;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Form das fuer die Einstellungen verwendet wird.
 *
 * @author Ibrahim Apachi
 * @author Ruben Smidt
 */
public class SettingsForm extends VBox implements Initializable {
    private final Settings settings = Settings.getInstance();
    private final LanguageService languageService = LanguageService.getInstance();

    @FXML
    private Spinner<Integer> settingsFontSizeSpinner;

    @FXML
    private ComboBox<Locale> languageComboBox;

    @FXML
    private Label languageLabel;

    public SettingsForm() throws IOException {
        final var fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SettingsForm.fxml"));

        ResourceHelper.inject(fxmlLoader, "lang/SettingsForm");
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        addFontSizeTextFieldListener();
        setUpFontSizeSpinner();

        languageComboBox.getItems().addAll(Settings.SUPPORTED_LOCALES);

        languageComboBox.setValue(
                Settings.isDefaultLocaleSupported() ? Locale.getDefault() : Locale.GERMAN
        );

        languageComboBox.setOnAction(event -> {
            languageLabel.setVisible(true);
            languageLabel.setManaged(true);

            if (!languageService.saveLocale(languageComboBox.getValue())) {
                final var alert = new SyanAlert(
                        Alert.AlertType.ERROR,
                        resourceBundle.getString("writeFailedTitle"),
                        resourceBundle.getString("writeFailedContent")
                );

                alert.showAndWait();
            }
        });

        Settings.registerNodes(this);
    }

    private void setUpFontSizeSpinner() {
        final var factory = new SpinnerValueFactory.IntegerSpinnerValueFactory(
                Settings.MIN_FONT_SIZE, Settings.MAX_FONT_SIZE, Settings.getFontSize()
        );

        settingsFontSizeSpinner.setValueFactory(factory);
    }

    private void addFontSizeTextFieldListener() {
        settingsFontSizeSpinner.valueProperty()
                               .addListener((observable, oldValue, newValue) -> settings.setFontSize(newValue));
    }
}
