package wang.ginf.syan.userinterface.controls;

import javafx.beans.Observable;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Skin;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import wang.ginf.syan.NotNull;

/**
 * Rundes Context Menu, aehnlich einem Faecher.
 *
 * @author Arne Kiesewetter
 */
public class FanMenu extends ContextMenu implements Skin<FanMenu> {
    private static final double MOUSEOVER_OPACITY = 1;
    private static final double MOUSEEXITED_OPACITY = 0.5;
    private static final double DISABLED_OPACITY = 0.1;
    private final SimpleDoubleProperty outerRadius = new SimpleDoubleProperty(this, "outerRadius", 100);
    private final SimpleDoubleProperty innerRadius = new SimpleDoubleProperty(this, "innerRadius", 35);
    private final SimpleDoubleProperty radialSpan = new SimpleDoubleProperty(this, "radialSpan", 270);
    private final SimpleBooleanProperty disabled = new SimpleBooleanProperty(this, "disabled", false);
    private StackPane skin = new StackPane();
    private Canvas canvas = new Canvas();
    private Pane itemOverlay = new Pane();

    /**
     * Erstellt ein neues Fan Menu.
     *
     * @param menuItems die optionalen anfaenglichen Menueeintraege.
     */
    public FanMenu(final MenuItem... menuItems) {
        super(menuItems);

        skin.getChildren().addAll(canvas, itemOverlay);

        outerRadius.addListener(this::parameterChanged);
        innerRadius.addListener(this::parameterChanged);
        radialSpan.addListener(this::parameterChanged);
        getItems().addListener(this::itemsChanged);

        layoutFan();
        setSkin(this);
    }

    public boolean isDisabled() {
        return disabled.get();
    }

    public void setDisabled(final boolean disabled) {
        this.disabled.set(disabled);
    }

    public SimpleBooleanProperty disabledProperty() {
        return disabled;
    }

    private void parameterChanged(ObservableValue<? extends Number> obsValue, Number oldValue, Number newValue) {
        layoutFan();
    }

    private void itemsChanged(final Observable observable) {
        layoutFan();
    }

    private void layoutFan() {
        drawSemiRing();
        addItems();
    }

    private void drawSemiRing() {
        canvas.setWidth(2 * getCenter());
        canvas.setHeight(2 * getCenter());

        final var gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gc.setStroke(Color.LIGHTGRAY);
        gc.setFill(Color.WHITESMOKE);

        // positive angle = counter-clockwise
        gc.beginPath();
        gc.arc(getCenter(), getCenter(), getOuterRadius(), getOuterRadius(), getStartAngle(), getRadialSpan());
        gc.arc(getCenter(), getCenter(), getInnerRadius(), getInnerRadius(), getEndAngle(), -getRadialSpan());
        gc.closePath();

        gc.fill();
        gc.stroke();
    }

    private void addItems() {
        itemOverlay.getChildren().clear();
        final var items = getItems();

        // radian math is 180° shifted to arc math
        final var step = Math.toRadians(getRadialSpan()) / items.size();
        final var start = Math.toRadians(180 + getStartAngle()) + 0.5 * step;
        final var width = getOuterRadius() - getInnerRadius();
        final var radius = getInnerRadius() + 0.5 * width;
        final var stepWidth = step * radius;

        for (var i = 0; i < items.size(); ++i) {
            itemOverlay.getChildren()
                       .add(setupItem(items.get(i), radius, Math.min(0.7 * width, 0.9 * stepWidth), start + i * step));
        }
    }

    private Node setupItem(@NotNull MenuItem item, double radius, double width, double radialPosition) {
        final var graphic = item.getGraphic();

        final EventHandler<MouseEvent> itemMouseEntered = (MouseEvent event) -> {
            if (!item.isDisable()) {
                graphic.setOpacity(MOUSEOVER_OPACITY);
            }
        };

        final EventHandler<MouseEvent> itemMouseExited = (MouseEvent event) -> {
            if (!item.isDisable()) {
                graphic.setOpacity(MOUSEEXITED_OPACITY);
            }
        };

        item.disableProperty().addListener((observable, oldValue, newValue) -> {
            graphic.setOpacity(newValue ? DISABLED_OPACITY : MOUSEEXITED_OPACITY);
        });

        graphic.setOpacity(item.isDisable() ? DISABLED_OPACITY : MOUSEEXITED_OPACITY);
        graphic.setOnMouseEntered(itemMouseEntered);
        graphic.setOnMouseExited(itemMouseExited);
        graphic.setOnMouseClicked(event -> {
            hide();
            if (!item.isDisable()) {
                item.fire();
            }
        });

        final var tp = new Tooltip(item.getText());
        tp.setShowDelay(Duration.millis(600.0));
        Tooltip.install(graphic, tp);

        final var graphicDiagonal = Math.sqrt(Math.pow(graphic.getLayoutBounds().getWidth(), 2) +
                                              Math.pow(graphic.getLayoutBounds().getHeight(), 2));
        final var scale = width / graphicDiagonal;

        graphic.setScaleX(scale);
        graphic.setScaleY(scale);

        graphic.setTranslateX(radius * Math.cos(radialPosition) + getCenter() -
                              0.5 * graphic.getLayoutBounds().getWidth());
        graphic.setTranslateY(radius * Math.sin(radialPosition) + getCenter() -
                              0.5 * graphic.getLayoutBounds().getHeight());

        return graphic;
    }

    private double getCenter() {
        return getOuterRadius() + 1;
    }

    private double getStartAngle() {
        return -90 + 0.5 * (360 - getRadialSpan());
    }

    private double getEndAngle() {
        return -90 - 0.5 * (360 - getRadialSpan());
    }

    @Override
    public void show(final Node anchor, final double screenX, final double screenY) {
        if (isDisabled()) {
            return;
        }

        super.show(anchor, screenX - (canvas.getWidth() / 2), screenY - (canvas.getHeight() / 2));
    }

    /**
     * Gibt die Außenradiusproperty zurueck. Wert in Pixeln.
     *
     * @return Die Außenradiusproperty.
     */
    public SimpleDoubleProperty outerRadiusProperty() { return outerRadius; }

    /**
     * Gibt den Außenradius in Pixeln zurueck.
     *
     * @return Der Außenradius in Pixeln.
     */
    public double getOuterRadius() { return outerRadius.get(); }

    /**
     * Setzt den Außenradius in Pixeln.
     *
     * @param value Der neue Außenradius in Pixeln.
     */
    public void setOuterRadius(double value) { outerRadius.set(value); }

    /**
     * Gibt die Innenradiusproperty zurueck. Wert in Pixeln.
     *
     * @return Die Innenradiusproperty.
     */
    public SimpleDoubleProperty innerRadiusProperty() { return innerRadius; }

    /**
     * Gibt den Innenradius in Pixeln zurueck.
     *
     * @return Der Innenradius in Pixeln.
     */
    public double getInnerRadius() { return innerRadius.get(); }

    /**
     * Setzt den Innenradius in Pixeln.
     *
     * @param value Der neue Innenradius in Pixeln.
     */
    public void setInnerRadius(double value) { innerRadius.set(value); }

    /**
     * Gibt die Property fuer die Spanne des Fans in Grad [0-360] zurueck.
     *
     * @return Die Property fuer die Spanne des Fans in Grad [0-360].
     */
    public SimpleDoubleProperty radialSpanProperty() { return radialSpan; }

    /**
     * Gibt die Spanne des Fans in Grad [0-360] zurueck.
     *
     * @return Die Spanne des Fans in Grad [0-360].
     */
    public double getRadialSpan() { return radialSpan.get(); }

    /**
     * Setzt die Spanne des Fans in Grad [0-360].
     *
     * @param value Die neue Spanne des Fans in Grad [0-360].
     */
    public void setRadialSpan(double value) { radialSpan.set(value); }

    @Override
    public FanMenu getSkinnable() { return this; }

    @Override
    public Node getNode() {
        return skin;
    }

    @Override
    public void dispose() {
        skin = null;
    }
}
