package wang.ginf.syan.userinterface.util;

/**
 * Eine Rectangleklasse mit einfacher Moeglichkeit zur Kollisionsabfrage.
 *
 * @author Ibrahim Apachi
 */
public class Rectangle {
    private final double height;
    private final double width;

    private final double x;
    private final double y;

    public Rectangle(final double x, final double y, final double width, final double height) {
        this.height = height;
        this.width = width;
        this.x = x;
        this.y = y;
    }

    public boolean collidesWith(final double x, final double y) {
        return this.x <= x && this.x + this.width >= x && this.y <= y && this.y + this.height >= y;
    }
}
