package wang.ginf.syan.userinterface.windows;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.SetChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import org.hibernate.SessionFactory;
import wang.ginf.syan.businesslogic.AbortedException;
import wang.ginf.syan.businesslogic.IllegalFileException;
import wang.ginf.syan.businesslogic.SyanFileLogic;
import wang.ginf.syan.businesslogic.analysis.GraphicalAnalysisAdministrator;
import wang.ginf.syan.businesslogic.analysis.SyndromeInterconnectivityRateAnalyzer;
import wang.ginf.syan.model.*;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;
import wang.ginf.syan.persistence.CommandsDatabase;
import wang.ginf.syan.persistence.ExportService;
import wang.ginf.syan.userinterface.controls.*;
import wang.ginf.syan.userinterface.util.ResourceHelper;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.ProviderNotFoundException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Fenster das den gesamten Canvas der Syndrombearbeitung enthaelt.
 *
 * @author Arne Kiesewetter
 * @author Ruben Smidt
 * @author Jan-Luca-Kiok
 * @author Jonas Luehrs
 * @author Ibrahim Apachi
 */
public class SyndromeEditorWindow extends GridPane implements Initializable {
    public static final StringProperty title = new SimpleStringProperty("SyAn");

    private static final int DEFAULT_HEIGHT = 500;
    private static final int DEFAULT_WIDTH = 500;
    private static final int DEFAULT_PADDING = 50;
    private static final int DEFAULT_ELEMENTS_PER_ROW = 4;
    private static final int DEFAULT_ELEMENTS_PER_COLUMN = 4;

    private static final Logger logger = Logger.getLogger(SyndromeEditorWindow.class.getName());

    private final GraphicalAnalysisAdministrator administrator = GraphicalAnalysisAdministrator.getInstance();
    private final SyanFileLogic fileLogic = new SyanFileLogic();
    private final CommandsDatabase commandsDatabase = CommandsDatabase.getInstance();
    private final SyndromeInterconnectivityRateAnalyzer interconnectivityRateAnalyzer =
            SyndromeInterconnectivityRateAnalyzer.getInstance();

    private final SessionFactory sessionFactory;

    private ResourceBundle bundle;
    private HelpWindow helpWindow;
    private File safeFile;

    @FXML
    private GridPane baseGrid;
    @FXML
    private MenuBar menuBar;
    @FXML
    private MinimapPane minimapPane;
    @FXML
    private SyndromeEditor syndromeEditor;
    @FXML
    private CheckMenuItem viewZoomButtonRadioMenuItem;
    @FXML
    private CheckMenuItem viewMiniMapRadioMenuItem;
    @FXML
    private CheckMenuItem viewUndoneCommands;
    @FXML
    private MenuItem createTemplateMenuItem;
    @FXML
    private CheckMenuItem modeCreatorCheckMenuItem;
    @FXML
    private CheckMenuItem modeAnalyzerCheckMenuItem;
    @FXML
    private Text currentMode;
    @FXML
    private CheckMenuItem viewCommandHistoryMenuItem;
    @FXML
    private CheckMenuItem viewNonCanonicalCommandsMenuItem;
    @FXML
    private CheckMenuItem viewStatisticsMenuItem;
    @FXML
    private CheckMenuItem viewInterconnectivityRateMenuItem;
    @FXML
    private CheckMenuItem viewEditButtonsMenuItem;
    @FXML
    private CommandHistoryListView commandHistoryListView;
    @FXML
    private ScrollPane commandHistory;
    @FXML
    private HBox lowerBar;
    @FXML
    private MenuItem helpUserGuideMenuItem;
    @FXML
    private MenuItem fileExportPDFMenuItem;
    @FXML
    private MenuItem fileExportImageMenuItem;
    @FXML
    private StatisticsPane statisticsScrollPane;
    @FXML
    private SplitPane rightSideBarSplitPane;
    @FXML
    private ScrollPane attributeEditorScrollPane;
    @FXML
    private Toolbox toolbox;
    @FXML
    private Text analystModeHint;

    /**
     * Erstellt ein neues Editor Fenster.
     */
    public SyndromeEditorWindow(SessionFactory sessionFactory) throws IOException {
        final var fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SyndromeEditorWindow.fxml"));

        ResourceHelper.inject(fxmlLoader, "lang/SyndromeEditorWindow");
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        this.sessionFactory = sessionFactory;

        final var scaledFontSizeBinding = Settings.scaleFontSizeBinding(18);
        final var fontBinding = Bindings.createObjectBinding(
                () -> new Font(scaledFontSizeBinding.get()),
                scaledFontSizeBinding
        );

        currentMode.fontProperty().bind(fontBinding);
        analystModeHint.fontProperty().bind(fontBinding);
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        bundle = resourceBundle;

        setupCommonMenuItems();
        setupAnalysisMenuItems();

        setupMinimap();

        handleModeChange();
        setInitialMode();
        handleUndoRedoButtons();

        setupRightBar();

        Settings.registerNodes(this);

        fillOnDebug();
    }

    private void updateDividerPositions() {
        if (viewStatisticsMenuItem.isSelected()) {
            if (viewCommandHistoryMenuItem.isSelected()) {
                rightSideBarSplitPane.setDividerPosition(1, 2 / 3.0);
            } else {
                rightSideBarSplitPane.setDividerPosition(1, 0.5);
            }
        } else {
            rightSideBarSplitPane.setDividerPosition(1, 1);
        }

        if (viewCommandHistoryMenuItem.isSelected()) {
            if (viewStatisticsMenuItem.isSelected()) {
                rightSideBarSplitPane.setDividerPosition(0, 1 / 3.0);
            } else {
                rightSideBarSplitPane.setDividerPosition(0, 0.5);
            }
        } else {
            rightSideBarSplitPane.setDividerPosition(0, 0);
        }
    }

    private void setupCommonMenuItems() {
        heightProperty().addListener((observable, oldValue, newValue) -> updateDividerPositions());

        viewCommandHistoryMenuItem.selectedProperty()
                                  .addListener((observable, oldValue, newValue) -> updateDividerPositions());

        viewStatisticsMenuItem.selectedProperty()
                              .addListener((observable, oldValue, newValue) -> updateDividerPositions());
    }

    private void setupAnalysisMenuItems() {
        final var syndromeEmptyBinding = ((SyndromeEditor) minimapPane.getContent()).syndromeProperty().isNull();

        modeAnalyzerCheckMenuItem.disableProperty().bind(syndromeEmptyBinding);

        viewStatisticsMenuItem.disableProperty().bind(
                GlobalState.atLeastAnalystProperty().not()
                           .or(syndromeEmptyBinding)
        );

        viewInterconnectivityRateMenuItem.disableProperty().bind(
                GlobalState.atLeastAnalystProperty().not()
                           .or(syndromeEmptyBinding)
        );

        interconnectivityRateAnalyzer.analyzableProperty().bind(viewInterconnectivityRateMenuItem.selectedProperty());

        final ChangeListener<Boolean> selectedChangeListener = (observable, oldValue, newValue) -> {
            if (newValue && viewInterconnectivityRateMenuItem.isSelected()) {
                interconnectivityRateAnalyzer.showAnalysis(getSyndromeEditor());

                return;
            }

            if (!modeAnalyzerCheckMenuItem.isSelected() && viewInterconnectivityRateMenuItem.isSelected()) {
                viewInterconnectivityRateMenuItem.setSelected(false);
            }

            interconnectivityRateAnalyzer.hideAnalysis(getSyndromeEditor());
        };

        viewInterconnectivityRateMenuItem.selectedProperty().addListener(selectedChangeListener);
        modeAnalyzerCheckMenuItem.selectedProperty().addListener(selectedChangeListener);
    }

    private void setupMinimap() {
        minimapPane.getZoombuttons().visibleProperty().bind(viewZoomButtonRadioMenuItem.selectedProperty());
        minimapPane.getMinimap().visibleProperty().bind(viewMiniMapRadioMenuItem.selectedProperty());
    }

    private void setupRightBar() {
        commandHistoryListView.managedProperty().bind(commandHistoryListView.visibleProperty());
        commandHistoryListView.visibleProperty().bind(viewCommandHistoryMenuItem.selectedProperty());

        commandHistoryListView.showFullHistoryProperty()
                              .bindBidirectional(viewNonCanonicalCommandsMenuItem.selectedProperty());
        commandHistoryListView.commandHistoryProperty().bind(syndromeEditor.commandHistoryProperty());

        commandHistoryListView.managedProperty().bind(commandHistoryListView.visibleProperty());
        commandHistoryListView.visibleProperty().bind(viewCommandHistoryMenuItem.selectedProperty());

        commandHistoryListView.showFullHistoryProperty()
                              .bindBidirectional(viewNonCanonicalCommandsMenuItem.selectedProperty());
        commandHistoryListView.commandHistoryProperty().bind(syndromeEditor.commandHistoryProperty());

        statisticsScrollPane.visibleProperty()
                            .bindBidirectional(viewStatisticsMenuItem.selectedProperty());
        statisticsScrollPane.managedProperty().bindBidirectional(statisticsScrollPane.visibleProperty());

        final var syndromeEmptyBinding = ((SyndromeEditor) minimapPane.getContent()).syndromeProperty().isNull();

        rightSideBarSplitPane.setDividerPosition(0, 0);
        rightSideBarSplitPane.setDividerPosition(1, 1);

        rightSideBarSplitPane.visibleProperty().bind(syndromeEmptyBinding.not());
        rightSideBarSplitPane.managedProperty().bind(syndromeEmptyBinding.not());

        attributeEditorScrollPane.setContent(AttributeEditor.getInstance());
    }

    private void handleUndoRedoButtons() {
        final var scaleFactor = Settings.getScaleFactorBinding();

        final var undoImage = new ImageView("/icons/fa-undo-transparent.png");
        undoImage.setFitHeight(60);
        undoImage.setFitWidth(60);
        undoImage.fitHeightProperty().bind(scaleFactor.multiply(60));
        undoImage.fitWidthProperty().bind(scaleFactor.multiply(60));

        final var redoImage = new ImageView("/icons/fa-repeat-transparent.png");
        redoImage.setFitHeight(60);
        redoImage.setFitWidth(60);
        redoImage.fitHeightProperty().bind(scaleFactor.multiply(60));
        redoImage.fitWidthProperty().bind(scaleFactor.multiply(60));

        final var undoButton = new Button("", undoImage);
        undoButton.setPrefSize(60, 60);
        undoButton.setPadding(Insets.EMPTY);
        undoButton.setStyle("-fx-background-color: transparent;");
        undoButton.setOnAction(event -> syndromeEditor.getCommandHistory().undo());

        final var redoButton = new Button("", redoImage);
        redoButton.setPrefSize(60, 60);
        redoButton.setPadding(Insets.EMPTY);
        redoButton.setStyle("-fx-background-color: transparent;");
        redoButton.setOnAction(event -> syndromeEditor.getCommandHistory().redo());

        final var buttonGroup = new HBox(undoButton, redoButton);
        buttonGroup.setMaxSize(130, 60);
        buttonGroup.setSpacing(5);

        GridPane.setColumnIndex(buttonGroup, 1);
        GridPane.setRowIndex(buttonGroup, 1);
        GridPane.setVgrow(buttonGroup, Priority.ALWAYS);
        GridPane.setHalignment(buttonGroup, HPos.RIGHT);
        GridPane.setValignment(buttonGroup, VPos.BOTTOM);
        GridPane.setMargin(buttonGroup, new Insets(0, 35, 30, 0));

        getChildren().add(buttonGroup);

        viewEditButtonsMenuItem.disableProperty().bind(GlobalState.analystButNotCreatorProperty());

        buttonGroup.visibleProperty().bind(
                viewEditButtonsMenuItem.selectedProperty()
                                       .and(GlobalState.analystButNotCreatorProperty().not())
                                       .and(GlobalState.undoRedoAllowedProperty())
        );
    }

    private void handleModeChange() {
        modeCreatorCheckMenuItem.selectedProperty()
                                .addListener(getChangeListenerForMode(UserMode.CREATOR));

        modeAnalyzerCheckMenuItem.selectedProperty()
                                 .addListener(getChangeListenerForMode(UserMode.ANALYST));

        Settings.registerNodes(this);

        GlobalState.getUserMode().addListener((SetChangeListener<? super UserMode>) change -> {
            currentMode.setText(change.getSet().toString());
            if (change.wasAdded()) {
                if (change.getElementAdded() == UserMode.CREATOR) {
                    modeCreatorCheckMenuItem.setSelected(true);
                } else if (change.getElementAdded() == UserMode.ANALYST) {
                    modeAnalyzerCheckMenuItem.setSelected(true);
                }
            } else {
                if (change.getElementRemoved() == UserMode.CREATOR) {
                    modeCreatorCheckMenuItem.setSelected(false);
                } else if (change.getElementRemoved() == UserMode.ANALYST) {
                    administrator.disableGraphicalAnalysis();
                    modeAnalyzerCheckMenuItem.setSelected(false);
                }
            }
        });

        final var analystModeHintBinding =
                Bindings.when(GlobalState.atLeastAnalystProperty().and(GlobalState.atLeastCreatorProperty()))
                        .then(bundle.getString("analystModeHint"))
                        .otherwise("");

        analystModeHint.textProperty().bind(analystModeHintBinding);
    }

    private ChangeListener<Boolean> getChangeListenerForMode(final UserMode mode) {
        return (observable, oldValue, newValue) -> {
            if (newValue) {
                GlobalState.enableMode(mode);
            } else {
                GlobalState.disableMode(mode);
            }
        };
    }

    private void setInitialMode() {
        GlobalState.enableMode(UserMode.OBSERVER);
    }

    private void fillOnDebug() {
        final var enableDebugTemplate = Boolean.valueOf(System.getProperty("enableDebugTemplate"));

        if (!enableDebugTemplate) {
            return;
        }

        var debugSphereCount = 8;

        try {
            final String debugSphereCountProperty = System.getProperty("debugSphereCount");
            if (Objects.nonNull(debugSphereCountProperty) && !debugSphereCountProperty.equals("")) {
                debugSphereCount = Integer.parseInt(debugSphereCountProperty);
            } else {
                logger.info("No debug sphere count supplied. Using default now.");
            }
        } catch (final NumberFormatException e) {
            logger.info("Failed to parse debugSphereCount as integer. Using default now.");
        } finally {
            final var debugSphereNames = IntStream.rangeClosed(1, debugSphereCount)
                                                  .mapToObj(i -> String.format("Sph\u00e4re :%d", i))
                                                  .collect(Collectors.toCollection(LinkedHashSet::new));

            final var template = new TemplateCreationResultBuilder()
                    .setUniqueSphereNames(debugSphereNames)
                    .setName("Debug Template")
                    .setCreationOfWeakeningRelationAllowed(true)
                    .setCreationOfUnknownRelationAllowed(true)
                    .setCreationOfStrengtheningRelationAllowed(true)
                    .createTemplateCreationResult();

            resetEditorWith(template);
        }

        final var enableDebugSymptoms = Boolean.valueOf(System.getProperty("enableDebugSymptoms"));

        if (!enableDebugSymptoms) {
            return;
        }

        final var index = new AtomicInteger(0);

        syndromeEditor.getSyndrome().getSpheres().forEach(sphere -> {
            sphere.getSymptoms().add(new Symptom("Symptom " + index.incrementAndGet(), Color.WHITE));
        });
    }

    @FXML
    private void handleCreateTemplateClicked() throws IOException {
        if (shouldInitializeNewDocument()) {
            final var creatorWindow = new TemplateCreatorWindow();

            creatorWindow.showAndWaitForResult().ifPresent(this::resetEditorWith);
        }
    }

    @FXML
    private void handleStatisticsClicked() throws IOException {
        if (viewStatisticsMenuItem.isSelected() && !modeAnalyzerCheckMenuItem.isSelected()) {
            viewStatisticsMenuItem.setSelected(false);
        }
    }

    /**
     * Es wird ueberprueft, ob der Dialog fuer eine neue Vorlage geoeffnet werden soll
     * oder ob zunaechst mittels eines anderen Dialogs gewarnt werden soll,
     * dass das Erstellen einer neuen Vorlage die aktuelle ueberschreibt.
     *
     * @return ob ein neues Dokument geoeffnet werden soll.
     */
    private boolean shouldInitializeNewDocument() {
        if (Objects.isNull(syndromeEditor.getSyndrome())) {
            return true;
        }

        final var discardChangesDialog = new SyanAlert(
                Alert.AlertType.CONFIRMATION,
                bundle.getString("confirmDialogTitle"),
                bundle.getString("confirmDialogText")
        );
        discardChangesDialog.setHeaderText(bundle.getString("confirmDialogHeader"));

        Settings.registerNodes(discardChangesDialog.getDialogPane());

        final var result = discardChangesDialog.showAndWait();

        return result.isEmpty() || !result.get().equals(ButtonType.CANCEL);
    }

    /**
     * Eine neu erstellte Vorlage verfuegt ueber keine initialen Layouting-
     * Attribute. Aus diesem Grund werden die Sphaeren zunaechst in einem klassischen
     * Raster angeordnet.
     *
     * @param result die neue Vorlage.
     */
    private void resetEditorWith(final TemplateCreationResult result) {
        title.set(result.getName());

        final var sphereIndex = new AtomicInteger(0);

        final var spheres = result.getUniqueSphereNames().stream().map(name -> {
            final var sphere = new Sphere(name);
            sphere.setHeight(DEFAULT_HEIGHT);
            sphere.setWidth(DEFAULT_WIDTH);

            final var rowIndex = sphereIndex.get() % DEFAULT_ELEMENTS_PER_ROW;
            final var paddingX = (rowIndex + 1) * DEFAULT_PADDING;
            final var marginX = paddingX + rowIndex * DEFAULT_WIDTH;
            sphere.setLayoutX(marginX);

            final var columnIndex = sphereIndex.get() / DEFAULT_ELEMENTS_PER_COLUMN;
            final var paddingY = (columnIndex + 1) * DEFAULT_PADDING;
            final var marginY = paddingY + (columnIndex) * DEFAULT_HEIGHT;
            sphere.setLayoutY(marginY);

            sphereIndex.getAndIncrement();

            return sphere;
        }).collect(Collectors.toCollection(LinkedHashSet::new));

        GlobalState.setStrengtheningRelationAllowed(result.isCreationOfStrengtheningRelationAllowed());
        GlobalState.setUnknownRelationAllowed(result.isCreationOfUnknownRelationAllowed());
        GlobalState.setWeakeningRelationAllowed(result.isCreationOfWeakeningRelationAllowed());
        GlobalState.setSymptomCreationAllowed(result.isCreationOfSymptomsAllowed());

        GlobalState.enableMode(UserMode.CREATOR);
        GlobalState.setTemplate(true);

        syndromeEditor.getCommandHistory().resetWith(Collections.emptyList());
        commandsDatabase.resetWith(Collections.emptyList());
        syndromeEditor.resetWith(new Syndrome(spheres));
    }

    @FXML
    private void handleOpenSettingsClicked() {
        try {
            SettingsWindow.create().show();
        } catch (final IOException e) {
            final var alert = new SyanAlert(
                    Alert.AlertType.ERROR,
                    bundle.getString("SettingsError"),
                    bundle.getString("SettingsErrorContent")
            );

            alert.showAndWait().filter(response -> response == ButtonType.OK).ifPresent(response -> alert.close());
        }
    }

    /**
     * Methode, die ein Fenster fuer das Benutzerhandbuch oeffnet.
     *
     * @param event Ausloesendes Event.
     */
    @FXML
    private void getUserGuide(final ActionEvent event) {
        try {
            if (Objects.isNull(helpWindow)) {
                helpWindow = new HelpWindow();
                helpWindow.setOnCloseRequest(closeEvent -> helpWindow = null);
                helpWindow.navigateTo("/html/user_guide.html");
                helpWindow.toFront();
            }
        } catch (final IOException e) {
            final var alert = new SyanAlert(
                    Alert.AlertType.ERROR,
                    bundle.getString("HelpError"),
                    bundle.getString("HelpErrorContent")
            );

            alert.showAndWait()
                 .filter(response -> response == ButtonType.OK)
                 .ifPresent(response -> alert.close());
        }
    }

    private boolean shouldShowExitDialog() {
        final var didCancel = new AtomicBoolean(false);

        if (GlobalState.isUnsavedChanges()) {
            final var save = new ButtonType(bundle.getString("saveOnExit"));
            final var doNotSave = new ButtonType(bundle.getString("noSaveOnExit"));

            final var alert = new Alert(
                    Alert.AlertType.CONFIRMATION,
                    bundle.getString("ExitContent"),
                    save,
                    doNotSave,
                    ButtonType.CANCEL
            );

            alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
            alert.showAndWait().
                    ifPresent(response -> {
                        if (response == save) {
                            handleSave();
                        } else if (response == ButtonType.CANCEL) {
                            didCancel.set(true);
                        }
                    });
        }

        if (didCancel.get()) {
            setDisabled(false);

            return false;
        }

        return true;
    }

    @FXML
    private void handleOpen() {
        setDisabled(true);

        if (!shouldShowExitDialog()) {
            return;
        }

        try {
            final var result = fileLogic.open(this.getScene().getWindow(), sessionFactory, title.get());
            final var newCommands = result.getCommands();
            final var newSyndrome = result.getSyndrome();
            final var newState = result.getState();

            if (!newState.isTemplate() || handleInitialTemplateSave(newSyndrome)) {
                // Ist jetzt auf jeden Fall kein Template mehr.
                // newState kann aber noch template = true enthalten, daher setzten wir es manuell auf false.
                GlobalState.resetWith(newState);
                GlobalState.setTemplate(false);
                syndromeEditor.resetWith(newSyndrome);
                commandsDatabase.resetWith(newCommands);
                syndromeEditor.getCommandHistory().resetWith(newCommands);
            }

            GlobalState.setUnsavedChanges(false);
        } catch (final IOException | ProviderNotFoundException e) {
            final var alert = new SyanAlert(
                    Alert.AlertType.ERROR,
                    bundle.getString("openTitle"),
                    bundle.getString("openError")
            );

            alert.showAndWait()
                 .filter(response -> response == ButtonType.OK)
                 .ifPresent(response -> alert.close());
        } catch (final AbortedException ae) {
            logger.info("User aborted file dialog.");
        } catch (final IllegalFileException | ClassNotFoundException other) {
            final var alert = new SyanAlert(
                    Alert.AlertType.ERROR,
                    bundle.getString("openTitle"),
                    bundle.getString("openWrongFile")
            );

            alert.showAndWait()
                 .filter(response -> response == ButtonType.OK)
                 .ifPresent(response -> alert.close());
        }

        setDisabled(false);
    }

    private boolean handleInitialTemplateSave(final Syndrome syndrome) {
        final var infoAlert = new SyanAlert(
                Alert.AlertType.INFORMATION,
                bundle.getString("initialTemplateTitle"),
                bundle.getString("initialTemplateMessage")
        );

        final var result = infoAlert.showAndWait();

        if (result.isPresent() && result.get().equals(ButtonType.CANCEL)) {
            return false;
        }

        final var wasTemplate = GlobalState.isTemplate();

        try {
            GlobalState.setTemplate(false);
            saveWithSyndrome(syndrome);
        } catch (final AbortedException e) {
            GlobalState.setTemplate(wasTemplate);

            return false;
        }

        return true;
    }

    private void saveWithSyndrome(final Syndrome syndrome) throws AbortedException {
        if (!GlobalState.isTemplate()) {
            safeFile = fileLogic.getFile("syan", this.getScene().getWindow(), title.get());
        } else {
            safeFile = fileLogic.getFile("syantemplate", this.getScene().getWindow(), title.get());
        }

        saveHandler(syndrome);
    }

    /**
     * Behandelt den 'Speichern unter...'-Dialog des Drop-Out-Menus. Fuehrt die 'Speichern'-Methode der
     * SyanFileLogic aus
     */
    @FXML
    private void handleSaveAs() {
        safeFile = null;
        handleSave();
    }

    /**
     * Initiiert einen neuen Speicherdialog, sofern in dieser Session noch kein File festgelegt wurde, speichert
     * ansonsten in diesen
     */
    @FXML
    private void handleSave() {
        if (Objects.isNull(safeFile)) {
            try {
                saveWithSyndrome(syndromeEditor.getSyndrome());
            } catch (final AbortedException ae) {
                logger.info("User aborted file dialog.");
            }

            return;
        }

        saveHandler(syndromeEditor.getSyndrome());
    }

    private void saveHandler(final Syndrome syndrome) {
        setDisabled(true);

        try {
            fileLogic.save(safeFile, syndrome, sessionFactory, title.get());
            GlobalState.setUnsavedChanges(false);
        } catch (final IOException ioe) {
            fileLogic.saveError(bundle.getString("saveError"));
        }

        setDisabled(false);
    }

    @FXML
    private void exportPDF() {
        try {
            minimapPane.takeSnapshot();

            final var exportFile = fileLogic.getFile("pdf", getScene().getWindow(), title.get());
            ExportService.exportAsPdf(minimapPane.getSnapshot(), exportFile);
        } catch (final AbortedException e) {
            logger.info("User aborted file dialog.");
        }
    }

    @FXML
    private void exportImage() {
        try {
            minimapPane.takeSnapshot();

            final var exportFile = fileLogic.getFile("img", getScene().getWindow(), title.get());
            ExportService.exportAsImage(minimapPane.getSnapshot(), exportFile);
        } catch (final AbortedException e) {
            logger.info("User aborted file dialog.");
        }
    }

    @FXML
    private void exportCSV(final ActionEvent actionEvent) {
        final var commands = commandsDatabase.getAll();

        final File csvFile;

        try {
            csvFile = fileLogic.getFile("csv", getScene().getWindow(), title.get());
        } catch (final AbortedException e) {
            logger.info("User aborted file dialog.");
            return;
        }

        try {
            ExportService.writeCommandsToCSV(commands, csvFile);
        } catch (final IOException ioe) {
            fileLogic.saveError(bundle.getString("saveError"));
        }
    }

    /**
     * Druckt den momentanen Canvas ueber die Java Print API aus
     */
    @FXML
    private void print(final ActionEvent actionEvent) {
        if (!ExportService.print(minimapPane.getSnapshot())) {
            final var alert = new SyanAlert(
                    Alert.AlertType.ERROR,
                    bundle.getString("PrintError"),
                    bundle.getString("PrintErrorContent")
            );

            alert.showAndWait()
                 .filter(response -> response == ButtonType.OK)
                 .ifPresent(response -> alert.close());
        }
    }

    /**
     * Schliesst das Programm aud doppelte Bestaetigung
     */
    @FXML
    public boolean close() {
        if (!GlobalState.isUnsavedChanges()) {
            Platform.exit();
        }
        final var save = new ButtonType(bundle.getString("saveOnExit"));
        final var doNotSave = new ButtonType(bundle.getString("noSaveOnExit"));

        final var alert = new Alert(
                Alert.AlertType.CONFIRMATION,
                bundle.getString("ExitContent"),
                save,
                doNotSave,
                ButtonType.CANCEL
        );

        alert.setTitle(bundle.getString("ExitTitle"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait().ifPresent(response -> {
            if (response == save) {
                handleSave();
                Platform.exit();
            } else if (response == doNotSave) {
                Platform.exit();
            }
        });

        return false;
    }

    public SyndromeEditor getSyndromeEditor() {
        return syndromeEditor;
    }

    public StringProperty titleProperty() {
        return title;
    }
}
