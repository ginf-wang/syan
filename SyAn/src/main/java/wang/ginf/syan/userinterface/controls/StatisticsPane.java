package wang.ginf.syan.userinterface.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import wang.ginf.syan.businesslogic.analysis.Statistics;
import wang.ginf.syan.userinterface.util.ResourceHelper;

import java.io.IOException;

/**
 * Das StatisticsPane enthaelt die relevanten Kennzahlen eines Syndroms/Symptoms.
 *
 * @author Jonas Luehrs
 * @author Ruben Smidt
 */
public class StatisticsPane extends ScrollPane {

    @FXML
    private Label neighbours;
    @FXML
    private Label predecessors;
    @FXML
    private Label successors;
    @FXML
    private Label arrowChains;
    @FXML
    private Label ramification;
    @FXML
    private Label convergentRamification;
    @FXML
    private Label divergentRamification;
    @FXML
    private Label relations;
    @FXML
    private Label incomingRelations;
    @FXML
    private Label incomingUnknownRelations;
    @FXML
    private Label incomingLesseningRelations;
    @FXML
    private Label incomingIncreasingRelations;
    @FXML
    private Label outgoingRelations;
    @FXML
    private Label outgoingUnknownRelations;
    @FXML
    private Label outgoingLesseningRelations;
    @FXML
    private Label outgoingIncreasingRelations;
    @FXML
    private Label symptoms;
    @FXML
    private Label syndromeArrowChains;
    @FXML
    private Label syndromeRamification;
    @FXML
    private Label syndromeConvergentRamification;
    @FXML
    private Label syndromeDivergentRamification;
    @FXML
    private Label syndromeRelations;
    @FXML
    private Label syndromeUnknownRelations;
    @FXML
    private Label syndromeLesseningRelations;
    @FXML
    private Label syndromeIncreasingRelations;
    @FXML
    private Label circumference;
    @FXML
    private Label interconnectivity;

    public StatisticsPane() throws IOException {
        final var loader = new FXMLLoader();
        loader.setRoot(this);
        loader.setController(this);
        loader.setLocation(getClass().getResource("/fxml/StatisticsPane.fxml"));
        ResourceHelper.inject(loader, "lang/StatisticsPane");
        loader.load();

        final var statistics = Statistics.getInstance();

        neighbours.textProperty().bind(statistics.numOfNeighboursProperty().asString());
        predecessors.textProperty().bind(statistics.numOfPredecessorsProperty().asString());
        successors.textProperty().bind(statistics.numOfSuccessorsProperty().asString());
        arrowChains.textProperty().bind(statistics.numOfArrowChainsProperty().asString());
        ramification.textProperty().bind(statistics.numOfRamificationProperty().asString());
        convergentRamification.textProperty().bind(statistics.numOfConvergentRamificationProperty().asString());
        divergentRamification.textProperty().bind(statistics.numOfDivergentRamificationProperty().asString());
        relations.textProperty().bind(statistics.numOfRelationsProperty().asString());
        incomingRelations.textProperty().bind(statistics.numOfIncomingRelationsProperty().asString());
        incomingUnknownRelations.textProperty().bind(statistics.numOfIncomingUnknownRelationsProperty().asString());
        incomingLesseningRelations.textProperty().bind(statistics.numOfIncomingLesseningRelationsProperty().asString());
        incomingIncreasingRelations.textProperty()
                                   .bind(statistics.numOfIncomingIncreasingRelationsProperty().asString());
        outgoingRelations.textProperty().bind(statistics.numOfOutgoingRelationsProperty().asString());
        outgoingUnknownRelations.textProperty().bind(statistics.numOfOutgoingUnknownRelationsProperty().asString());
        outgoingLesseningRelations.textProperty().bind(statistics.numOfOutgoingLesseningRelationsProperty().asString());
        outgoingIncreasingRelations.textProperty()
                                   .bind(statistics.numOfOutgoingIncreasingRelationsProperty().asString());

        symptoms.textProperty().bind(statistics.numOfSyndromeSymptomsProperty().asString());
        syndromeArrowChains.textProperty().bind(statistics.numOfSyndromeArrowChainsProperty().asString());
        syndromeRamification.textProperty().bind(statistics.numOfSyndromeRamificationsProperty().asString());
        syndromeConvergentRamification.textProperty()
                                      .bind(statistics.numOfSyndromeConvergentRamificationsProperty().asString());
        syndromeDivergentRamification.textProperty()
                                     .bind(statistics.numOfSyndromeDivergentRamificationsProperty().asString());
        syndromeRelations.textProperty().bind(statistics.numOfSyndromeRelationsProperty().asString());
        syndromeUnknownRelations.textProperty().bind(statistics.numOfSyndromeUnknownRelationsProperty().asString());
        syndromeLesseningRelations.textProperty().bind(statistics.numOfSyndromeLesseningRelationsProperty().asString());
        syndromeIncreasingRelations.textProperty()
                                   .bind(statistics.numOfSyndromeIncreasingRelationsProperty().asString());
        circumference.textProperty().bind(statistics.circumferenceProperty().asString());
        interconnectivity.textProperty().bind(statistics.interconnectivityProperty().asString());
    }
}
