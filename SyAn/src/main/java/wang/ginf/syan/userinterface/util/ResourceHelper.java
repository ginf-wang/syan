package wang.ginf.syan.userinterface.util;

import javafx.fxml.FXMLLoader;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Der ResourceHelper bietet Utilities im Umgang mit Resourcen.
 *
 * @author Ruben Smidt
 */
public final class ResourceHelper {
    private ResourceHelper() {}

    /**
     * Injiziert das gewuenschte ResourceBundle in den {@link javafx.fxml.FXML}.
     *
     * @param loader       der Loader.
     * @param resourcePath der Pfad des ResourceBundle.
     * @throws IOException wenn es Probleme bei dem Laden der Resourcen gibt.
     */
    public static void inject(final FXMLLoader loader, final String resourcePath) throws IOException {
        final var resourceBundle = ResourceBundle.getBundle(resourcePath, Locale.getDefault());

        loader.setResources(resourceBundle);
    }
}
