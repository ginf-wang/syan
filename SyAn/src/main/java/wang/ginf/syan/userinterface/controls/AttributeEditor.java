package wang.ginf.syan.userinterface.controls;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.SetChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import wang.ginf.syan.businesslogic.analysis.GraphicalAnalysisAdministrator;
import wang.ginf.syan.model.EntityBase;
import wang.ginf.syan.model.GlobalState;
import wang.ginf.syan.model.Settings;
import wang.ginf.syan.model.UserMode;
import wang.ginf.syan.model.history.CommandHistory;
import wang.ginf.syan.model.history.commands.*;
import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.RelationStrength;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;

import java.util.*;
import java.util.function.Predicate;

/**
 * Der Attribute Editor koordiniert das Bearbeiten von diversen Attributen einer Domainentitaet.
 * Dies umfasst aktuell Sphaeren, Symptome und Relationen.
 * <p>
 * In Zukunft koennen weitere Optionen, wie das Anzeigen bestimmter graphischer Analysen, benutzt werden.
 * <p>
 * Die Klasse fuehrt fuer jede Aktualisierung eine Liste an Logiken, die ausgefuehrt werden sollten,
 * sobald die naechste Aktualisierung angefragt wird. Dies versichert, dass jedes Element korrekt gesaeubert werden
 * kann und dadurch keine Probleme wie Memory Leaks entstehen koennen.
 *
 * @author Ruben Smidt
 */
public class AttributeEditor extends VBox {
    private static final AttributeEditor instance = new AttributeEditor();
    private final ObjectProperty<EntityBase> entity = new SimpleObjectProperty<>();
    private final List<Runnable> teardownLogic = new ArrayList<>(3);
    private final ResourceBundle bundle;
    private final GraphicalAnalysisAdministrator administrator = GraphicalAnalysisAdministrator.getInstance();

    private AttributeEditor() {
        bundle = ResourceBundle.getBundle("lang/AttributeEditor", Locale.getDefault());

        setSpacing(2);

        entity.addListener((observable, oldValue, newValue) -> {
            reset();
        });

        GlobalState.getUserMode().addListener((SetChangeListener<? super UserMode>) change -> {
            reset();
        });
    }

    /**
     * Setzt den Attributeditor zurueck, sollte der Ausdruck zutreffen.
     *
     * @param shouldRemove der zu testende Ausdruck.
     * @return ob das Zuruecksetzen erfolgreich war.
     */
    public static boolean resetIf(final Predicate<EntityBase> shouldRemove) {
        final var entity = getInstance().getEntity();

        if (Objects.nonNull(entity) && shouldRemove.test(entity)) {
            renewWith(null);
            return true;
        }

        return false;
    }

    public static AttributeEditor getInstance() { return instance; }

    /**
     * Aktualisiert den Attribute Editor mit der neuen Entitaet.
     *
     * @param entity die neue Entitaet.
     */
    public static void renewWith(final EntityBase entity) {
        getInstance().setEntity(entity);
    }

    public EntityBase getEntity() {
        return entity.get();
    }

    public void setEntity(final EntityBase entity) {
        this.entity.set(entity);
    }

    public ObjectProperty<EntityBase> entityProperty() {
        return entity;
    }

    private boolean isSymptom() {
        return getEntity() instanceof Symptom;
    }

    private boolean isRelation() {
        return getEntity() instanceof Relation;
    }

    private void reset() {
        teardownLogic.forEach(Runnable::run);
        teardownLogic.clear();
        build();
    }

    /**
     * Versucht die einzelnen Elemente des Attribute Editors zu bauen.
     */
    private void build() {
        final var currentEntity = getEntity();

        // Null bedeutet, dass nichts ausgweahlt ist. Daher wird ein Hinweis zum Ausaehlen gerendert.
        if (Objects.isNull(currentEntity)) {
            buildSelectionHint();
            return;
        }

        // Die Ueberschrift, die den konkreten Typ der Entitaet anzeigt.
        buildHeader(currentEntity);

        if (GlobalState.isAtLeastAnalyst() && !GlobalState.isAtLeastCreator()) {
            // Befindet sich im Analystmodus, aber nicht im Ersteller.
            // Daher wird die Bearbeitung verboten.
            buildIsAnalystInformation();
        } else if (!GlobalState.isAtLeastCreator() && !currentEntity.isChangeable()) {
            // Befindet sich nicht im Erstellermodus und die Entitaet darf nicht veraendert werden.
            buildNotChangeableInformation();
        }

        // Baut die gemeinsamen Attribute einer Entitaet.
        buildEntityBase(currentEntity);

        if (isSymptom()) {
            buildAnalysis((Symptom) currentEntity);
        }

        // Abfrage fuer einzelne Entitaeten.
        if (isRelation()) {
            buildRelation((Relation) currentEntity);
        }
    }

    private boolean shouldControlsBeDisabled(final EntityBase entityBase) {
        return GlobalState.shouldControlsBeDisabled(entityBase.isChangeable());
    }

    private void buildIsAnalystInformation() {
        final var isAnalystLabel = buildBasicLabel(bundle.getString("isAnalystLabel"));
        isAnalystLabel.setTextAlignment(TextAlignment.CENTER);

        HBox.setMargin(isAnalystLabel, new Insets(0, 0, 4, 0));
    }

    private void buildSelectionHint() {
        buildBasicLabel(bundle.getString("selectionHintLabel"));
    }

    private Label buildBasicLabel(final String labelText) {
        final var label = new Label(labelText);
        final var labelGroup = new HBox(label);
        labelGroup.setAlignment(Pos.CENTER);

        getChildren().add(labelGroup);

        teardownLogic.add(() -> getChildren().remove(labelGroup));

        return label;
    }

    private void buildHeader(final EntityBase entityBase) {
        final var localizedHeader = bundle.getString("headerLabel");
        final var localizedType = bundle.getString(entityBase.getTypeKey());
        final var localizedHeaderLabel = buildBasicLabel(String.format(localizedHeader, localizedType));

        final var scaledFontSizeBinding = Settings.scaleFontSizeBinding(14);
        final var fontBinding = Bindings.createObjectBinding(
                () -> new Font(scaledFontSizeBinding.get()),
                scaledFontSizeBinding
        );

        localizedHeaderLabel.setFont(fontBinding.get());
        localizedHeaderLabel.fontProperty().bind(fontBinding);

        HBox.setMargin(localizedHeaderLabel, new Insets(1, 1, 5, 1));
    }

    private void buildNotChangeableInformation() {
        final var notChangeableInfoLabel = buildBasicLabel(bundle.getString("notChangeableInfo"));

        HBox.setMargin(notChangeableInfoLabel, new Insets(0, 0, 4, 0));
    }

    private void buildEntityBase(final EntityBase entityBase) {
        final var colorPickerLabel = new Label(bundle.getString("colourLabel"));
        final var colorPicker = new ColorPicker(entityBase.getColor());

        colorPicker.setDisable(shouldControlsBeDisabled(entityBase));

        final ChangeListener<Color> colorChangeListener = (observable, oldValue, newValue) -> {
            colorPicker.setValue(newValue);
        };

        entityBase.colorProperty().addListener(colorChangeListener);

        colorPicker.setOnAction(event -> {
            Command recolorCommand;

            if (entityBase instanceof Relation) {
                recolorCommand = new RecolorRelationCommand((Relation) entityBase, colorPicker.getValue());
            } else if (entityBase instanceof Symptom) {
                recolorCommand = new RecolorSymptomCommand((Symptom) entityBase, colorPicker.getValue());
            } else {
                recolorCommand = new RecolorSphereCommand((Sphere) entityBase, colorPicker.getValue());
            }

            CommandHistory.getInstance().execute(recolorCommand);
        });

        final var colorPickerGroup = new HBox(5, colorPickerLabel, colorPicker);
        colorPickerGroup.setAlignment(Pos.CENTER_LEFT);

        getChildren().add(colorPickerGroup);

        final Runnable colorPickerTeardown = () -> {
            entityBase.colorProperty().removeListener(colorChangeListener);

            getChildren().remove(colorPickerGroup);
        };

        teardownLogic.add(colorPickerTeardown);

        if (GlobalState.isAtLeastCreator()) {
            final var changeableToggleLabel = new Label(bundle.getString("changeableLabel"));
            final var changeableToggle = new CheckBox();
            changeableToggle.selectedProperty().bindBidirectional(entityBase.changeableProperty());

            final var changeableToggleGroup = new HBox(5, changeableToggleLabel, changeableToggle);
            changeableToggleGroup.setAlignment(Pos.CENTER_LEFT);

            getChildren().add(changeableToggleGroup);

            final Runnable changeableToggleTeardown = () -> {
                changeableToggle.selectedProperty().unbindBidirectional(entityBase.changeableProperty());

                getChildren().remove(changeableToggleGroup);
            };

            teardownLogic.add(changeableToggleTeardown);
        }
    }

    private void buildAnalysis(final Symptom symptom) {
        if (GlobalState.isAtLeastAnalyst() && isSymptom()) {
            buildBasicLabel("");
            buildBasicLabel(bundle.getString("symptomAnalysis"));

            final var allReachableNodesToggleLabel = new Label(bundle.getString("allReachableNodes"));
            final var allReachableNodesToggle = new CheckBox();
            allReachableNodesToggle.selectedProperty()
                                   .bindBidirectional(symptom.getGraphicalAnalysis().allReachableProperty());
            final ChangeListener<Boolean> showOnlyAllReachableNodesListener = (observable, oldValue, newValue) -> {
                if(isSymptom()) {
                    symptom.getGraphicalAnalysis().setSymptom(symptom);
                    symptom.getGraphicalAnalysis().setAllReachable((Symptom) entity.get());
                }
                administrator.updateGraphicalAnalysis();

            };
            allReachableNodesToggle.selectedProperty().addListener(showOnlyAllReachableNodesListener);

            final var onlyForwardReachableNodesToggleLabel = new Label(bundle.getString("onlyForwardReachable"));
            final var onlyForwardReachableNodesToggle = new CheckBox();
            onlyForwardReachableNodesToggle.selectedProperty()
                                           .bindBidirectional(symptom.getGraphicalAnalysis()
                                                                     .onlyForwardReachableProperty());
            final ChangeListener<Boolean> showOnlyForwardReachableNodesListener = (observable, oldValue, newValue) -> {
                if(isSymptom()) {
                    symptom.getGraphicalAnalysis().setSymptom(symptom);
                    symptom.getGraphicalAnalysis().setOnlyForwardReachable((Symptom) entity.get());
                }
                administrator.updateGraphicalAnalysis();
            };
            onlyForwardReachableNodesToggle.selectedProperty().addListener(showOnlyForwardReachableNodesListener);

            final var onlyReverseReachableNodesToggleLabel = new Label(bundle.getString("onlyReverseReachable"));
            final var onlyReverseReachableNodesToggle = new CheckBox();
            onlyReverseReachableNodesToggle.selectedProperty()
                                           .bindBidirectional(symptom.getGraphicalAnalysis()
                                                                     .onlyReverseReachableProperty());
            final ChangeListener<Boolean> showOnlyReverseReachableNodesListener = (observable, oldValue, newValue) -> {
                if(isSymptom()) {
                    symptom.getGraphicalAnalysis().setSymptom(symptom);
                    symptom.getGraphicalAnalysis().setOnlyReverseReachable((Symptom) entity.get());
                }
                administrator.updateGraphicalAnalysis();

            };
            onlyReverseReachableNodesToggle.selectedProperty().addListener(showOnlyReverseReachableNodesListener);

            final var allReachableNodesOBSNToggleLabel = new Label(bundle.getString("allReachableNodesOBSN"));
            final var allReachableNodesOBSNToggle = new CheckBox();
            allReachableNodesOBSNToggle.selectedProperty()
                                   .bindBidirectional(symptom.getGraphicalAnalysis().allReachableOBSNProperty());
            final ChangeListener<Boolean> showOnlyAllReachableNodesOBSNListener = (observable, oldValue, newValue) -> {
                if(isSymptom()) {
                    symptom.getGraphicalAnalysis().setSymptom(symptom);
                    symptom.getGraphicalAnalysis().setAllReachableOBSN((Symptom) entity.get());
                }
                administrator.updateGraphicalAnalysis();

            };
            allReachableNodesOBSNToggle.selectedProperty().addListener(showOnlyAllReachableNodesOBSNListener);

            final var onlyForwardReachableNodesOBSNToggleLabel = new Label(bundle.getString("onlyForwardReachableOBSN"));
            final var onlyForwardReachableNodesOBSNToggle = new CheckBox();
            onlyForwardReachableNodesOBSNToggle.selectedProperty()
                                           .bindBidirectional(symptom.getGraphicalAnalysis()
                                                                     .onlyForwardReachableOBSNProperty());
            final ChangeListener<Boolean> showOnlyForwardReachableNodesOBSNListener = (observable, oldValue, newValue) -> {
                if(isSymptom()) {
                    symptom.getGraphicalAnalysis().setSymptom(symptom);
                    symptom.getGraphicalAnalysis().setOnlyForwardReachableOBSN((Symptom) entity.get());
                }
                administrator.updateGraphicalAnalysis();

            };
            onlyForwardReachableNodesOBSNToggle.selectedProperty().addListener(showOnlyForwardReachableNodesOBSNListener);

            final var onlyReverseReachableNodesOBSNToggleLabel = new Label(bundle.getString("onlyReverseReachableOBSN"));
            final var onlyReverseReachableNodesOBSNToggle = new CheckBox();
            onlyReverseReachableNodesOBSNToggle.selectedProperty()
                                           .bindBidirectional(symptom.getGraphicalAnalysis()
                                                                     .onlyReverseReachableOBSNProperty());
            final ChangeListener<Boolean> showOnlyReverseReachableNodesOBSNListener = (observable, oldValue, newValue) -> {
                if(isSymptom()) {
                    symptom.getGraphicalAnalysis().setSymptom(symptom);
                    symptom.getGraphicalAnalysis().setOnlyReverseReachableOBSN((Symptom) entity.get());
                }
                administrator.updateGraphicalAnalysis();

            };
            onlyReverseReachableNodesOBSNToggle.selectedProperty().addListener(showOnlyReverseReachableNodesOBSNListener);

            //Hier werden die Checkboxes blockiert, sodass nur eine Auswahl zurzeit möglich ist
            BooleanBinding allReachableNodesToggleIsDisabled = onlyForwardReachableNodesToggle.selectedProperty().or(onlyReverseReachableNodesToggle.selectedProperty().or(allReachableNodesOBSNToggle.selectedProperty().or(onlyForwardReachableNodesOBSNToggle.selectedProperty().or(onlyReverseReachableNodesOBSNToggle.selectedProperty()))));
            allReachableNodesToggle.disableProperty().bind(allReachableNodesToggleIsDisabled);
            BooleanBinding onlyForwardReachableNodesToggleIsDisabled = allReachableNodesToggle.selectedProperty().or(onlyReverseReachableNodesToggle.selectedProperty().or(allReachableNodesOBSNToggle.selectedProperty().or(onlyForwardReachableNodesOBSNToggle.selectedProperty().or(onlyReverseReachableNodesOBSNToggle.selectedProperty()))));
            onlyForwardReachableNodesToggle.disableProperty().bind(onlyForwardReachableNodesToggleIsDisabled);
            BooleanBinding onlyReverseReachableNodesToggleIsDisabled = allReachableNodesToggle.selectedProperty().or(onlyForwardReachableNodesToggle.selectedProperty().or(allReachableNodesOBSNToggle.selectedProperty().or(onlyForwardReachableNodesOBSNToggle.selectedProperty().or(onlyReverseReachableNodesOBSNToggle.selectedProperty()))));
            onlyReverseReachableNodesToggle.disableProperty().bind(onlyReverseReachableNodesToggleIsDisabled);

            BooleanBinding allReachableNodesOBSNToggleIsDisabled = allReachableNodesToggle.selectedProperty().or(onlyForwardReachableNodesToggle.selectedProperty().or(onlyReverseReachableNodesToggle.selectedProperty().or(onlyForwardReachableNodesOBSNToggle.selectedProperty().or(onlyReverseReachableNodesOBSNToggle.selectedProperty()))));
            allReachableNodesOBSNToggle.disableProperty().bind(allReachableNodesOBSNToggleIsDisabled);
            BooleanBinding onlyForwardReachableNodesOBSNToggleIsDisabled = allReachableNodesToggle.selectedProperty().or(onlyForwardReachableNodesToggle.selectedProperty().or(onlyReverseReachableNodesToggle.selectedProperty().or(allReachableNodesOBSNToggle.selectedProperty().or(onlyReverseReachableNodesOBSNToggle.selectedProperty()))));
            onlyForwardReachableNodesOBSNToggle.disableProperty().bind(onlyForwardReachableNodesOBSNToggleIsDisabled);
            BooleanBinding onlyReverseReachableNodesOBSNToggleIsDisabled = allReachableNodesToggle.selectedProperty().or(onlyForwardReachableNodesToggle.selectedProperty().or(onlyReverseReachableNodesToggle.selectedProperty().or(allReachableNodesOBSNToggle.selectedProperty().or(onlyForwardReachableNodesOBSNToggle.selectedProperty()))));
            onlyReverseReachableNodesOBSNToggle.disableProperty().bind(onlyReverseReachableNodesOBSNToggleIsDisabled);

            final var reachableToggleGroup = new GridPane();
            reachableToggleGroup.add(new Label(bundle.getString("allNodes")), 0, 0);
            reachableToggleGroup.add(allReachableNodesToggleLabel, 0, 1);
            reachableToggleGroup.add(allReachableNodesToggle, 1, 1);
            reachableToggleGroup.add(onlyForwardReachableNodesToggleLabel, 0, 2);
            reachableToggleGroup.add(onlyForwardReachableNodesToggle, 1, 2);
            reachableToggleGroup.add(onlyReverseReachableNodesToggleLabel, 0, 3);
            reachableToggleGroup.add(onlyReverseReachableNodesToggle, 1, 3);
            reachableToggleGroup.add(new Label(""), 0, 4);

            reachableToggleGroup.add(new Label(bundle.getString("onlyBetweenSelectedNodes")), 0, 5);
            reachableToggleGroup.add(allReachableNodesOBSNToggleLabel, 0, 6);
            reachableToggleGroup.add(allReachableNodesOBSNToggle, 1, 6);
            reachableToggleGroup.add(onlyForwardReachableNodesOBSNToggleLabel, 0, 7);
            reachableToggleGroup.add(onlyForwardReachableNodesOBSNToggle, 1, 7);
            reachableToggleGroup.add(onlyReverseReachableNodesOBSNToggleLabel, 0, 8);
            reachableToggleGroup.add(onlyReverseReachableNodesOBSNToggle, 1, 8);

            reachableToggleGroup.setAlignment(Pos.CENTER_LEFT);
            reachableToggleGroup.setHgap(5);

            getChildren().add(reachableToggleGroup);

            final Runnable allReachableNodesToggleTeardown = () -> {
                allReachableNodesToggle.selectedProperty()
                                       .unbindBidirectional(symptom.getGraphicalAnalysis().allReachableProperty());
                onlyForwardReachableNodesToggle.selectedProperty()
                                               .unbindBidirectional(symptom.getGraphicalAnalysis()
                                                                           .onlyForwardReachableProperty());
                onlyReverseReachableNodesToggle.selectedProperty()
                                               .unbindBidirectional(symptom.getGraphicalAnalysis()
                                                                           .onlyReverseReachableProperty());
                allReachableNodesOBSNToggle.selectedProperty()
                                       .unbindBidirectional(symptom.getGraphicalAnalysis().allReachableOBSNProperty());
                onlyForwardReachableNodesOBSNToggle.selectedProperty()
                                               .unbindBidirectional(symptom.getGraphicalAnalysis()
                                                                           .onlyForwardReachableOBSNProperty());
                onlyReverseReachableNodesOBSNToggle.selectedProperty()
                                               .unbindBidirectional(symptom.getGraphicalAnalysis()
                                                                           .onlyReverseReachableOBSNProperty());
                getChildren().remove(reachableToggleGroup);
            };

            teardownLogic.add(allReachableNodesToggleTeardown);
        }
    }

    private void buildRelation(final Relation relation) {
        final var relationStrengthLabel = new Label(bundle.getString("relationStrengthLabel"));
        final ChoiceBox<RelationStrength> relationStrengthChoiceBox = new ChoiceBox<>();
        relationStrengthChoiceBox.setValue(relation.getRelationStrength());
        relationStrengthChoiceBox.setDisable(shouldControlsBeDisabled(relation));

        final ChangeListener<RelationStrength> relationStrengthChangeListener = (observable, oldValue, newValue) -> {
            relationStrengthChoiceBox.setValue(newValue);
        };
        relation.relationStrengthProperty().addListener(relationStrengthChangeListener);

        relationStrengthChoiceBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            final var relationStrengthChangedCommand =
                    new ChangeRelationStrengthCommand(relation, newValue);

            CommandHistory.getInstance().execute(relationStrengthChangedCommand);
        });

        relationStrengthChoiceBox.setItems(FXCollections.observableArrayList(RelationStrength.values()));

        final var relationStrengthGroup = new HBox(5, relationStrengthLabel, relationStrengthChoiceBox);
        relationStrengthGroup.setAlignment(Pos.CENTER_LEFT);

        getChildren().add(relationStrengthGroup);

        final Runnable relationTeardown = () -> {
            relation.relationStrengthProperty().removeListener(relationStrengthChangeListener);
            getChildren().removeAll(relationStrengthGroup);
        };

        teardownLogic.add(relationTeardown);
    }
}
