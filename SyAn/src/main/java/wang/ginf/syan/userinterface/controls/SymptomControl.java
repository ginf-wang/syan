package wang.ginf.syan.userinterface.controls;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.businesslogic.analysis.GraphicalAnalysisAdministrator;
import wang.ginf.syan.businesslogic.analysis.Statistics;
import wang.ginf.syan.model.GlobalState;
import wang.ginf.syan.model.history.CommandHistory;
import wang.ginf.syan.model.history.commands.MoveSymptomCommand;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Waypoint;
import wang.ginf.syan.userinterface.util.MoveableResizer;
import wang.ginf.syan.userinterface.util.RelationHandler;

import java.util.HashSet;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Control, dass ein {@see Symptom} im {@see SyndromeEditor} darstellt.
 *
 * @author Arne Kiesewetter
 */
public class SymptomControl extends TextField {
    public static final Color USE_STATUS_BORDER_COLOR = null;
    private static final int USE_STATUS_BORDER_WIDTH = 0;
    private final ObjectProperty<State> status = new SimpleObjectProperty<>();
    private final ObjectProperty<Color> borderColor = new SimpleObjectProperty<>();
    private final StringProperty errorText = new SimpleStringProperty();
    private final DoubleProperty borderWidth = new SimpleDoubleProperty(USE_STATUS_BORDER_WIDTH);
    private final BooleanProperty selectable = new SimpleBooleanProperty(true);

    private final BooleanProperty disabled = new SimpleBooleanProperty(false);
    private final BooleanProperty movingDisabled = new SimpleBooleanProperty(false);
    private final BooleanProperty relationsAllowed = new SimpleBooleanProperty(true);

    private final Statistics stats = Statistics.getInstance();
    private final GraphicalAnalysisAdministrator administrator = GraphicalAnalysisAdministrator.getInstance();

    private final Set<Consumer<SymptomControl>> listeners = new HashSet<>();
    private final Tooltip errorTooltip = new Tooltip();

    private final Symptom symptom;
    private final MoveableResizer moveableResizer;

    private String textBeforeEdit;
    private Cursor cursorBeforeDisabled = Cursor.TEXT;
    private Predicate<String> textChangedEventPredicate;

    /**
     * Erstellt eine neue Instanz eines SymptomControls mit dem gegebenen Sympptomnamen.
     *
     * @param name          der Name des Symptoms.
     * @param textPredicate der Ausdruck, der prueft, ob eine Namensaenderung des Symptoms valide ist.
     * @param startEditable ob das Symptom editierbar anfangen soll.
     */
    public SymptomControl(@NotNull final String name,
                          @NotNull final Predicate<String> textPredicate,
                          @NotNull final boolean startEditable) {
        this(new Symptom(name, Color.WHITE), textPredicate, startEditable);
    }

    /**
     * Erstellt eine neue Instanz eines SymptomControls mit dem gegebenen Symptom.
     *
     * @param symptom                das Symptom auf dem das Control basieren soll.
     * @param moveableResizerAllowed ob das Symptom bewegt werden darf.
     * @param relationsAllowed       ob das Symptom Teil einer Relation seien darf.
     */
    public SymptomControl(@NotNull final Symptom symptom,
                          @NotNull final boolean moveableResizerAllowed,
                          @NotNull final boolean relationsAllowed) {
        this(symptom, s -> true, false);
        setRelationsAllowed(relationsAllowed);
        movingDisabled.set(!moveableResizerAllowed);
    }

    /**
     * Erstellt eine neue Instanz eines SymptomControls mit dem gegebenen Symptom.
     *
     * @param symptom       das Symptom auf dem das Control basieren soll.
     * @param textPredicate der Ausdruck, der prueft, ob eine Namensaenderung des Symptoms valide ist.
     * @param startEditable ob das Symptom editierbar anfangen soll.
     */
    public SymptomControl(@NotNull final Symptom symptom,
                          @NotNull final Predicate<String> textPredicate,
                          @NotNull final boolean startEditable) {
        this.symptom = symptom;
        textChangedEventPredicate = textPredicate;

        setLayoutX(symptom.getLayoutX());
        setLayoutY(symptom.getLayoutY());

        symptom.layoutXProperty().bindBidirectional(layoutXProperty());
        symptom.layoutYProperty().bindBidirectional(layoutYProperty());

        statusProperty().addListener(this::statusChanged);
        textProperty().addListener(this::checkTextLength);
        setText(symptom.getName());

        setAlignment(Pos.CENTER);

        borderColorProperty().addListener(this::updateBackground);
        borderWidthProperty().addListener(this::updateBackground);
        symptom.colorProperty().addListener(this::updateBackground);

        setMinWidth(64);
        setMaxWidth(Region.USE_PREF_SIZE);

        textProperty().addListener(this::updateWidth);
        fontProperty().addListener(this::updateWidth);

        focusedProperty().addListener(this::focusChanged);
        setOnMouseClicked(this::clicked);
        setOnAction(this::confirmed);

        editableProperty().addListener(this::editableChanged);

        disabled.bind(
                GlobalState.atLeastCreatorProperty()
                           .not()
                           .and(symptom.changeableProperty().not())
                           .or(GlobalState.analystButNotCreatorProperty())
        );

        disabled.addListener((observable, oldValue, newValue) -> {
            if (!oldValue && newValue) {
                cursorBeforeDisabled = getCursor();
            }

            setCursor(newValue ? Cursor.DEFAULT : cursorBeforeDisabled);
        });

        if (disabled.get()) {
            setCursor(Cursor.DEFAULT);
        }

        setStatus(startEditable && !disabled.get() ? State.EDITING : State.DESELECTED);

        errorTooltip.textProperty().bind(errorTextProperty());
        errorTooltip.setAutoHide(true);

        updateBackground(null, null, null);

        moveableResizer = MoveableResizer.enhance(this);
        moveableResizer.attachHook(MoveableResizer.CollisionHook.get());
        moveableResizer.attachHook(MoveableResizer.ParentBoundsHook.get());

        moveableResizer.onMoved((oldX, oldY, newX, newY) -> {
            CommandHistory.getInstance().execute(new MoveSymptomCommand(getSymptom(), oldX, oldY, newX, newY));
        });

        moveableResizer.setResizingDisabled(true);
        moveableResizer.movingDisabledProperty()
                       .bind(editableProperty().or(selectableProperty().not()).or(disabled).or(movingDisabled));

        symptom.colorProperty().addListener((observable, oldValue, newValue) -> {
            final var color = (newValue.getBrightness() < 0.7) ? "white" : "black";

            setStyle("-fx-text-fill: " + color);
        });
    }

    private void statusChanged(final ObservableValue<? extends State> obsValue,
                               final State oldValue,
                               final State newValue) {
        setBorderColor(null);
        switch (newValue) {
            case SELECTED:
                AttributeEditor.renewWith(symptom);
                stats.setSymptomStatistics(getSymptom());
                break;
            case DESELECTED:
                setEditable(false);
                stats.resetSymptomStatistics();
                administrator.updateGraphicalAnalysis();
                break;
            case EDITING:
                setEditable(true);
                break;
            case ERROR:
                setEditable(true);

                if (Objects.isNull(getErrorText())) {
                    return;
                }

                Tooltip.install(this, errorTooltip);

                final var anchor = localToScreen(0, getHeight() + 4);
                errorTooltip.show(this, anchor.getX(), anchor.getY());
                break;
            case ERROR_SOLVED:
                Tooltip.uninstall(this, errorTooltip);
                setStatus(State.DESELECTED);
                break;
        }

        updateBackground(null, null, null);
    }

    private void editableChanged(final ObservableValue<? extends Boolean> obsValue,
                                 final boolean oldValue,
                                 final boolean newValue) {
        if (newValue) {
            setStyle("");
            return;
        }

        setStyle("-fx-highlight-fill: null;" +
                 "-fx-highlight-text-fill: null;");

        fireTextEdit();
    }

    private void fireTextEdit() {
        if (getText().equals(textBeforeEdit)) {
            setStatus(State.ERROR_SOLVED);
            return;
        }

        if (textChangedEventPredicate.test(getText())) {
            textBeforeEdit = getText();
            setStatus(State.ERROR_SOLVED);
        } else {
            setStatus(State.ERROR);
        }
    }

    private void updateBackground(final ObservableValue<?> obsValue,
                                  final Object oldValue,
                                  final Object newValue) {
        setPadding(new Insets(5 + getBorderWidth(),
                              10 + getBorderWidth(),
                              5 + getBorderWidth(),
                              10 + getBorderWidth()));

        final var borderFill =
                new BackgroundFill(getBorderColor(), new CornerRadii(20 + getBorderWidth()), null);

        final var backgroundFill =
                new BackgroundFill(symptom.getColor(), new CornerRadii(20), new Insets(getBorderWidth()));

        setBackground(new Background(borderFill, backgroundFill));
        updateWidth(null, null, null);
    }

    private void checkTextLength(final ObservableValue<? extends String> observableValue,
                                 final String oldValue,
                                 final String newValue) {
        if (newValue.length() > 32) {
            textProperty().setValue(oldValue);
            setErrorText(ResourceBundle.getBundle("lang/SyndromeEditor").getString("nameToLongError"));
            Tooltip.install(this, errorTooltip);

            final var anchor = localToScreen(0, getHeight() + 4);
            errorTooltip.show(this, anchor.getX(), anchor.getY());
        } else {
            errorTooltip.hide();
        }
    }

    private void updateWidth(final ObservableValue<?> obsValue,
                             final Object oldValue,
                             final Object newValue) {
        // adapted from: https://stackoverflow.com/a/25643696
        final var text = new Text(getText());
        text.setFont(getFont()); // Set the same font, so the size is the same

        final var width = text.getLayoutBounds().getWidth() // This big is the Text in the TextField
                          + getPadding().getLeft() + getPadding().getRight() // Add the padding of the TextField
                          + 5d; // Add some spacing

        setPrefWidth(width); // Set the width
        positionCaret(getCaretPosition()); // If you remove this line, it flashes a little bit
    }

    private void focusChanged(final ObservableValue<? extends Boolean> obsValue,
                              final Boolean oldValue,
                              final Boolean newValue) {
        if (!newValue) {
            setStatus(State.DESELECTED);
        } else if (getStatus() != State.EDITING && isSelectable()) {
            setStatus(State.SELECTED);
        }
    }

    private void clicked(final MouseEvent mouseEvent) {
        this.listeners.forEach(listener -> listener.accept(this));

        if (mouseEvent.getButton() != MouseButton.PRIMARY ||
            getStatus() == State.EDITING ||
            getStatus() == State.ERROR ||
            !isSelectable() ||
            !isRelationsAllowed()) {
            RelationHandler.reset();
            return;
        }

        if (!RelationHandler.isRelationBeingSet()) {
            if (mouseEvent.getClickCount() >= 2 && !disabled.get()) {
                RelationHandler.reset();
                setStatus(State.EDITING);
            }
        } else {
            final var mouseEventWaypoint = new Waypoint(mouseEvent.getX(), mouseEvent.getY());
            RelationHandler.getInstance().registerSymptomControl(this, mouseEventWaypoint);
        }
    }

    private void confirmed(final ActionEvent actionEvent) {
        setStatus(State.DESELECTED);
    }

    public void edit() {
        setStatus(State.EDITING);
        requestFocus();
    }

    public Symptom getSymptom() { return symptom; }

    public void setTextChangedEventPredicate(Predicate<String> predicate) { textChangedEventPredicate = predicate; }

    public ObjectProperty<State> statusProperty() { return status; }

    public State getStatus() { return status.get(); }

    public void setStatus(final State value) {
        if ((value == State.ERROR_SOLVED && getStatus() != State.ERROR)
            || (value == getStatus())) {
            return;
        }

        if (getStatus() == State.ERROR && value != State.ERROR_SOLVED) {
            fireTextEdit();
            return;
        }

        status.set(value);
    }

    public ObjectProperty<Color> borderColorProperty() { return borderColor; }

    public Color getBorderColor() {
        return borderColor.get() != null ? borderColor.get() : getStatus().getBorderColor();
    }

    public void setBorderColor(final Color value) { borderColor.set(value); }

    public DoubleProperty borderWidthProperty() { return borderWidth; }

    public double getBorderWidth() {
        return borderWidth.get() > USE_STATUS_BORDER_WIDTH ? borderWidth.get() : getStatus().getBorderWidth();
    }

    public void setBorderWidth(final double value) { borderWidth.set(value); }

    public void resetBorder() {
        setBorderColor(USE_STATUS_BORDER_COLOR);
        setBorderWidth(USE_STATUS_BORDER_WIDTH);
    }

    public StringProperty errorTextProperty() { return errorText; }

    public String getErrorText() { return errorText.get(); }

    public void setErrorText(final String value) { errorText.set(value); }

    public BooleanProperty selectableProperty() { return selectable; }

    public boolean isSelectable() { return selectable.get(); }

    public void setSelectable(final boolean value) {
        selectable.set(value);
    }

    public boolean isRelationsAllowed() {
        return relationsAllowed.get();
    }

    public void setRelationsAllowed(final boolean relationsAllowed) {
        this.relationsAllowed.set(relationsAllowed);
    }

    public BooleanProperty relationsAllowedProperty() {
        return relationsAllowed;
    }

    public boolean isMovingDisabled() { return moveableResizer.isMovingDisabled(); }

    public void setMovingDisabled(final boolean value) { moveableResizer.setMovingDisabled(value); }

    public BooleanProperty movingDisabledProperty() { return movingDisabled; }

    public void addListener(final Consumer<SymptomControl> listener) {
        this.listeners.add(listener);
    }

    public enum State {
        DESELECTED(Color.BLACK, 2),
        SELECTED(Color.CORNFLOWERBLUE, 2),
        EDITING(Color.ROYALBLUE, 2),
        ERROR(Color.DARKRED, 2),
        ERROR_SOLVED(Color.GREEN, 2);

        private final ObjectProperty<Color> borderColor = new SimpleObjectProperty<>();
        private final DoubleProperty borderWidth = new SimpleDoubleProperty();

        State(final Color color, final double width) {
            borderColor.set(color);
            borderWidth.set(width);
        }

        public ObjectProperty<Color> borderColorProperty() { return borderColor; }

        public Color getBorderColor() { return borderColor.get(); }

        public void setBorderColor(final Color value) { borderColor.set(value); }

        public DoubleProperty borderWidthProperty() { return borderWidth; }

        public double getBorderWidth() { return borderWidth.get(); }

        public void setBorderWidth(final double value) { borderWidth.set(value); }
    }
}
