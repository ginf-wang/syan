package wang.ginf.syan.userinterface.windows;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wang.ginf.syan.model.Settings;
import wang.ginf.syan.userinterface.util.ResourceHelper;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Fenster das fuer die Einstellungen verwendet wird.
 *
 * @author Ibrahim Apachi
 * @author Ruben Smidt
 */
public class SettingsWindow extends Stage implements Initializable {
    private SettingsWindow() throws IOException {
        final var fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SettingsWindow.fxml"));

        ResourceHelper.inject(fxmlLoader, "lang/SettingsForm");
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();
    }

    /**
     * Erstellt eine neue Instanz des SettingsWindow.
     *
     * @return die neue Instanz.
     * @throws IOException wenn das Instanziieren fehlschlaegt.
     */
    public static SettingsWindow create() throws IOException {
        return new SettingsWindow();
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        initModality(Modality.APPLICATION_MODAL);
        setAlwaysOnTop(true);
        setTitle(resourceBundle.getString("settings"));
        sizeToScene();

        Settings.scaleFontSizeBinding(10).addListener((observable, oldValue, newValue) -> {
            sizeToScene();
        });
    }
}
