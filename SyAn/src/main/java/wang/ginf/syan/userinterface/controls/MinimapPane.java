package wang.ginf.syan.userinterface.controls;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.DefaultProperty;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.Settings;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * Pane, welches eine einzige {@see Pane} als Inhalt annimmt, diese in einem {@see ScrollPane} darstellt
 * und eine Minimap mit Positionshervorhebung dafuer anzeigt.
 *
 * @author Arne Kiesewetter
 * @author Jan-Luca Kiok
 * @author Jonas Luehrs
 */
@DefaultProperty("content")
public class MinimapPane extends StackPane implements Initializable {
    private final SimpleObjectProperty<Pane> content = new SimpleObjectProperty<>(this, "content", new Pane());
    private final SimpleDoubleProperty minimapScale = new SimpleDoubleProperty(this, "minimapScale", 3);
    private Node scalingGroup;
    private Node scaledGroup;
    private double maxMinimapScale;

    private Image snapshot;

    @FXML
    private Canvas minimap;

    @FXML
    private ScrollPane scrollpane;

    @FXML
    private GridPane widgetGrid;

    @FXML
    private VBox zoombuttons;

    /**
     * Erstellt eine neue Minimap Pane.
     *
     * @throws IOException wenn das Laden der FXML Datei fehlschlaegt.
     */
    public MinimapPane() throws IOException {
        final var loader = new FXMLLoader();
        loader.setController(this);
        loader.setRoot(this);
        loader.setLocation(getClass().getResource("/fxml/MinimapPane.fxml"));
        loader.load();
    }

    private void setupZoomButtons() {
        final var scaleFactor = Settings.getScaleFactorBinding();
        final var buttonDimension = 85;

        final var zoomInImage = new ImageView("/icons/search-plus.png");
        zoomInImage.setFitHeight(buttonDimension);
        zoomInImage.setFitWidth(buttonDimension);
        zoomInImage.fitHeightProperty().bind(scaleFactor.multiply(buttonDimension));
        zoomInImage.fitWidthProperty().bind(scaleFactor.multiply(buttonDimension));

        final var zoomOutImage = new ImageView("/icons/search-minus.png");
        zoomOutImage.setFitHeight(buttonDimension);
        zoomOutImage.setFitWidth(buttonDimension);
        zoomOutImage.fitHeightProperty().bind(scaleFactor.multiply(buttonDimension));
        zoomOutImage.fitWidthProperty().bind(scaleFactor.multiply(buttonDimension));

        final var zoomInButton = new Button("", zoomInImage);
        zoomInButton.setPrefSize(buttonDimension, buttonDimension);
        zoomInButton.setPadding(Insets.EMPTY);
        zoomInButton.setStyle("-fx-background-color: transparent;");
        zoomInButton.setOnAction(this::zoomIn);

        final var zoomOutButton = new Button("", zoomOutImage);
        zoomOutButton.setPrefSize(buttonDimension, buttonDimension);
        zoomOutButton.setPadding(Insets.EMPTY);
        zoomOutButton.setStyle("-fx-background-color: transparent;");
        zoomOutButton.setOnAction(this::zoomOut);

        zoombuttons.getChildren().addAll(zoomInButton, zoomOutButton);
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        setupZoomButtons();

        final ChangeListener<Number> changeListener = (obsValue, oldValue, newValue) -> redrawMinimap();

        scrollpane.contentProperty().bind(Bindings.createObjectBinding(() -> {
            scalingGroup = new Group(getContent());
            scalingGroup.scaleXProperty().addListener(changeListener);
            scalingGroup.scaleYProperty().addListener(changeListener);

            scaledGroup = new Group(scalingGroup);
            return scaledGroup;
        }, contentProperty()));

        widgetGrid.getRowConstraints().get(1).maxHeightProperty().bind(minimap.heightProperty());
        minimap.managedProperty().bind(minimap.visibleProperty());
        minimap.setOnMousePressed(mouseEvent -> moveViewFromMinimap(mouseEvent.getX(), mouseEvent.getY()));
        minimap.setOnMouseDragged(dragEvent -> moveViewFromMinimap(dragEvent.getX(), dragEvent.getY()));
        zoombuttons.managedProperty().bind(zoombuttons.visibleProperty());

        heightProperty().addListener(changeListener);
        widthProperty().addListener(changeListener);

        scrollpane.viewportBoundsProperty().addListener((obsValue, oldValue, newValue) -> {
            getContent().setPrefHeight(Math.max(newValue.getHeight(), getContent().minHeight(0)));
            getContent().setPrefWidth(Math.max(newValue.getWidth(), getContent().minWidth(0)));
            redrawMinimap();
        });

        scrollpane.hvalueProperty().addListener(changeListener);
        scrollpane.vvalueProperty().addListener(changeListener);

        final var updateMinimapTimeline = new Timeline(new KeyFrame(Duration.millis(250), actionEvent -> {
            if (minimap.isVisible()) {
                takeSnapshot();
            }
        }));

        updateMinimapTimeline.setCycleCount(Timeline.INDEFINITE);
        updateMinimapTimeline.play();
    }

    private void moveViewFromMinimap(final double x, final double y) {
        final var adjustedX = x -
                              0.5 *
                              getViewportWidthRatio() *
                              Math.ceil(getContent().getLayoutBounds().getWidth() / maxMinimapScale);
        final var adjustedY = y -
                              0.5 *
                              getViewportHeightRatio() *
                              Math.ceil(getContent().getLayoutBounds().getHeight() / maxMinimapScale);

        final var hValue = scrollpane.getHmin() +
                           (adjustedX * getHRange()) / (getUnshownWidth() / maxMinimapScale / scalingGroup.getScaleX());
        final var vValue = scrollpane.getVmin() +
                           (adjustedY * getVRange()) /
                           (getUnshownHeight() / maxMinimapScale / scalingGroup.getScaleY());

        scrollpane.setHvalue(hValue);
        scrollpane.setVvalue(vValue);
    }

    private void redrawMinimap() {
        if (snapshot == null) {
            return;
        }

        final var vpBounds = scrollpane.getViewportBounds();
        final var contentBounds = getContent().getLayoutBounds();
        final var scaledBounds = scaledGroup.getLayoutBounds();

        final var vpWidth = Math.ceil(vpBounds.getWidth());
        final var vpHeight = Math.ceil(vpBounds.getHeight());

        final var contentWidth = Math.ceil(contentBounds.getWidth());
        final var contentHeight = Math.ceil(contentBounds.getHeight());

        minimap.setWidth(Math.ceil(vpWidth / getMinimapScale()));
        minimap.setHeight(Math.ceil(vpHeight / getMinimapScale()));

        final var widthScale = contentWidth / minimap.getWidth();
        final var heightScale = contentHeight / minimap.getHeight();
        maxMinimapScale = Math.max(widthScale, heightScale);

        final var scaledWidth = Math.ceil(scaledBounds.getWidth());
        final var scaledHeight = Math.ceil(scaledBounds.getHeight());

        if (scaledWidth <= 0 || scaledHeight <= 0) {
            return;
        }

        // ungezeigter bereich * (hvalue offset / hrange) = bereich * hpercent
        final var hoffset = Math.max(0, getUnshownWidth()) * (getHOffset() / getHRange());
        final var voffset = Math.max(0, getUnshownHeight()) * (getVOffset() / getVRange());

        final var gc = minimap.getGraphicsContext2D();

        gc.setFill(getContent().getBackground().getFills().get(0).getFill());
        gc.fillRect(0, 0, minimap.getWidth(), minimap.getHeight());

        gc.drawImage(snapshot,
                     0,
                     0,
                     snapshot.getWidth(),
                     snapshot.getHeight(),
                     0,
                     0,
                     Math.ceil(contentWidth / maxMinimapScale),
                     Math.ceil(contentHeight / maxMinimapScale));

        final var lowX = hoffset / maxMinimapScale / scalingGroup.getScaleX();
        final var highX = (hoffset + vpWidth) / maxMinimapScale / scalingGroup.getScaleX();
        final var lowY = voffset / maxMinimapScale / scalingGroup.getScaleY();
        final var highY = (voffset + vpHeight) / maxMinimapScale / scalingGroup.getScaleY();

        // draw "shade" on invisible area
        gc.setFill(Color.color(0.7, 0.7, 0.7, 0.4));
        gc.fillRect(0, 0, minimap.getWidth(), lowY); // top
        gc.fillRect(0, lowY, lowX, highY - lowY); // left
        gc.fillRect(0, highY, minimap.getWidth(), minimap.getHeight() - highY); // bottom
        gc.fillRect(highX, lowY, minimap.getWidth() - highX, highY - lowY); // right

        gc.setLineWidth(1);
        gc.setStroke(Color.BLACK);
        gc.strokeRect(0.5, 0.5, minimap.getWidth() - 1, minimap.getHeight() - 1);
        gc.strokeRect(lowX, lowY, highX - lowX, highY - lowY);
    }

    public synchronized void takeSnapshot() {
        snapshot = getContent().snapshot(new SnapshotParameters(), null);

        redrawMinimap();
    }

    private double getHRange() {
        return scrollpane.getHmax() - scrollpane.getHmin();
    }

    private double getVRange() {
        return scrollpane.getVmax() - scrollpane.getVmin();
    }

    private double getHOffset() {
        return scrollpane.getHvalue() - scrollpane.getHmin();
    }

    private double getVOffset() {
        return scrollpane.getVvalue() - scrollpane.getVmin();
    }

    private double getUnshownWidth() {
        return scrollpane.getContent().getLayoutBounds().getWidth() - scrollpane.getViewportBounds().getWidth();
    }

    private double getUnshownHeight() {
        return scrollpane.getContent().getLayoutBounds().getHeight() - scrollpane.getViewportBounds().getHeight();
    }

    private double getViewportWidthRatio() {
        return scrollpane.getViewportBounds().getWidth() / scrollpane.getContent().getLayoutBounds().getWidth();
    }

    private double getViewportHeightRatio() {
        return scrollpane.getViewportBounds().getHeight() / scrollpane.getContent().getLayoutBounds().getHeight();
    }

    public SimpleObjectProperty<Pane> contentProperty() { return content; }

    public Pane getContent() {
        return content.getValue();
    }

    public void setContent(@NotNull Pane value) {
        content.setValue(value);
    }

    public SimpleDoubleProperty minimapScaleProperty() { return minimapScale; }

    public double getMinimapScale() {
        return minimapScale.getValue();
    }

    public void setMinimapScale(@NotNull double value) {
        minimapScale.set(value);
    }

    public VBox getZoombuttons() {
        return zoombuttons;
    }

    @FXML
    protected void zoomIn(ActionEvent event) {
        scalingGroup.setScaleX(scalingGroup.getScaleX() + 0.1);
        scalingGroup.setScaleY(scalingGroup.getScaleY() + 0.1);
    }

    @FXML
    protected void zoomOut(ActionEvent event) {
        scalingGroup.setScaleX(Math.max(0.1, scalingGroup.getScaleX() - 0.1));
        scalingGroup.setScaleY(Math.max(0.1, scalingGroup.getScaleY() - 0.1));
    }

    public Canvas getMinimap() {
        return minimap;
    }

    /**
     * Gibt den letzten Snapshot des Editors zurueck oder erstellt einen neuen,
     * sollte keiner vorhanden sein.
     *
     * @return ein Snapshot des SyndromeEditor.
     */
    public Image getSnapshot() {
        if (Objects.isNull(snapshot)) {
            snapshot = snapshot(new SnapshotParameters(), null);
        }

        return snapshot;
    }
}
