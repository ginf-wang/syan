package wang.ginf.syan.userinterface.util;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.RelationStrength;
import wang.ginf.syan.model.syndrome.RelationType;
import wang.ginf.syan.model.syndrome.Waypoint;
import wang.ginf.syan.userinterface.controls.SymptomControl;

import java.util.*;

/**
 * Der RelationHandler koordiniert das Erstellen einer neuen Relation.
 * Dies beinhaltet das Verfolgen des Start- und Endpunktes einer Relation,
 * aber auch das Validieren dieser Punkte.
 *
 * @author Ibrahim Apachi
 * @author Ruben Smidt
 */
public class RelationHandler {
    private static final BooleanProperty relationBeingSet = new SimpleBooleanProperty(false);
    private static final RelationHandler instance = new RelationHandler();

    private final List<SymptomControl> relationCandidates = new ArrayList<>(2);
    private final List<Waypoint> coordinates = new ArrayList<>(2);
    private final List<RelationDissolvenceListener> listeners = new ArrayList<>();
    private final ResourceBundle bundle;

    private RelationType relationType;

    private RelationHandler() {
        bundle = ResourceBundle.getBundle("lang/RelationHandler");
    }

    public static RelationHandler getInstance() { return instance; }

    public static boolean isRelationBeingSet() {
        return relationBeingSet.get();
    }

    private static void setRelationBeingSet(final boolean pRelationBeingSet) {
        relationBeingSet.set(pRelationBeingSet);
    }

    public static BooleanProperty relationBeingSetProperty() {
        return relationBeingSet;
    }

    /**
     * Beginnt den Prozess der Relationserstellung fuer einen bestimmen Relationstypen.
     *
     * @param relationType der Typ der Relation.
     */
    public static void startRelationAddingOfType(final RelationType relationType) {
        setRelationBeingSet(true);
        getInstance().relationType = relationType;
    }

    /**
     * Setzt den RelationsHandler zurueck auf einen Ausgangszustand.
     */
    public static void reset() {
        setRelationBeingSet(false);

        final var instance = getInstance();
        instance.relationType = null;
        instance.relationCandidates.clear();
        instance.coordinates.clear();
    }

    /**
     * Versucht ein neues SymptomControl fuer die Relations zu registrieren.
     * Beendet die Erstellung nach 2 Registrierungen.
     *
     * @param symptomControl das zu registrierende UI Control.
     * @param coordinate     die Koordinaten des Mausklicks.
     * @return ob das Symptom registriert werden konnte.
     */
    public boolean registerSymptomControl(final SymptomControl symptomControl, final Waypoint coordinate) {
        if (relationCandidates.contains(symptomControl)) {
            return false;
        }

        relationCandidates.add(symptomControl);
        coordinates.add(coordinate);

        if (relationCandidates.size() == 2) {
            dissolve();
        }

        return true;
    }

    private SymptomControl getStartSymptomControl() {
        return relationCandidates.get(0);
    }

    private SymptomControl getEndSymptomControl() {
        return relationCandidates.get(1);
    }

    private void dissolve() {
        final int wantedStart = whichSide(getStartSymptomControl(), coordinates.get(0));
        final int wantedEnd = whichSide(getEndSymptomControl(), coordinates.get(1));

        final var startSymptom = getStartSymptomControl().getSymptom();
        final var endSymptom = getEndSymptomControl().getSymptom();

        if (!isRelationAllowed()) {
            triggerListener(RelationCreationResult.withError(bundle.getString("relationNotAllowed")));
            reset();

            return;
        }

        final int freeStart = startSymptom.getFreeStartIfOccupied(wantedStart);
        final int freeEnd = endSymptom.getFreeEndIfOccupied(wantedEnd, relationType);

        if (freeStart == -1) {
            triggerListener(RelationCreationResult.withError(bundle.getString("noFreeStart")));
        } else if (freeEnd == -1) {
            triggerListener(RelationCreationResult.withError(bundle.getString("noFreeEnd")));
        } else {
            final var relation =
                    new Relation(RelationStrength.UNKNOWN, relationType, startSymptom, endSymptom, coordinates);

            relation.setStartSymptomAttachedSide(freeStart);
            relation.setEndSymptomAttachedSide(freeEnd);

            triggerListener(RelationCreationResult.withResult(relation));
        }

        reset();
    }

    private boolean isRelationAllowed() {
        final var startSymptom = getStartSymptomControl().getSymptom();
        final var endSymptom = getEndSymptomControl().getSymptom();

        return startSymptom.getRelations().stream()
                           .noneMatch(relation -> relation.containsSymptomsOrdered(startSymptom, endSymptom));
    }

    /*  This is how the hitbox of a symptom is displayed and at what point it is possible to attach an anchor point
     *   _ _ _ _ _
     *  | |1|2|3| |
     *  |0| | | |4|
     *  |_|7|6|5|_|
     */
    private int whichSide(final SymptomControl symptomControl, final Waypoint waypoint) {
        final var height = symptomControl.getHeight();
        final var width = symptomControl.getWidth();
        final var x = 0;
        final var y = 0;

        final var columnWidth = width / 5;
        final var columnHeight = height / 2;

        final var hitBoxes = new HashMap<Rectangle, Integer>();

        hitBoxes.put(new Rectangle(x, y, columnWidth, columnHeight * 2), 0);
        hitBoxes.put(new Rectangle(x + columnWidth, y + columnHeight, columnWidth, columnHeight), 1);
        hitBoxes.put(new Rectangle(x + columnWidth * 2, y + columnHeight, columnWidth, columnHeight), 2);
        hitBoxes.put(new Rectangle(x + columnWidth * 3, y + columnHeight, columnWidth, columnHeight), 3);
        hitBoxes.put(new Rectangle(x + columnWidth * 4, y, columnWidth, columnHeight * 2), 4);
        hitBoxes.put(new Rectangle(x + columnWidth * 3, y, columnWidth, columnHeight), 5);
        hitBoxes.put(new Rectangle(x + columnWidth * 2, y, columnWidth, columnHeight), 6);
        hitBoxes.put(new Rectangle(x + columnWidth, y, columnWidth, columnHeight), 7);

        return hitBoxes.entrySet().stream()
                       .filter(e -> e.getKey().collidesWith(waypoint.getLayoutX(), height - waypoint.getLayoutY()))
                       .map(Map.Entry::getValue)
                       .findFirst()
                       .orElse(-1);
    }

    private void triggerListener(final RelationCreationResult result) {
        listeners.forEach(listener -> listener.onRelationDissolvence(result));
    }

    /**
     * Fuegt einen neuen Listener dem Handler hinzu. Dieser wird aufgerufen,
     * sobald die Relationserstellung beendet werden soll.
     *
     * @param listener der zusaetzliche Listener.
     */
    public void registerListener(final RelationDissolvenceListener listener) {
        listeners.add(listener);
    }
}
