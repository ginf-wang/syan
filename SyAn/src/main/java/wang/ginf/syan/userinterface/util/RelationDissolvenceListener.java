package wang.ginf.syan.userinterface.util;

/**
 * Ein RelationDissolveneListener hoert auf Ergebnisse einer
 * haendischen Relationserstellung.
 *
 * @author Ruben Smidt
 * @author Ibrahim Apachi
 */
public interface RelationDissolvenceListener {
    /**
     * Wird aufgerufen, sobald eine Relation erstellt wurde.
     *
     * @param result Das Resultat.
     */
    void onRelationDissolvence(final RelationCreationResult result);
}
