package wang.ginf.syan.userinterface.controls;

import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import wang.ginf.syan.NotNull;
import wang.ginf.syan.model.history.CommandHistory;
import wang.ginf.syan.model.history.commands.*;

import java.util.function.Predicate;

/**
 * Die CommandHistoryListView listet die kanonischen und nicht-kanonischen Kommandos auf,
 * die im Zuge der Bearbeitung des Graphens entstanden sind.
 * <p>
 * Die Interaktion mit der Liste erlaubt es, Kommandos vor- oder zurueck zuspulen.
 *
 * @author Arne Kiesewetter
 * @author Ruben Smidt
 */
public class CommandHistoryListView extends ListView<Command> {
    private static final Background UNDONE_BACKGROUND_ODD =
            new Background(new BackgroundFill(Color.grayRgb(194), null, null));
    private static final Background UNDONE_BACKGROUND_EVEN =
            new Background(new BackgroundFill(Color.grayRgb(180), null, null));
    private static final Background EXECUTED_BACKGROUND_ODD =
            new Background(new BackgroundFill(Color.grayRgb(250), null, null));
    private static final Background EXECUTED_BACKGROUND_EVEN =
            new Background(new BackgroundFill(Color.grayRgb(240), null, null));

    private final SimpleObjectProperty<CommandHistory> commandHistory;
    private final SimpleBooleanProperty showFullHistory = new SimpleBooleanProperty(this, "showFullHistory", false);
    private FilteredList<Command> filteredCommands;

    public CommandHistoryListView() {
        showFullHistoryProperty().addListener(this::showFullHistoryChanged);
        getSelectionModel().getSelectedIndices().addListener(this::selectionChanged);

        this.commandHistory = new SimpleObjectProperty<>(this, "commandHistory", new CommandHistory());
        this.commandHistory.addListener(this::commandHistoryChanged);

        setCellFactory(this::makeCell);
    }

    private void showFullHistoryChanged(final ObservableValue<? extends Boolean> observableValue,
                                        final Boolean oldValue,
                                        final Boolean newValue) {
        filteredCommands.setPredicate(getFilterPredicate());
    }

    private void commandHistoryChanged(final ObservableValue<? extends CommandHistory> observableValue,
                                       final CommandHistory oldValue,
                                       final CommandHistory newValue) {
        filteredCommands = newValue.getHistory().filtered(getFilterPredicate());
        setItems(filteredCommands);
    }

    private Predicate<Command> getFilterPredicate() {
        if (getShowFullHistory()) {
            return command -> true;
        } else {
            return command -> !(command instanceof UndoCommand ||
                                command instanceof RedoCommand ||
                                command.isNonCanonical());
        }
    }

    private void selectionChanged(final ListChangeListener.Change<? extends Integer> change) {
        while (change.next()) {
            if (change.wasAdded()) {
                // Disabled because it adds too many undo commands or something.
                //getCommandHistory().moveTo(filteredCommands.getSourceIndex(change.getAddedSubList().get(0)));
                Platform.runLater(() -> getSelectionModel().clearSelection());
            }
        }
    }

    public SimpleBooleanProperty showFullHistoryProperty() { return showFullHistory; }

    public boolean getShowFullHistory() { return showFullHistory.get(); }

    public void setShowFullHistory(boolean value) { showFullHistory.set(value); }

    public SimpleObjectProperty<CommandHistory> commandHistoryProperty() { return commandHistory; }

    public CommandHistory getCommandHistory() { return commandHistory.get(); }

    /* Custom ListCell below here */

    public void setCommandHistory(@NotNull final CommandHistory value) { commandHistory.set(value); }

    private ListCell<Command> makeCell(final ListView<Command> commandListView) {
        return new CommandCell();
    }

    private class CommandCell extends ListCell<Command> {
        @Override
        protected void updateItem(final Command command, final boolean empty) {
            if (!isEmpty()) {
                getItem().statusProperty().removeListener(this::commandStatusChanged);
                getItem().nonCanonicalProperty().removeListener(this::updateGraphic);
            }

            super.updateItem(command, empty);

            updateBackground();
            updateGraphic(null, false, false);

            if (empty) {
                setText("");
                return;
            }

            setText(command.toString());

            command.statusProperty().addListener(this::commandStatusChanged);
            command.nonCanonicalProperty().addListener(this::updateGraphic);
        }

        private void commandStatusChanged(final ObservableValue<? extends CommandStatus> observableValue,
                                          final CommandStatus oldStatus,
                                          final CommandStatus newStatus) {
            updateBackground();
        }

        private void updateBackground() {
            if (isEmpty()) {
                setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
                return;
            }

            // Alternate background shades for items
            var even = getIndex() % 2 == 0;
            if (getItem().getStatus() == CommandStatus.UNDONE) {
                setBackground(even ? UNDONE_BACKGROUND_EVEN : UNDONE_BACKGROUND_ODD);
            } else {
                setBackground(even ? EXECUTED_BACKGROUND_EVEN : EXECUTED_BACKGROUND_ODD);
            }
        }

        private void updateGraphic(final ObservableValue<? extends Boolean> obsValue,
                                   final boolean oldVale,
                                   final boolean newVale) {
            if (isEmpty()) {
                setGraphic(null);
                return;
            }
            if (getItem() instanceof LegacyCommand) {
                setGraphic(new Rectangle(10, 10, Color.GRAY));
            } else if (getItem().isNonCanonical()) {
                setGraphic(new Rectangle(10, 10, Color.RED));
            } else {
                setGraphic(new Rectangle(10, 10, Color.GREEN));
            }
        }
    }
}

