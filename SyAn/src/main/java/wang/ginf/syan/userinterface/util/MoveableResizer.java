package wang.ginf.syan.userinterface.util;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.transform.NonInvertibleTransformException;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Der MoveableResizer verfolgt in weiten Teilen das Decorator-Pattern,
 * als dass er eine Komponente um weitere Funktionalitaeten dynamisch
 * erweitert. Der Unterschied besteht darin, dass der MoveableResizer nicht
 * mit der API der zu dekorierenden Komponente (hier Region) uebereinstimmt.
 * <p>
 * Komponenten, die mit dem MoveableResizer dekoriert werden, koennen fortan
 * zur Laufzeit bewegt (durch Dragging) und in der Groesse angepasst werden.
 * <p>
 * Das Design orientiert sich an:
 * https://github.com/varren/JavaFX-Resizable-Draggable-Node
 *
 * @author Ruben Smidt
 * @author Arne Kiesewetter
 */
public class MoveableResizer {
    private static final double DRAG_DISTANCE_THRESHOLD = 10;

    private final Region region;

    private final IntegerProperty resizeRegionPadding = new SimpleIntegerProperty(25);
    private final BooleanProperty movingDisabled = new SimpleBooleanProperty(false);
    private final BooleanProperty resizingDisabled = new SimpleBooleanProperty(false);

    private final Set<MoveableResizeHook> hooks = new HashSet<>();
    private MoveConsumer moveConsumer;
    private ResizeConsumer resizeConsumer;

    private boolean isResizing;
    private double yOfResizeStart;
    private double xOfResizeStart;

    private boolean isDragging;
    private boolean passedDragThreshold;
    private double xOfDragStart;
    private double yOfDragStart;
    private Cursor cursorBeforeAction;
    private double oldY;
    private double oldX;
    private double oldWidth;
    private double oldHeight;

    private MoveableResizer(final Region region) {
        this.region = region;

        register();
    }

    /**
     * Diese Fabrikmethode nimmt die zu aufwertende Komponente,
     * und initialisiert die noetige Logik, damit die Komponente beweg-
     * und in der Groesse veraenderbar wird.
     *
     * @param region die zu aufwertende Komponente.
     * @return die verwaltende Instanz.
     */
    public static MoveableResizer enhance(final Region region) {
        return new MoveableResizer(region);
    }

    /**
     * Setzt einen Listener auf das Fertigstellen eines Move-Events.
     *
     * @param moveConsumer der Listener.
     */
    public void onMoved(final MoveConsumer moveConsumer) {
        this.moveConsumer = moveConsumer;
    }

    /**
     * Setzt einen Listener auf das Fertigstellen eines Resize-Events.
     *
     * @param resizeConsumer der Listener.
     */
    public void onResize(final ResizeConsumer resizeConsumer) {
        this.resizeConsumer = resizeConsumer;
    }

    public void attachHook(final MoveableResizeHook hook) {
        hooks.add(hook);
    }

    private void onMouseDragged(final MouseEvent event) {
        if (isDragging) {
            var transformed = getScaledCoordinates(event.getSceneX(), event.getSceneY());
            final var movedX = xOfDragStart + transformed.getX();
            final var movedY = yOfDragStart + transformed.getY();

            final var xDif = oldX - movedX;
            final var yDif = oldY - movedY;
            final var movedDistance = Math.sqrt(xDif * xDif + yDif * yDif);

            if (passedDragThreshold || movedDistance >= DRAG_DISTANCE_THRESHOLD) {
                passedDragThreshold = true;

                final AtomicBoolean shouldContinue = new AtomicBoolean(true);
                hooks.forEach(hook -> {
                    if (!hook.onDrag(region, movedX, movedY)) {
                        shouldContinue.set(false);
                    }
                });

                if (shouldContinue.get()) {
                    region.setLayoutX(movedX);
                    region.setLayoutY(movedY);
                }
            }
        }

        if (isResizing) {
            final var newHeight = region.getMinHeight() + event.getY() - yOfResizeStart;
            final var newWidth = region.getMinWidth() + event.getX() - xOfResizeStart;

            final AtomicBoolean shouldContinue = new AtomicBoolean(true);
            hooks.forEach(hook -> {
                if (!hook.onResize(region, newWidth, newHeight)) {
                    shouldContinue.set(false);
                }
            });

            if (shouldContinue.get()) {
                region.setPrefSize(newWidth, newHeight);

                yOfResizeStart = event.getY();
                xOfResizeStart = event.getX();
            }
        }
    }

    private void onMousePressed(final MouseEvent event) {
        if (!event.isPrimaryButtonDown()) {
            return;
        }

        cursorBeforeAction = region.getCursor();

        if (!isResizingDisabled() && isInZone(event)) {
            isResizing = true;

            yOfResizeStart = event.getY();
            xOfResizeStart = event.getX();

            oldWidth = region.getPrefWidth();
            oldHeight = region.getPrefHeight();
        } else if (!isMovingDisabled()) {
            isDragging = true;

            var transformed = getScaledCoordinates(event.getSceneX(), event.getSceneY());
            xOfDragStart = region.getLayoutX() - transformed.getX();
            yOfDragStart = region.getLayoutY() - transformed.getY();

            oldX = region.getLayoutX();
            oldY = region.getLayoutY();

            region.setCursor(Cursor.CLOSED_HAND);
        }
    }

    public int getResizeRegionPadding() {
        return resizeRegionPadding.get();
    }

    public void setResizeRegionPadding(int value) { resizeRegionPadding.set(value); }

    public IntegerProperty resizeRegionPaddingProperty() { return resizeRegionPadding; }

    public boolean isMovingDisabled() {
        return movingDisabled.get();
    }

    public void setMovingDisabled(boolean value) { movingDisabled.set(value); }

    public BooleanProperty movingDisabledProperty() {
        return movingDisabled;
    }

    public boolean isResizingDisabled() {
        return resizingDisabled.get();
    }

    public void setResizingDisabled(boolean value) { resizingDisabled.set(value); }

    public BooleanProperty resizingDisabledProperty() {
        return resizingDisabled;
    }

    private void register() {
        region.setOnMouseMoved(this::onMouseMoved);
        region.setOnMousePressed(this::onMousePressed);
        region.setOnMouseDragged(this::onMouseDragged);
        region.setOnMouseReleased(this::onMouseReleased);

        region.parentProperty().addListener(this::parentChanged);
        if (region.getParent() != null) {
            region.getParent().cursorProperty().addListener(this::parentCursorChanged);
        }
    }

    private void parentChanged(final ObservableValue<? extends Parent> obsValue, Parent oldValue, Parent newValue) {
        if (oldValue != null) {
            oldValue.cursorProperty().removeListener(this::parentCursorChanged);
        }

        if (newValue != null) {
            newValue.cursorProperty().addListener(this::parentCursorChanged);
        }
    }

    private void parentCursorChanged(ObservableValue<? extends Cursor> observable, Cursor oldValue, Cursor newValue) {
        reset(newValue);
    }

    private void reset(final Cursor parentCursor) {
        isResizing = false;
        isDragging = false;
        passedDragThreshold = false;

        region.setCursor(parentCursor);
    }

    private boolean isInZone(final MouseEvent event) {
        return event.getY() > region.getHeight() - getResizeRegionPadding() &&
               event.getX() > region.getWidth() - getResizeRegionPadding();
    }

    private void onMouseMoved(final MouseEvent event) {
        if ((!isResizingDisabled() && isInZone(event)) || isResizing) {
            region.setCursor(Cursor.SE_RESIZE);
        } else if (isNotPartiallyDisabled()) {
            region.setCursor(Cursor.DEFAULT);
        }
    }

    private void onMouseReleased(final MouseEvent event) {
        if (event.getButton() != MouseButton.PRIMARY) {
            return;
        }

        if (isCurrentlyActionOnGoing() && region.getParent() != null) {
            if (Objects.nonNull(moveConsumer) && isDragging) {
                final var newX = region.getLayoutX();
                final var newY = region.getLayoutY();

                if (newX != oldX || newY != oldY) {
                    moveConsumer.accept(oldX, oldY, region.getLayoutX(), region.getLayoutY());
                }
            }

            if (Objects.nonNull(resizeConsumer) && isResizing) {
                final var newWidth = region.getPrefWidth();
                final var newHeight = region.getPrefHeight();

                if (newWidth != oldWidth || newHeight != oldHeight) {
                    resizeConsumer.accept(oldWidth, oldHeight, region.getPrefWidth(), region.getPrefHeight());
                }
            }

            reset(cursorBeforeAction);
        }
    }

    private boolean isCurrentlyActionOnGoing() {
        return isResizing || isDragging;
    }

    private boolean isNotPartiallyDisabled() {
        return !isResizingDisabled() && !isMovingDisabled();
    }

    private Point2D getScaledCoordinates(final double x, final double y) {
        try {
            return region.getLocalToSceneTransform().inverseDeltaTransform(x, y);
        } catch (NonInvertibleTransformException ex) {
            return new Point2D(x, y);
        }
    }

    @FunctionalInterface
    public interface MoveConsumer {
        void accept(final double oldX, final double oldY, final double newX, final double newY);
    }

    @FunctionalInterface
    public interface ResizeConsumer {
        void accept(final double oldWidth, final double oldHeight, final double newWidth, final double newHeight);
    }

    /**
     * Ein MoveableResizeHook erlaubt das Einklingen in den Prozess des Bewegens
     * und Grosseveraenderns eines aufgewerteten Knoten (Region).
     * <p>
     * Das Einklingen erlaubt das bedingte Abbrechen des aktuellen Bewegungs-
     * oder Groesserveraenderungsschrittes. Dafuer wird der naechste Wert der Aktion
     * uebergeben, anhand dessen der Ausgang des Prozesses bestimmt werden kann.
     *
     * @author Ruben Smidt
     */
    public interface MoveableResizeHook {
        boolean onDrag(final Region region, final double newX, final double newY);

        boolean onResize(final Region region, final double newWidth, final double newHeight);
    }

    /**
     * Einfacher {@see MoveableResizeHook} der das Verschieben beziehungsweise Veraendern der Groeße stoppt,
     * bevor das Element mit einem anderen kolidiert.
     *
     * @author Ruben Smidt
     * @author Arne Kiesewetter
     */
    public static class CollisionHook implements MoveableResizeHook {
        private static final CollisionHook instance = new CollisionHook();

        private CollisionHook() { }

        public static MoveableResizeHook get() { return instance; }

        @Override
        public boolean onDrag(final Region region, final double newX, final double newY) {
            if (region.getParent() == null) {
                return true;
            }

            for (final var pane : region.getParent().getChildrenUnmodifiable()) {
                if (pane != region &&
                    pane.getBoundsInParent()
                        .intersects(newX,
                                    newY,
                                    region.getBoundsInParent().getWidth(),
                                    region.getBoundsInParent().getHeight()) &&
                    !pane.getBoundsInParent().intersects(region.getBoundsInParent())) {
                    return false;
                }
            }

            return true;
        }

        @Override
        public boolean onResize(final Region region, final double newWidth, final double newHeight) {
            if (region.getParent() == null) {
                return true;
            }

            for (final var pane : region.getParent().getChildrenUnmodifiable()) {
                if (pane != region &&
                    pane.getBoundsInParent()
                        .intersects(region.getLayoutX(), region.getLayoutY(), newWidth, newHeight) &&
                    !pane.getBoundsInParent().intersects(region.getBoundsInParent())) {
                    return false;
                }
            }

            return true;
        }
    }

    /**
     * Einfacher {@see MoveableResizeHook} der das Verschieben beziehungsweise Veraendern der Groeße stoppt,
     * bevor das Element die Bounds des Parents verlaesst.
     *
     * @author Ruben Smidt
     * @author Arne Kiesewetter
     */
    public static class ParentBoundsHook implements MoveableResizeHook {
        private static final ParentBoundsHook instance = new ParentBoundsHook();

        private ParentBoundsHook() { }

        public static MoveableResizeHook get() { return instance; }

        @Override
        public boolean onDrag(final Region region, final double newX, final double newY) {
            if (region.getParent() == null) {
                return true;
            }

            return region.getParent()
                         .getLayoutBounds()
                         .contains(newX,
                                   newY,
                                   region.getBoundsInParent().getWidth(),
                                   region.getBoundsInParent().getHeight());
        }

        @Override
        public boolean onResize(final Region region, final double newWidth, final double newHeight) {
            if (region.getParent() == null) {
                return true;
            }

            return region.getParent()
                         .getLayoutBounds()
                         .contains(region.getBoundsInParent().getMinX(),
                                   region.getBoundsInParent().getMinY(),
                                   newWidth,
                                   newHeight);
        }
    }
}
