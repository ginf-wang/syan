package wang.ginf.syan.userinterface.controls;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import wang.ginf.syan.model.GlobalState;
import wang.ginf.syan.model.Settings;
import wang.ginf.syan.model.history.CommandHistory;
import wang.ginf.syan.model.history.commands.DeleteSymptomCommand;
import wang.ginf.syan.model.history.commands.MoveSymptomToSphereCommand;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;
import wang.ginf.syan.userinterface.util.ResourceHelper;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * Die Toolbox erlaubt das Vorbereiten eines Syndromansatzes.
 *
 * @author Ruben Smidt
 */
public class Toolbox extends VBox implements Initializable {
    private final ObjectProperty<Syndrome> syndrome = new SimpleObjectProperty<>();
    private final HBox symptomNameErrorHBox;
    private ResourceBundle bundle;
    @FXML
    private ListView<Symptom> toolboxListView;
    @FXML
    private TextField symptomTextField;
    @FXML
    private HBox submitGroup;
    @FXML
    private Text toolboxLabel;

    public Toolbox() throws IOException {
        final var loader = new FXMLLoader();
        loader.setRoot(this);
        loader.setController(this);
        loader.setLocation(getClass().getResource("/fxml/Toolbox.fxml"));
        ResourceHelper.inject(loader, "lang/Toolbox");
        loader.load();

        symptomNameErrorHBox = new HBox(new Text(bundle.getString("errorText")));
        symptomNameErrorHBox.setAlignment(Pos.CENTER);

        // Die Toolbox soll nur dann angezeigt werden, sollten Baukasensymptome existieren
        // oder wenn die Anwendung im Erstellermodus ist.
        syndromeProperty().addListener((observable, oldValue, newValue) -> {
            if (Objects.nonNull(oldValue)) {
                visibleProperty().unbind();
                managedProperty().unbind();
            }

            if (Objects.nonNull(newValue)) {
                final var showBinding = Bindings.createBooleanBinding(
                        () -> !newValue.getToolboxSphere().getSymptoms().isEmpty(),
                        newValue.getToolboxSphere().getSymptoms()
                ).or(GlobalState.atLeastCreatorProperty());

                visibleProperty().bind(showBinding);
                managedProperty().bind(showBinding);
            }
        });
        Settings.registerNodes(this);

        final var scaledFontSizeBinding = Settings.scaleFontSizeBinding(toolboxLabel.getFont().getSize());
        final var fontBinding = Bindings.createObjectBinding(
                () -> new Font(scaledFontSizeBinding.get()),
                scaledFontSizeBinding
        );
        toolboxLabel.fontProperty().bind(fontBinding);
        symptomTextField.textProperty().addListener(this::checkNameLength);
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        bundle = resources;

        syndromeProperty().addListener((observable, oldValue, newValue) -> {
            if (Objects.isNull(newValue)) {
                return;
            }

            // Wandelt das ObservableSet von Symptomen in der ToolboxSphere zu einer ObservableList.
            final var symptomsBinding = Bindings.createObjectBinding(
                    () -> FXCollections.observableArrayList(getSyndrome().getToolboxSphere().getSymptoms()),
                    getSyndrome().getToolboxSphere().getSymptoms()
            );

            toolboxListView.itemsProperty().bind(symptomsBinding);
            toolboxListView.refresh();
        });

        getStylesheets().add(getClass().getResource("/stylesheets/inputs.css").toExternalForm());
        symptomTextField.getStyleClass().addListener((ListChangeListener<? super String>) c -> {
            c.next();
            if (c.wasAdded()) {
                if (!getChildren().contains(symptomNameErrorHBox)) {
                    getChildren().add(symptomNameErrorHBox);
                }
            } else {
                getChildren().remove(symptomNameErrorHBox);
            }
        });

        toolboxListView.setCellFactory(symptomListView -> new ToolBoxCell());

        // Das Erstellen von neuen Baukasensymptomen soll nur moeglich sein,
        // wenn die Anwendung sich im Erstellermodus befindet.
        submitGroup.visibleProperty().bind(
                GlobalState.atLeastCreatorProperty()
                           .and(syndromeProperty().isNotNull())
        );
    }

    @FXML
    private void handleCreateClicked(final ActionEvent event) {
        final var symptomName = symptomTextField.getText().trim();

        if (verifyNewSymptom(symptomName)) {
            symptomTextField.getStyleClass().remove("error");

            getSyndrome().getToolboxSphere().getSymptoms().add(new Symptom(symptomName, Color.WHITE));
            symptomTextField.clear();
        } else {
            if (!symptomTextField.getStyleClass().contains("error")) {
                symptomTextField.getStyleClass().add("error");
            }
        }
    }

    private void checkNameLength(final ObservableValue<? extends String> observableValue,
                                 final String oldValue,
                                 final String newValue) {
        if (newValue.length() > 32) {
            symptomTextField.textProperty().setValue(oldValue);
            symptomTextField.getStyleClass().add("error");
        } else {
            symptomTextField.getStyleClass().remove("error");
        }
    }

    private boolean verifyNewSymptom(final String name) {
        if (name.length() > 32 || name.length() < 1) {
            return false;
        }

        return !getSyndrome().containsSymptom(name);
    }

    public Syndrome getSyndrome() {
        return syndrome.get();
    }

    public void setSyndrome(final Syndrome syndrome) {
        this.syndrome.set(syndrome);
    }

    public ObjectProperty<Syndrome> syndromeProperty() {
        return syndrome;
    }

    class ToolBoxCell extends ListCell<Symptom> {
        private final ContextMenu menu = new ContextMenu();

        public ToolBoxCell() {
            setStyle("-fx-alignment: center");
        }

        @Override
        protected void updateItem(final Symptom item, final boolean empty) {
            super.updateItem(item, empty);

            if (Objects.isNull(item) || empty) {
                setText(null);
                setGraphic(null);
                return;
            }

            createMenu();

            final var symptomControl = new SymptomControl(item, false, false);
            symptomControl.setContextMenu(menu);

            setGraphic(symptomControl);
        }

        private void createMenu() {
            if (Objects.isNull(getItem()) || isEmpty()) {
                setText(null);
                setGraphic(null);
                return;
            }

            menu.getItems().clear();

            final var deleteMenuItem = new MenuItem(bundle.getString("deleteSymptomText"));
            deleteMenuItem.setOnAction(event -> {
                final var command =
                        new DeleteSymptomCommand(getItem(), getSyndrome().getToolboxSphere(), getSyndrome());

                CommandHistory.getInstance().execute(command);
            });

            menu.getItems().add(deleteMenuItem);

            getSyndrome().getSpheres().forEach(sphere -> {
                final var menuItem = new MenuItem(String.format(bundle.getString("moveText"), sphere.getName()));
                menuItem.setOnAction(event -> {
                    final var command = new MoveSymptomToSphereCommand(
                            getItem(),
                            getSyndrome().getToolboxSphere(),
                            sphere
                    );

                    CommandHistory.getInstance().execute(command);
                });

                menu.getItems().add(menuItem);
            });
        }
    }
}
