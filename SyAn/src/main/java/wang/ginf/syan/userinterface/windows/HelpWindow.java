package wang.ginf.syan.userinterface.windows;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import wang.ginf.syan.userinterface.util.ResourceHelper;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Das HelpWindow stellt den Rahmen zur Darstellung des Benutzerhandbuchs.
 * <p>
 * Die Darstellung des Handbuchs erfolgt ueber eine WebView,
 * die das in Markup verfasste Dokumenent rendert.
 *
 * @author Jonas Luehrs
 * @author Ruben Smidt
 */
public class HelpWindow extends Stage implements Initializable {
    @FXML
    private WebView webView;

    public HelpWindow() throws IOException {
        final var fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/HelpWindow.fxml"));

        ResourceHelper.inject(fxmlLoader, "lang/HelpWindow");
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        show();
    }

    public void navigateTo(final String helpFile) {
        webView.getEngine().load(getClass().getResource(helpFile).toString());
    }
}
