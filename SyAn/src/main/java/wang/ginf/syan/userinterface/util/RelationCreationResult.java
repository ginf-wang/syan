package wang.ginf.syan.userinterface.util;

import wang.ginf.syan.model.syndrome.Relation;

import java.util.Optional;

/**
 * Ein Datentransferobjekt, das das Ergebnis einer Relationserstellung repraesentiert.
 *
 * @author Ruben Smidt
 */
public class RelationCreationResult {
    private final Relation relation;
    private final String errorMessage;

    private RelationCreationResult(final Relation relation, final String errorMessage) {
        this.relation = relation;
        this.errorMessage = errorMessage;
    }

    /**
     * Erstellt ein neues Resultat mit vorhandener Relation.
     *
     * @param relation Die Relation, die erstellt wurde.
     * @return Das Resultat.
     */
    public static RelationCreationResult withResult(final Relation relation) {
        return new RelationCreationResult(relation, null);
    }

    /**
     * Erstellt ein neues Resultat mit Fehler (ohne Relation).
     *
     * @param error Der Fehler.
     * @return Das Resultat.
     */
    public static RelationCreationResult withError(final String error) {
        return new RelationCreationResult(null, error);
    }

    public Optional<Relation> getRelation() {
        return Optional.ofNullable(relation);
    }

    public String getErrorMessage() { return errorMessage; }
}
