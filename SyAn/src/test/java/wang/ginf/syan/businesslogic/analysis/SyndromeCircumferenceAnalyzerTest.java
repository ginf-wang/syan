package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.syndrome.*;

import java.util.*;

/**
 * Testklasse, die die analyze Methode des SyndromeCircomferenceAnalyzer testet.
 *
 * @author Jonas Lührs
 */
public class SyndromeCircumferenceAnalyzerTest {

    /**
     * Testmethode, die ein leeres Syndrome testet.
     */
    @Test
    public void testSyndromeWithoutSymptomsAndRelations() throws Exception {
        Collection<Symptom> symptoms1 = new HashSet<>();
        Collection<Symptom> symptoms2 = new HashSet<>();

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Sphere sphere2 = new Sphere("Sphere2", Color.WHITE, symptoms2);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);
        spheres.add(sphere2);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeCircumferenceAnalyzer analyzer = new SyndromeCircumferenceAnalyzer();
        int circumference = analyzer.analyze(syndrome);

        Assertions.assertEquals(circumference, 0);
    }

    /**
     * Testmethode, die ein Syndrome testet, das aus nur einem Symptom besteht.
     */
    @Test
    public void testSyndromeWithOneSymptom() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeCircumferenceAnalyzer analyzer = new SyndromeCircumferenceAnalyzer();
        int circumference = analyzer.analyze(syndrome);

        Assertions.assertEquals(circumference, 1);
    }

    /**
     * Testmethode, die ein Syndrome testet, das aus zwei oder mehr Symptomen besteht.
     */
    @Test
    public void testSyndromeWithTwoOrMoreSymptoms() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeCircumferenceAnalyzer analyzer = new SyndromeCircumferenceAnalyzer();
        int circumference = analyzer.analyze(syndrome);

        Assertions.assertEquals(circumference, 2);
    }

    /**
     * Testmethode, die ein Syndrome testet, das keine Relationen enthält.
     */
    @Test
    public void testSyndromeWithoutRelations() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        Collection<Symptom> symptoms1 = new HashSet<>();
        Collection<Symptom> symptoms2 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms2.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Sphere sphere2 = new Sphere("Sphere2", Color.WHITE, symptoms2);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);
        spheres.add(sphere2);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeCircumferenceAnalyzer analyzer = new SyndromeCircumferenceAnalyzer();
        int circumference = analyzer.analyze(syndrome);

        Assertions.assertEquals(circumference, 3);
    }

    /**
     * Testmethode, die ein Syndrome testet, das eine Relation enthält.
     */
    @Test
    public void testSyndromeWithOneRelation() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        Collection<Symptom> symptoms2 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms2.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Sphere sphere2 = new Sphere("Sphere2", Color.WHITE, symptoms2);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);
        spheres.add(sphere2);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeCircumferenceAnalyzer analyzer = new SyndromeCircumferenceAnalyzer();
        int circumference = analyzer.analyze(syndrome);

        Assertions.assertEquals(circumference, 4);
    }

    /**
     * Testmethode, die ein Syndrome testet, das mehr als eine Relation enthält.
     */
    @Test
    public void testSyndromeWithTwoOrMoreRelations() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        Collection<Symptom> symptoms2 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms2.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Sphere sphere2 = new Sphere("Sphere2", Color.WHITE, symptoms2);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);
        spheres.add(sphere2);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeCircumferenceAnalyzer analyzer = new SyndromeCircumferenceAnalyzer();
        int circumference = analyzer.analyze(syndrome);

        Assertions.assertEquals(circumference, 5);
    }
}
