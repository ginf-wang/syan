package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.syndrome.*;

import java.util.*;

/**
 * Testklasse, die die Methoden von Statistics testet.
 *
 * @author Jonas Lührs
 */
class StatisticsTest {

    /**
     * Testmethode, die die Werte der Syndrom-Statistik nach Aufruf der setSyndromeStatistics()-Methode überprüft.
     */
    @Test
    public void testSetSyndromeStatistics() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.WEAK, RelationType.INCREASING, symptom1, symptom3, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        Collection<Symptom> symptoms2 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms2.add(symptom3);
        symptoms2.add(symptom4);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Sphere sphere2 = new Sphere("Sphere2", Color.WHITE, symptoms2);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);
        spheres.add(sphere2);

        Syndrome syndrome = new Syndrome(spheres);

        Statistics statistics = Statistics.getInstance();
        statistics.setSyndromeStatistics(syndrome);

        Assertions.assertEquals(statistics.numOfSyndromeSymptomsProperty().get(), 4);
        Assertions.assertEquals(statistics.numOfSyndromeArrowChainsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfSyndromeRamificationsProperty().get(), 1);
        Assertions.assertEquals(statistics.numOfSyndromeConvergentRamificationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfSyndromeDivergentRamificationsProperty().get(), 1);
        Assertions.assertEquals(statistics.numOfSyndromeRelationsProperty().get(), 2);
        Assertions.assertEquals(statistics.numOfSyndromeUnknownRelationsProperty().get(), 1);
        Assertions.assertEquals(statistics.numOfSyndromeLesseningRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfSyndromeIncreasingRelationsProperty().get(), 1);
        Assertions.assertEquals(statistics.circumferenceProperty().get(), 6);
        Assertions.assertEquals(statistics.interconnectivityProperty().get(), 4.0 / 4.0);
    }

    /**
     * Testmethode, die die Werte der Symptom-Statistik nach Aufruf der setSymptomStatistics()-Methode überprüft.
     */
    @Test
    public void testSetSymptomStatistics() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.WEAK, RelationType.INCREASING, symptom1, symptom3, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        Collection<Symptom> symptoms2 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms2.add(symptom3);
        symptoms2.add(symptom4);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Sphere sphere2 = new Sphere("Sphere2", Color.WHITE, symptoms2);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);
        spheres.add(sphere2);

        Syndrome syndrome = new Syndrome(spheres);

        Statistics statistics = Statistics.getInstance();
        statistics.setSymptomStatistics(symptom1);

        Assertions.assertEquals(statistics.numOfNeighboursProperty().get(), 2);
        Assertions.assertEquals(statistics.numOfPredecessorsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfSuccessorsProperty().get(), 2);
        Assertions.assertEquals(statistics.numOfArrowChainsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfRamificationProperty().get(), 1);
        Assertions.assertEquals(statistics.numOfConvergentRamificationProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfDivergentRamificationProperty().get(), 1);
        Assertions.assertEquals(statistics.numOfRelationsProperty().get(), 2);
        Assertions.assertEquals(statistics.numOfIncomingRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfIncomingUnknownRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfIncomingLesseningRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfIncomingIncreasingRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfOutgoingRelationsProperty().get(), 2);
        Assertions.assertEquals(statistics.numOfOutgoingUnknownRelationsProperty().get(), 1);
        Assertions.assertEquals(statistics.numOfOutgoingLesseningRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfOutgoingIncreasingRelationsProperty().get(), 1);
    }

    /**
     * Testmethode, die die Werte der Symptom-Statistik nach Aufruf der resetSymptomStatistics()-Methode überprüft.
     */
    @Test
    public void testResetSymptomStatistics() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.WEAK, RelationType.INCREASING, symptom1, symptom3, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        Collection<Symptom> symptoms2 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms2.add(symptom3);
        symptoms2.add(symptom4);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Sphere sphere2 = new Sphere("Sphere2", Color.WHITE, symptoms2);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);
        spheres.add(sphere2);

        Syndrome syndrome = new Syndrome(spheres);

        Statistics statistics = Statistics.getInstance();
        statistics.setSymptomStatistics(symptom1);
        statistics.resetSymptomStatistics();

        Assertions.assertEquals(statistics.numOfNeighboursProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfPredecessorsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfSuccessorsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfArrowChainsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfRamificationProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfConvergentRamificationProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfDivergentRamificationProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfIncomingRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfIncomingUnknownRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfIncomingLesseningRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfIncomingIncreasingRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfOutgoingRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfOutgoingUnknownRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfOutgoingLesseningRelationsProperty().get(), 0);
        Assertions.assertEquals(statistics.numOfOutgoingIncreasingRelationsProperty().get(), 0);
    }
}
