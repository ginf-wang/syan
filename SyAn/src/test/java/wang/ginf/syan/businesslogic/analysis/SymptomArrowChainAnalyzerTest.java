package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.syndrome.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Testklasse, die die Methoden des SymptomArrowChainAnalyzer testet.
 *
 * @author Jonas Lührs
 */
class SymptomArrowChainAnalyzerTest {

    /**
     * Testmethode, die ein Symptom testet, das einen inneren Knoten einer Pfeilkette darstellt.
     */
    @Test
    public void testInternalNodeWithoutArrowChain() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom6, waypoints).addToSymptoms();

        SymptomArrowChainAnalyzer analyzer = new SymptomArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(symptom3);

        Assertions.assertEquals(numOfArrowChains, 0);
    }

    /**
     * Testmethode, die ein Symptom testet, das einen inneren Knoten einer Pfeilkette darstellt.
     */
    @Test
    public void testInternalNodeWithArrowChain() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        // TODO: Add calls to .addToSymptoms() if you use these
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom6, waypoints).addToSymptoms();

        SymptomArrowChainAnalyzer analyzer = new SymptomArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(symptom2);

        Assertions.assertEquals(numOfArrowChains, 1);
    }

    /**
     * Testmethode, die ein Symptom testet, das einen potentiellen Startknoten darstellt, aber kein Teil einer
     * Pfeilkette ist.
     */
    @Test
    public void testStartNodeWithoutArrowChains() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        // TODO: Add calls to .addToSymptoms() if you use these
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom6, waypoints).addToSymptoms();

        SymptomArrowChainAnalyzer analyzer = new SymptomArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(symptom2);

        Assertions.assertEquals(numOfArrowChains, 0);
    }

    /**
     * Testmethode, die ein Symptom testet, das den Startknoten einer Pfeilkette darstellt.
     */
    @Test
    public void testStartNodeWithOneArrowChain() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);
        Symptom symptom7 = new Symptom("Symptom7", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        // TODO: Add calls to .addToSymptoms() if you use these
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom6, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom7, waypoints).addToSymptoms();

        SymptomArrowChainAnalyzer analyzer = new SymptomArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(symptom2);

        Assertions.assertEquals(numOfArrowChains, 1);
    }

    /**
     * Testmethode, die ein Symptom testet, das den Startknoten von mehr als zwei Pfeilketten darstellt.
     */
    @Test
    public void testStartNodeWithTwoOrMoreArrowChains() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);
        Symptom symptom7 = new Symptom("Symptom7", Color.PINK);
        Symptom symptom8 = new Symptom("Symptom8", Color.PINK);
        Symptom symptom9 = new Symptom("Symptom9", Color.PINK);
        Symptom symptom10 = new Symptom("Symptom10", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        // TODO: Add calls to .addToSymptoms() if you use these
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom6, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom7, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom7, symptom8, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom8, symptom9, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom9, symptom10, waypoints).addToSymptoms();

        SymptomArrowChainAnalyzer analyzer = new SymptomArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(symptom2);

        Assertions.assertEquals(numOfArrowChains, 2);
    }

    /**
     * Testmethode, die ein Symptom testet, das einen potentiellen Startknoten darstellt, aber kein Teil einer
     * Pfeilkette ist.
     */
    @Test
    public void testEndNodeWithoutArrowChains() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        // TODO: Add calls to .addToSymptoms() if you use these
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom6, waypoints).addToSymptoms();

        SymptomArrowChainAnalyzer analyzer = new SymptomArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(symptom4);

        Assertions.assertEquals(numOfArrowChains, 0);
    }

    /**
     * Testmethode, die ein Symptom testet, das den Startknoten einer Pfeilkette darstellt.
     */
    @Test
    public void testEndNodeWithOneArrowChain() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);
        Symptom symptom7 = new Symptom("Symptom7", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        // TODO: Add calls to .addToSymptoms() if you use these
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom6, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom7, waypoints).addToSymptoms();

        SymptomArrowChainAnalyzer analyzer = new SymptomArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(symptom5);

        Assertions.assertEquals(numOfArrowChains, 1);
    }

    /**
     * Testmethode, die ein Symptom testet, das den Startknoten von mehr als zwei Pfeilketten darstellt.
     */
    @Test
    public void testEndNodeWithTwoOrMoreArrowChains() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);
        Symptom symptom7 = new Symptom("Symptom7", Color.PINK);
        Symptom symptom8 = new Symptom("Symptom8", Color.PINK);
        Symptom symptom9 = new Symptom("Symptom9", Color.PINK);
        Symptom symptom10 = new Symptom("Symptom10", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        // TODO: Add calls to .addToSymptoms() if you use these
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom6, symptom7, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom7, symptom8, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom8, symptom9, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom9, symptom10, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom10, symptom5, waypoints).addToSymptoms();

        SymptomArrowChainAnalyzer analyzer = new SymptomArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(symptom5);

        Assertions.assertEquals(numOfArrowChains, 2);
    }
}
