package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.syndrome.*;

import java.util.*;

/**
 * Testklasse, die die Methoden des SyndromeRamificationAnalyzer testet.
 *
 * @author Jonas Lührs
 */
class SyndromeRamificationAnalyzerTest {

    /**
     * Testmethode, die ein Syndrom ohne Verzweigungen testet.
     */
    @Test
    public void testSyndromeWithoutRamifications() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRamificationAnalyzer analyzer = new SyndromeRamificationAnalyzer();
        int numOfRamifications = analyzer.analyze(syndrome);

        Assertions.assertEquals(numOfRamifications, 0);
    }

    /**
     * Testmethode, die ein Syndrom mit einer Verzweigung testet.
     */
    @Test
    public void testSyndromeWithOneRamification() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRamificationAnalyzer analyzer = new SyndromeRamificationAnalyzer();
        int numOfRamifications = analyzer.analyze(syndrome);

        Assertions.assertEquals(numOfRamifications, 1);
    }

    /**
     * Testmethode, die ein Syndrom mit zwei oder mehr Verzweigungen testet.
     */
    @Test
    public void testSyndromeWithTwoOrMoreRamifications() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom1, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);
        symptoms1.add(symptom4);
        symptoms1.add(symptom5);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRamificationAnalyzer analyzer = new SyndromeRamificationAnalyzer();
        int numOfRamifications = analyzer.analyze(syndrome);

        Assertions.assertEquals(numOfRamifications, 2);
    }

    /**
     * Testmethode, die ein Syndrom ohne kovergente Verzweigungen testet.
     */
    @Test
    public void testSymptomWithoutConvergentRamifications() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRamificationAnalyzer analyzer = new SyndromeRamificationAnalyzer();
        int numOfConvergentRamifications = analyzer.getConvergentRamification(syndrome);


        Assertions.assertEquals(numOfConvergentRamifications, 0);
    }

    /**
     * Testmethode, die ein Syndrom mit einer kovergenten Verzweigung testet.
     */
    @Test
    public void testSymptomWithOneConvergentRamification() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom1, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRamificationAnalyzer analyzer = new SyndromeRamificationAnalyzer();
        int numOfConvergentRamifications = analyzer.getConvergentRamification(syndrome);


        Assertions.assertEquals(numOfConvergentRamifications, 1);
    }

    /**
     * Testmethode, die ein Syndrom mit zwei oder mehr kovergenten Verzweigungen testet.
     */
    @Test
    public void testSymptomWithTwoOrMoreConvergentRamifications() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom2, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRamificationAnalyzer analyzer = new SyndromeRamificationAnalyzer();
        int numOfConvergentRamifications = analyzer.getConvergentRamification(syndrome);


        Assertions.assertEquals(numOfConvergentRamifications, 2);
    }

    /**
     * Testmethode, die ein Syndrom ohne divergente Verzweigungen testet.
     */
    @Test
    public void testSyndromeWithoutDivergentRamifications() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom1, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRamificationAnalyzer analyzer = new SyndromeRamificationAnalyzer();
        int numOfDivergentRamifications = analyzer.getDivergentRamification(syndrome);

        Assertions.assertEquals(numOfDivergentRamifications, 0);
    }

    /**
     * Testmethode, die ein Syndrom mit einer divergenten Verzweigung testet.
     */
    @Test
    public void testSyndromeWithOneDivergentRamification() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRamificationAnalyzer analyzer = new SyndromeRamificationAnalyzer();
        int numOfDivergentRamifications = analyzer.getDivergentRamification(syndrome);

        Assertions.assertEquals(numOfDivergentRamifications, 1);
    }

    /**
     * Testmethode, die ein Syndrom mit zwei oder mehr divergenten Verzweigungen testet.
     */
    @Test
    public void testSyndromeWithTwoOrMoreDivergentRamifications() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRamificationAnalyzer analyzer = new SyndromeRamificationAnalyzer();
        int numOfDivergentRamifications = analyzer.getDivergentRamification(syndrome);

        Assertions.assertEquals(numOfDivergentRamifications, 2);
    }

}
