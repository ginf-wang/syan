package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.syndrome.*;

import java.util.ArrayList;
import java.util.List;

import static wang.ginf.syan.model.syndrome.RelationType.*;

/**
 * Testklasse, die die Methoden des SymptomRelationAnalyzer testet.
 *
 * @author Jonas Lührs
 */
class SymptomRelationAnalyzerTest {

    /**
     * Testmethode, die ein Symptom ohne Relationen testet.
     */
    @Test
    public void testSymptomWithoutRelations() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);

        SymptomRelationAnalyzer analyzer = new SymptomRelationAnalyzer();
        int relations = analyzer.analyze(symptom1);

        Assertions.assertEquals(relations, 0);
    }

    /**
     * Testmethode, die ein Symptom mit einer Relation testet.
     */
    @Test
    public void testSymptomWithOneRelations() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();

        SymptomRelationAnalyzer analyzer = new SymptomRelationAnalyzer();
        int relations = analyzer.analyze(symptom1);

        Assertions.assertEquals(relations, 1);
    }

    /**
     * Testmethode, die ein Symptom mit zwei oder mehr Relationen testet.
     */
    @Test
    public void testSymptomWithTwoOrMoreRelations() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();

        SymptomRelationAnalyzer analyzer = new SymptomRelationAnalyzer();
        int relations = analyzer.analyze(symptom1);

        Assertions.assertEquals(relations, 2);
    }

    /**
     * Testmethode, die ein Symptom mit drei eingehenden Relationen eines jeden Relationstypen testet.
     */
    @Test
    public void testSymptomWithThreeIncomingRelationsOfEachType() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, LESSENING, symptom3, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, INCREASING, symptom4, symptom1, waypoints).addToSymptoms();

        SymptomRelationAnalyzer analyzer = new SymptomRelationAnalyzer();
        int unknownRelations = analyzer.getNumberOfSpecialTypeRelations(symptom1, UNKNOWN, "Incoming");
        int lesseningRelations = analyzer.getNumberOfSpecialTypeRelations(symptom1, LESSENING, "Incoming");
        int increasingRelations = analyzer.getNumberOfSpecialTypeRelations(symptom1, INCREASING, "Incoming");

        Assertions.assertEquals(unknownRelations, 1);
        Assertions.assertEquals(lesseningRelations, 1);
        Assertions.assertEquals(increasingRelations, 1);
    }

    /**
     * Testmethode, die ein Symptom mit drei ausgehende Relationen eines jeden Relationstypen testet.
     */
    @Test
    public void testSymptomWithThreeOutgoingRelationsOfEachType() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, LESSENING, symptom1, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, INCREASING, symptom1, symptom4, waypoints).addToSymptoms();

        SymptomRelationAnalyzer analyzer = new SymptomRelationAnalyzer();
        int unknownRelations = analyzer.getNumberOfSpecialTypeRelations(symptom1, UNKNOWN, "Outgoing");
        int lesseningRelations = analyzer.getNumberOfSpecialTypeRelations(symptom1, LESSENING, "Outgoing");
        int increasingRelations = analyzer.getNumberOfSpecialTypeRelations(symptom1, INCREASING, "Outgoing");

        Assertions.assertEquals(unknownRelations, 1);
        Assertions.assertEquals(lesseningRelations, 1);
        Assertions.assertEquals(increasingRelations, 1);
    }
}
