package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.syndrome.Relation;
import wang.ginf.syan.model.syndrome.RelationStrength;
import wang.ginf.syan.model.syndrome.RelationType;
import wang.ginf.syan.model.syndrome.Symptom;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Enthält Tests für die {@see SymptomPathBetweenNodesAnalyzer} Klasse.
 *
 * @author Arne Kiesewetter
 */
class SymptomPathBetweenNodesAnalyzerTest {
    private Symptom start;
    private Symptom end;
    private Set<Symptom> expected;

    @BeforeEach
    public void beforeEach() {
        start = new Symptom("start", Color.WHITE);
        end = new Symptom("end", Color.WHITE);
        expected = new HashSet<>();
    }

    /**
     * Testmethode, die den simpelsten Fall von zwei komplett unverbundenen Symptomen testet.
     */
    @Test
    public void testForwardReachableNoOtherSymptoms() {
        final var result = SymptomPathBetweenNodesAnalyzer.getForwardReachableSymptomsInbetween(start, end);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die den simpelsten Fall von zwei komplett unverbundenen Symptomen testet.
     */
    @Test
    public void testReverseReachableNoOtherSymptoms() {
        final var result = SymptomPathBetweenNodesAnalyzer.getReverseReachableSymptomsInbetween(start, end);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation aber keinem Pfad testet.
     */
    @Test
    public void testForwardReachableOtherSymptomNoPath() {
        final var other = new Symptom("other", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, start, other, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getForwardReachableSymptomsInbetween(start, end);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation aber keinem Pfad testet.
     */
    @Test
    public void testReverseReachableOtherSymptomNoPath() {
        final var other = new Symptom("other", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, start, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getReverseReachableSymptomsInbetween(start, end);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet.
     */
    @Test
    public void testForwardReachableOtherSymptom() {
        final var other = new Symptom("other", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, start, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, end, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getForwardReachableSymptomsInbetween(start, end);
        expected.add(other);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet.
     */
    @Test
    public void testReverseReachableOtherSymptom() {
        final var other = new Symptom("other", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, start, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, end, other, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getReverseReachableSymptomsInbetween(start, end);
        expected.add(other);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet,
     * der einen Loop zwischen den Pfadsymptomen enthält.
     */
    @Test
    public void testForwardReachableLoopingSymptoms() {
        final var other = new Symptom("other", Color.WHITE);
        final var other2 = new Symptom("other2", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, start, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, other2, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, end, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getForwardReachableSymptomsInbetween(start, end);
        expected.add(other);
        expected.add(other2);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet,
     * der einen Loop zwischen den Pfadsymptomen enthält.
     */
    @Test
    public void testReverseReachableLoopingSymptoms() {
        final var other = new Symptom("other", Color.WHITE);
        final var other2 = new Symptom("other2", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, start, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, other2, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, end, other2, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getReverseReachableSymptomsInbetween(start, end);
        expected.add(other);
        expected.add(other2);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet,
     * der einen Diamond zwischen den Pfadsymptomen enthält.
     */
    @Test
    public void testForwardReachableDiamondSymptoms() {
        final var other = new Symptom("other", Color.WHITE);
        final var other2 = new Symptom("other2", Color.WHITE);
        final var other3 = new Symptom("other3", Color.WHITE);
        final var other4 = new Symptom("other4", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, start, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, other2, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, other4, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, other3, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other3, other4, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, end, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getForwardReachableSymptomsInbetween(start, end);
        expected.add(other);
        expected.add(other2);
        expected.add(other3);
        expected.add(other4);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet,
     * der einen Diamond zwischen den Pfadsymptomen enthält.
     */
    @Test
    public void testReverseReachableDiamondSymptoms() {
        final var other = new Symptom("other", Color.WHITE);
        final var other2 = new Symptom("other2", Color.WHITE);
        final var other3 = new Symptom("other3", Color.WHITE);
        final var other4 = new Symptom("other4", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, start, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, other2, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other3, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, other3, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, end, other4, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getReverseReachableSymptomsInbetween(start, end);
        expected.add(other);
        expected.add(other2);
        expected.add(other3);
        expected.add(other4);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet,
     * der einen Diamond zwischen den Pfadsymptomen enthält, bei dem eine Relation umgedreht ist.
     */
    @Test
    public void testForwardReachableDiamondWithReverseSymptoms() {
        final var other = new Symptom("other", Color.WHITE);
        final var other2 = new Symptom("other2", Color.WHITE);
        final var other3 = new Symptom("other3", Color.WHITE);
        final var other4 = new Symptom("other4", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, start, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, other2, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, other4, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, other3, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, other3, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, end, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getForwardReachableSymptomsInbetween(start, end);
        expected.add(other);
        expected.add(other2);
        expected.add(other4);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet,
     * der einen Diamond zwischen den Pfadsymptomen enthält, bei dem eine Relation umgedreht ist.
     */
    @Test
    public void testReverseReachableDiamondWithReverseSymptoms() {
        final var other = new Symptom("other", Color.WHITE);
        final var other2 = new Symptom("other2", Color.WHITE);
        final var other3 = new Symptom("other3", Color.WHITE);
        final var other4 = new Symptom("other4", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, start, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, other2, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other3, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other3, other4, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, end, other4, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getReverseReachableSymptomsInbetween(start, end);
        expected.add(other);
        expected.add(other2);
        expected.add(other4);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet,
     * der eine Gabelung enthält, bei der nur ein Zweig zum Ziel führt.
     */
    @Test
    public void testForwardReachableSeparateBranchSymptoms() {
        final var other = new Symptom("other", Color.WHITE);
        final var other2 = new Symptom("other2", Color.WHITE);
        final var other3 = new Symptom("other3", Color.WHITE);
        final var other4 = new Symptom("other4", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, start, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, other2, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, other4, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, other3, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, end, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getForwardReachableSymptomsInbetween(start, end);
        expected.add(other);
        expected.add(other2);
        expected.add(other4);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet,
     * der eine Gabelung enthält, bei der nur ein Zweig zum Ziel führt.
     */
    @Test
    public void testReverseReachableSeparateBranchSymptoms() {
        final var other = new Symptom("other", Color.WHITE);
        final var other2 = new Symptom("other2", Color.WHITE);
        final var other3 = new Symptom("other3", Color.WHITE);
        final var other4 = new Symptom("other4", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, start, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, other2, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other3, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, end, other4, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getReverseReachableSymptomsInbetween(start, end);
        expected.add(other);
        expected.add(other2);
        expected.add(other4);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet,
     * der einen loop mit mehreren Symptomen enthält.
     */
    @Test
    public void testForwardReachableLargerLoopSymptoms() {
        final var other = new Symptom("other", Color.WHITE);
        final var other2 = new Symptom("other2", Color.WHITE);
        final var other3 = new Symptom("other3", Color.WHITE);
        final var other4 = new Symptom("other4", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, start, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, other2, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, other4, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, end, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, other3, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other3, other, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getForwardReachableSymptomsInbetween(start, end);
        expected.add(other);
        expected.add(other2);
        expected.add(other3);
        expected.add(other4);

        assertEquals(expected, result);
    }

    /**
     * Testmethode, die ein Startsymptom mit einer in die richtige Richtung gehenden Relation und einem Pfad testet,
     * der einen Diamond zwischen den Pfadsymptomen enthält, bei dem eine Relation umgedreht ist.
     */
    @Test
    public void testReverseReachableLargerLoopSymptoms() {
        final var other = new Symptom("other", Color.WHITE);
        final var other2 = new Symptom("other2", Color.WHITE);
        final var other3 = new Symptom("other3", Color.WHITE);
        final var other4 = new Symptom("other4", Color.WHITE);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, start, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other2, other, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other4, other2, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, end, other4, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other3, other4, null).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, other, other3, null).addToSymptoms();

        final var result = SymptomPathBetweenNodesAnalyzer.getReverseReachableSymptomsInbetween(start, end);
        expected.add(other);
        expected.add(other2);
        expected.add(other3);
        expected.add(other4);

        assertEquals(expected, result);
    }
}
