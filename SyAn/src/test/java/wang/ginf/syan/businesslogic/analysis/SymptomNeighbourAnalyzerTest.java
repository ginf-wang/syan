package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.syndrome.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Testklasse, die die Methoden des SymptomNeighbourAnalyzer testet.
 *
 * @author Jonas Lührs
 */
class SymptomNeighbourAnalyzerTest {

    /**
     * Testmethode, die ein Symptom ohne Nachbarn testet.
     */
    @Test
    public void testSymptomWithoutNeighbours() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);

        SymptomNeighbourAnalyzer analyzer = new SymptomNeighbourAnalyzer();
        int numOfNeighbour = analyzer.analyze(symptom1);

        Assertions.assertEquals(numOfNeighbour, 0);
    }

    /**
     * Testmethode, die ein Symptom mit einem Nachbarn testet.
     */
    @Test
    public void testSymptomWithOneNeighbour() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();

        SymptomNeighbourAnalyzer analyzer = new SymptomNeighbourAnalyzer();
        int numOfNeighbour = analyzer.analyze(symptom1);

        Assertions.assertEquals(numOfNeighbour, 1);
    }

    /**
     * Testmethode, die ein Symptom mit zwei oder mehr Nachbarn testet.
     */
    @Test
    public void testSymptomWithTwoOrMoreNeighbours() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom1, waypoints).addToSymptoms();

        SymptomNeighbourAnalyzer analyzer = new SymptomNeighbourAnalyzer();
        int numOfNeighbour = analyzer.analyze(symptom1);

        Assertions.assertEquals(numOfNeighbour, 4);
    }

    /**
     * Testmethode, die ein Symptom ohne Vorgaenger testet.
     */
    @Test
    public void testSymptomWithoutPredecessors() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);

        SymptomNeighbourAnalyzer analyzer = new SymptomNeighbourAnalyzer();
        int numOfPredecessors = analyzer.getNumberOfPredecessors(symptom1);

        Assertions.assertEquals(numOfPredecessors, 0);
    }

    /**
     * Testmethode, die ein Symptom mit einem Vorgaenger testet.
     */
    @Test
    public void testSymptomWithOnePredecessor() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();

        SymptomNeighbourAnalyzer analyzer = new SymptomNeighbourAnalyzer();
        int numOfPredecessors = analyzer.getNumberOfPredecessors(symptom1);

        Assertions.assertEquals(numOfPredecessors, 1);
    }

    /**
     * Testmethode, die ein Symptom mit zwei oder mehr Vorgaengern testet.
     */
    @Test
    public void testSymptomWithTwoOrMorePredecessors() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom1, waypoints).addToSymptoms();

        SymptomNeighbourAnalyzer analyzer = new SymptomNeighbourAnalyzer();
        int numOfPredecessors = analyzer.getNumberOfPredecessors(symptom1);

        Assertions.assertEquals(numOfPredecessors, 2);
    }

    /**
     * Testmethode, die ein Symptom ohne Nachfolger testet.
     */
    @Test
    public void testSymptomWithoutSuccessors() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);

        SymptomNeighbourAnalyzer analyzer = new SymptomNeighbourAnalyzer();
        int numOfSuccessors = analyzer.getNumberOfSuccessors(symptom1);

        Assertions.assertEquals(numOfSuccessors, 0);
    }

    /**
     * Testmethode, die ein Symptom mit einem Nachfolger testet.
     */
    @Test
    public void testSymptomWithOneSuccessor() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();

        SymptomNeighbourAnalyzer analyzer = new SymptomNeighbourAnalyzer();
        int numOfSuccessors = analyzer.getNumberOfSuccessors(symptom1);

        Assertions.assertEquals(numOfSuccessors, 1);
    }

    /**
     * Testmethode, die ein Symptom mit zwei oder mehr Vorgaengern testet.
     */
    @Test
    public void testSymptomWithTwoOrMoreSuccessors() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();

        SymptomNeighbourAnalyzer analyzer = new SymptomNeighbourAnalyzer();
        int numOfSuccessors = analyzer.getNumberOfSuccessors(symptom1);

        Assertions.assertEquals(numOfSuccessors, 2);
    }
}
