package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.syndrome.*;

import java.util.*;

import static wang.ginf.syan.model.syndrome.RelationStrength.MEDIUM;
import static wang.ginf.syan.model.syndrome.RelationType.*;

class SyndromeRelationAnalyzerTest {

    /**
     * Testmethode, die ein Syndrom ohne Relationen testet.
     */
    @Test
    public void testSyndromeWithoutRelations() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRelationAnalyzer analyzer = new SyndromeRelationAnalyzer();
        int numOfRelations = analyzer.analyze(syndrome);

        Assertions.assertEquals(numOfRelations, 0);
    }

    /**
     * Testmethode, die ein Syndrom mit einer Relation testet.
     */
    @Test
    public void testSyndromeWithOneRelations() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(MEDIUM, UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRelationAnalyzer analyzer = new SyndromeRelationAnalyzer();
        int numOfRelations = analyzer.analyze(syndrome);

        Assertions.assertEquals(numOfRelations, 1);
    }

    /**
     * Testmethode, die ein Syndrom mit zwei oder mehr Relationen testet.
     */
    @Test
    public void testSyndromeWithTwoOrMoreRelations() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(MEDIUM, UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(MEDIUM, UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRelationAnalyzer analyzer = new SyndromeRelationAnalyzer();
        int numOfRelations = analyzer.analyze(syndrome);

        Assertions.assertEquals(numOfRelations, 2);
    }

    /**
     * Testmethode, die ein Syndrom mit drei Relationen eines jeden Relationstypen testet.
     */
    @Test
    public void testSyndromeWithThreeRelationsOfEachType() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(MEDIUM, UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(MEDIUM, LESSENING, symptom3, symptom1, waypoints).addToSymptoms();
        new Relation(MEDIUM, INCREASING, symptom4, symptom1, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);
        symptoms1.add(symptom4);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeRelationAnalyzer analyzer = new SyndromeRelationAnalyzer();
        int unknownRelations = analyzer.getNumberOfSpecialTypeRelations(syndrome, UNKNOWN);
        int lesseningRelations = analyzer.getNumberOfSpecialTypeRelations(syndrome, LESSENING);
        int increasingRelations = analyzer.getNumberOfSpecialTypeRelations(syndrome, INCREASING);

        Assertions.assertEquals(unknownRelations, 1);
        Assertions.assertEquals(lesseningRelations, 1);
        Assertions.assertEquals(increasingRelations, 1);
    }
}
