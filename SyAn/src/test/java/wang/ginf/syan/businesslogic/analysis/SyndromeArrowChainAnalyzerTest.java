package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.syndrome.*;

import java.util.*;

/**
 * Testklasse, die die Methoden des SyndromeArrowChainAnalyzer testet.
 *
 * @author Jonas Lührs
 */
class SyndromeArrowChainAnalyzerTest {

    /**
     * Testmethode, die ein Syndrom ohne Pfeilketten testet.
     */
    @Test
    public void testSyndromeWithoutArrowChains() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom6, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);
        symptoms1.add(symptom4);
        symptoms1.add(symptom5);
        symptoms1.add(symptom6);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeArrowChainAnalyzer analyzer = new SyndromeArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(syndrome);

        Assertions.assertEquals(numOfArrowChains, 0);
    }

    /**
     * Testmethode, die ein Syndrom mit einer Pfeilkette testet.
     */
    @Test
    public void testSyndromeWithOneArrowChain() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);
        Symptom symptom7 = new Symptom("Symptom7", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom6, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom7, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);
        symptoms1.add(symptom4);
        symptoms1.add(symptom5);
        symptoms1.add(symptom6);
        symptoms1.add(symptom7);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeArrowChainAnalyzer analyzer = new SyndromeArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(syndrome);

        Assertions.assertEquals(numOfArrowChains, 1);
    }

    /**
     * Testmethode, die ein Syndrom mit zwei oder mehr Pfeilketten testet.
     */
    @Test
    public void testSyndromeWithTwoOrMoreArrowChains() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);
        Symptom symptom6 = new Symptom("Symptom6", Color.PINK);
        Symptom symptom7 = new Symptom("Symptom7", Color.PINK);
        Symptom symptom8 = new Symptom("Symptom8", Color.PINK);
        Symptom symptom9 = new Symptom("Symptom9", Color.PINK);
        Symptom symptom10 = new Symptom("Symptom10", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom4, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom5, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom6, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom7, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom7, symptom8, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom8, symptom9, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom9, symptom10, waypoints).addToSymptoms();

        Collection<Symptom> symptoms1 = new HashSet<>();
        symptoms1.add(symptom1);
        symptoms1.add(symptom2);
        symptoms1.add(symptom3);
        symptoms1.add(symptom4);
        symptoms1.add(symptom5);
        symptoms1.add(symptom6);
        symptoms1.add(symptom7);
        symptoms1.add(symptom8);
        symptoms1.add(symptom9);
        symptoms1.add(symptom10);

        Sphere sphere1 = new Sphere("Sphere1", Color.WHITE, symptoms1);
        Set<Sphere> spheres = new HashSet<>();
        spheres.add(sphere1);

        Syndrome syndrome = new Syndrome(spheres);

        SyndromeArrowChainAnalyzer analyzer = new SyndromeArrowChainAnalyzer();
        int numOfArrowChains = analyzer.analyze(syndrome);

        Assertions.assertEquals(numOfArrowChains, 2);
    }

}
