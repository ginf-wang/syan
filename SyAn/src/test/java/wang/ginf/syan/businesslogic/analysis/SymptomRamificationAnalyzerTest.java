package wang.ginf.syan.businesslogic.analysis;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.syndrome.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Testklasse, die die Methoden des SymptomRamificationAnalyzer testet.
 *
 * @author Jonas Lührs
 */
class SymptomRamificationAnalyzerTest {

    /**
     * Testmethode, die ein Symptom ohne Verzweigungen testet.
     */
    @Test
    public void testSymptomWithoutRamifications() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom3, waypoints).addToSymptoms();

        SymptomRamificationAnalyzer analyzer = new SymptomRamificationAnalyzer();
        int ramification = analyzer.analyze(symptom2);

        Assertions.assertEquals(ramification, 0);
    }

    /**
     * Testmethode, die ein Symptom mit einer kovergenten und einer divergenten
     * Verzweigung testet.
     */
    @Test
    public void testSymptomWithOneRamification() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();

        SymptomRamificationAnalyzer analyzer = new SymptomRamificationAnalyzer();
        int ramification = analyzer.analyze(symptom1);

        Assertions.assertEquals(ramification, 1);
    }

    /**
     * Testmethode, die ein Symptom mit einer kovergenten und einer divergenten
     * Verzweigung testet.
     */
    @Test
    public void testSymptomWithTwoRamifications() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);
        Symptom symptom4 = new Symptom("Symptom4", Color.PINK);
        Symptom symptom5 = new Symptom("Symptom5", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom4, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom5, symptom1, waypoints).addToSymptoms();

        SymptomRamificationAnalyzer analyzer = new SymptomRamificationAnalyzer();
        int ramification = analyzer.analyze(symptom1);

        Assertions.assertEquals(ramification, 2);
    }

    /**
     * Testmethode, die ein Symptom mit einer kovergenten Verzweigung testet.
     */
    @Test
    public void testSymptomWithOneConvergentRamification() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom2, symptom1, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom3, symptom1, waypoints).addToSymptoms();

        SymptomRamificationAnalyzer analyzer = new SymptomRamificationAnalyzer();
        int convergentRamification = analyzer.getConvergentRamification(symptom1);

        Assertions.assertEquals(convergentRamification, 1);
    }

    /**
     * Testmethode, die ein Symptom mit einer divergenten Verzweigung testet.
     */
    @Test
    public void testSymptomWithOneDivergentRamification() throws Exception {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Symptom symptom3 = new Symptom("Symptom3", Color.PINK);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint(2.0, 2.0);
        waypoints.add(waypoint);

        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom2, waypoints).addToSymptoms();
        new Relation(RelationStrength.MEDIUM, RelationType.UNKNOWN, symptom1, symptom3, waypoints).addToSymptoms();

        SymptomRamificationAnalyzer analyzer = new SymptomRamificationAnalyzer();
        int divergentRamification = analyzer.getDivergentRamification(symptom1);

        Assertions.assertEquals(divergentRamification, 1);
    }
}
