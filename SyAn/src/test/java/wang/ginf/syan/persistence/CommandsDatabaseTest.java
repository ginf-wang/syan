package wang.ginf.syan.persistence;

import javafx.scene.paint.Color;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.history.CommandHistory;
import wang.ginf.syan.model.history.commands.AddSymptomCommand;
import wang.ginf.syan.model.history.commands.Command;
import wang.ginf.syan.model.history.commands.CommandStatus;
import wang.ginf.syan.model.history.commands.LegacyCommand;
import wang.ginf.syan.model.syndrome.Sphere;
import wang.ginf.syan.model.syndrome.Symptom;
import wang.ginf.syan.model.syndrome.Syndrome;

import java.sql.Timestamp;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CommandsDatabaseTest {

    private static CommandsDatabase cd = CommandsDatabase.getInstance();
    private static CommandHistory ch = CommandsDatabase.createHistoryWithDatabase();
    private static SessionFactory sf = CommandsDatabase.getSessionFactory();

    private Syndrome syndrome;
    private Set<Sphere> spheres;
    private Sphere sphere1;
    private Sphere sphere2;
    private Sphere sphere3;

    @BeforeEach
    private void setUp() {
        sphere1 = new Sphere("Sphere1", Color.PINK, null);
        sphere2 = new Sphere("Sphere2", Color.PINK, null);
        sphere3 = new Sphere("Sphere3", Color.PINK, null);
        spheres = new HashSet<>();
        spheres.add(sphere1);
        spheres.add(sphere2);
        spheres.add(sphere3);
        syndrome = new Syndrome(spheres);
    }

    @AfterEach
    private void tearDown() {
        cd.resetWith(Collections.emptyList());
    }

    @Test
    void testGetInstance() {
        assertEquals(CommandsDatabase.getInstance(), cd);
    }

    @Test
    void testGetSessionFactory() {
        assertEquals(CommandsDatabase.getSessionFactory(), sf);
    }

    @Test
    void testGetCommandId() {
        LegacyCommand lc = new LegacyCommand(1,
                                             LegacyCommand.class.toString(),
                                             CommandStatus.EXECUTED,
                                             new Timestamp(System.currentTimeMillis()),
                                             "testparams");
        cd.add(lc);
        LegacyCommand addedLc = cd.getAll().get(0);
        assertEquals(addedLc.getId(), lc.getId());
    }

    @Test
    void testAdd() {
        LegacyCommand cmd = new LegacyCommand(1,
                                              LegacyCommand.class.toString(),
                                              CommandStatus.EXECUTED,
                                              new Timestamp(System.currentTimeMillis()),
                                              "test");
        cd.add(cmd);
        LegacyCommand addedLc = cd.getAll().get(0);
        assertEquals(addedLc.getStatus(), cmd.getStatus());
        assertEquals(addedLc.getConcreteClass(), cmd.getConcreteClass());
        assertEquals(addedLc.getTimestamp(), cmd.getTimestamp());
        assertEquals(addedLc.getParams(), cmd.getParams());
    }

    @Test
    void testGetAll() {
        List<Command> commandListEmpty = Collections.emptyList();
        List<Command> commandListSingle = new ArrayList<>(1);
        List<Command> commandListCouple = new ArrayList<>(6);
        List<Command> commandListMany = new ArrayList<>(1000);

        commandListSingle.add(new LegacyCommand(1,
                                                LegacyCommand.class.toString(),
                                                CommandStatus.EXECUTED,
                                                new Timestamp(System.currentTimeMillis()),
                                                "test"));

        for (int i = 1; i <= 6; i++) {
            commandListCouple.add(new LegacyCommand(i,
                                                    LegacyCommand.class.toString(),
                                                    CommandStatus.EXECUTED,
                                                    new Timestamp(System.currentTimeMillis()),
                                                    "test " + i));
        }
        for (int i = 1; i <= 1000; i++) {
            commandListMany.add(new LegacyCommand(i,
                                                  LegacyCommand.class.toString(),
                                                  CommandStatus.EXECUTED,
                                                  new Timestamp(System.currentTimeMillis()),
                                                  "test " + i));
        }

        cd.resetWith(commandListEmpty);
        for (int i = 0; i < cd.getAll().size(); i++) {
            assertTrue(testAdd(cd.getAll().get(i), i));
        }

        cd.resetWith(commandListSingle);
        for (int i = 0; i < cd.getAll().size(); i++) {
            assertTrue(testAdd(cd.getAll().get(i), i));
        }

        cd.resetWith(commandListCouple);
        for (int i = 0; i < cd.getAll().size(); i++) {
            assertTrue(testAdd(cd.getAll().get(i), i));
        }

        cd.resetWith(commandListMany);
        for (int i = 0; i < cd.getAll().size(); i++) {
            assertTrue(testAdd(cd.getAll().get(i), i));
        }
    }

    private boolean testAdd(Command cmd, int index) {
        boolean b = true;
        LegacyCommand addedLc = cd.getAll().get(index);
        if (!addedLc.getStatus().equals(cmd.getStatus()) ||
            !addedLc.getConcreteClass().equals(cmd.getConcreteClass()) ||
            !addedLc.getTimestamp().equals(cmd.getTimestamp()) ||
            !addedLc.getParams().equals(cmd.getParams())) {
            b = false;
        }
        return b;
    }

    @Test
    void testSetCommandStatus() {
        AddSymptomCommand asc = new AddSymptomCommand(new Symptom("test", Color.WHITE), sphere2, syndrome);
        cd.add(asc);
        asc.setStatus(CommandStatus.UNDONE);
        cd.setCommandStatus(asc);
        assertEquals(cd.getAll().get(0).getStatus(), CommandStatus.UNDONE);
    }
}
