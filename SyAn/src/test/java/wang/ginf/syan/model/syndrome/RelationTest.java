package wang.ginf.syan.model.syndrome;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class RelationTest {
    private List<Symptom> symptoms;
    private Relation testRelation1;
    private Relation testRelation2;

    @BeforeEach
    public void beforeEach() {
        symptoms = IntStream.rangeClosed(1, 20)
                            .mapToObj(i -> new Symptom("symptom" + i, Color.WHITE))
                            .collect(Collectors.toList());
        // *   _ _ _ _ _
        // *  | |1|2|3| |
        // *  |0| | | |4|
        // *  |_|7|6|5|_|
        testRelation1 = new Relation(RelationStrength.MEDIUM,
                                     RelationType.UNKNOWN,
                                     symptoms.get(0),
                                     symptoms.get(1),
                                     Collections.emptyList());
        testRelation1.addToSymptoms();

        testRelation1.setStartSymptomAttachedSide(4);
        testRelation1.setEndSymptomAttachedSide(0);

        testRelation2 = new Relation(RelationStrength.MEDIUM,
                                     RelationType.UNKNOWN,
                                     symptoms.get(1),
                                     symptoms.get(2),
                                     Collections.emptyList());
        testRelation2.addToSymptoms();

        testRelation2.setStartSymptomAttachedSide(4);
        testRelation2.setEndSymptomAttachedSide(0);
    }

    @Test
    public void testStartZoneValidIfOccupied() {
        assertEquals(symptoms.get(0).getFreeStartIfOccupied(4), 4);
    }

    @Test
    public void testStartZoneValidIfNotOccupied() {
        assertEquals(symptoms.get(0).getFreeStartIfOccupied(7), 7);
    }

    @Test
    public void testStartZoneNotValidIfOccupiedByEnd() {
        assertNotEquals(symptoms.get(1).getFreeStartIfOccupied(0), 0);
    }

    @Test
    public void testEndZoneNotValidIfOccupiedByStart() {
        assertNotEquals(symptoms.get(0).getFreeEndIfOccupied(4, RelationType.UNKNOWN), 4);
    }

    @Test
    public void testEndZoneNotValidIfOccupiedByEndWithDifferentType() {
        assertNotEquals(symptoms.get(1).getFreeEndIfOccupied(0, RelationType.INCREASING), 0);
    }

    @Test
    public void testEndZoneValidIfOccupiedByEndWithSameType() {
        assertEquals(symptoms.get(1).getFreeEndIfOccupied(0, RelationType.UNKNOWN), 0);
    }
}
