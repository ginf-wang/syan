package wang.ginf.syan.model.syndrome;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wang.ginf.syan.model.history.CommandHistory;
import wang.ginf.syan.model.history.commands.*;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class CommonCommandSequencesTest {
    private Syndrome syndrome;
    private Set<Sphere> spheres;
    private Sphere sphere1;
    private Sphere sphere2;
    private Sphere sphere3;

    @BeforeEach
    public void beforeEach() {
        sphere1 = new Sphere("Sphere1", Color.PINK, null);
        sphere2 = new Sphere("Sphere2", Color.PINK, null);
        sphere3 = new Sphere("Sphere3", Color.PINK, null);
        spheres = new HashSet<>();
        spheres.add(sphere1);
        spheres.add(sphere2);
        spheres.add(sphere3);
        syndrome = new Syndrome(spheres);
    }

    @Test
    public void testAddOneSymptom() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        assertTrue(sphere1.containsSymptom(symptom1));
    }

    @Test
    public void testDeleteSymptom() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        assertTrue(syndrome.containsSymptom(symptom1));
        CommandHistory.getInstance().execute(new DeleteSymptomCommand(symptom1, sphere1, syndrome));
        assertFalse(syndrome.containsSymptom(symptom1));
    }

    @Test
    public void testChangeSymptomname() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        CommandHistory.getInstance().execute(new RenameSymptomCommand(syndrome, symptom1, "Changed"));
        assertEquals(symptom1.getName(), "Changed");
    }

    @Test
    public void testChangeSymptomnameToSame() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        CommandHistory.getInstance().execute(new RenameSymptomCommand(syndrome, symptom1, "Symptom1"));
        assertEquals(symptom1.getName(), "Symptom1");
    }

    @Test
    public void testChangeSymptomColor() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        assertEquals(symptom1.getColor(), Color.PINK);
        CommandHistory.getInstance().execute(new RecolorSymptomCommand(symptom1, Color.YELLOW));
        assertEquals(symptom1.getColor(), Color.YELLOW);
    }

    @Test
    public void testChangeSymptomColorToSame() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        assertEquals(symptom1.getColor(), Color.PINK);
        CommandHistory.getInstance().execute(new RecolorSymptomCommand(symptom1, Color.PINK));
        assertEquals(symptom1.getColor(), Color.PINK);
    }

    @Test
    public void testAddOneRelation() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Relation relation = new Relation(RelationStrength.UNKNOWN,
                                         RelationType.UNKNOWN,
                                         symptom1,
                                         symptom2,
                                         null);

        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom2, sphere1, syndrome));
        CommandHistory.getInstance()
                      .execute(new AddRelationCommand(relation, syndrome));
        assertTrue(symptom1.getRelations().contains(relation));
        assertTrue(symptom2.getRelations().contains(relation));
        assertEquals(relation.getStartSymptom(), symptom1);
        assertEquals(relation.getEndSymptom(), symptom2);
        assertTrue(syndrome.containsRelation(relation));
    }

    @Test
    public void testDeleteRelation() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Relation relation = new Relation(RelationStrength.UNKNOWN,
                                         RelationType.UNKNOWN,
                                         symptom1,
                                         symptom2,
                                         null);

        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom2, sphere1, syndrome));
        CommandHistory.getInstance()
                      .execute(new AddRelationCommand(relation, syndrome));
        CommandHistory.getInstance().execute(new DeleteRelationCommand(relation, syndrome, null));
        assertFalse(syndrome.containsRelation(relation));
    }

    @Test
    public void testChangeRelationColor() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Relation relation = new Relation(RelationStrength.UNKNOWN,
                                         RelationType.UNKNOWN,
                                         symptom1,
                                         symptom2,
                                         null);

        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom2, sphere1, syndrome));
        CommandHistory.getInstance()
                      .execute(new AddRelationCommand(relation, syndrome));
        assertEquals(relation.getColor(), Color.BLACK);
        CommandHistory.getInstance().execute(new RecolorRelationCommand(relation, Color.YELLOW));
        assertEquals(relation.getColor(), Color.YELLOW);
    }

    @Test
    public void testChangeRelationColorToSame() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Relation relation = new Relation(RelationStrength.UNKNOWN,
                                         RelationType.UNKNOWN,
                                         symptom1,
                                         symptom2,
                                         null);

        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom2, sphere1, syndrome));
        CommandHistory.getInstance()
                      .execute(new AddRelationCommand(relation, syndrome));
        assertEquals(relation.getColor(), Color.BLACK);
        CommandHistory.getInstance().execute(new RecolorRelationCommand(relation, Color.BLACK));
        assertEquals(relation.getColor(), Color.BLACK);
    }

    @Test
    public void testChangeRelationStrength() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Relation relation = new Relation(RelationStrength.UNKNOWN,
                                         RelationType.UNKNOWN,
                                         symptom1,
                                         symptom2,
                                         null);

        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom2, sphere1, syndrome));
        CommandHistory.getInstance()
                      .execute(new AddRelationCommand(relation, syndrome));
        assertEquals(relation.getRelationStrength(), RelationStrength.UNKNOWN);
        CommandHistory.getInstance().execute(new ChangeRelationStrengthCommand(relation, RelationStrength.STRONG));
        assertEquals(relation.getRelationStrength(), RelationStrength.STRONG);
    }

    @Test
    public void testChangeRelationStrengthToSame() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Relation relation = new Relation(RelationStrength.UNKNOWN,
                                         RelationType.UNKNOWN,
                                         symptom1,
                                         symptom2,
                                         null);

        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom2, sphere1, syndrome));
        CommandHistory.getInstance()
                      .execute(new AddRelationCommand(relation, syndrome));
        assertEquals(relation.getRelationStrength(), RelationStrength.UNKNOWN);
        CommandHistory.getInstance().execute(new ChangeRelationStrengthCommand(relation, RelationStrength.UNKNOWN));
        assertEquals(relation.getRelationStrength(), RelationStrength.UNKNOWN);
    }

    @Test
    public void testChangeSphereColor() {
        CommandHistory.getInstance().execute(new RecolorSphereCommand(sphere1, Color.YELLOW));
        assertEquals(sphere1.getColor(), Color.YELLOW);
    }

    @Test
    public void testChangeSphereColorToSame() {
        CommandHistory.getInstance().execute(new RecolorSphereCommand(sphere1, Color.PINK));
        assertEquals(sphere1.getColor(), Color.PINK);
    }

    @Test
    public void testChangeSphereSize() {
        CommandHistory.getInstance()
                      .execute(new ResizeSphereCommand(sphere1, sphere1.getWidth(), sphere1.getHeight(), 200, 200));
        assertEquals(sphere1.getWidth(), 200);
        assertEquals(sphere1.getHeight(), 200);
    }

    @Test
    public void testMoveSphere() {
        CommandHistory.getInstance()
                      .execute(new MoveSphereCommand(sphere1, sphere1.getLayoutX(), sphere1.getLayoutY(), 111, 21));
        assertEquals(sphere1.getLayoutX(), 111);
        assertEquals(sphere1.getLayoutY(), 21);
    }

    @Test
    public void testUndo() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Relation relation = new Relation(RelationStrength.UNKNOWN,
                                         RelationType.UNKNOWN,
                                         symptom1,
                                         symptom2,
                                         null);

        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom2, sphere1, syndrome));
        CommandHistory.getInstance()
                      .execute(new AddRelationCommand(relation, syndrome));
        CommandHistory.getInstance().undo();
        CommandHistory.getInstance().undo();
        CommandHistory.getInstance().undo();
        assertFalse(syndrome.containsRelation(relation));
        assertFalse(syndrome.containsSymptom(symptom1));
        assertFalse(syndrome.containsSymptom(symptom2));
    }

    @Test
    public void testRedo() {
        Symptom symptom1 = new Symptom("Symptom1", Color.PINK);
        Symptom symptom2 = new Symptom("Symptom2", Color.PINK);
        Relation relation = new Relation(RelationStrength.UNKNOWN,
                                         RelationType.UNKNOWN,
                                         symptom1,
                                         symptom2,
                                         null);

        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom1, sphere1, syndrome));
        CommandHistory.getInstance().execute(new AddSymptomCommand(symptom2, sphere1, syndrome));
        CommandHistory.getInstance()
                      .execute(new AddRelationCommand(relation, syndrome));
        CommandHistory.getInstance().undo();
        CommandHistory.getInstance().undo();
        CommandHistory.getInstance().undo();
        CommandHistory.getInstance().redo();
        CommandHistory.getInstance().redo();
        CommandHistory.getInstance().redo();
        assertTrue(symptom1.getRelations().contains(relation));
        assertTrue(symptom2.getRelations().contains(relation));
        assertEquals(relation.getStartSymptom(), symptom1);
        assertEquals(relation.getEndSymptom(), symptom2);
        assertTrue(syndrome.containsRelation(relation));
        assertTrue(syndrome.containsSymptom(symptom1));
        assertTrue(syndrome.containsSymptom(symptom2));
    }
}
